package com.allthings.consumer.advancedSearch;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.offers.OffersAdapter;
import com.allthings.consumer.offers.OffersVO;
import com.allthings.consumer.offers.UpdateCategoryTitle;
import com.allthings.consumer.offers.UpdateSearchedData;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.CustomDialog;
import com.allthings.consumer.utils.DateUtil;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import de.greenrobot.event.EventBus;

public class AdvancedSearchOffersListFragment extends BaseFragment {

    private static final String TAG = "AdvancedListFragment";
    private View rootView;
    private ArrayList<OffersVO> mOfferList, searchList;
    private RecyclerView recyclerView;
    private Context mContext;
    private static UpdateCategoryTitle updateTitle;
    private DatabaseReference mOfferTable, mConsumerUser;
    public ImageView imageView_placeholder;
    HashMap<String, String> categoryListFromFirebase;
    GenericTypeIndicator<HashMap<String, String>> t;
    protected ProgressDialog progressDialog;
    private String city, country, gender, searchCategory, searchOfferType, searchLocation, searchGender;

    public static AdvancedSearchOffersListFragment newInstance(String title1, UpdateCategoryTitle updateTitle1,
                                                               String searchGender, String searchCategory,
                                                               String searchLocation, String searchOfferType,
                                                               String city,String country) {

        Bundle args = new Bundle();
        args.putString("Title", title1);
        args.putString(Constants.SEARCH_GENDER, searchGender);
        args.putString(Constants.SEARCH_CATEGORY, searchCategory);
        args.putString(Constants.SEARCH_LOCATION, searchLocation);
        args.putString(Constants.SEARCH_OFFER_TYPE, searchOfferType);
        args.putString(Constants.CITY, city);
        args.putString(Constants.COUNTRY, country);
        AdvancedSearchOffersListFragment fragment = new AdvancedSearchOffersListFragment();
        fragment.setArguments(args);
        updateTitle = updateTitle1;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_advanced_search_offers_list, container, false);
            disableBack(rootView);
            init();
            /*new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setRecyclerViewData();
                }
            },500);*/
            return rootView;
        } else {
            disableBack(rootView);
            return rootView;
        }

    }

    //Initialize Components
    private void init() {
        recyclerView = rootView.findViewById(R.id.rvAdvancedSearch);
        imageView_placeholder = rootView.findViewById(R.id.iv_placeholder);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 3);
        recyclerView.setLayoutManager(layoutManager);

        mOfferList = new ArrayList<>();

        mOfferTable = mDatabase.getReference(Constants.OFFER);
        mConsumerUser = mDatabase.getReference(Constants.CONSUMER_USER_MASTER);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "1");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "OfferFragment");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "text");
        progressDialog = CustomDialog.ctor(mContext);
        //Logs an app event.
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST, bundle);

        getArgument();
        //getAllOffer();
        getOfferList();

        //getActivity().findViewById(R.id.ivToolbarsearchbutton).setOnClickListener(this);

    }

    private void getArgument() {
        Bundle bundle = getArguments();

        searchCategory = bundle.getString(Constants.SEARCH_CATEGORY);
        searchLocation = bundle.getString(Constants.SEARCH_LOCATION);
        searchOfferType = bundle.getString(Constants.SEARCH_OFFER_TYPE);
        searchGender = bundle.getString(Constants.SEARCH_GENDER);
        city = bundle.getString(Constants.CITY);
        country = bundle.getString(Constants.COUNTRY);
    }

    private void getAllOffer() {
        mOfferTable.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.e(TAG, "Count" + dataSnapshot.getChildrenCount());
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Log.e(TAG, child.getKey());
                    OffersVO offersVO = child.getValue(OffersVO.class);
                    offersVO.setOfferID(child.getKey());
                    mOfferList.add(offersVO);
                }
                //setAdapterData(mOfferList);
                if (mOfferList.size() == 0) {
                    imageView_placeholder.setVisibility(View.VISIBLE);
                } else {
                    imageView_placeholder.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    /**
     * Get Offer Category Data From Business and set it according to Category
     */
    private void getOfferList() {
        mOfferList.clear();
        if (NetworkUtil.isOnline(mContext)) {
            Log.e("SEARCH_GENDER", getArguments().getString(Constants.SEARCH_GENDER));
            Log.e("SEARCH_CATEGORY", getArguments().getString(Constants.SEARCH_CATEGORY));
            Log.e("SEARCH_LOCATION", getArguments().getString(Constants.SEARCH_LOCATION));
            Log.e("SEARCH_OFFER_TYPE", getArguments().getString(Constants.SEARCH_OFFER_TYPE));

            progressDialog.show();

            if (!searchCategory.equals(" ") && !searchOfferType.equals(" ") && !searchGender.equals(" ") && !searchLocation.equals(" ")) {
                if (!searchGender.equalsIgnoreCase("All")) {
                    Query query = mOfferTable.orderByChild(Constants.PLAN_TYPE).equalTo(searchOfferType);
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            //Log.e(TAG, "Count" + dataSnapshot.getChildrenCount());
                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                try {
                                    if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                        Log.e(TAG, "COMPANY_ADDRESS" + child.child(Constants.COMPANY_ADDRESS).getValue(String.class));
                                        String cityName = child.child(Constants.CITY).getValue(String.class);
                                        String countryName =child.child(Constants.COUNTRY).getValue(String.class);
                                        if ((child.child(Constants.COMPANY_ADDRESS).getValue(String.class).toLowerCase().contains(searchLocation.toLowerCase())) ||
                                                (cityName.toLowerCase().equalsIgnoreCase(city.toLowerCase()) &&
                                                        countryName.toLowerCase().equalsIgnoreCase(country.toLowerCase()))) {

                                            if (child.child(Constants.SEARCHCATEGORY).getValue(String.class).equals(searchCategory) &&
                                                    ((child.child(Constants.GENDER).getValue().toString().equals(searchGender)) ||
                                                            (child.child(Constants.GENDER).getValue().toString().equals(Constants.GENDER_TYPE_ALL)))) {
                                                //Log.e(TAG, child.getKey());
                                                OffersVO offersVO = child.getValue(OffersVO.class);
                                                offersVO.setOfferID(child.getKey());
                                                mOfferList.add(offersVO);
                                            }
                                        }
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            setAdapterData(mOfferList);

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progressDialog.dismiss();
                            showToast(databaseError.getMessage());
                            //Log.e(TAG,databaseError.getMessage());
                        }
                    });
                } else {
                    Query query = mOfferTable.orderByChild(Constants.PLAN_TYPE).equalTo(searchOfferType);
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            //Log.e(TAG, "Count" + dataSnapshot.getChildrenCount());
                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                try {
                                    if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                        Log.e(TAG, "COMPANY_ADDRESS " + child.child(Constants.COMPANY_ADDRESS).getValue(String.class));
                                        Log.e(TAG, "SEARCHCATEGORY " + child.child(Constants.SEARCHCATEGORY).getValue(String.class));
                                        String cityName = child.child(Constants.CITY).getValue(String.class);
                                        String countryName =child.child(Constants.COUNTRY).getValue(String.class);
                                        if ((child.child(Constants.COMPANY_ADDRESS).getValue(String.class).toLowerCase()
                                                .contains(searchLocation.toLowerCase())) ||
                                                (cityName.toLowerCase().equalsIgnoreCase(city.toLowerCase()) &&
                                                        countryName.toLowerCase().equalsIgnoreCase(country.toLowerCase()))) {
                                            if (child.child(Constants.SEARCHCATEGORY).getValue(String.class).equals(searchCategory)) {
                                                // Log.e(TAG, child.getKey());
                                                OffersVO offersVO = child.getValue(OffersVO.class);
                                                offersVO.setOfferID(child.getKey());

                                                mOfferList.add(offersVO);
                                            }
                                        }
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            setAdapterData(mOfferList);

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progressDialog.dismiss();
                            showToast(databaseError.getMessage());
                            // Log.e(TAG,databaseError.getMessage());
                        }
                    });
                }
            } else if (searchGender.equals(" ") && !searchOfferType.equals(" ") && searchLocation.equals(" ") && searchCategory.equals(" ")) {
                //getOfferList(Constants.SEARCH_OFFER_TYPE,searchOfferType);
                Query getOfferListQuery = mOfferTable.orderByChild(Constants.PLAN_TYPE).equalTo(searchOfferType);

                getOfferListQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        progressDialog.dismiss();
                        //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            try {
                                if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                    //Log.e(TAG, child.getKey());

                                    OffersVO offersVO = child.getValue(OffersVO.class);
                                    offersVO.setOfferID(child.getKey());
                                    mOfferList.add(offersVO);


                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        setAdapterData(mOfferList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progressDialog.dismiss();
                        showToast(databaseError.getMessage());
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            } else if (!searchGender.equals(" ") && searchOfferType.equals(" ") && searchLocation.equals(" ") && searchCategory.equals(" ")) {
                //getOfferList(Constants.SEARCH_GENDER,searchGender);
                if (!searchGender.equals("All")) {
                    //Query getOfferListQuery = mOfferTable.orderByChild(Constants.GENDER).equalTo(searchGender);
                    mOfferTable.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            //Log.e(TAG, "Count" + dataSnapshot.getChildrenCount());
                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                try {
                                    if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                        //Log.e(TAG, child.getKey());
                                        if(((child.child(Constants.GENDER).getValue().toString().equals(searchGender)) ||
                                                (child.child(Constants.GENDER).getValue().toString().equals(Constants.GENDER_TYPE_ALL)))) {
                                            OffersVO offersVO = child.getValue(OffersVO.class);
                                            offersVO.setOfferID(child.getKey());
                                            mOfferList.add(offersVO);
                                        }
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            setAdapterData(mOfferList);

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progressDialog.dismiss();
                            showToast(databaseError.getMessage());
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                } else {
                    mOfferTable.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            //Log.e(TAG, "Count" + dataSnapshot.getChildrenCount());
                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                try {
                                    if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                        //Log.e(TAG, child.getKey());
                                        OffersVO offersVO = child.getValue(OffersVO.class);
                                        offersVO.setOfferID(child.getKey());
                                        mOfferList.add(offersVO);
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            setAdapterData(mOfferList);

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progressDialog.dismiss();
                            showToast(databaseError.getMessage());
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                }
            } else if (searchGender.equals(" ") && searchOfferType.equals(" ") && !searchLocation.equals(" ") && searchCategory.equals(" ")) {
                mOfferTable.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        progressDialog.dismiss();
                        //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            try {
                                if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                    //Log.e(TAG, child.getKey());
                                    if (child.hasChild(Constants.COMPANY_ADDRESS)) {
                                        String location = child.child(Constants.COMPANY_ADDRESS).getValue(String.class).toLowerCase();
                                        String cityName = child.child(Constants.CITY).getValue(String.class);
                                        String countryName =child.child(Constants.COUNTRY).getValue(String.class);
                                        if ((location.contains(searchLocation.toLowerCase())) ||
                                                (cityName.toLowerCase().equalsIgnoreCase(city.toLowerCase()) &&
                                                        countryName.toLowerCase().equalsIgnoreCase(country.toLowerCase()))) {
                                            OffersVO offersVO = child.getValue(OffersVO.class);
                                            offersVO.setOfferID(child.getKey());
                                            mOfferList.add(offersVO);
                                        }
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        setAdapterData(mOfferList);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progressDialog.dismiss();
                    }
                });

               /* mOfferTable.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        progressDialog.dismiss();
                        Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                        for(DataSnapshot child : dataSnapshot.getChildren()){
                            Log.e(TAG,child.getKey());
                            if(child.child(Constants.COMPANY_ADDRESS).getValue(String.class).toLowerCase().contains(searchLocation.toLowerCase()))
                                *//*child.child(Constants.COUNTRY).getValue().toString().toLowerCase().contains(searchLocation.toLowerCase()) ||
                                    child.child(Constants.COUNTY).getValue().toString().toLowerCase().contains(searchLocation.toLowerCase()) ||
                                    child.child(Constants.CITY).getValue().toString().toLowerCase().contains(searchLocation.toLowerCase())*//* {
                                OffersVO offersVO = child.getValue(OffersVO.class);
                                offersVO.setOfferID(child.getKey());
                                mOfferList.add(offersVO);
                            }
                        }
                        setAdapterData(mOfferList);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        progressDialog.dismiss();
                        showToast(databaseError.getMessage());
                        Log.e(TAG,databaseError.getMessage());
                    }
                });*/
            } else if (searchGender.equals(" ") && searchOfferType.equals(" ") && searchLocation.equals(" ") && !searchCategory.equals(" ")) {
                Query getOfferListQuery = mOfferTable.orderByChild(Constants.SEARCHCATEGORY).equalTo(searchCategory);
                getOfferListQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        dataSnapShot1 = dataSnapshot;
                        progressDialog.dismiss();
                        //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            try {
                                if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                    //Log.e(TAG, child.getKey());
                                    OffersVO offersVO = child.getValue(OffersVO.class);
                                    offersVO.setOfferID(child.getKey());
                                    mOfferList.add(offersVO);
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        setAdapterData(mOfferList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progressDialog.dismiss();
                        showToast(databaseError.getMessage());
                        Log.e(TAG, databaseError.getMessage());
                    }
                });

            } else if (!searchCategory.equals(" ") && !searchGender.equals(" ") && searchLocation.equals(" ") && searchOfferType.equals(" ")) {

                if (searchGender.equals("All")) {
                    Query query = mOfferTable.orderByChild(Constants.SEARCHCATEGORY).equalTo(searchCategory);
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                try {
                                    if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                        //Log.e(TAG, child.getKey());
                                        OffersVO offersVO = child.getValue(OffersVO.class);
                                        offersVO.setOfferID(child.getKey());

                                        //Log.e(TAG,"OfferCategory"+child.child(Constants.SEARCHCATEGORY).getValue(String.class));
                                        // Log.e(TAG,"OfferPlanType"+child.child(Constants.PLAN_TYPE).getValue(String.class));
                                        //Log.e(TAG,"OfferGender"+child.child(Constants.GENDER).getValue(String.class));

                                        mOfferList.add(offersVO);
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            setAdapterData(mOfferList);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progressDialog.dismiss();
                        }
                    });
                } else {
                    Query query = mOfferTable.orderByChild(Constants.SEARCHCATEGORY).equalTo(searchCategory);
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            Log.e(TAG, "Count" + dataSnapshot.getChildrenCount());
                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                try {
                                    if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                        if (((child.child(Constants.GENDER).getValue().toString().equals(searchGender)) ||
                                                (child.child(Constants.GENDER).getValue().toString().equals(Constants.GENDER_TYPE_ALL)))
                                            /*child.child(Constants.PLAN_TYPE).getValue(String.class).equals(searchOfferType) &&*/) {
                                            Log.e(TAG, child.getKey());
                                            OffersVO offersVO = child.getValue(OffersVO.class);
                                            offersVO.setOfferID(child.getKey());

                                            Log.e(TAG, "OfferCategory" + child.child(Constants.SEARCHCATEGORY).getValue(String.class));
                                            // Log.e(TAG,"OfferPlanType"+child.child(Constants.PLAN_TYPE).getValue(String.class));
                                            Log.e(TAG, "OfferGender" + child.child(Constants.GENDER).getValue(String.class));

                                            mOfferList.add(offersVO);
                                        }
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            setAdapterData(mOfferList);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progressDialog.dismiss();
                            showToast(databaseError.getMessage());
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                }
            } else if (!searchCategory.equals(" ") && !searchOfferType.equals(" ") && searchGender.equals(" ") && searchLocation.equals(" ")) {
                Query query = mOfferTable.orderByChild(Constants.PLAN_TYPE).equalTo(searchOfferType);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        progressDialog.dismiss();
                        //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            try {
                                if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                    if (child.child(Constants.SEARCHCATEGORY).getValue(String.class).equals(searchCategory) /*&&
                                        child.child(Constants.GENDER).getValue(String.class).equals(searchGender)*/) {
                                        //Log.e(TAG, child.getKey());
                                        OffersVO offersVO = child.getValue(OffersVO.class);
                                        offersVO.setOfferID(child.getKey());

                                        //Log.e(TAG, "OfferCategory" + child.child(Constants.SEARCHCATEGORY).getValue(String.class));
                                        //Log.e(TAG, "OfferPlanType" + child.child(Constants.PLAN_TYPE).getValue(String.class));
                                        //Log.e(TAG,"OfferGender"+child.child(Constants.GENDER).getValue(String.class));

                                        mOfferList.add(offersVO);
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        setAdapterData(mOfferList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progressDialog.dismiss();
                        showToast(databaseError.getMessage());
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            } else if (!searchCategory.equals(" ") && !searchLocation.equals(" ") && searchGender.equals(" ") && searchOfferType.equals(" ")) {
                Query query = mOfferTable.orderByChild(Constants.SEARCHCATEGORY).equalTo(searchCategory);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        progressDialog.dismiss();
                        //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            try {
                                if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                    String location = child.child(Constants.COMPANY_ADDRESS).getValue(String.class).toLowerCase();
                                    String cityName = child.child(Constants.CITY).getValue(String.class);
                                    String countryName =child.child(Constants.COUNTRY).getValue(String.class);
                                    if ((location.contains(searchLocation.toLowerCase())) ||
                                            (cityName.toLowerCase().equalsIgnoreCase(city.toLowerCase()) &&
                                                    countryName.toLowerCase().equalsIgnoreCase(country.toLowerCase()))) {
                                        //Log.e(TAG, child.getKey());
                                        OffersVO offersVO = child.getValue(OffersVO.class);
                                        offersVO.setOfferID(child.getKey());

                                        //Log.e(TAG, "COMPANY_ADDRESS" + child.child(Constants.COMPANY_ADDRESS).getValue(String.class));
                                        //Log.e(TAG, "OfferCategory" + child.child(Constants.SEARCHCATEGORY).getValue(String.class));
                                        //Log.e(TAG,"OfferPlanType"+child.child(Constants.PLAN_TYPE).getValue(String.class));
                                        //Log.e(TAG,"OfferGender"+child.child(Constants.GENDER).getValue(String.class));

                                        mOfferList.add(offersVO);
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        setAdapterData(mOfferList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progressDialog.dismiss();
                        showToast(databaseError.getMessage());
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            } else if (!searchGender.equals(" ") && !searchOfferType.equals(" ") && searchLocation.equals(" ") && searchCategory.equals(" ")) {
                if (searchGender.equals("All")) {
                    Query query = mOfferTable.orderByChild(Constants.PLAN_TYPE).equalTo(searchOfferType);
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            Log.e(TAG, "Count" + dataSnapshot.getChildrenCount());
                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                try {
                                    if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                        Log.e(TAG, child.getKey());
                                        OffersVO offersVO = child.getValue(OffersVO.class);
                                        offersVO.setOfferID(child.getKey());
                                        //Log.e(TAG,"OfferCategory"+child.child(Constants.SEARCHCATEGORY).getValue(String.class));
                                        Log.e(TAG, "OfferPlanType" + child.child(Constants.PLAN_TYPE).getValue(String.class));
                                        Log.e(TAG, "OfferGender" + child.child(Constants.GENDER).getValue(String.class));
                                        mOfferList.add(offersVO);
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            setAdapterData(mOfferList);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progressDialog.dismiss();
                            showToast(databaseError.getMessage());
                            //Log.e(TAG,databaseError.getMessage());
                        }
                    });
                } else {
                    Query query = mOfferTable.orderByChild(Constants.PLAN_TYPE).equalTo(searchOfferType);
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            //Log.e(TAG, "Count" + dataSnapshot.getChildrenCount());
                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                try {
                                    if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                        if (((child.child(Constants.GENDER).getValue().toString().equals(searchGender)) ||
                                                (child.child(Constants.GENDER).getValue().toString().equals(Constants.GENDER_TYPE_ALL)))) {
                                            // Log.e(TAG, child.getKey());
                                            OffersVO offersVO = child.getValue(OffersVO.class);
                                            offersVO.setOfferID(child.getKey());

                                            //Log.e(TAG,"OfferCategory"+child.child(Constants.SEARCHCATEGORY).getValue(String.class));
                                            //Log.e(TAG, "OfferPlanType" + child.child(Constants.PLAN_TYPE).getValue(String.class));
                                            //Log.e(TAG, "OfferGender" + child.child(Constants.GENDER).getValue(String.class));

                                            mOfferList.add(offersVO);
                                        }
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            setAdapterData(mOfferList);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progressDialog.dismiss();
                            showToast(databaseError.getMessage());
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                }
            } else if (!searchGender.equals(" ") && !searchLocation.equals(" ") && searchCategory.equals(" ") && searchOfferType.equals(" ")) {
                if (searchGender.equalsIgnoreCase("All")) {
                    mOfferTable.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                try {
                                    if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                        if (child.child(Constants.COMPANY_ADDRESS).getValue(String.class) != null) {
                                            String location = child.child(Constants.COMPANY_ADDRESS).getValue(String.class).toLowerCase();
                                            String cityName = child.child(Constants.CITY).getValue(String.class);
                                            String countryName =child.child(Constants.COUNTRY).getValue(String.class);
                                            if ((location.contains(searchLocation.toLowerCase())) ||
                                                    (cityName.toLowerCase().equalsIgnoreCase(city.toLowerCase()) &&
                                                            countryName.toLowerCase().equalsIgnoreCase(country.toLowerCase()))) {
                                                //Log.e(TAG, child.getKey());
                                                OffersVO offersVO = child.getValue(OffersVO.class);
                                                offersVO.setOfferID(child.getKey());

                                                //Log.e(TAG, "COMPANY_ADDRESS" + child.child(Constants.COMPANY_ADDRESS).getValue(String.class));
                                                //Log.e(TAG, "OfferGender" + child.child(Constants.GENDER).getValue(String.class));

                                                mOfferList.add(offersVO);
                                            }
                                        }

                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            setAdapterData(mOfferList);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progressDialog.dismiss();
                            showToast(databaseError.getMessage());
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                } else {
                    //Query query = mOfferTable.orderByChild(Constants.GENDER).equalTo(searchGender);
                    mOfferTable.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                try {
                                    if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                        String location = child.child(Constants.COMPANY_ADDRESS).getValue(String.class).toLowerCase();
                                        String cityName = child.child(Constants.CITY).getValue(String.class);
                                        String countryName =child.child(Constants.COUNTRY).getValue(String.class);
                                        if (((child.child(Constants.GENDER).getValue().toString().equals(searchGender)) ||
                                                (child.child(Constants.GENDER).getValue().toString().equals(Constants.GENDER_TYPE_ALL))) &&
                                                ((location.contains(searchLocation.toLowerCase())) ||
                                                (cityName.toLowerCase().equalsIgnoreCase(city.toLowerCase()) &&
                                                        countryName.toLowerCase().equalsIgnoreCase(country.toLowerCase())))){
                                            //Log.e(TAG, child.getKey());
                                            OffersVO offersVO = child.getValue(OffersVO.class);
                                            offersVO.setOfferID(child.getKey());

                                            //Log.e(TAG, "COMPANY_ADDRESS" + child.child(Constants.COMPANY_ADDRESS).getValue(String.class));
                                            //Log.e(TAG, "OfferGender" + child.child(Constants.GENDER).getValue(String.class));

                                            mOfferList.add(offersVO);
                                        }
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            setAdapterData(mOfferList);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progressDialog.dismiss();
                            showToast(databaseError.getMessage());
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                }

            } else if (!searchOfferType.equals(" ") && !searchLocation.equals(" ") && searchGender.equals(" ") && searchCategory.equals(" ")) {
                Query query = mOfferTable.orderByChild(Constants.PLAN_TYPE).equalTo(searchOfferType);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        progressDialog.dismiss();
                        //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            try {
                                if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                    String location = child.child(Constants.COMPANY_ADDRESS).getValue(String.class).toLowerCase();
                                    String cityName = child.child(Constants.CITY).getValue(String.class);
                                    String countryName =child.child(Constants.COUNTRY).getValue(String.class);
                                    if ((location.contains(searchLocation.toLowerCase())) ||
                                            (cityName.toLowerCase().equalsIgnoreCase(city.toLowerCase()) &&
                                                    countryName.toLowerCase().equalsIgnoreCase(country.toLowerCase()))) {
                                        //Log.e(TAG, child.getKey());
                                        OffersVO offersVO = child.getValue(OffersVO.class);
                                        offersVO.setOfferID(child.getKey());

                                        //Log.e(TAG,"OfferCategory"+child.child(Constants.SEARCHCATEGORY).getValue(String.class));
                                        //Log.e(TAG, "OfferPlanType" + child.child(Constants.PLAN_TYPE).getValue(String.class));
                                        //Log.e(TAG, "COMPANY_ADDRESS" + child.child(Constants.COMPANY_ADDRESS).getValue(String.class));

                                        //Log.e(TAG,"OfferGender"+child.child(Constants.GENDER).getValue(String.class));

                                        mOfferList.add(offersVO);
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        setAdapterData(mOfferList);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progressDialog.dismiss();
                        showToast(databaseError.getMessage());
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            } else if (!searchOfferType.equals(" ") && !searchGender.equals(" ") && !searchCategory.equals(" ") && searchLocation.equals(" ")) {
                if (searchGender.equals("All")) {
                    Query query = mOfferTable.orderByChild(Constants.PLAN_TYPE).equalTo(searchOfferType);
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                try {
                                    if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                        if (child.child(Constants.SEARCHCATEGORY).getValue().toString().equals(searchCategory)) {
                                            //Log.e(TAG, child.getKey());
                                            OffersVO offersVO = child.getValue(OffersVO.class);
                                            offersVO.setOfferID(child.getKey());

                                            //Log.e(TAG,"GENDER"+child.child(Constants.GENDER).getValue(String.class));
                                            //Log.e(TAG, "OfferPlanType" + child.child(Constants.PLAN_TYPE).getValue(String.class));
                                            //Log.e(TAG, "COMPANY_ADDRESS" + child.child(Constants.COMPANY_ADDRESS).getValue(String.class));
                                            //Log.e(TAG,"OfferGender"+child.child(Constants.GENDER).getValue(String.class));

                                            mOfferList.add(offersVO);

                                        }

                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            setAdapterData(mOfferList);

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progressDialog.dismiss();
                            showToast(databaseError.getMessage());
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                } else {
                    Query query = mOfferTable.orderByChild(Constants.PLAN_TYPE).equalTo(searchOfferType);
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                try {
                                    if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                        if (((child.child(Constants.GENDER).getValue().toString().equals(searchGender)) ||
                                                (child.child(Constants.GENDER).getValue().toString().equals(Constants.GENDER_TYPE_ALL))) &&
                                                child.child(Constants.SEARCHCATEGORY).getValue().toString().equals(searchCategory)) {
                                            //Log.e(TAG, child.getKey());
                                            OffersVO offersVO = child.getValue(OffersVO.class);
                                            offersVO.setOfferID(child.getKey());

                                            //Log.e(TAG,"GENDER"+child.child(Constants.GENDER).getValue(String.class));
                                            //Log.e(TAG, "OfferPlanType" + child.child(Constants.PLAN_TYPE).getValue(String.class));
                                            //Log.e(TAG, "COMPANY_ADDRESS" + child.child(Constants.COMPANY_ADDRESS).getValue(String.class));

                                            //Log.e(TAG,"OfferGender"+child.child(Constants.GENDER).getValue(String.class));

                                            mOfferList.add(offersVO);

                                        }

                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            setAdapterData(mOfferList);

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progressDialog.dismiss();
                            showToast(databaseError.getMessage());
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                }


            } else if (searchOfferType.equals(" ") && !searchGender.equals(" ") && !searchCategory.equals(" ") && !searchLocation.equals(" ")) {
                Query query = mOfferTable.orderByChild(Constants.SEARCHCATEGORY).equalTo(searchCategory);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        progressDialog.dismiss();
                        //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            try {
                                if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                    String location = child.child(Constants.COMPANY_ADDRESS).getValue(String.class).toLowerCase();
                                    String cityName = child.child(Constants.CITY).getValue(String.class);
                                    String countryName =child.child(Constants.COUNTRY).getValue(String.class);
                                    if (searchGender.equals("All")) {
                                        if ((location.contains(searchLocation.toLowerCase())) ||
                                                (cityName.toLowerCase().equalsIgnoreCase(city.toLowerCase()) &&
                                                        countryName.toLowerCase().equalsIgnoreCase(country.toLowerCase()))) {
                                            //Log.e(TAG, child.getKey());
                                            OffersVO offersVO = child.getValue(OffersVO.class);
                                            offersVO.setOfferID(child.getKey());

                                            //Log.e(TAG,"OfferCategory"+child.child(Constants.SEARCHCATEGORY).getValue(String.class));
                                            //Log.e(TAG, "OfferPlanType" + child.child(Constants.PLAN_TYPE).getValue(String.class));
                                            //Log.e(TAG, "COMPANY_ADDRESS" + child.child(Constants.COMPANY_ADDRESS).getValue(String.class));

                                            //Log.e(TAG,"OfferGender"+child.child(Constants.GENDER).getValue(String.class));

                                            mOfferList.add(offersVO);
                                        }
                                    } else {
                                        if (((location.contains(searchLocation.toLowerCase())) ||
                                                (cityName.toLowerCase().equalsIgnoreCase(city.toLowerCase()) &&
                                                        countryName.toLowerCase().equalsIgnoreCase(country.toLowerCase()))) &&
                                                ((child.child(Constants.GENDER).getValue().toString().equals(searchGender)) ||
                                                        (child.child(Constants.GENDER).getValue().toString().equals(Constants.GENDER_TYPE_ALL)))) {
                                            //Log.e(TAG, child.getKey());
                                            OffersVO offersVO = child.getValue(OffersVO.class);
                                            offersVO.setOfferID(child.getKey());

                                            //Log.e(TAG,"OfferCategory"+child.child(Constants.SEARCHCATEGORY).getValue(String.class));
                                            //Log.e(TAG, "OfferPlanType" + child.child(Constants.PLAN_TYPE).getValue(String.class));
                                            //Log.e(TAG, "COMPANY_ADDRESS" + child.child(Constants.COMPANY_ADDRESS).getValue(String.class));

                                            //Log.e(TAG,"OfferGender"+child.child(Constants.GENDER).getValue(String.class));

                                            mOfferList.add(offersVO);
                                        }
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        setAdapterData(mOfferList);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progressDialog.dismiss();
                        showToast(databaseError.getMessage());
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            } else if (!searchOfferType.equals(" ") && searchGender.equals(" ") && !searchCategory.equals(" ") && !searchLocation.equals(" ")) {
                Query query = mOfferTable.orderByChild(Constants.PLAN_TYPE).equalTo(searchOfferType);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        progressDialog.dismiss();
                        //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            try {
                                if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                    String location = child.child(Constants.COMPANY_ADDRESS).getValue(String.class).toLowerCase();
                                    String cityName = child.child(Constants.CITY).getValue(String.class);
                                    String countryName =child.child(Constants.COUNTRY).getValue(String.class);
                                    if ((child.child(Constants.SEARCHCATEGORY).getValue().toString().equals(searchCategory)) &&
                                            ((location.contains(searchLocation.toLowerCase())) ||
                                                    (cityName.toLowerCase().equalsIgnoreCase(city.toLowerCase()) &&
                                                            countryName.toLowerCase().equalsIgnoreCase(country.toLowerCase())))) {
                                        //Log.e(TAG, child.getKey());
                                        OffersVO offersVO = child.getValue(OffersVO.class);
                                        offersVO.setOfferID(child.getKey());

                                        //Log.e(TAG,"OfferCategory"+child.child(Constants.SEARCHCATEGORY).getValue(String.class));
                                        ///Log.e(TAG, "OfferPlanType" + child.child(Constants.PLAN_TYPE).getValue(String.class));
                                        //Log.e(TAG, "COMPANY_ADDRESS" + child.child(Constants.COMPANY_ADDRESS).getValue(String.class));

                                        //Log.e(TAG,"OfferGender"+child.child(Constants.GENDER).getValue(String.class));

                                        mOfferList.add(offersVO);
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        setAdapterData(mOfferList);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progressDialog.dismiss();
                        showToast(databaseError.getMessage());
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            } else if (!searchOfferType.equals(" ") && !searchGender.equals(" ") && searchCategory.equals(" ") && !searchLocation.equals(" ")) {
                Query query = mOfferTable.orderByChild(Constants.PLAN_TYPE).equalTo(searchOfferType);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        progressDialog.dismiss();
                        //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            try {
                                if (getOnGoingOffer(child.child("offerExpiryDate").getValue() + "")) {
                                    String location = child.child(Constants.COMPANY_ADDRESS).getValue(String.class).toLowerCase();
                                    String cityName = child.child(Constants.CITY).getValue(String.class);
                                    String countryName =child.child(Constants.COUNTRY).getValue(String.class);
                                    if (searchGender.equals("All")) {
                                        if (((location.contains(searchLocation.toLowerCase())) ||
                                                (cityName.toLowerCase().equalsIgnoreCase(city.toLowerCase()) &&
                                                        countryName.toLowerCase().equalsIgnoreCase(country.toLowerCase())))) {
                                            //Log.e(TAG, child.getKey());
                                            OffersVO offersVO = child.getValue(OffersVO.class);
                                            offersVO.setOfferID(child.getKey());

                                            //Log.e(TAG,"OfferCategory"+child.child(Constants.SEARCHCATEGORY).getValue(String.class));
                                            //Log.e(TAG, "OfferPlanType" + child.child(Constants.PLAN_TYPE).getValue(String.class));
                                            //Log.e(TAG, "COMPANY_ADDRESS" + child.child(Constants.COMPANY_ADDRESS).getValue(String.class));

                                            //Log.e(TAG,"OfferGender"+child.child(Constants.GENDER).getValue(String.class));

                                            mOfferList.add(offersVO);
                                        }
                                    } else {
                                        if (((location.contains(searchLocation.toLowerCase())) ||
                                                (cityName.toLowerCase().equalsIgnoreCase(city.toLowerCase()) &&
                                                        countryName.toLowerCase().equalsIgnoreCase(country.toLowerCase()))) &&
                                                ((child.child(Constants.GENDER).getValue().toString().equals(searchGender)) ||
                                                        (child.child(Constants.GENDER).getValue().toString().equals(Constants.GENDER_TYPE_ALL)))) {
                                            //Log.e(TAG, child.getKey());
                                            OffersVO offersVO = child.getValue(OffersVO.class);
                                            offersVO.setOfferID(child.getKey());
                                            //Log.e(TAG,"OfferCategory"+child.child(Constants.SEARCHCATEGORY).getValue(String.class));
                                            //Log.e(TAG, "OfferPlanType" + child.child(Constants.PLAN_TYPE).getValue(String.class));
                                            //Log.e(TAG, "COMPANY_ADDRESS" + child.child(Constants.COMPANY_ADDRESS).getValue(String.class));

                                            //Log.e(TAG,"OfferGender"+child.child(Constants.GENDER).getValue(String.class));

                                            mOfferList.add(offersVO);
                                        }
                                    }


                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        setAdapterData(mOfferList);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progressDialog.dismiss();
                        showToast(databaseError.getMessage());
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            }
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }

    DataSnapshot dataSnapShot1;

    private DataSnapshot getOfferListByCategory() {

        Query getOfferListQuery = mOfferTable.orderByChild(Constants.SEARCHCATEGORY).equalTo(searchCategory);
        getOfferListQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dataSnapShot1 = dataSnapshot;
                progressDialog.dismiss();
                //Log.e(TAG,"Count"+dataSnapshot.getChildrenCount());
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    // Log.e(TAG,child.getKey());
                    OffersVO offersVO = child.getValue(OffersVO.class);
                    offersVO.setOfferID(child.getKey());
                    mOfferList.add(offersVO);
                }
                setAdapterData(mOfferList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });

        return dataSnapShot1;
    }

    private boolean getOnGoingOffer(String offerExpiryDate) throws ParseException {
        Long offer_expiry_time = Long.valueOf(offerExpiryDate);

        Timestamp expiryDateTimeStamp = new Timestamp(offer_expiry_time * 1000);

        Date today = new Date();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        String offerEndDate = dateFormat.format(expiryDateTimeStamp);
        String todayDateString = dateFormat.format(today);

        Date endDate = dateFormat.parse(offerEndDate);
        Date todayDate = dateFormat.parse(todayDateString);

        // Log.e(TAG, "today Date " + dateFormat.format(todayDate));
        // Log.e(TAG, "Expiry Date " + dateFormat.format(endDate));
        return todayDate.equals(endDate) || todayDate.before(endDate);
    }


    //Set Adapter Data
    private void setAdapterData(ArrayList<OffersVO> mOfferList) {
        sortList(mOfferList);
    }

    private void sortList(ArrayList<OffersVO> mOfferList) {
        Collections.sort(mOfferList, new Comparator<OffersVO>() {
            public int compare(OffersVO o1, OffersVO o2) {
                Date date1, date2;

                date1 = DateUtil.getDateTime(Long.valueOf(o1.getOfferExpiryDate()));
                date2 = DateUtil.getDateTime(Long.valueOf(o2.getOfferExpiryDate()));

                /*if(o1.getCreatedAt() > 0){
                    date1 = DateUtil.getDateTime(o1.getCreatedAt());
                }else
                    date1 = DateUtil.getDateTime(o1.getUpdatedAt());

                if(o2.getCreatedAt() > 0){
                    date2 = DateUtil.getDateTime(o2.getCreatedAt());
                }else
                    date2 = DateUtil.getDateTime(o2.getUpdatedAt());

                if (date1 == null || date2 == null)
                    return 0;
                return date2.compareTo(date1); //descending order through start date of offer*/

                if (date1 == null || date2 == null)
                    return 0;
                return date1.compareTo(date2);
            }

        });

        setAdapter(mOfferList);

    }

    private void setAdapter(ArrayList<OffersVO> mOfferList) {
        OffersAdapter offersAdapter = new OffersAdapter(mContext, mOfferList, "SearchOffer");
        recyclerView.setAdapter(offersAdapter);
        offersAdapter.notifyDataSetChanged();
        imageView_placeholder.setVisibility(View.GONE);

        if (mOfferList.size() == 0) {
            imageView_placeholder.setVisibility(View.VISIBLE);
        } else {
            imageView_placeholder.setVisibility(View.GONE);
        }
    }

    //Set toolbar settings
    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).showHideToolbarBackButton(true);
        ((BaseActivity) mContext).showHideToolbarSerachEdittext(false);
        ((BaseActivity) mContext).showHideToolbarSearchCancelButton(false);
        ((BaseActivity) mContext).showHideToolbarTitle(true);
        ((BaseActivity) mContext).setToolbarTitle(getString(R.string.advanced_search));
        ((BaseActivity) mContext).showHideToolbarSearchButton(false);
        ((BaseActivity) mContext).showHideToolbarMenuButton(false);
        ((BaseActivity) mContext).showHideToolbarChatOfferTitle(false);
        ((BaseActivity) mContext).showHideToolbarChatImage(false);
        ((BaseActivity) mContext).showHideToolbarChatEmail(false);
        ((BaseActivity) mContext).showHideToolbarRemainder(false);
        //disableBack(rootView);
        // EventBus.getDefault().post( new UpdateSearchedData());
        //((BaseActivity)mContext).setToolbarTitle(mContext.getString(R.string.l_browse_offers));

    }


}
