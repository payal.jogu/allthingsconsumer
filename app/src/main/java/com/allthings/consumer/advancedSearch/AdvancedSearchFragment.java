package com.allthings.consumer.advancedSearch;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.ListPopupWindow;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.adapters.PlaceAutoCompleteAdapter;
import com.allthings.consumer.model.CountryDetailsVo;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.GeocoderApi;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.OnResultInterface;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class AdvancedSearchFragment extends BaseFragment implements View.OnClickListener, View.OnFocusChangeListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnResultInterface {
    private static final String TAG = "AdvancedSearchFragment";
    private View rootView;
    private TextView tvAdvancedSearch;
    private ImageView ivSearchClose;
    private EditText etSearchOfferType, etSearchGender, etSearchCategory;
    private AutoCompleteTextView etSearchLocation;
    private Button btnSearchClear, btnSearch;
    private ArrayList<String> categoryList, offerTypeList, genderList;
    private DatabaseReference dbRefToCategory, dbRefToCountry;
    private String category = " ", location = " ", offerType = " ", gender = " ";
    private GoogleApiClient mGoogleApiClient;
    private PlaceAutoCompleteAdapter mAdapter;
    private int PLACE_PICKER_REQUEST = 1;
    private String city = "", country = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_advanced_search, container, false);
            init();
            setListeners();
            setFontStyle();
            getCategoryList();
            getOfferPlanType();
            return rootView;
        } else {
            return rootView;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void init() {
        tvAdvancedSearch = rootView.findViewById(R.id.tvAdvancedSearch);

        ivSearchClose = rootView.findViewById(R.id.ivSearchClose);

        etSearchCategory = rootView.findViewById(R.id.etSearchCategory);
        etSearchGender = rootView.findViewById(R.id.etSearchGender);
        etSearchOfferType = rootView.findViewById(R.id.etSearchOfferType);
        etSearchLocation = rootView.findViewById(R.id.etSearchLocation);

        btnSearch = rootView.findViewById(R.id.btnSearch);
        btnSearchClear = rootView.findViewById(R.id.btnSearchClear);

        dbRefToCategory = mDatabase.getReference(Constants.CONSUMER_USER_MASTER);
        dbRefToCountry = mDatabase.getReference(Constants.COUNTRY_MASTER);

        categoryList = new ArrayList<>();
        offerTypeList = new ArrayList<>();
        genderList = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= 21) {
            // etSearchLocation.setShowSoftInputOnFocus(false);
            etSearchOfferType.setShowSoftInputOnFocus(false);
            etSearchGender.setShowSoftInputOnFocus(false);
            etSearchCategory.setShowSoftInputOnFocus(false);
        }

        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        genderList.add("Male");
        genderList.add("Female");
        genderList.add("All");

        etSearchLocation.setThreshold(2);
        //setBounds();

    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient.isConnected() && mGoogleApiClient != null) {

            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    //Set The Lat Long
    private void setBounds(/*double Lat ,double Long*/) {

     /*   AutocompleteFilter filter = new AutocompleteFilter.Builder()
                .setCountry(countryObject.getRegionCode())
                .build();*/
       /* double latRadian, degLatKm = 110.574235, degLongKm, deltaLat, deltaLong, minLat, minLong, maxLat, maxLong;
        int mDistanceInMeters = 1000;
        //latRadian = Math.toRadians(Lat);

        System.out.print("Here");

        degLongKm = 110.572833 * Math.cos(latRadian);
        deltaLat = mDistanceInMeters / 1000.0 / degLatKm;
        deltaLong = mDistanceInMeters / 1000.0 / degLongKm;

        minLat =Lat - deltaLat;
        minLong = Long- deltaLong;
        maxLat =Lat + deltaLat;
        maxLong = Long+ deltaLong;
*/
        mAdapter = new PlaceAutoCompleteAdapter(mContext, android.R.layout.simple_list_item_1,
                mGoogleApiClient /*new LatLngBounds(new LatLng(minLat, minLong), new LatLng(maxLat, maxLong))*/);
        etSearchLocation.setAdapter(mAdapter);
    }

    private void setFontStyle() {
        setTypeFaceTextView(tvAdvancedSearch, getString(R.string.fontOpenSansSemiBold));

        setTypeFaceEdittext(etSearchCategory, getString(R.string.fontOpenSansRegular));
        setTypeFaceEdittext(etSearchGender, getString(R.string.fontOpenSansRegular));
        setTypeFaceEdittext(etSearchOfferType, getString(R.string.fontOpenSansRegular));
        setTypeFaceAutoCompleteTextView(etSearchLocation, getString(R.string.fontOpenSansRegular));

        setTypeFaceButton(btnSearch, getString(R.string.fontOpenSansRegular));
        setTypeFaceButton(btnSearchClear, getString(R.string.fontOpenSansRegular));
    }

    private void setListeners() {
        ivSearchClose.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnSearchClear.setOnClickListener(this);
        etSearchLocation.setOnClickListener(this);
        etSearchLocation.setInputType(InputType.TYPE_NULL);

        etSearchCategory.setOnClickListener(this);
        etSearchGender.setOnClickListener(this);
        etSearchOfferType.setOnClickListener(this);
        //etSearchLocation.setOnClickListener(this);

        etSearchCategory.setOnFocusChangeListener(this);
        etSearchGender.setOnFocusChangeListener(this);
        etSearchOfferType.setOnFocusChangeListener(this);
        etSearchLocation.setOnFocusChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).showHideToolbarMenuButton(false);
        ((BaseActivity) mContext).showHideToolbarSerachEdittext(false);
        ((BaseActivity) mContext).showHideToolbarSearchCancelButton(false);
        ((BaseActivity) mContext).showHideToolbarTitle(true);
        ((BaseActivity) mContext).setToolbarTitle("Advanced Search");
        ((BaseActivity) mContext).showHideToolbarSearchButton(false);
        ((BaseActivity) mContext).showHideToolbarBackButton(true);
        ((BaseActivity) mContext).showHideToolbarChatOfferTitle(false);
        ((BaseActivity) mContext).showHideToolbarChatImage(false);
        ((BaseActivity) mContext).showHideToolbarChatEmail(false);
        ((BaseActivity) mContext).showHideToolbarRemainder(false);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivSearchClose:
                ((BaseActivity) mContext).popBackStack();
                break;
            case R.id.btnSearchClear:
                etSearchCategory.getText().clear();
                etSearchGender.getText().clear();
                etSearchLocation.setText("");
                etSearchOfferType.getText().clear();

                gender = " ";
                offerType = " ";
                category = " ";
                location = " ";

                break;
            case R.id.btnSearch:
                hideKeyboard();
                if (NetworkUtil.isOnline(mContext)) {
                    if (etSearchCategory.getText().toString().trim().length() != 0 ||
                            etSearchOfferType.getText().toString().trim().length() != 0 ||
                            etSearchGender.getText().toString().trim().length() != 0 ||
                            etSearchLocation.getText().toString().trim().length() != 0) {
                        Bundle bundle = getBundleData();
                        Fragment fragment = new AdvancedSearchOffersListFragment();
                        fragment.setArguments(bundle);
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, fragment)
                                .addToBackStack("AdvancedSearchFragment")
                                .commit();
                    } else {
                        showDialogAlertPositiveButton(getString(R.string.app_name), getString(R.string.please_select_any_criteria));
                    }
                } else {
                    showDialogAlertPositiveButton(getString(R.string.app_name), getString(R.string.please_check_network));
                }

                break;
            case R.id.etSearchCategory:
                if (categoryList.size() == 0) {
                    getCategoryList();
                }
                setCategoryList();
                break;
            case R.id.etSearchOfferType:
                if (offerTypeList.size() == 0) {
                    getOfferPlanType();
                }
                setOfferTypeList();
                break;
            case R.id.etSearchGender:
                setGenderList();
                break;
            case R.id.etSearchLocation:
                openPlacePicker();
                break;

        }

    }

    private Bundle getBundleData() {
        Bundle bundle = new Bundle();

        if (!etSearchCategory.getText().toString().trim().isEmpty())
            category = etSearchCategory.getText().toString().trim();
        if (!etSearchLocation.getText().toString().trim().isEmpty())
            location = etSearchLocation.getText().toString().trim();
        if (!etSearchGender.getText().toString().trim().isEmpty())
            gender = etSearchGender.getText().toString().trim();
        if (!etSearchOfferType.getText().toString().trim().isEmpty())
            offerType = etSearchOfferType.getText().toString().trim();

        bundle.putString(Constants.SEARCH_CATEGORY, category);
        bundle.putString(Constants.SEARCH_GENDER, gender);
        bundle.putString(Constants.SEARCH_OFFER_TYPE, offerType);
        bundle.putString(Constants.SEARCH_LOCATION, location);
        bundle.putString(Constants.CITY, city);
        bundle.putString(Constants.COUNTRY, country);

        return bundle;

    }


    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId()) {
            case R.id.etSearchCategory:
                if (!b) {
                    etSearchCategory.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
                } else {
                    etSearchCategory.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
                    if (categoryList.size() == 0) {
                        getCategoryList();
                    }
                    setCategoryList();
                }
                break;
            case R.id.etSearchOfferType:
                if (!b) {
                    etSearchOfferType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
                } else {
                    etSearchOfferType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
                    if (offerTypeList.size() == 0) {
                        getOfferPlanType();
                    }
                    setOfferTypeList();
                }
                break;
            case R.id.etSearchGender:
                if (!b) {
                    etSearchGender.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
                } else {
                    etSearchGender.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
                    setGenderList();
                }
                break;
            case R.id.etSearchLocation:
                if (!b) {
                    etSearchLocation.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
                } else {
                    etSearchLocation.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
                    openPlacePicker();
                }
                break;

        }
    }

    /**
     * Get category list from database
     */
    private void getCategoryList() {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            Log.e("USER ID", SharedPreferenceUtil.getString(Constants.USER_ID, ""));

            dbRefToCategory.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).child(Constants.CATEGORY)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            progressDialog.dismiss();
                            categoryList.clear();
                            if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    if (child.hasChildren()) {
                                        for (DataSnapshot subChild : child.getChildren()) {
                                            categoryList.add(subChild.getKey());
                                            Log.e("category", subChild.getKey());
                                        }
                                    }
                                    //categoryList.add(child.getKey());
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            progressDialog.dismiss();
                            LogUtils.LOG_E(TAG, databaseError.getMessage());
                        }
                    });
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    /**
     * Set Category list from database
     */
    private void setCategoryList() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, categoryList);
        final ListPopupWindow lpw = new ListPopupWindow(getContext());
        lpw.setAdapter(dataAdapter);
        lpw.setAnchorView(etSearchCategory);
        lpw.setModal(true);
        lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String item = categoryList.get(position);
                etSearchCategory.setText(item);
                lpw.dismiss();
            }
        });
        lpw.show();

    }

    /**
     * set gender list from database
     */
    private void setGenderList() {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, genderList);
        final ListPopupWindow lpw = new ListPopupWindow(getContext());
        lpw.setAdapter(dataAdapter);
        lpw.setAnchorView(etSearchGender);
        lpw.setModal(true);
        lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String item = genderList.get(position);
                etSearchGender.setText(item);
                lpw.dismiss();
            }
        });
        lpw.show();

    }

    /**
     * Get Plan type list from database
     */
    private void getOfferPlanType() {
        if (NetworkUtil.isOnline(mContext)) {

            dbRefToCountry.child(SharedPreferenceUtil.getString(Constants.COUNTRY, ""))
                    .child(Constants.SUBSCRIPTION_TYPE_MASTER).addListenerForSingleValueEvent
                    (new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            offerTypeList.clear();
                            if (dataSnapshot.getValue() != null) {
                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    Log.e(TAG, "offerType" + child.getKey());
                                    offerTypeList.add(child.getKey());
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    /**
     * set Plan type list from database
     */
    private void setOfferTypeList() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, offerTypeList);
        final ListPopupWindow lpw = new ListPopupWindow(getContext());
        lpw.setAdapter(dataAdapter);
        lpw.setAnchorView(etSearchOfferType);
        lpw.setModal(true);
        lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String item = offerTypeList.get(position);
                etSearchOfferType.setText(item);
                lpw.dismiss();
            }
        });
        lpw.show();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LogUtils.LOG_E(TAG, "GoogleClientApi is connected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        LogUtils.LOG_E(TAG, "GoogleClientApi is hot connected");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        LogUtils.LOG_E(TAG, "GoogleClientApi is disconnected" + connectionResult.toString());
    }

    private void openPlacePicker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, mContext);

                if (place.getAddress() != null) {
                    etSearchLocation.setText(String.valueOf(place.getAddress()));
                    LogUtils.LOG_E("Address", String.valueOf(place.getAddress()));
                }

                if (place.getLatLng() != null) {
                    double latitude = place.getLatLng().latitude;
                    double longitude = place.getLatLng().longitude;
                    LogUtils.LOG_E("LAT", String.valueOf(latitude));
                    LogUtils.LOG_E("LONG", String.valueOf(longitude));

                    GeocoderApi geocoderApi = new GeocoderApi(this);
                    geocoderApi.execute(latitude + "," + longitude);

                   /* Geocoder geocoder = new Geocoder(mContext,Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
                        //String address = addresses.get(0).getAddressLine(0);
                        cityName = addresses.get(0).getLocality();
                        if (cityName != null)
                            LogUtils.LOG_E("CITY NAME", cityName);
                        //String country = addresses.get(0).getAddressLine(2);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                }

            }
        }
    }

    @Override
    public void getResult(String response, String countryName) {
        city = response;
        country = countryName;

        LogUtils.LOG_E(TAG, "city " + city);
        LogUtils.LOG_E(TAG, "country " + country);

    }
}
