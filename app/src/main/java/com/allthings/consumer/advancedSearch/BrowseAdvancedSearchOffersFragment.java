package com.allthings.consumer.advancedSearch;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.offers.BrowseOffersFragment;
import com.allthings.consumer.offers.OffersFragment;
import com.allthings.consumer.offers.UpdateCategoryTitle;
import com.allthings.consumer.utils.Constants;


public class BrowseAdvancedSearchOffersFragment extends BaseFragment implements TabLayout.OnTabSelectedListener ,UpdateCategoryTitle {
    private static final String TAG ="BrowseAdvancedSearchOffersFragment" ;
    private View rootView;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private UpdateCategoryTitle updateTitle;
    private String searchCategory,searchOfferType,searchLocation,searchGender;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_browse_advanced_search_offers, container, false);
            init();
            setListeners();
            getArgument();
            //setFontStyle();
            disableBack(rootView);
            return rootView;
        } else {
            return rootView;
        }
    }

    //Set toolbar settings
    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity)mContext).showToolbar(true);
        ((BaseActivity)mContext).showHideToolbarBackButton(true);
        ((BaseActivity)mContext).showHideToolbarSerachEdittext(false);
        ((BaseActivity)mContext).showHideToolbarSearchCancelButton(false);
        ((BaseActivity)mContext).showHideToolbarTitle(true);
        ((BaseActivity)mContext).setToolbarTitle(getString(R.string.advanced_search));
        ((BaseActivity)mContext).showHideToolbarSearchButton(false);
        ((BaseActivity)mContext).showHideToolbarMenuButton(false);
       /* if(((BaseActivity) mContext).searching)
        {
            ((BaseActivity)mContext).showHideToolbarSerachEdittext(true);
            ((BaseActivity)mContext).showHideToolbarSearchCancelButton(true);
            ((BaseActivity)mContext).showHideToolbarSearchButton(false);
            ((BaseActivity)mContext).showHideToolbarTitle(false);
            ((BaseActivity)mContext).showHideToolbarMenuButton(false);
        }else {
            ((BaseActivity)mContext).showHideToolbarSerachEdittext(false);
            ((BaseActivity)mContext).showHideToolbarSearchCancelButton(false);
            ((BaseActivity)mContext).showHideToolbarTitle(true);
            ((BaseActivity)mContext).setToolbarTitle(getString(R.string.l_browse_offers));
            ((BaseActivity)mContext).showHideToolbarSearchButton(true);
            ((BaseActivity)mContext).showHideToolbarMenuButton(true);
        }*/

      /*  ((BaseActivity)mContext).setToolbarTitle(mContext.getString(R.string.l_browse_offers));
        ((BaseActivity)mContext).showHideToolbarMenuButton(true);
        ((BaseActivity)mContext).showHideToolbarSearchButton(true);*/
        ((BaseActivity)mContext).showHideToolbarChatOfferTitle(false);
        ((BaseActivity)mContext).showHideToolbarChatImage(false);
        ((BaseActivity)mContext).showHideToolbarChatEmail(false);
    }

    private void getArgument(){
        Bundle bundle = getArguments();

        searchCategory = bundle.getString(Constants.SEARCH_CATEGORY);
        searchLocation = bundle.getString(Constants.SEARCH_LOCATION);
        searchOfferType = bundle.getString(Constants.SEARCH_OFFER_TYPE);
        searchGender = bundle.getString(Constants.SEARCH_GENDER);
    }

    //Set view pager in tab layout
    private void setListeners() {
        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(this);
    }
    //Initialize components
    private void init() {
        updateTitle=this;
        tabLayout=(TabLayout)rootView.findViewById(R.id.tbAdvancedSearch);
        viewPager=(ViewPager)rootView.findViewById(R.id.vpAdavcedSearch);
        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.gold)));
        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.silver)));
        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.bronze)));
        viewPager.setAdapter(new TabAdapter1(getChildFragmentManager()));
        viewPager.setOffscreenPageLimit(3);


    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition()); //Select Tab And Change It Position

    }
    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    //Set Category  With Count
    @Override
    public void setCateTitle(String title, int count) {
        if (title.equalsIgnoreCase(mContext.getString(R.string.gold)))
        {
            tabLayout.getTabAt(0).setText(mContext.getString(R.string.gold)+"("+count+")");
        }
        else if (title.equalsIgnoreCase(mContext.getString(R.string.silver)))
        {
            tabLayout.getTabAt(1).setText(mContext.getString(R.string.silver)+"("+count+")");
        }
        else if (title.equalsIgnoreCase(mContext.getString(R.string.bronze)))
        {
            tabLayout.getTabAt(2).setText(mContext.getString(R.string.bronze)+"("+count+")");
        }
    }
    //Adapter For Tab
    private class TabAdapter1 extends FragmentPagerAdapter {
        TabAdapter1(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int index) {
            String title="";
            if (index == 0)
            {
                title="Gold";
            }
            else if (index == 1)
            {
                title="Silver";
            }
            else if (index == 2)
            {
                title="Bronze";
            }
            return AdvancedSearchOffersListFragment.newInstance(title,updateTitle,
                    searchGender,searchCategory,searchLocation,searchOfferType,"","");
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return super.getPageTitle(position);
        }

        @Override
        public int getCount()
        {
            return 3;
        }

    }

}
