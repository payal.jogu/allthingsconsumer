package com.allthings.consumer.about;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.BuildConfig;
import com.allthings.consumer.R;


/**
 * Created by parth on 15/05/18.
 */

public class AboutFragment extends BaseFragment {
    private final String TAG = "AboutFragment";
    private View rootView;
    private TextView txtViewContent1, txtViewContent2, txtViewVersion, txtViewPvtLtd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_about, container, false);
            init();
            setFontStyle();
            setListeners();

            return rootView;
        }
        return rootView;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    private void setFontStyle() {
        setTypeFaceTextView(txtViewContent1, getString(R.string.fontOpenSansRegular));
        setTypeFaceTextView(txtViewContent2, getString(R.string.fontOpenSansRegular));
        setTypeFaceTextView(txtViewVersion, getString(R.string.fontOpenSansRegular));
        setTypeFaceTextView(txtViewPvtLtd, getString(R.string.fontOpenSansBold));

    }


    private void setListeners() {

    }

    private void init() {
        txtViewContent1 = (TextView) rootView.findViewById(R.id.txtViewContent1);
        txtViewContent2 = (TextView) rootView.findViewById(R.id.txtViewContent2);
        txtViewVersion = (TextView) rootView.findViewById(R.id.txtViewVersion);
        txtViewPvtLtd = (TextView) rootView.findViewById(R.id.txtViewPvtLtd);

        txtViewVersion.setText("v "+ BuildConfig.VERSION_NAME);

        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).setToolbarTitle(mContext.getResources().getString(R.string.about_small));
        ((BaseActivity) mContext).showHideToolbarMenuButton(true);
        ((BaseActivity) mContext).showHideToolbarSearchButton(false);

    }


}

