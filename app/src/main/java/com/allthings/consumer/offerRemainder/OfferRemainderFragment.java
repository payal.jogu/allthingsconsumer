package com.allthings.consumer.offerRemainder;


import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.offers.MyOfferDetailsFragment;
import com.allthings.consumer.offers.OffersVO;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.SharedPreferenceUtil;

import java.sql.Timestamp;
import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;


public class OfferRemainderFragment extends DialogFragment implements View.OnClickListener{
    private View rootView;
    private final String TAG = "OfferRemainderFragment";
    private EditText etSelectDateTimeRemainder;
    private Button btnSetDateTime,btnCancelDateTime;
    private Context context;
    private  DatePickerDialog datePickerDialog;
    private TimePickerDialog  timePickerDialog;
    int selectedHour,selectedMinute,selectedDate,selectedYear,selectedMonth;
    String offerID,expiryDate,title;
    private static int remainderCount = 0;
    private  Timestamp expiryDateTimestamp;
    private  String am_pm = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.border_rounded);
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_offer_remainder, container, false);
            init();
            setListeners();
            getArgument();
            showDatePickerDialog();
            return rootView;
        } else
            return rootView;

    }

    private void getArgument() {
        Bundle bundle = getArguments();
        if(bundle != null){
            if(bundle.containsKey(Constants.OFFER_ID)){
                offerID = bundle.getString(Constants.OFFER_ID);
                title = bundle.getString(Constants.OFFER_TITLE);
                expiryDate = bundle.getString(Constants.OFFER_EXPIRY_DATE);
                 expiryDateTimestamp = new Timestamp(Long.valueOf(expiryDate)*1000);
                Log.e("OFFER_ID",offerID);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = getTargetFragment().getContext();
    }

    private void init(){
        etSelectDateTimeRemainder = (EditText) rootView.findViewById(R.id.etSelectDateTimeRemainder);
        btnSetDateTime = (Button) rootView.findViewById(R.id.btnSetDateTime);
        btnCancelDateTime = (Button) rootView.findViewById(R.id.btnCancelDateTime);

    }

    private void setListeners(){
        etSelectDateTimeRemainder.setOnClickListener(this);
        btnSetDateTime.setOnClickListener(this);
        btnCancelDateTime.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.etSelectDateTimeRemainder:
                datePickerDialog.show();
                //showDatePickerDialog();
                break;
            case R.id.btnSetDateTime:
                if(selectedDate != 0 && selectedMonth != 0 && selectedYear != 0) {
                    setAlarm();
                    RemainderDialogInterface remainderDialogInterface = (RemainderDialogInterface) getTargetFragment();
                    remainderDialogInterface.refreshRemainder();
                }else {
                    Toast.makeText(context,"Please select date & time.",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnCancelDateTime:
                dismiss();
                break;
        }
    }

    private void showDatePickerDialog(){
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {


                        selectedDate = dayOfMonth;
                        selectedYear = year;
                        selectedMonth = monthOfYear;
                        etSelectDateTimeRemainder.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        showTimePickerDialog();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.getDatePicker().setMaxDate(expiryDateTimestamp.getTime());

    }


    private void showTimePickerDialog(){
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        int pm = c.get(Calendar.AM_PM);

        // Launch Time Picker Dialog
        timePickerDialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        selectedHour =  hourOfDay % 12; ;
                        selectedMinute = minute;

                        //setCalenderObject();
                        etSelectDateTimeRemainder.append(String.format(" %02d:%02d %s", selectedHour == 0 ? 12 : selectedHour,
                                selectedMinute, hourOfDay < 12 ? "AM" : "PM"));
                       // etSelectDateTimeRemainder.append("  "+hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute,false);
        timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                etSelectDateTimeRemainder.getText().clear();
                selectedDate = 0;
                selectedHour = 0;
                selectedMonth = 0;
            }
        });
        timePickerDialog.show();
    }

    private void setAlarm(){
        Calendar calendar  = Calendar.getInstance();
        calendar.set(Calendar.MONTH, selectedMonth);
        calendar.set(Calendar.YEAR, selectedYear);
        calendar.set(Calendar.DAY_OF_MONTH, selectedDate);

        calendar.set(Calendar.HOUR,selectedHour);
        calendar.set(Calendar.MINUTE, selectedMinute);
        calendar.set(Calendar.SECOND, 0);

       /* Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.DAY_OF_MONTH, 25);

        calendar.set(Calendar.HOUR, 1);
        calendar.set(Calendar.MINUTE, 50);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND,0);
       // calendar.set(Calendar.AM_PM,Calendar.PM);*/

        Log.e("Time"," Calendar AM_PM :"+calendar.get(Calendar.AM_PM)+" Calendar Year:"+calendar.get(Calendar.YEAR)+" Calendar Month: "
                +calendar.get(Calendar.MONTH)+"Calendar Date :"+calendar.get(Calendar.DAY_OF_MONTH));
        Log.e("Time","Calendar Hour :"+calendar.get(Calendar.HOUR_OF_DAY)+" Calendar Minute: "+calendar.get(Calendar.MINUTE));

        Log.e("Time"," Year:"+selectedYear+" Month: "+selectedMonth+" Date :"+selectedDate);
        Log.e("Time"," Hour :"+selectedHour+" Minute: "+selectedMinute);

        Log.e("Alarm",calendar.getTimeInMillis()+"");

        if(offerID != null && title != null && expiryDate != null){

            int requestCode = (int) System.currentTimeMillis();

            SharedPreferenceUtil.putValue(offerID,requestCode);
            SharedPreferenceUtil.save();

            Intent intentReceiver = new Intent(getContext(), AlarmReceiver.class);
            intentReceiver.putExtra(Constants.OFFER_ID,offerID);
            intentReceiver.putExtra(Constants.OFFER_TITLE,title);
            intentReceiver.putExtra(Constants.OFFER_EXPIRY_DATE,expiryDate);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    getContext(),requestCode, intentReceiver,  PendingIntent.FLAG_UPDATE_CURRENT);

            AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);

            Timestamp timestamp = new Timestamp(calendar.getTimeInMillis());
            Log.e("Alarm",timestamp.getTime()+"");


         /*
            The following sets the Alarm in the specific time by getting the long value of the alarm  date time which is in calendar object by calling the getTimeInMillis(). Since Alarm supports only long value , we're using this method.
          */
            //alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),6000, pendingIntent);
            alarmManager.setExact(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(), pendingIntent);
            Log.e("Alarm","set"+SharedPreferenceUtil.getInt(offerID,0));
        }

        dismiss();
    }
}
