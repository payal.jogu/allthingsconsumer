package com.allthings.consumer.offerRemainder;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Payal jogu on 25/9/17.
 */

public class RemainderStorageClass extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "OffersRemainder.db";
    private static final int DATABASE_VERSION = 1;
    public static final String  REMAINDER_TABLE_NAME = "Remainder";
    public static final String REMAINDER_COLUMN_ID = "remainder_id";
    public static final String REMAINDER_COLUMN_NAME = "remainder_offer_id";

    public RemainderStorageClass(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + REMAINDER_TABLE_NAME + "(" +
                REMAINDER_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                REMAINDER_COLUMN_NAME + " TEXT) "
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
