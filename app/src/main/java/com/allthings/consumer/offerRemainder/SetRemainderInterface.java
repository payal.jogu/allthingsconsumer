package com.allthings.consumer.offerRemainder;

/**
 * Created by Payal jogu on 27/9/17.
 */

public class SetRemainderInterface {
    String offerID;

    public String getOfferID() {
        return offerID;
    }

    public void setOfferID(String offerID) {
        this.offerID = offerID;
    }
}
