package com.allthings.consumer.offerRemainder;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.view.WindowManager;

import com.allthings.consumer.R;
import com.allthings.consumer.offers.MyOfferDetailsFragment;
import com.allthings.consumer.pushNotification.NotificationUtils;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.SharedPreferenceUtil;

import de.greenrobot.event.EventBus;

/**
 * Created by Payal jogu on 23/9/17.
 */

public class AlarmReceiver extends WakefulBroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
            // When our Alaram time is triggered , this method will be excuted (onReceive)
            // We're invoking a service in this method which shows Notification to the User
        Log.e("AlarmReceiver","AlarmReceiver");
        Log.e("OFFER_ID",intent.getStringExtra(Constants.OFFER_ID));

        if(NotificationUtils.isAppIsInBackground(context)){
            Intent alarmService = new Intent(context, AlarmService.class);
            alarmService.putExtra(Constants.OFFER_ID, intent.getStringExtra(Constants.OFFER_ID));
            alarmService.putExtra(Constants.OFFER_TITLE, intent.getStringExtra(Constants.OFFER_TITLE));
            alarmService.putExtra(Constants.OFFER_EXPIRY_DATE, intent.getStringExtra(Constants.OFFER_EXPIRY_DATE));
            context.startService(alarmService);
        }else if (!SharedPreferenceUtil.getBoolean("IsOfferDetailScreen", false) && !SharedPreferenceUtil.getString("OfferIdScreen", "")
                .equalsIgnoreCase(intent.getStringExtra(Constants.OFFER_ID))) {

            if(SharedPreferenceUtil.contains(intent.getStringExtra(Constants.OFFER_ID))) {
                SharedPreferenceUtil.remove(intent.getStringExtra(Constants.OFFER_ID));
                SharedPreferenceUtil.save();
            }

            Intent alarmService = new Intent(context, AlarmService.class);
            alarmService.putExtra(Constants.OFFER_ID, intent.getStringExtra(Constants.OFFER_ID));
            alarmService.putExtra(Constants.OFFER_TITLE, intent.getStringExtra(Constants.OFFER_TITLE));
            alarmService.putExtra(Constants.OFFER_EXPIRY_DATE, intent.getStringExtra(Constants.OFFER_EXPIRY_DATE));

            context.startService(alarmService);
        }else {
            if(SharedPreferenceUtil.contains(intent.getStringExtra(Constants.OFFER_ID))) {
                SharedPreferenceUtil.remove(intent.getStringExtra(Constants.OFFER_ID));
                SharedPreferenceUtil.save();
            }

            SetRemainderInterface remainderInterface = new SetRemainderInterface();
            remainderInterface.setOfferID(intent.getStringExtra(Constants.OFFER_ID));

            EventBus.getDefault().post(remainderInterface);

            //if(!SharedPreferenceUtil.isSharedPreferenceNull()) {

        }

           /* if(!SharedPreferenceUtil.getBoolean("IsOfferDetailScreen", false) &&
                !SharedPreferenceUtil.getString("OfferIdScreen","")
                        .equalsIgnoreCase(intent.getStringExtra(Constants.OFFER_ID))){

            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Offer Remainder")
                        .setCancelable(false)
                        .setTitle(intent.getStringExtra(Constants.OFFER_TITLE))
                        .setPositiveButton(context.getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });

                AlertDialog alert = builder.create();
                alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                alert.show();

            }
*/

    }

}

