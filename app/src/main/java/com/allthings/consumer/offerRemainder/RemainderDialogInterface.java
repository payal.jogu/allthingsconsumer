package com.allthings.consumer.offerRemainder;

/**
 * Created by Payal jogu on 26/9/17.
 */

public interface RemainderDialogInterface  {
     void refreshRemainder();
}
