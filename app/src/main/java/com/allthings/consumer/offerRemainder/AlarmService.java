package com.allthings.consumer.offerRemainder;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.R;
import com.allthings.consumer.utils.Constants;

/**
 * Created by Payal jogu on 23/9/17.
 */

public class AlarmService extends Service {
    private NotificationManager notificationManager;
    static int i = 0;
    private LocalBroadcastManager broadcaster;

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        broadcaster = LocalBroadcastManager.getInstance(this);
        initChannels(this);
    }

    public void initChannels(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }
        NotificationChannel channel = new NotificationChannel("default",
                "AllThings",
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("customer app");
        notificationManager.createNotificationChannel(channel);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.e("AlarmService", "AlarmService");
        //Toast.makeText(getApplicationContext(), "Alarm Activate", Toast.LENGTH_SHORT).show();
       /* if(SharedPreferenceUtil.contains("IsOfferDetailScreen") && SharedPreferenceUtil.getBoolean("IsOfferDetailScreen", false)
                &&SharedPreferenceUtil.getString("OfferIdScreen", "")
                            .equalsIgnoreCase(intent.getExtras().getString(Constants.OFFER_ID))) {
                Intent receiverIntent = new Intent(this, AlertDialogActivity.class);
                receiverIntent.putExtra(Constants.OFFER_TITLE,intent.getExtras().getString(Constants.OFFER_TITLE));
                receiverIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(receiverIntent);


            *//*AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Offer Remainder")
                    .setCancelable(false)
                    .setTitle(intent.getExtras().getString(Constants.OFFER_TITLE))
                    .setPositiveButton(this.getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

            AlertDialog alert = builder.create();
            alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            alert.show();*//*

    }else*/
        if (intent.getExtras().containsKey(Constants.OFFER_ID)) {
            Intent receiverIntent = new Intent(this, BaseActivity.class);
            receiverIntent.putExtra("Type", "OfferRemainder");
            receiverIntent.putExtra(Constants.OFFER_ID, intent.getExtras().getString(Constants.OFFER_ID));
            receiverIntent.putExtra(Constants.OFFER_TITLE, intent.getExtras().getString(Constants.OFFER_TITLE));
            receiverIntent.putExtra(Constants.OFFER_EXPIRY_DATE, intent.getExtras().getString(Constants.OFFER_EXPIRY_DATE));
            receiverIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // use System.currentTimeMillis() to have a unique ID for the pending intent
            PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), receiverIntent, 0);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            // build notification
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this, "default")
                            .setSmallIcon(R.drawable.notification_icon)
                            .setColor(ContextCompat.getColor(this, R.color.colorRed))
                            .setContentTitle("Offer Remainder")
                            .setContentText(intent.getExtras().getString(Constants.OFFER_TITLE))
                            .setContentIntent(pIntent)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri);

           if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
               mBuilder.setPriority(NotificationManager.IMPORTANCE_HIGH);
           }else
               mBuilder.setPriority(Notification.PRIORITY_MAX);

            /*NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);*/

            notificationManager.notify(0, mBuilder.build());

            notifyLocally();
        }


        return START_REDELIVER_INTENT;
    }

    private void notifyLocally(){
        Intent intent1 = new Intent("offerRemainder");
        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        broadcaster.sendBroadcast(intent1);
    }

    @SuppressWarnings("static-access")
    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
      /*  Log.e("AlarmService","AlarmService");
        Toast.makeText(getApplicationContext(),"Alarm Activate",Toast.LENGTH_SHORT).show();

        if(intent.getExtras().containsKey(Constants.OFFER_ID)){
            Intent receiverIntent = new Intent(this, BaseActivity.class);
            receiverIntent.putExtra("Type","OfferRemainder");
            receiverIntent.putExtra(Constants.OFFER_ID,intent.getExtras().getString(Constants.OFFER_ID));
            receiverIntent.putExtra(Constants.OFFER_TITLE,intent.getExtras().getString(Constants.OFFER_TITLE));
            receiverIntent.putExtra(Constants.OFFER_EXPIRY_DATE,intent.getExtras().getString(Constants.OFFER_EXPIRY_DATE));
            receiverIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // use System.currentTimeMillis() to have a unique ID for the pending intent
            PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), receiverIntent, 0);

            // build notification
            // the addAction re-use the same intent to keep the example short
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.notification_icon)
                            .setColor(ContextCompat.getColor(this,R.color.colorRed))
                            .setContentTitle(intent.getStringExtra(Constants.OFFER_TITLE))
                            .setContentText("Your offer is expiry soon on"+intent.getStringExtra(Constants.OFFER_EXPIRY_DATE))
                            .setContentIntent(pIntent);


            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            notificationManager.notify(0, mBuilder.build());
        }

*/


    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }
}
