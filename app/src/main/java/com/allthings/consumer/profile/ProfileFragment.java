package com.allthings.consumer.profile;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.ListPopupWindow;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.adapters.PlaceAutoCompleteAdapter;
import com.allthings.consumer.model.CountryDetailsVo;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.GeocoderApi;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.OnResultInterface;
import com.allthings.consumer.utils.RoundedImageTransform;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import eu.janmuller.android.simplecropimage.CropImage;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, View.OnFocusChangeListener,
        OnResultInterface {

    private View rootView;
    private DatabaseReference mRefrenceToConsumerUser;

    private ImageView ivProfilePic;
    private TextView tvCountry, tvLocation, tvEmail, tvPhone, tvCountryCode, tvProfileGender,
            tvSelectProfileRegion, tvProfileCity;
    private EditText etCountry, etEmail, etPhone, etProfileGender, etSelectProfileRegion,
            etProfileLocation, etProfileCity;
    //private AutoCompleteTextView autotvLocation;
    private Button btn_edit;
    private String TAG = "Profile Fragment";
    private PlaceAutoCompleteAdapter mAdapter;
    private ArrayList<String> countryList, countyName;
    private GoogleApiClient mGoogleApiClient;
    private double Longitude, Latitude;
    private Geocoder geocoder;
    private DatabaseReference mRefrenceToCountry;
    ArrayAdapter<String> dataAdapter;
    private ArrayList<CountryDetailsVo> countryDetailsList;
    private String selectedCountryName;
    private CountryDetailsVo countryObject;
    private double latitude, longitude;
    String uploadPic = "", city = "";
    Uri downloadUrl;
    private final int PLACE_PICKER_REQUEST = 10;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_profile, container, false);
            init();
            setDisableEditText();
            setDisableImageview();
            setDisableAUtocompleteTextView();
            setListeners();
            setFontStyle();
            disableBack(rootView);

            return rootView;
        } else {
            return rootView;
        }

    }

    private void init() {

        mRefrenceToConsumerUser = mDatabase.getReference(Constants.CONSUMER_USER_MASTER);
        mRefrenceToCountry = mDatabase.getReference(Constants.COUNTRY_MASTER);

        ivProfilePic = rootView.findViewById(R.id.ivViewProfilePic);

        //autotvLocation=(AutoCompleteTextView)rootView.findViewById(R.id.etProfileLocation);
        etProfileLocation = rootView.findViewById(R.id.etProfileLocation);
        tvCountry = rootView.findViewById(R.id.tvProfileSelectCountry);
        tvEmail = rootView.findViewById(R.id.tvProfileEmail);
        tvLocation = rootView.findViewById(R.id.tvProfileLocation);
        tvPhone = rootView.findViewById(R.id.tvProfilePhone);
        tvProfileGender = rootView.findViewById(R.id.tvProfileGender);
        tvCountryCode = rootView.findViewById(R.id.tvProfileCountryCode);
        tvSelectProfileRegion = rootView.findViewById(R.id.tvSelectProfileRegion);
        tvProfileCity = rootView.findViewById(R.id.tvProfileCity);

        etCountry = rootView.findViewById(R.id.etProfileSelectCountry);
        etSelectProfileRegion = rootView.findViewById(R.id.etSelectProfileRegion);
        etProfileCity = rootView.findViewById(R.id.etProfileCity);

        if (Build.VERSION.SDK_INT >= 21) {
            etCountry.setShowSoftInputOnFocus(false);
            etSelectProfileRegion.setShowSoftInputOnFocus(false);
        }

        etEmail = rootView.findViewById(R.id.etProfileEmail);
        etPhone = rootView.findViewById(R.id.etProfilePhonenumber);
        etProfileGender = rootView.findViewById(R.id.etProfileGender);

        btn_edit = rootView.findViewById(R.id.btn_profile_edit);

        countryList = new ArrayList<>();
        countryDetailsList = new ArrayList<>();
        countyName = new ArrayList<>();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //getProfileDataFromFirebase();
                setProfileData();
                getCountryList();

            }
        }, 500);

        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        //autotvLocation.setThreshold(2);
    }

    private void enableEditText(EditText editText) {
        editText.setFocusable(true);
        editText.setEnabled(true);
    }

    private void setDisableAUtocompleteTextView() {
        //autotvLocation.setEnabled(false);
    }

    private void setEnableAUtocompleteTextView() {
        //autotvLocation.setEnabled(true);
    }

    private void setEnableEditText() {
        enableEditText(etCountry);
        enableEditText(etProfileCity);
        enableEditText(etEmail);
        enableEditText(etProfileLocation);
        enableEditText(etSelectProfileRegion);
        //enableEditText(etPhone);
        //showKeyboard(etEmail);

    }

    private void setDisableEditText() {
        disableEditText(etCountry);
        disableEditText(etProfileCity);
        disableEditText(etEmail);
        disableEditText(etProfileLocation);
        disableEditText(etSelectProfileRegion);
        // disableEditText(etPhone);
    }

    private void setEnableImageview() {
        ivProfilePic.setEnabled(true);
    }

    private void setDisableImageview() {
        ivProfilePic.setEnabled(false);
    }

    private void disableEditText(EditText editText) {
        editText.setEnabled(false);
    }

    //Set toolbar settings
    @Override
    public void onResume() {
        super.onResume();

        setToolbarSettings();

    }

    //Set toolbar settings
    private void setToolbarSettings() {
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).showHideToolbarBackButton(false);
        ((BaseActivity) mContext).setToolbarTitle(mContext.getString(R.string.l_profile));
        ((BaseActivity) mContext).showHideToolbarMenuButton(true);
        ((BaseActivity) mContext).showHideToolbarSearchButton(false);
    }

    private void setFontStyle() {
        setTypeFaceEdittext(etCountry, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceEdittext(etProfileGender, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceEdittext(etSelectProfileRegion, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceEdittext(etEmail, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceEdittext(etProfileGender, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceEdittext(etPhone, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceEdittext(etProfileLocation, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceEdittext(etProfileCity, mContext.getString(R.string.fontOpenSansSemiBold));

        setTypeFaceButton(btn_edit, mContext.getString(R.string.fontOpenSansSemiBold));

        setTypeFaceTextView(tvCountry, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvSelectProfileRegion, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvProfileGender, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvEmail, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvProfileGender, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvLocation, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvPhone, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvCountryCode, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvProfileCity, mContext.getString(R.string.fontOpenSansSemiBold));

        // setTypeFaceAutoCompleteTextView(autotvLocation,mContext.getString(R.string.fontOpenSansSemiBold));

    }

    private void setListeners() {
        etCountry.setOnClickListener(this);
        etSelectProfileRegion.setOnClickListener(this);
        btn_edit.setOnClickListener(this);
        ivProfilePic.setOnClickListener(this);
        etProfileLocation.setOnClickListener(this);

        etCountry.setOnFocusChangeListener(this);
        etProfileCity.setOnFocusChangeListener(this);
        etSelectProfileRegion.setOnFocusChangeListener(this);
        //autotvLocation.setOnFocusChangeListener(this);
        etEmail.setOnFocusChangeListener(this);

    }

    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId()) {
            case R.id.etProfileSelectCountry:
                if (!b)
                    etCountry.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
                else {
                    etCountry.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
                    showCountryDialog();
                }
                break;
            case R.id.etSelectProfileRegion:
                if (!b)
                    etSelectProfileRegion.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
                else {
                    etSelectProfileRegion.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
                    if (etCountry.getText().toString().trim().length() == 0)
                        showDialogAlertPositiveButton(getString(R.string.app_name), getString(R.string.please_select_country));
                    else
                        getCountyList();
                }
                break;
            case R.id.etProfileLocation:
//                if (!b)
//                    autotvLocation.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
//                else
//                    autotvLocation.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
//                break;
            case R.id.etProfileEmail:
                if (!b)
                    etEmail.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
                else
                    etEmail.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
                break;
            case R.id.etProfileCity:
                if (!b)
                    etProfileCity.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
                else
                    etProfileCity.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
                break;
        }
    }

    //Get Country List From Firebase
    private void getCountryList() {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            mRefrenceToCountry.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    countryDetailsList.clear();
                    countryList.clear();
                    progressDialog.dismiss();
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        CountryDetailsVo country = child.getValue(CountryDetailsVo.class);
                        if (country != null && country.getStatus() != null
                                && country.getStatus().equalsIgnoreCase(Constants.STATUS_VALUE)) {
                            if (etCountry.getText().toString().trim().length() > 0) {
                                if (country.getCountryName().equals(etCountry.getText().toString().trim())) {
                                    countryObject = country;
                                    Log.e(TAG, countryObject.getRegionCode());
                                }
                            }

                            countryDetailsList.add(country);
                            countryList.add(child.getKey());

                        }
                    }
                 /*   new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            //setAutoCompleteCountryAddress();
                        }
                    }, 500);
*/
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                }
            });
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    //Open Country Select Dialog
    private void showCountryDialog() {
        if (NetworkUtil.isOnline(mContext)) {
            dataAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, countryList);
            final ListPopupWindow lpw = new ListPopupWindow(mContext);
            lpw.setAdapter(dataAdapter);
            lpw.setAnchorView(etCountry);
            lpw.setModal(true);
            lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    String item = countryList.get(position);
                    etCountry.setText(item);

                    selectedCountryName = item;
                    countryObject = countryDetailsList.get(position);

                    tvCountryCode.setText(countryObject.getCountryCode());

                    etSelectProfileRegion.setText("");
                    etSelectProfileRegion.setHint(getString(R.string.hint_select_region));

                    //setAutoCompleteCountryAddress();

                    lpw.dismiss();
                }
            });
            lpw.show();
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    //get County list from database
    private void getCountyList() {
        if (NetworkUtil.isOnline(mContext)) {

            if (etCountry.getText().toString().trim().length() > 0) {
                mRefrenceToCountry.child(etCountry.getText().toString()).child(Constants.COUNTY_MASTER).
                        addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.getValue() != null) {
                                    countyName.clear();
                                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                                        if (child.hasChild(Constants.STATUS) && child.child(Constants.STATUS).getValue() != null) {
                                            if (child.child(Constants.STATUS).getValue().equals(Constants.STATUS_VALUE)) {
                                                countyName.add(child.getKey());
                                                LogUtils.LOG_I(TAG, child.getKey());
                                            }

                                        }
                                    }
                                    setCounty();
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                LogUtils.LOG_E(TAG, databaseError.getMessage());
                            }
                        });

            }
        } else {
            showDialogAlertPositiveButton(getString(R.string.app_name), getString(R.string.please_check_network));
        }

    }

    private void setCounty() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1,
                countyName);

        final ListPopupWindow lpw = new ListPopupWindow(mContext);
        lpw.setAdapter(dataAdapter);
        lpw.setAnchorView(etSelectProfileRegion);
        lpw.setModal(true);
        lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String item = countyName.get(position);
                etSelectProfileRegion.setText(item);

//                autotvLocation.setText("");
//                autotvLocation.setHint(getString(R.string.hint_location));
                lpw.dismiss();
            }
        });
        lpw.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e(TAG, "onRequestPermissionsResult" + requestCode);
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    Log.e(TAG, "PERMISSION GRANTED");
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        showImageChooseDialog(true);
                    }

                } else {
                    Log.e(TAG, "PERMISSION NOT GRANTED");

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }
            case REQUEST_WRITE_STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "PERMISSION GRANTED");
                    showImageChooseDialog(true);
                } else {
                    Log.e(TAG, "PERMISSION NOT GRANTED");

                }
                return;
            }

        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.etProfileSelectCountry:
                showCountryDialog();
                break;
            case R.id.etSelectProfileRegion:
                if (etCountry.getText().toString().trim().length() == 0)
                    showDialogAlertPositiveButton(getString(R.string.app_name), getString(R.string.please_select_country));
                else
                    getCountyList();

                break;
            case R.id.btn_profile_edit:
                if (btn_edit.getText().equals(mContext.getString(R.string.edit))) {
                    setEnableEditText();
                    setEnableImageview();
                    setEnableAUtocompleteTextView();
                    btn_edit.setText(mContext.getString(R.string.save));
                } else if (btn_edit.getText().equals(mContext.getString(R.string.save))) {
                    if (isValidDetails()) {

                        etCountry.clearFocus();
                        etEmail.clearFocus();
                        etProfileCity.clearFocus();
                        // autotvLocation.clearFocus();
                        etSelectProfileRegion.clearFocus();

                        setDisableAUtocompleteTextView();
                        setDisableEditText();
                        setDisableImageview();
                        SendRequestForUpdateProfile();
                        btn_edit.setText(mContext.getString(R.string.edit));

                    }
                }

                break;
            case R.id.ivViewProfilePic:
                hideKeyboard();

                if (Build.VERSION.SDK_INT >= 23) {
                    if (!isCameraPermissionExist()) {
                        requestCameraPermission();
                    } else if (!isWriteStoragePermissionExist()) {
                        requestWriteStoragePermission();
                    } else {
                        showImageChooseDialog(true);
                    }
                } else {
                    showImageChooseDialog(true);
                }
                break;
            case R.id.etProfileLocation:
                openPlacePicker();
                break;

        }

    }

    private void openPlacePicker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void SendRequestForUpdateProfile() {
        if (!uploadPic.equals(""))
            saveImageToFirebaseStorageWithData();
        else
            saveProfileDataWithoutImage();
    }

    //Save data with image to firebase
    private void saveImageToFirebaseStorageWithData() {
        if (NetworkUtil.isOnline(mContext)) {

            final StorageReference storageReference = mStorage.getReference(Constants.CONSUMER_USER_MASTER)
                    .child(SharedPreferenceUtil.getString(Constants.USER_ID, ""));

            InputStream stream = null;
            try {
                stream = new FileInputStream(new File(uploadPic));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            UploadTask uploadTask = storageReference.putStream(stream);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.

                    storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            downloadUrl = uri;
                            LogUtils.LOG_E("Download Url:", String.valueOf(downloadUrl));
                            if (!city.isEmpty()) {
                                progressDialog.show();
                                HashMap<String, Object> updateData = new HashMap<String, Object>();
                                updateData.put(Constants.EMAIL, etEmail.getText().toString().trim());
                                // updateData.put(Constants.LOCATION, autotvLocation.getText().toString());
                                updateData.put(Constants.LOCATION, etProfileLocation.getText().toString());
                                updateData.put(Constants.COUNTRY, etCountry.getText().toString().trim());
                                updateData.put(Constants.COUNTY, etSelectProfileRegion.getText().toString().trim());
                                updateData.put(Constants.LATITUDE, latitude);
                                updateData.put(Constants.LONGITUDE, longitude);
                                //updateData.put(Constants.PHONENUMBER,tvCountryCode.getText().toString()+etPhone.getText().toString().trim());
                                updateData.put(Constants.PROFILEPHOTOURL, downloadUrl.toString());
                                updateData.put(Constants.CITY, city.trim());

                                mRefrenceToConsumerUser.child(SharedPreferenceUtil.getString(Constants.USER_ID, ""))
                                        .updateChildren(updateData).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        progressDialog.dismiss();
                                        SharedPreferenceUtil.putValue(Constants.EMAIL, etEmail.getText().toString().trim());
                                        SharedPreferenceUtil.putValue(Constants.COUNTRY, etCountry.getText().toString().trim());
                                        SharedPreferenceUtil.putValue(Constants.COUNTY, etSelectProfileRegion.getText().toString().trim());
                                        SharedPreferenceUtil.putValue(Constants.CITY, city.trim());
                                        //SharedPreferenceUtil.putValue(Constants.LOCATION,autotvLocation.getText().toString());
                                        SharedPreferenceUtil.putValue(Constants.LOCATION, etProfileLocation.getText().toString());
                                        SharedPreferenceUtil.putValue(Constants.PROFILEPHOTOURL, downloadUrl.toString());
                                        SharedPreferenceUtil.save();
                                        showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.profile_updated_successfully));

                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        progressDialog.dismiss();

                                    }
                                });
                            } else
                                showToast("Please try again");


                        }
                    });


                    // getLatLongFromAddress(autotvLocation.getText().toString());
                    //  LogUtils.LOG_E("Update Profile CLick","Update Profile CLick");

                }
            });

        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    //Save Profile Data with out Image to firebase
    private void saveProfileDataWithoutImage() {
        if (NetworkUtil.isOnline(mContext)) {

            // getLatLongFromAddress(autotvLocation.getText().toString());
            if (etProfileCity.getText().toString().length() > 0) {
                progressDialog.show();
                city = etProfileCity.getText().toString().trim();
                HashMap<String, Object> updateData = new HashMap<String, Object>();
                updateData.put(Constants.EMAIL, etEmail.getText().toString().trim());
                // updateData.put(Constants.LOCATION, autotvLocation.getText().toString());
                updateData.put(Constants.LOCATION, etProfileLocation.getText().toString().trim());
                updateData.put(Constants.COUNTRY, etCountry.getText().toString().trim());
                updateData.put(Constants.COUNTY, etSelectProfileRegion.getText().toString().trim());
                updateData.put(Constants.LATITUDE, latitude);
                updateData.put(Constants.LONGITUDE, longitude);
                //updateData.put(Constants.PHONENUMBER, tvCountryCode.getText().toString() + etPhone.getText().toString().trim());
                updateData.put(Constants.CITY, city);

                mRefrenceToConsumerUser.child(SharedPreferenceUtil.getString(Constants.USER_ID, ""))
                        .updateChildren(updateData).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        progressDialog.dismiss();
                        SharedPreferenceUtil.putValue(Constants.EMAIL, etEmail.getText().toString().trim());
                        SharedPreferenceUtil.putValue(Constants.COUNTRY, etCountry.getText().toString().trim());
                        SharedPreferenceUtil.putValue(Constants.COUNTY, etSelectProfileRegion.getText().toString().trim());
                        SharedPreferenceUtil.putValue(Constants.CITY, city);
                        SharedPreferenceUtil.putValue(Constants.LOCATION, etProfileLocation.getText().toString());
                        SharedPreferenceUtil.save();
                        showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings),
                                mContext.getString(R.string.profile_updated_successfully));
                    }

                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                    }
                });
            } else {
                showDialogAlertPositiveButton(getString(R.string.app_name), "Please select location with city");
                setProfileData();
            }

        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    //Get Lat Long from selected Address
    private void getLatLongFromAddress(String companyAddress) {

        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
            List addressList = geocoder.getFromLocationName(companyAddress, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = (Address) addressList.get(0);
                Log.e(TAG, address.getLatitude() + "");
                Log.e(TAG, address.getLongitude() + "");
                latitude = address.getLatitude();
                longitude = address.getLongitude();
                city = address.getLocality();

            }
        } catch (IOException e) {
            Log.e(TAG, "Unable to connect to Geocoder", e);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

    }

    @Override
    public void onStop() {
        if (mGoogleApiClient.isConnected() && mGoogleApiClient != null) {

            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    //Get data from firebase and set it in profile
    private void setProfileData() {
        etEmail.setText(SharedPreferenceUtil.getString(Constants.EMAIL, ""));
        etPhone.setText(SharedPreferenceUtil.getString(Constants.PHONENUMBER, ""));
        etProfileGender.setText(SharedPreferenceUtil.getString(Constants.GENDER, ""));
        etCountry.setText(SharedPreferenceUtil.getString(Constants.COUNTRY, ""));
        tvCountryCode.setText(SharedPreferenceUtil.getString(Constants.COUNTRY_CODE, ""));
        //autotvLocation.setText(SharedPreferenceUtil.getString(Constants.LOCATION,""));
        etProfileLocation.setText(SharedPreferenceUtil.getString(Constants.LOCATION, ""));
        city = SharedPreferenceUtil.getString(Constants.CITY, "");
        etProfileCity.setText(city);
        etSelectProfileRegion.setText(SharedPreferenceUtil.getString(Constants.COUNTY, ""));
        if (!SharedPreferenceUtil.getString(Constants.PROFILEPHOTOURL, "").equals("")) {
            Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.myprofile_dp_placeholder);
            int height = drawable.getIntrinsicHeight();
            Picasso.with(mContext).load(SharedPreferenceUtil.getString(Constants.PROFILEPHOTOURL, ""))
                    .placeholder(R.drawable.myprofile_dp_placeholder)
                    .resize(height, height)
                    .transform(new RoundedImageTransform(mContext, 0, R.drawable.myprofile_dp_placeholder))
                    .into(ivProfilePic);
        }
    }

    private void getProfileDataFromFirebase() {
        /*if(NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            mRefrenceToConsumerUser.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    progressDialog.dismiss();
                    SharedPreferenceUtil.putValue(Constants.EMAIL,dataSnapshot.child(Constants.EMAIL).getValue(String.class));
                    SharedPreferenceUtil.putValue(Constants.PHONENUMBER,dataSnapshot.child(Constants.PHONENUMBER).getValue(String.class).substring(3));
                    SharedPreferenceUtil.putValue(Constants.COUNTRY_CODE,dataSnapshot.child(Constants.PHONENUMBER).getValue(String.class).substring(0, 3));
                    SharedPreferenceUtil.putValue(Constants.COUNTRY,dataSnapshot.child(Constants.COUNTRY).getValue(String.class));
                    SharedPreferenceUtil.putValue(Constants.LOCATION,dataSnapshot.child(Constants.LOCATION).getValue(String.class));
                    SharedPreferenceUtil.putValue(Constants.PROFILEPHOTOURL,dataSnapshot.child(Constants.PROFILEPHOTOURL).getValue(String.class));
                    SharedPreferenceUtil.save();
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    progressDialog.dismiss();

                }
            });
        }
        else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings),mContext.getString(R.string.please_check_network));
        }*/
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    //Choose image from gallary
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        Bitmap bitmap;
        switch (requestCode) {
            case PLACE_PICKER_REQUEST:
                Place place = PlacePicker.getPlace(data, mContext);
                if (place != null) {
                    if (place.getAddress() != null) {
                        etProfileLocation.setText(String.valueOf(place.getAddress()));
                        LogUtils.LOG_E("Address", String.valueOf(place.getAddress()));
                    }

                    if (place.getLatLng() != null) {
                        latitude = place.getLatLng().latitude;
                        longitude = place.getLatLng().longitude;
                        LogUtils.LOG_E("LAT", String.valueOf(latitude));
                        LogUtils.LOG_E("LONG", String.valueOf(longitude));

                        GeocoderApi geocoderApi = new GeocoderApi(this);
                        geocoderApi.execute(latitude + "," + longitude);

                        /*Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
                        try {
                            List<Address> addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
                            //String address = addresses.get(0).getAddressLine(0);
                            city = addresses.get(0).getLocality();
                            if (city != null)
                                LogUtils.LOG_E("CITY NAME", city);
                            //String country = addresses.get(0).getAddressLine(2);

                        } catch (IOException e) {

                            e.printStackTrace();
                        }
*/
                    }
                }
                break;
            case REQUEST_CODE_GALLERY:

                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();

                    startCropImage();

                } catch (Exception e) {

                    LogUtils.LOG_E("Galary", "Error while creating temp file");
                }
                break;
            case REQUEST_CODE_TAKE_PICTURE:
                startCropImage();
                break;
            case FOR_CROPED_GALLERY:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {

                    return;
                }

                bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                uploadPic = mFileTemp.getPath();
                ivProfilePic.setImageBitmap(roundedTransform(bitmap, R.drawable.myprofile_dp_placeholder));
                //  LogUtils.LOG_E("Base64 Image",strProfilePicBase64);
                break;
        }
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    //Validations
    private boolean isValidDetails() {
        if (etCountry.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_select_country));
            etCountry.requestFocus();
            return false;
        }
        if (etSelectProfileRegion.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_select_county));
            etSelectProfileRegion.requestFocus();
            return false;
        }
        if (etProfileCity.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_city));
            etProfileCity.requestFocus();
            return false;
        }
//        if(autotvLocation.getText().toString().trim().length() < 1)
//        {
//            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_select_location));
//            autotvLocation.requestFocus();
//            return false;
//        }
        if (etProfileLocation.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_select_location));
            etProfileLocation.requestFocus();
            return false;
        }
        if (etEmail.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_email));
            etEmail.requestFocus();
            return false;
        }
        if (!isValidEmail(etEmail.getText().toString().trim())) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_valid_email));
            etEmail.requestFocus();
            return false;

        }
        if (etPhone.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_phonenumber));
            etPhone.requestFocus();
            return false;
        }
        if (etPhone.getText().toString().trim().length() < 9 || etPhone.getText().toString().trim().length() > 20) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings),
                    mContext.getString(R.string.please_enter_valid_phone));
            etPhone.requestFocus();
            return false;
        }

       /* if(etPhone.getText().toString().trim().length()< Integer.valueOf(countryObject.getMaxPhoneLength() ))
        {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.alert_valid_phone)
                    +Integer.valueOf(countryObject.getMaxPhoneLength())+" digit");
            etPhone.requestFocus();
            return false;
        }*/

        return true;
    }

    @Override
    public void getResult(String response,String countryName) {
        city = response;

        if (city.equalsIgnoreCase("")) {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

            try {
                List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
                //String address = addresses.get(0).getAddressLine(0);
                if (addresses.get(0).getLocality() != null)
                    city = addresses.get(0).getLocality();
                else if (addresses.get(0).getSubLocality() != null)
                    city = addresses.get(0).getSubLocality();

                if (city != null)
                    etProfileCity.setText(city);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            etProfileCity.setText(city);
        }

    }
}
