package com.allthings.consumer;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.CustomDialog;
import com.allthings.consumer.utils.InternalStorageContentProvider;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import eu.janmuller.android.simplecropimage.CropImage;

/**
 * Created by bypt4 on 1/6/17.
 */

public class BaseFragment extends Fragment {

    protected Context mContext;
    private String TAG = "Base Fragment";
    protected FirebaseDatabase mDatabase;
    protected FirebaseStorage mStorage;
    protected FirebaseAuth mAuth;
    protected FirebaseAnalytics mFirebaseAnalytics;
    protected PhoneAuthProvider mPhoneProvider;
    protected static int firebaseAnalyticsId = 0;

    protected FragmentManager fragmentManager;
    protected FragmentTransaction fragmentTransaction;
    protected ProgressDialog progressDialog;
    protected Handler handler;

    protected File mFileTemp;

    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final int REQUEST_CODE_GALLERY = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int FOR_CROPED_GALLERY = 4;
    public static final int REQUEST_CAMERA_PERMISSION = 1;
    public static final int REQUEST_CALL_PERMISSION = 2;
    protected static final int REQUEST_WRITE_STORAGE_PERMISSION = 3;
    protected static final int REQUEST_SMS_PERMISSION = 4;
    protected static final int REQUEST_ACCESS_FINE_LOCATION = 5;

    public String SERVER_KEY;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferenceUtil.putValue(Constants.CURRENT_FRAGMENT, "");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    //Iniatialize components
    private void init() {

        progressDialog = CustomDialog.ctor(mContext);
        fragmentManager = (getActivity()).getSupportFragmentManager();

        handler = new Handler();
        mDatabase = FirebaseDatabase.getInstance();
        mStorage = FirebaseStorage.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mPhoneProvider = PhoneAuthProvider.getInstance();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);

       /* mFirebaseAnalytics.setMinimumSessionDuration(10000);

        mFirebaseAnalytics.setSessionTimeoutDuration(300);*/

        // firebaseSignIn();
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
        } else {
            mFileTemp = new File(mContext.getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }
        // progressDialog = CustomDialog.ctor(mContext);

        if (Constants.IS_DEV_MODE)
            SERVER_KEY = Constants.DEV_SERVER_KEY;
        else
            SERVER_KEY = Constants.CLIENT_SERVER_KEY;

    }

    protected void clearBackStack() {

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(0, 0);
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

    }

    public void checkUserSignInOrNot() {
        mAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth != null) {
                    if (firebaseAuth.getCurrentUser() != null) {
                        Log.e(TAG, "Current User Id :" + firebaseAuth.getCurrentUser().getUid());
                    } else {
                        firebaseSignIn();
                    }
                }
            }
        });

    }


    //Firebase login using mail
    public void firebaseSignIn() {
        mAuth.signInWithEmailAndPassword("undefine@undefine.com", "undefine@2017")
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.e(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.e(TAG, "signInWithEmail:failure", task.getException());

                        }
                    }
                });
    }

    public void logout() {
        // SharedPreferenceUtil.clear();
        mAuth.signOut();
        Log.e(TAG, "logout");
    }


    //take the photo
    protected void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    mImageCaptureUri = Uri.fromFile(mFileTemp);
                } else {
                    //File file = new File(getPhotoFileUri().getPath());
                    mImageCaptureUri = FileProvider.getUriForFile(mContext, mContext.getPackageName() + ".provider", mFileTemp);
                    //intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                }

            } else {
                /*
                 * The solution is taken from here: http://stackoverflow.com/questions/10042695/how-to-get-camera-result-as-a-uri-in-data-folder
	        	 */
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }
            Log.e("mImageCaptureUri", "" + mImageCaptureUri);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {

            Log.d("TAG", "cannot take picture", e);
        }
    }

    //open camera
    protected void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }

    //open gallery
    protected void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    //crop the image
    protected void startCropImage() {
        Intent intent = new Intent(getActivity(), CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 2);
        intent.putExtra(CropImage.ASPECT_Y, 2);

        startActivityForResult(intent, FOR_CROPED_GALLERY);
    }

    public boolean isAccess_Fine_Location_Exist() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            return false;
        } else
            return true;
    }

    public boolean isGPSEnabled() {
        return ((LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE))
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void requestAccessFineLocation() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_ACCESS_FINE_LOCATION);
        }

        //And finally ask for the permission
        requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_ACCESS_FINE_LOCATION);
       /* ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_ACCESS_FINE_LOCATION);*/
    }

    //camera permission
    protected void requestCameraPermission() {
        Log.i("CAMERA PERMISSION", "CAMERA permission has NOT been granted. Requesting permission.");
        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            requestPermissions(new String[]{android.Manifest.permission.CAMERA,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CAMERA_PERMISSION);


        } else {
            // Camera permission has not been granted yet. Request it directly.
            requestPermissions(new String[]{android.Manifest.permission.CAMERA,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CAMERA_PERMISSION);

        }
    }

    protected void requestWriteStoragePermission() {
        Log.i("WRITE STORAGE ", "Write storge permission has NOT been granted. Requesting permission.");
        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            /*ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE_PERMISSION);*/

            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE_PERMISSION);

        } else {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE_PERMISSION);
            // Camera permission has not been granted yet. Request it directly.
           /* ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE_PERMISSION);*/
        }
    }

    //call permission
    protected void requestCallPermission() {
        Log.i("CALL PERMISSION", "CALL permission has NOT been granted. Requesting permission.");
        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CALL_PHONE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE,
                    Manifest.permission.CALL_PHONE}, REQUEST_CALL_PERMISSION);
        } else {
            // Camera permission has not been granted yet. Request it directly.
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE,
                            Manifest.permission.CALL_PHONE},
                    REQUEST_CALL_PERMISSION);
        }
    }

    //SMS permission
    protected void requestSMSPermission() {
        Log.i("CALL PERMISSION", "CALL permission has NOT been granted. Requesting permission.");
        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_SMS)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            requestPermissions(new String[]{Manifest.permission.READ_SMS,
                    Manifest.permission.READ_SMS}, REQUEST_SMS_PERMISSION);
        } else {
            // Camera permission has not been granted yet. Request it directly.
            requestPermissions(new String[]{Manifest.permission.READ_SMS,
                            Manifest.permission.READ_SMS},
                    REQUEST_SMS_PERMISSION);
        }
    }

    //alert for getting permission
    protected void showAlertForGettingPermission() {
        final android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Permissions Required")
                .setMessage("You have forcefully denied some of the required permissions " +
                        "for this action. Please open settings, go to permissions and allow them.")
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getActivity().getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }


    //disable back button
    protected void disableBack(View view) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    // handle back button

                    return true;

                }

                return false;
            }
        });
    }

    //show keyboard
    protected void showKeyboard(EditText editText) {
        editText.requestFocus();
        editText.setFocusableInTouchMode(true);

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED);

    }

    //image choose dialog open
    protected void showImageChooseDialog(final boolean isCropEnable) {
        // TODO Auto-generated method stub

        final Dialog dlgAlert = new Dialog(getActivity());
        dlgAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgAlert.setContentView(R.layout.custom_dialog_layout);
        dlgAlert.setCanceledOnTouchOutside(false);
        TextView txtViewChoosePic, txtViewCamera, txtViewGallery, txtViewCancel;

        txtViewChoosePic = (TextView) dlgAlert.findViewById(R.id.txtViewChoosePic);
        txtViewCamera = (TextView) dlgAlert.findViewById(R.id.txtViewCamera);
        txtViewGallery = (TextView) dlgAlert.findViewById(R.id.txtViewGallery);
        txtViewCancel = (TextView) dlgAlert.findViewById(R.id.txtViewCancel);

        txtViewCamera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View dialog) {
                // TODO Auto-generated method stub
                dlgAlert.dismiss();
                if (isCropEnable) {
                    takePicture();
                } else {
                    cameraIntent();
                }

            }
        });
        txtViewGallery.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View dialog) {
                // TODO Auto-generated method stub
                dlgAlert.dismiss();
                openGallery();

            }
        });
        txtViewCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View dialog) {
                // TODO Auto-generated method stub
                dlgAlert.dismiss();

            }
        });

        WindowManager.LayoutParams wmlp = dlgAlert.getWindow().getAttributes();
        wmlp.width = getDeviceWidth() - 40;
        wmlp.gravity = Gravity.BOTTOM;
        dlgAlert.getWindow().getAttributes().windowAnimations = R.style.dialog_animation_bottom_to_up;
        dlgAlert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dlgAlert.show();
    }

    //convert bitmap to round
    protected Bitmap roundedTransform(final Bitmap source, int drableId) {
        Drawable drawable = ContextCompat.getDrawable(mContext, drableId);

        float scale = Math.min(((float) drawable.getIntrinsicHeight() / source.getWidth()), ((float) drawable.getIntrinsicWidth() / source.getHeight()));

        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);

        Bitmap newBitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);


        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(new BitmapShader(newBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

        Bitmap output = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        canvas.drawCircle((drawable.getIntrinsicWidth()) / 2, (drawable.getIntrinsicHeight()) / 2, getDrawableSize(drableId), paint);

        if (source != output) {
            source.recycle();
        }

        return output;
    }

    //get current system timestamp
    public String getCurrentTimeStamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        LogUtils.LOG_E("Current Time Stamp:", dateFormat.format(new Date()));
        return dateFormat.format(new Date());

    }

    //get drawable size
    public int getDrawableSize(int drableId) {
        Drawable drawable = ContextCompat.getDrawable(mContext, drableId);
        return Math.max(drawable.getIntrinsicHeight() / 2, drawable.getIntrinsicWidth() / 2);
    }

    protected int getDeviceWidth() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }
   /* //check permission exist or not
    protected boolean isCameraPermissionExist() {
        // Check if the Camera permission is already available.
        return ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }*/

    protected boolean isCameraPermissionExist() {
        // Check if the Camera permission is already available.
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Camera permission has not been granted.
            return false;
        } else {
            return true;
            // Camera permissions is already available, show the camera preview.
        }
    }


    protected boolean isWriteStoragePermissionExist() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    protected boolean isReadSmsPermissionExist() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }


    //check permission denied or not
    protected boolean isCameraPermissionDenied() {
        // Check if the Camera permission is already available.
        return ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.CAMERA);
    }

    protected boolean isCallPermissionExist() {
        // Check if the Call permission is already available.
        /*return ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED;*/
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    //toast message
    protected void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }

    //set typeface
    protected void setTypeFaceTextView(TextView txtView, String typeFace) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        txtView.setTypeface(tf);
    }

    protected void setTypeFaceEdittext(EditText editText, String typeFace) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        editText.setTypeface(tf);
    }

    protected void setTypeFaceAutoCompleteTextView(AutoCompleteTextView autoCompleteTextView, String typeFace) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        autoCompleteTextView.setTypeface(tf);
    }

    protected void setTypeFaceButton(Button button, String typeFace) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        button.setTypeface(tf);
    }

    protected void setTypeFaceRadioButton(RadioButton button, String typeFace) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        button.setTypeface(tf);
    }

    protected void setTypeFaceCheckbox(CheckBox checkBox, String typeFace) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        checkBox.setTypeface(tf);
    }

    //get current visible fragment
    protected Fragment getVisibleFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for (Fragment fragment : fragments) {
            if (fragment != null && fragment.isVisible())
                return fragment;
        }
        return null;


    }

    //hide the keyboard
    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getActivity().getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    //show alert dialog
    protected void showDialogAlertPositiveButton(String title, String message) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(message)
                    .setCancelable(false)
                    .setTitle(title)
                    .setPositiveButton(mContext.getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    //check email valid or not
    protected static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                    .matches();
        }
    }

    protected void addFirebaseAnalytics(String eventType, String name) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, String.valueOf(System.currentTimeMillis()));
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "text");
        mFirebaseAnalytics.logEvent(eventType, bundle);
    }

    protected void addFirebaseAnalyticsForClick(String id, String name) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "text");
        mFirebaseAnalytics.logEvent("ClickAction", bundle);
    }


}
