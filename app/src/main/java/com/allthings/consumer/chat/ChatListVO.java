package com.allthings.consumer.chat;

import java.util.List;

/**
 * Created by Vishal Dhaduk on 7/12/2017.
 */

public class ChatListVO {

    String  offerID,senderID,receiverID,offerImage,offerTitle,receiverEmail,text,senderEmail;
    List<ChatDetailsVO> messageList;

    public ChatListVO() {
    }

    public ChatListVO(String offerID,String senderID ,String receiverID, String offerImage,String offerTitle,
                      String receiverEmail,String senderEmail,List<ChatDetailsVO> messageList) {
        this.offerID = offerID;
        this.senderID = senderID;
        this.receiverID = receiverID;
        this.offerImage = offerImage;
        this.offerTitle = offerTitle;
        this.receiverEmail = receiverEmail;
        this.senderEmail = senderEmail;
        this.messageList = messageList;

    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getOfferID() {
        return offerID;
    }

    public List<ChatDetailsVO> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<ChatDetailsVO> messageList) {
        this.messageList = messageList;
    }

    public void setOfferID(String offerID) {
        this.offerID = offerID;
    }

    public String getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getOfferImage() {
        return offerImage;
    }

    public void setOfferImage(String offerImage) {
        this.offerImage = offerImage;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

   /* private String offerTitle="",offerImageURL="",receiverId="",offerId="",text="",receiverEmailId;

    public String getReceiverEmailId() {
        return receiverEmailId;
    }

    public void setReceiverEmailId(String receiverEmailId) {
        this.receiverEmailId = receiverEmailId;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public String getOfferImageURL() {
        return offerImageURL;
    }

    public void setOfferImageURL(String offerImageURL) {
        this.offerImageURL = offerImageURL;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }*/


}
