package com.allthings.consumer.chat;

/**
 * Created by Payal jogu on 3/8/17.
 */

public class ChatListModel {
    String offerId,receiverID;

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }
}
