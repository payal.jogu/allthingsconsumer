package com.allthings.consumer.chat;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.allthings.consumer.R;
import com.allthings.consumer.utils.RoundedImageTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by ABC on 6/9/2017.
 */

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<ChatListVO> chatList;
    private View rootView;
    private FragmentManager fragmentManager;
    String TAG="ChatListAdapter";

    public ChatListAdapter(Context mContext, ArrayList<ChatListVO> chatListVO) {
        this.mContext = mContext;
        this.chatList= chatListVO;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_chat_list, parent, false);
        return new ChatListAdapter.ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final ChatListVO chatListVO = chatList.get(position);
        holder.tvChatListUserEmail.setText(chatListVO.getOfferTitle());

        String email = chatList.get(position).getReceiverEmail();
        email = email.substring(0, email.lastIndexOf("@"));
        holder.tvChatListUserMessage.setText(email.replace("@",""));

        //holder.tvChatListUserMessage.setText(chatListVO.getReceiverEmail());
       // Log.e(TAG,chatListVO.getText());
        //Log.e(TAG,chatListVO.getUserEmail());
        Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.company_placeholder);
        if(!chatListVO.getOfferImage().equals("")) {
            Picasso.with(mContext).load(chatList.get(position).getOfferImage())
                    .resize(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight())
                    .transform(new RoundedImageTransform(mContext, 0, R.drawable.company_placeholder))
                    .placeholder(R.drawable.company_placeholder)
                    .into(holder.ivChatListUserIcon);
        }

        holder.view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle=new Bundle();
                ChatDetailsFragment chatDetailsFragment=new ChatDetailsFragment();
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                fragmentManager=activity.getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                bundle.putString("receiverID",chatList.get(position).getSenderID());
                bundle.putString("senderID",chatList.get(position).getReceiverID());
                bundle.putString("offerID",chatList.get(position).getOfferID());
                bundle.putString("receiverEmail",chatList.get(position).getSenderEmail());
                bundle.putString("senderEmail",chatList.get(position).getReceiverEmail());
                bundle.putString("offerTitle",chatList.get(position).getOfferTitle());
                bundle.putString("offerImage",chatList.get(position).getOfferImage());

                chatDetailsFragment.setArguments(bundle);
                InputMethodManager inputManager = (InputMethodManager)mContext .getSystemService(INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(new View(mContext).getWindowToken(), 0);
                fragmentTransaction.replace(R.id.frameLayoutBaseContainer,chatDetailsFragment).addToBackStack(null).commitAllowingStateLoss();

            }
        });
    }
    @Override
    public int getItemCount() {
        return chatList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvChatListUserEmail,tvChatListUserMessage;
        ImageView ivChatListUserIcon;
        View view1;
        ViewHolder(View view) {
            super(view);
            view1=view;
            tvChatListUserEmail = (TextView) view.findViewById(R.id.tvChatListEmail);
            tvChatListUserMessage = (TextView) view.findViewById(R.id.tvChatDetailsEmail);
            ivChatListUserIcon = (ImageView) view.findViewById(R.id.ivChatListProfile);
            Typeface tf = Typeface.createFromAsset(view.getContext().getAssets(),
                    "OpenSans-Semibold.ttf");
            tvChatListUserEmail.setTypeface(tf);
            tvChatListUserMessage.setTypeface(tf);
        }
    }
}