package com.allthings.consumer.chat;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.allthings.consumer.R;
import com.allthings.consumer.utils.CornersRoundedTransformation;
import com.allthings.consumer.utils.RoundedImageTransform;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Vishal Dhaduk on 7/10/2017.
 */

public class ChatDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ChatDetailsVO> mDataSet;
    private String mId;
    private Context context;
    private View rootView;
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa",Locale.US);

    String TAG="ChatDetailsAdapter";

    private static final int CHAT_RIGHT = 1;
    private static final int CHAT_LEFT = 2;

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvMessage,tvMessageTime,tvMessageDate,tvImgMessageTime;
        ImageView ivChatImage;
        RelativeLayout relativeImageMessage;
        LinearLayout linearTextMessage;
        ProgressBar pgChatLeft;

        public ViewHolder(View itemView) {
            super(itemView);
            tvMessage = (TextView) itemView.findViewById(R.id.tvMessage);
            tvMessageTime = (TextView) itemView.findViewById(R.id.tvMessageTime);
            pgChatLeft = (ProgressBar) itemView.findViewById(R.id.pgChatLeft);
            ivChatImage = (ImageView) itemView.findViewById(R.id.ivChatImage);

            if(Build.VERSION.SDK_INT >= 21){
                ivChatImage.setClipToOutline(true);
            }
            tvMessage.setTypeface(Typeface.createFromAsset(context.getAssets(),"OpenSans-Regular.ttf"));

            linearTextMessage = (LinearLayout) itemView.findViewById(R.id.textMessage);
            relativeImageMessage = (RelativeLayout) itemView.findViewById(R.id.imgMessage);

            tvImgMessageTime = (TextView) itemView.findViewById(R.id.tvImgMessageTime);
            tvMessageDate = (TextView) itemView.findViewById(R.id.tvMessageDate);
        }

    }
    public ChatDetailsAdapter(Context context, List<ChatDetailsVO> dataSet, String id) {
        this.context = context;
        mDataSet = dataSet;
        mId = id;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (viewType == CHAT_RIGHT) {
            rootView = LayoutInflater.from(context)
                    .inflate(R.layout.list_item_chat_right, parent, false);
        } else {
            rootView = LayoutInflater.from(context)
                    .inflate(R.layout.list_item_chat_left, parent, false);
        }
        return new ViewHolder(rootView);
    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        if(mDataSet.size() != 0){

            ChatDetailsVO chat = mDataSet.get(position);
           /* final String dateFormatString = "MMM dd, yyyy";
            final String timeFormatString = "h:mm aa";*/
            SimpleDateFormat format = new SimpleDateFormat("dd:MM:yyyy hh:mm:ss");

            Timestamp chatdate = new Timestamp(Long.valueOf(String.valueOf(chat.getTimestamp())));

            /*Log.e("Chat TimeStamp :",chat.getTimestamp().toString());
            Log.e("Chat Time :",format.format(new Date(chatdate.getTime())));*/

            Calendar smsTime = Calendar.getInstance();
            smsTime.setTimeInMillis(chatdate.getTime());


            long previousTs = 0;
            if(position>=1){
                ChatDetailsVO pm = mDataSet.get(position-1);
                previousTs = (Long.valueOf(String.valueOf(pm.getTimestamp())));
            }

            setDateObjectVisibility(Long.valueOf(String.valueOf(chat.getTimestamp())),previousTs,viewHolder.tvMessageDate);

            if(!chat.getImageUrl().equals("")){
                viewHolder.relativeImageMessage.setVisibility(View.VISIBLE);
                viewHolder.linearTextMessage.setVisibility(View.GONE);
                viewHolder.tvImgMessageTime.setText(timeFormat.format(chatdate));
                if(!chat.getImageUrl().isEmpty()){
                    viewHolder.pgChatLeft.setVisibility(View.VISIBLE);
                    if(Build.VERSION.SDK_INT < 21){
                        Picasso.with(context)
                                .load(chat.getImageUrl())
                                //.placeholder(R.drawable.img_placeholder)
                                .transform(new CornersRoundedTransformation(20,0))
                                .into(viewHolder.ivChatImage, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        viewHolder.pgChatLeft.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {
                                        viewHolder.pgChatLeft.setVisibility(View.GONE);
                                    }
                                });
                    }else {
                        Picasso.with(context)
                                .load(chat.getImageUrl())
                                //.placeholder(R.drawable.img_placeholder)
                                .into(viewHolder.ivChatImage, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        viewHolder.pgChatLeft.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {
                                        viewHolder.pgChatLeft.setVisibility(View.GONE);
                                    }
                                });
                    }

                viewHolder.ivChatImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Bundle imageData = new Bundle();
                        imageData.putString("imageUrl",mDataSet.get(position).getImageUrl());

                        AppCompatActivity activity = (AppCompatActivity) context;
                        FragmentManager fm=activity.getSupportFragmentManager();

                        ImageZoomDialogFragment imageZoomDialogFragment = new ImageZoomDialogFragment();
                        imageZoomDialogFragment.setArguments(imageData);
                        imageZoomDialogFragment.show(fm, "Sample Fragment");

                            /*FragmentTransaction fragmentTransaction = fm.beginTransaction();
                           *//* fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
                                    R.anim.enter_from_left, R.anim.exit_to_right);*//*
                            fragmentTransaction.replace(R.id.frameContainer,imageZoomDialogFragment)
                                    .addToBackStack("OfferChat").commit();*/


                    }

                });


                }
            }else if(!chat.getText().equals("")){
                viewHolder.linearTextMessage.setVisibility(View.VISIBLE);
                viewHolder.relativeImageMessage.setVisibility(View.GONE);
                viewHolder.tvMessage.setText(chat.getText());
                viewHolder.tvMessageTime.setText(timeFormat.format(chatdate));
            }

        }
    }

    private void setDateObjectVisibility(long currentTS,long previousTS,TextView tvMessageDate) {

        final String dateFormatString = "MMM dd, yyyy";

        Calendar now = Calendar.getInstance();

        Timestamp previousDate = new Timestamp(previousTS);
        Timestamp chatdate = new Timestamp(currentTS);

        Calendar previousSmsTime = Calendar.getInstance();
        previousSmsTime.setTimeInMillis(previousDate.getTime());

        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(chatdate.getTime());

        if(previousSmsTime.getTimeInMillis() == 0){
            if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
                tvMessageDate.setVisibility(View.VISIBLE);
                tvMessageDate.setText("Today");
            } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
                tvMessageDate.setVisibility(View.VISIBLE);
                tvMessageDate.setText("Yesterday");
            } else {
                tvMessageDate.setVisibility(View.VISIBLE);
               // tvMessageDate.setText(DateFormat.format(dateFormatString, smsTime).toString());
                tvMessageDate.setText(dateFormat.format(chatdate));
            }
        }else if (previousSmsTime.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            tvMessageDate.setVisibility(View.GONE);
            tvMessageDate.setText("");
        }else if(previousSmsTime.get(Calendar.DATE) != smsTime.get(Calendar.DATE)){
            if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
                tvMessageDate.setVisibility(View.VISIBLE);
                tvMessageDate.setText("Today");
            } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
                tvMessageDate.setVisibility(View.VISIBLE);
                tvMessageDate.setText("Yesterday");
            } else {
                tvMessageDate.setVisibility(View.VISIBLE);
               // tvMessageDate.setText(DateFormat.format(dateFormatString, smsTime).toString());
                tvMessageDate.setText(dateFormat.format(chatdate));
            }
        }
    }

    @Override
    public int getItemViewType(int position) {

            if(mDataSet.size()!=0){
                if(mDataSet.get(position) != null){
                    if (mDataSet.get(position).getFromId().equals(mId))
                        return CHAT_RIGHT;
                }
           }

        return CHAT_LEFT;
    }
    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}
