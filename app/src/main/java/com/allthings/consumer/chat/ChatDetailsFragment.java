package com.allthings.consumer.chat;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.offers.ScrollingLinearLayoutManager;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import eu.janmuller.android.simplecropimage.CropImage;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by ABC on 6/9/2017.
 */
public class ChatDetailsFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;
    private RecyclerView rvChat;
    private EditText etText;
    private List<ChatDetailsVO> mChats;
    protected ChatDetailsVO chat;
    private ChatDetailsAdapter mAdapter;
    private String offerId, receiverId, senderID, receiverEmail, senderEmail, offerTitle, offerImage, text, textKey;
    private DatabaseReference dbRefToChatTB, dbRefToUser, dbRefToTypingIndicator;
    protected Map<String, Object> msgKey;
    private String TAG = "ChatDetailsFragment";
    private ImageView ivChatAudio, ivChatCamera;
    private boolean typingStarted = false;
    private DatabaseReference tbUserBlock;
    private String blockStatus = "Unblocked", myBlockStatus = "Unblocked";
    private LinearLayout layoutTyping;
    private Timer typingTimer;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_chat_details, container, false);
            init();
            //hideKeyboard();
            setListeners();
            setFontStyle();
            //disableBack(rootView);
        }
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    //Initialize the components
    private void init() {
        etText = rootView.findViewById(R.id.etText);
        etText.requestFocus();
        rvChat = rootView.findViewById(R.id.rvChat);
        ivChatAudio = rootView.findViewById(R.id.ivChatAudio);
        ivChatAudio.setVisibility(View.INVISIBLE);
        ivChatCamera = rootView.findViewById(R.id.ivChatCamera);
        layoutTyping = rootView.findViewById(R.id.layoutTyping);

        mChats = new ArrayList<>();
        dbRefToChatTB = mDatabase.getReference(Constants.CHAT_ROOM_TB).child("messages");
        dbRefToUser = mDatabase.getReference(Constants.USER_MESSAGES_TB);
        dbRefToTypingIndicator = mDatabase.getReference(Constants.CHAT_ROOM_TB).child(Constants.TB_TYPING_INDICATOR);

        rvChat.setLayoutManager(new ScrollingLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false, 1000));

        getArgument();
        getChatDataFromFirebase();
        getTypingIndicator();

        getActivity().findViewById(R.id.imgViewChatBlock).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.imgViewChatBlock).setOnClickListener(this);
        tbUserBlock = mDatabase.getReference(Constants.USER_BLOCK);
        getStatusForBlockUnblockUser();
        getReceiverBlockUnblockStatus();
    }

    private void getTypingIndicator() {
        if (!receiverId.isEmpty()) {
            mDatabase.getReference(Constants.CHAT_ROOM_TB).child(Constants.TB_TYPING_INDICATOR).
                    addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.hasChild(receiverId)) {
                                if (dataSnapshot.child(receiverId).getValue(Boolean.class).equals(true))
                                    layoutTyping.setVisibility(View.VISIBLE);
                                else
                                    layoutTyping.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            layoutTyping.setVisibility(View.GONE);
                        }
                    });
        }

    }

    //get chat list data from firebase
    private void getChatDataFromFirebase() {
        if (NetworkUtil.isOnline(mContext)) {
            //progressDialog.show();
            if (senderID != null && offerId != null && receiverId != null) {
                dbRefToUser.child(senderID).child(offerId).child(receiverId).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull final DataSnapshot dataSnapshot1, String s) {
                        if (dataSnapshot1.getValue() != null && dataSnapshot1.getKey() != null) {
                            System.out.println(dataSnapshot1.getKey());
                            dbRefToChatTB.child(dataSnapshot1.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    ChatDetailsVO c = dataSnapshot.getValue(ChatDetailsVO.class);
                                    c.setChatId(dataSnapshot1.getKey());

                                    boolean found = false;

                                    for (int i = 0; i < mChats.size(); i++) {
                                        if (c.getChatId().equalsIgnoreCase(mChats.get(i).getChatId())) {
                                            found = true;
                                            mChats.set(i, c);
                                            mAdapter.notifyDataSetChanged();
                                            rvChat.getLayoutManager().scrollToPosition(mAdapter.getItemCount() - 1);
                                            break;
                                        }
                                    }


                                    if (!found && !c.getIsSenderBlock().equals("")) {
                                        mChats.add(c);
                                        mAdapter.notifyDataSetChanged();
                                        rvChat.getLayoutManager().scrollToPosition(mAdapter.getItemCount() - 1);
                                    } else if (!found && c.getIsSenderBlock().equals("") && !c.getImageUrl().equalsIgnoreCase("")) {
                                        mChats.add(c);
                                        mAdapter.notifyDataSetChanged();
                                        rvChat.getLayoutManager().scrollToPosition(mAdapter.getItemCount() - 1);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    //progressDialog.dismiss();
                                    Log.e(TAG, "onCancelled" + databaseError.getMessage());

                                }
                            });

                            dbRefToChatTB.child(dataSnapshot1.getKey()).addChildEventListener(new ChildEventListener() {
                                @Override
                                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                }

                                @Override
                                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                    dbRefToChatTB.child(dataSnapshot1.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            ChatDetailsVO chatVO = dataSnapshot.getValue(ChatDetailsVO.class);
                                            chatVO.setChatId(dataSnapshot1.getKey());

                                            for (int i = 0; i < mChats.size(); i++) {
                                                if (chatVO.getChatId().equalsIgnoreCase(mChats.get(i).getChatId())) {
                                                    mChats.set(i, chatVO);
                                                    mAdapter.notifyDataSetChanged();
                                                    rvChat.getLayoutManager().scrollToPosition(mAdapter.getItemCount() - 1);
                                                    break;
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }

                                @Override
                                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                                }

                                @Override
                                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {
                        Log.e(TAG, "onChildChanged");
                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                        Log.e(TAG, "onChildRemoved");
                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) {
                        Log.e(TAG, "onChildMoved");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            }
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }


    //set the listeners
    private void setListeners() {
        ivChatAudio.setOnClickListener(this);
        ivChatCamera.setOnClickListener(this);


        etText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (etText.getText().toString().trim().length() > 0)
                    ivChatAudio.setVisibility(View.VISIBLE);
                else
                    ivChatAudio.setVisibility(View.INVISIBLE);

                if (NetworkUtil.isOnline(mContext)) {
                    if (!myBlockStatus.equals("Blocked")) {
                        // ivChatSent.setEnabled(true);

                        Log.e(TAG, "typing started");
                        typingStarted = true;
                        Map<String, Object> indicator = new HashMap<>();
                        indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
                        if (typingTimer != null) {
                            typingTimer.cancel();
                        }
                        dbRefToTypingIndicator.updateChildren(indicator);

                        //send typing started status

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
               /* if (!myBlockStatus.equals("Blocked")) {
                    if (!TextUtils.isEmpty(s.toString()) && s.toString().trim().length() == 1) {

                        Log.e(TAG, "typing started");
                        typingStarted = true;
                        Map<String, Object> indicator = new HashMap<>();
                        indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
                        dbRefToTypingIndicator.updateChildren(indicator);

                        //send typing started status

                    } else if (s.toString().trim().length() == 0 && typingStarted) {

                        Log.i(TAG, "typing stopped event");

                        typingStarted = false;
                        Map<String, Object> indicator = new HashMap<>();
                        indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
                        dbRefToTypingIndicator.updateChildren(indicator);

                        //send typing stopped status

                    }
                }*/
                if (NetworkUtil.isOnline(mContext)) {
                    Log.i(TAG, "typing stopped event");
                    typingTimer = new Timer();
                    typingTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            typingStarted = false;
                            Map<String, Object> indicator = new HashMap<>();
                            indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
                            dbRefToTypingIndicator.updateChildren(indicator);
                        }
                    }, 600);
                }
            }
        });
    }

    //set the fonts styles
    private void setFontStyle() {
    }

    private void getStatusForBlockUnblockUser() {
        //LogUtils.LOG_E("Current receiverId", receiverId);
        //LogUtils.LOG_E("Current sender Id", SharedPreferenceUtil.getString(Constants.USER_ID, ""));
        tbUserBlock.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).addChildEventListener(
                new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String s) {
                        if (dataSnapshot.getKey() != null) {
                            LogUtils.LOG_E("Block Snap", String.valueOf(dataSnapshot.getValue()));
                            LogUtils.LOG_E("Block Key", dataSnapshot.getKey());
                            if (dataSnapshot.getKey().equals(receiverId) && dataSnapshot.getValue().equals("Blocked"))
                                blockStatus = String.valueOf(dataSnapshot.getValue());
                            else
                                blockStatus = "Unblocked";
                        }
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {
                        if (dataSnapshot.getKey() != null) {
                            LogUtils.LOG_E("Block Snap", String.valueOf(dataSnapshot.getValue()));
                            LogUtils.LOG_E("Block Key", dataSnapshot.getKey());
                            if (dataSnapshot.getKey().equals(receiverId) && dataSnapshot.getValue().equals("Blocked"))
                                blockStatus = String.valueOf(dataSnapshot.getValue());
                            else
                                blockStatus = "Unblocked";
                        }
                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    //Set toolbar settings
    @Override
    public void onResume() {
        super.onResume();
        hideKeyboard();
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).showHideToolbarBackButton(true);
        ((BaseActivity) mContext).showHideToolbarMenuButton(false);
        ((BaseActivity) mContext).showHideToolbarSearchButton(false);
        ((BaseActivity) mContext).showHideToolbarSerachEdittext(false);
        ((BaseActivity) mContext).showHideToolbarSearchCancelButton(false);
        ((BaseActivity) mContext).showHideToolbarTitle(false);
        ((BaseActivity) mContext).showHideToolbarChatEmail(true);
        ((BaseActivity) mContext).showHideToolbarChatImage(true);
        ((BaseActivity) mContext).showHideToolbarChatOfferTitle(true);
        ((BaseActivity) mContext).showHideToolbarRemainder(false);

        if (receiverEmail != null && !receiverEmail.isEmpty()) {
            String email = receiverEmail.substring(0, receiverEmail.lastIndexOf("@"));
            ((BaseActivity) mContext).setToolbarChatEmailTitle(email.replace("@",""));
        }
        if (offerTitle != null && !offerTitle.isEmpty()) {
            ((BaseActivity) mContext).setToolbarChatOfferTitle(offerTitle);
        }
        if (offerImage != null && !offerImage.equals("")) {
            ((BaseActivity) mContext).setToolbarChatOfferImage(offerImage);
        }

        if (etText.getText().toString().trim().length() > 0) {
            typingStarted = true;
            Map<String, Object> indicator = new HashMap<>();
            indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
            dbRefToTypingIndicator.updateChildren(indicator);
        } else {
            typingStarted = false;
            Map<String, Object> indicator = new HashMap<>();
            indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
            dbRefToTypingIndicator.updateChildren(indicator);
        }

    }


    @Override
    public void onPause() {
        super.onPause();
        typingStarted = false;
        Map<String, Object> indicator = new HashMap<>();
        indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
        dbRefToTypingIndicator.updateChildren(indicator);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setAdapter();
    }

    private void setAdapter() {
        rvChat.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new ChatDetailsAdapter(getContext(), mChats, senderID);
        rvChat.setAdapter(mAdapter);

      /*  mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            public void onItemRangeInserted(int positionStart, int itemCount) {
                rvChat.smoothScrollToPosition(mAdapter.getItemCount());
            }
        });*/
    }

    //get the email of receiver
  /*  private void getReceiverEmail() {
        if(NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
         *//*   DatabaseReference databaseReference = mDatabase.getReference(Constants.BUSINESS_USER_TB).child(toId);
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot != null) {
                        //toName = dataSnapshot.child(Constants.EMAIL).getValue(String.class);
                       // ((BaseActivity) mContext).setToolbarTitle(toName);
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    progressDialog.dismiss();

                }
            });*//*
        }
        else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }*/
    //get argument data
    private void getArgument() {
        Bundle getData = getArguments();
        if (getData != null) {
            offerId = getData.getString("offerID");
            receiverId = getData.getString("senderID");
            senderID = getData.getString("receiverID");
            receiverEmail = getData.getString("senderEmail");
            senderEmail = getData.getString("receiverEmail");
            offerTitle = getData.getString("offerTitle");
            if (getData.containsKey("offerImageURL"))
                offerImage = getData.getString("offerImageURL");
            else
                offerImage = getData.getString("offerImage");

            SharedPreferenceUtil.putValue("IsChatFragment", true);
            SharedPreferenceUtil.putValue("ChatSenderID", senderID);
            SharedPreferenceUtil.putValue("ChatReceiverID", receiverId);
            SharedPreferenceUtil.putValue("ChatOfferID", offerId);
            SharedPreferenceUtil.save();

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferenceUtil.putValue("IsChatFragment", false);
        SharedPreferenceUtil.putValue("ChatSenderID", "");
        SharedPreferenceUtil.putValue("ChatReceiverID", "");
        SharedPreferenceUtil.putValue("ChatOfferID", "");
        SharedPreferenceUtil.save();
        getActivity().findViewById(R.id.imgViewChatBlock).setVisibility(View.GONE);
        typingStarted = false;
        Map<String, Object> indicator = new HashMap<>();
        indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
        dbRefToTypingIndicator.updateChildren(indicator);
    }

    /*public void setTypingIndicator()
        {
            etText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    boolean isTyping=false;
                    if(etText.getText().toString().length()>0)
                    {
                        isTyping=true;
                    }
                    else
                    {
                        isTyping=false;
                    }
                    mDatabase.getReference(Constants.CHAT_ROOM_TB).child(Constants.TB_TYPING_INDICATOR).
                            child(SharedPreferenceUtil.getString(Constants.USER_ID,"")).setValue(isTyping).
                            addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                        }
                    });
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

        }*/
    //send the message to firebase server
    public void sendMessage() {
        if (etText.getText().toString().trim().length() >= 1) {
            text = etText.getText().toString();
            if (isValid()) {
                chat = new ChatDetailsVO(etText.getText().toString(), senderID, receiverId, receiverEmail);
                /*if (myBlockStatus.equals("Blocked")) {
                    chat.setIsReceiverBlock(receiverId);
                    chat.setIsSenderBlock("");
                } else {
                    chat.setIsReceiverBlock("");
                    chat.setIsSenderBlock("");
                }*/

                if (myBlockStatus.equals("Blocked")) {
                    chat.setIsReceiverBlock("");
                    chat.setIsSenderBlock(receiverId);
                } else {
                    chat.setIsReceiverBlock(senderID);
                    chat.setIsSenderBlock(receiverId);
                }

                textKey = dbRefToChatTB.push().getKey();
                dbRefToChatTB.child(textKey).setValue(chat);
                msgKey = new HashMap<>();
                msgKey.put(textKey, "1");
                dbRefToUser.child(senderID).child(offerId).child(receiverId).updateChildren(msgKey);
                dbRefToUser.child(receiverId).child(offerId).child(senderID).updateChildren(msgKey);

                if (!myBlockStatus.equals("Blocked"))
                    sendNotification(etText.getText().toString());

            } else {
                showToast("Message not Sent");
            }
            etText.setText("");

        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), "Please Enter Message..");
        }

    }

    //check validations
    public Boolean isValid() {
        if (TextUtils.isEmpty(receiverEmail)) {
            // showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), "receiverEmail Not Found");
            return false;
        }
        if (TextUtils.isEmpty(senderID)) {
            //showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), "senderId Not Found");
            return false;
        }
        if (TextUtils.isEmpty(receiverId)) {
            //showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), "receiverId Not Found");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.ivChatAudio) {
            if (NetworkUtil.isOnline(mContext)) {
                if (blockStatus.equalsIgnoreCase("Unblocked")) {
                    sendMessage();
                } else {
                    showBlockUnblockDialog();
                }

            } else {
                showDialogAlertPositiveButton(getString(R.string.app_name), getString(R.string.please_check_network));
            }
        }
        if (view.getId() == R.id.ivChatCamera) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (!isCameraPermissionExist()) {
                    requestCameraPermission();
                } else if (!isWriteStoragePermissionExist()) {
                    requestWriteStoragePermission();
                } else {
                    if (blockStatus.equalsIgnoreCase("Unblocked")) {
                        showImageChooseDialog(true);
                    } else {
                        showBlockUnblockDialog();
                    }
                }
            } else {
                if (blockStatus.equalsIgnoreCase("Unblocked")) {
                    showImageChooseDialog(true);
                } else {
                    showBlockUnblockDialog();
                }
            }
        }
        if (view.getId() == R.id.imgViewChatBlock) {
            showBlockUnblockDialog();
        }
    }

    protected void showBlockUnblockDialog() {
        // LogUtils.LOG_E("Current Block Status",blockStatus);
        // LogUtils.LOG_E("Current Reciever Id",receiverId);
        String blockStatusForDialog;
        if (blockStatus.equals("Blocked")) {
            blockStatusForDialog = "Unblock";
        } else {
            blockStatusForDialog = "Block";
        }

        final Dialog dlgAlert = new Dialog(mContext);
        dlgAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgAlert.setContentView(R.layout.layout_chat_block_dialog);
        dlgAlert.setCanceledOnTouchOutside(false);
        TextView txtViewChooseOpt, txtViewBlock, txtViewCancel;

        txtViewChooseOpt = (TextView) dlgAlert.findViewById(R.id.txtViewChooseOpt);
        txtViewBlock = (TextView) dlgAlert.findViewById(R.id.txtViewBlock);
        txtViewBlock.setText(blockStatusForDialog);
        txtViewCancel = (TextView) dlgAlert.findViewById(R.id.txtViewCancel);

        txtViewBlock.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View dialog) {

                dlgAlert.dismiss();
                showConfirmationDialog();


            }
        });
        txtViewCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View dialog) {

                dlgAlert.dismiss();

            }
        });

        WindowManager.LayoutParams wmlp = dlgAlert.getWindow().getAttributes();
        wmlp.width = getDeviceWidth() - 40;
        wmlp.gravity = Gravity.BOTTOM;
        dlgAlert.getWindow().getAttributes().windowAnimations = R.style.dialog_animation_bottom_to_up;
        dlgAlert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dlgAlert.show();
    }

    protected void showConfirmationDialog() {
        String blockStatusForDialog;
        if (blockStatus.equals("Blocked")) {
            blockStatusForDialog = "Unblocked";
        } else {
            blockStatusForDialog = "Blocked";
        }

        final android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(mContext.getString(R.string.app_name))
                .setMessage("Are you sure you want to " + blockStatusForDialog + " this user?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendRequestForBlockUnblockUser();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    private void sendRequestForBlockUnblockUser() {
        if (blockStatus.equals("Blocked")) {
            blockStatus = "Unblocked";
        } else {
            blockStatus = "Blocked";
        }

//        LogUtils.LOG_E("Sender Id", senderID);
//        LogUtils.LOG_E("Reciever Id", receiverId);
//        LogUtils.LOG_E("Current Id", SharedPreferenceUtil.getString(Constants.USER_ID, ""));
        HashMap<String, Object> blockObject = new HashMap<>();
        blockObject.put(receiverId, blockStatus);
        tbUserBlock.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).updateChildren(blockObject);

    }

    //upload image to firebase
    private void uploadImage(String imagePath) {

        StorageReference refToChatImg = mStorage.getReference(Constants.CHAT_STORAGE)
                .child(SharedPreferenceUtil.getString(Constants.USER_ID, ""));
        final StorageReference refToImage = refToChatImg.child(String.valueOf(System.currentTimeMillis() / 1000))
                .child("image.jpg");

        InputStream stream = null;
        try {
            stream = new FileInputStream(new File(imagePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        UploadTask uploadTask = refToImage.putStream(stream);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                progressDialog.dismiss();
                Log.e(TAG, exception.getMessage());

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                progressDialog.dismiss();
                refToImage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        //final Uri downloadUrl = uri;
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                        Log.e(TAG, String.valueOf(uri));

            /*    HashMap<String,Object> stringObjectHashMap = new HashMap<>();
                stringObjectHashMap.put(Constants.OFFER_IMAGE_URL,downloadUrl.toString());*/
                        // chat = new ChatDetailsVO(downloadUrl.toString(),senderID, receiverId, receiverEmail);
                        chat = new ChatDetailsVO();
                        chat.setFromId(senderID);
                        chat.setToId(receiverId);

                       /* if (myBlockStatus.equals("Blocked")) {
                            chat.setIsReceiverBlock(receiverId);
                            chat.setIsSenderBlock("");
                        } else {
                            chat.setIsReceiverBlock("");
                            chat.setIsSenderBlock("");
                        }*/

                        if (myBlockStatus.equals("Blocked")) {
                            chat.setIsReceiverBlock("");
                            chat.setIsSenderBlock(receiverId);
                        } else {
                            chat.setIsReceiverBlock(senderID);
                            chat.setIsSenderBlock(receiverId);
                        }

                        chat.setImageUrl(uri.toString());
                        chat.setImageHeight(600);
                        chat.setImageWidth(600);
                        chat.setTimestamp(ServerValue.TIMESTAMP);

                        textKey = dbRefToChatTB.push().getKey();
                        dbRefToChatTB.child(textKey).setValue(chat);
                        msgKey = new HashMap<>();
                        msgKey.put(textKey, "1");
                        dbRefToUser.child(senderID).child(offerId).child(receiverId).updateChildren(msgKey);
                        dbRefToUser.child(receiverId).child(offerId).child(senderID).updateChildren(msgKey);

                        if (!myBlockStatus.equals("Blocked"))
                            sendNotification("image");

                    }
                });

            }
        });

       /* refToImage.putFile(imagePath)
                .addOnSuccessListener(getActivity(), new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // When the image has successfully uploaded, we get its download URL
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        // Send message with Image URL
                        ChatMessage chat = new ChatMessage(downloadUrl.toString(), username);
                        databaseRef.push().setValue(chat);
                        messageTxt.setText("");
                    }
                });*/


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {

            return;
        }
        Bitmap bitmap;

        switch (requestCode) {

            case REQUEST_CODE_GALLERY:
                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();
                    startCropImage();

                } catch (Exception e) {
                    Log.e("Galary", "Error while creating temp file", e);
                }
                break;

            case REQUEST_CODE_TAKE_PICTURE:
                startCropImage();
                break;

            case FOR_CROPED_GALLERY:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {

                    return;
                }

                // bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                // offerImageUrl = mFileTemp.getAbsolutePath();
                Log.d(TAG, path);
                uploadImage(path);
                // ivOfferImage.setImageBitmap(bitmap);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        LogUtils.LOG_E(TAG, "onRequestPermissionsResult" + requestCode);
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    LogUtils.LOG_E(TAG, "PERMISSION GRANTED");
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        showImageChooseDialog(true);
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "PERMISSION NOT GRANTED");
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                        showAlertForGettingPermission();
                    }
                }

                return;
            }
            case REQUEST_WRITE_STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LogUtils.LOG_E(TAG, "PERMISSION GRANTED");
                    showImageChooseDialog(true);
                } else {
                    Log.e(TAG, "PERMISSION NOT GRANTED");
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        showAlertForGettingPermission();
                    }
                }
                return;
            }
        }

    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    private void sendNotification(final String message) {
        mDatabase.getReference(Constants.BUSINESS_USER_TB)
                .child(receiverId)
                .child("FCMToken")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot userToken : dataSnapshot.getChildren()) {
                            Log.e(TAG, "Token" + userToken.getKey());
                            sendMessage(userToken.getKey(), message);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });

    }

    //Send Notification From device to device
    public void sendMessage(final String recipient, final String message) {
        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    JSONObject messageBody = new JSONObject();

                    //messageBody.put("message",message);
                    messageBody.put("senderID", senderID);
                    messageBody.put("offerID", offerId);
                    messageBody.put("offerTitle", offerTitle);
                    messageBody.put("receiverID", receiverId);
                    messageBody.put("senderEmail", senderEmail);
                    messageBody.put("receiverEmail", receiverEmail);
                    messageBody.put("offerImageURL", offerImage);
                    messageBody.put("notificationType", "newChat");

                    JSONObject json = new JSONObject();
                    JSONObject dataJson = new JSONObject();

                    dataJson.put("body", message);
                    dataJson.put("title", senderEmail);
                    dataJson.put("click_action", "clickAction");
                    dataJson.put("icon", ContextCompat.getDrawable(getActivity(), R.drawable.notification_icon));
                    //dataJson.put("color","#FF9900");

                    //dataJson.put("userID",SharedPreferenceUtil.getString(Constants.USER_ID,""));
                    json.put("notification", dataJson);
                    json.put("data", messageBody);


                    json.put("to", recipient);

                    Log.e("JsonObject : ", json.toString());

                    String result = postToFCM(json.toString());
                    Log.e(TAG, "Result: " + result);
                    return result;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
               /* try {
                    JSONObject resultJson = new JSONObject(result);
                    int success, failure;
                    success = resultJson.getInt("success");
                    failure = resultJson.getInt("failure");
                    if(success > 0){
                        //showToast("Your deal notification not send... please try again!");
                    }else{
                        //showToast("Your deal notification not send... please try again!");
                    }
                    //Toast.makeText(mContext, "Message Success: " + success + "Message Failed: " + failure, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "Message Failed, Unknown error occurred."+e.getMessage(), Toast.LENGTH_LONG).show();
                }*/
            }
        }.execute();
    }

    String postToFCM(String bodyString) throws IOException {

        OkHttpClient mClient = new OkHttpClient();
        final String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";
        final MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.create(JSON, bodyString);
        Request request = new Request.Builder()
                .url(FCM_MESSAGE_URL)
                .post(body)
                .addHeader("Authorization", "key=" + SERVER_KEY)
                .build();
        Response response = mClient.newCall(request).execute();
        return response.body().string();
    }

    private void getReceiverBlockUnblockStatus() {
        //LogUtils.LOG_E("Current receiverId", receiverId);
        //LogUtils.LOG_E("Current sender Id", SharedPreferenceUtil.getString(Constants.USER_ID, ""));
        tbUserBlock.child(receiverId).addChildEventListener(
                new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String s) {
                        if (dataSnapshot.getKey() != null) {
                            LogUtils.LOG_E("Block Snap", String.valueOf(dataSnapshot.getValue()));
                            LogUtils.LOG_E("Block Key", dataSnapshot.getKey());
                            if (dataSnapshot.getKey().equals(senderID) && dataSnapshot.getValue().equals("Blocked"))
                                myBlockStatus = String.valueOf(dataSnapshot.getValue());
                            else
                                myBlockStatus = "Unblocked";
                        }
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {
                        if (dataSnapshot.getKey() != null) {
                            LogUtils.LOG_E("Block Snap", String.valueOf(dataSnapshot.getValue()));
                            LogUtils.LOG_E("Block Key", dataSnapshot.getKey());
                            if (dataSnapshot.getKey().equals(senderID) && dataSnapshot.getValue().equals("Blocked"))
                                myBlockStatus = String.valueOf(dataSnapshot.getValue());
                            else
                                myBlockStatus = "Unblocked";
                        }
                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                        LogUtils.LOG_E("Block Snap", String.valueOf(dataSnapshot.getValue()));
                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) {
                        LogUtils.LOG_E("Block Snap", String.valueOf(dataSnapshot.getValue()));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        LogUtils.LOG_E("onCancelled", databaseError.getMessage());
                    }
                });
    }


}