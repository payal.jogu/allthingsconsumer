package com.allthings.consumer.chat;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.allthings.consumer.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by Payal jogu on 23/8/17.
 */

public class ImageZoomDialogFragment extends DialogFragment {
    private ImageView ivZoomImage;
    private  View rootView;
    private ProgressBar pgZoom;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         rootView = inflater.inflate(R.layout.image_zoom_layout, container, false);
        //getDialog().setTitle("Simple Dialog");
        init();
        setImage();

        return rootView;
    }

    private void setImage() {

        Bundle imageData = getArguments();
        String path = imageData.getString("imageUrl");
        if (!path.isEmpty()) {
            pgZoom.setVisibility(View.VISIBLE);
            Picasso.with(getActivity()).load(path)
                    //.placeholder(R.drawable.img_placeholder)
                    .into(ivZoomImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            pgZoom.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            pgZoom.setVisibility(View.GONE);
                        }
                    });
        }

        ivZoomImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                    dismiss();
                return true;
            }
        });
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public void init(){
        ivZoomImage = (ImageView) rootView.findViewById(R.id.ivZoomImage);
        pgZoom = (ProgressBar) rootView.findViewById(R.id.pgZoom);
    }




   /* @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        return super.onCreateDialog(savedInstanceState);

    }*/
}