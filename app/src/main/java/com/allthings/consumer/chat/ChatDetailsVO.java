package com.allthings.consumer.chat;

import com.google.firebase.database.ServerValue;

/**
 * Created by Bypt on 7/10/2017.
 */

public class ChatDetailsVO {

    private String text,fromId,toId,toName,imageUrl="",isSenderBlock="",isReceiverBlock="",chatId="";
    private Object timestamp;
    int imageHeight,imageWidth;

    public ChatDetailsVO() {
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public ChatDetailsVO(String text, String fromId, String toId, String toName) {
        this.text = text;
        this.fromId = fromId;
        this.toId = toId;
        this.toName = toName;
        this.timestamp= ServerValue.TIMESTAMP;

    }

    public String getIsSenderBlock() {
        return isSenderBlock;
    }

    public void setIsSenderBlock(String isSenderBlock) {
        this.isSenderBlock = isSenderBlock;
    }

    public String getIsReceiverBlock() {
        return isReceiverBlock;
    }

    public void setIsReceiverBlock(String isReceiverBlock) {
        this.isReceiverBlock = isReceiverBlock;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public Object getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Object timestamp) {
        this.timestamp = timestamp;
    }
}

