package com.allthings.consumer.chat;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.offers.ScrollingLinearLayoutManager;
import com.allthings.consumer.offers.UpdateSearchedData;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

public class ChatListFragment extends BaseFragment implements View.OnClickListener {

    private String TAG = "ChatListFragment";
    private View rootView;
    private ArrayList<ChatDetailsVO> mChatList;
    private RecyclerView rvChatList;
    private DatabaseReference dbRefToUser, dbRefToBusiness;
    private ArrayList<ChatListVO> chatList;
    private TextView tvNoChat;
    private Map<String, String> chatKeyList;
    //private ProgressDialog progressDialog;
    private ArrayList<ChatListModel> chatListModelList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_chatlist, container, false);
            init();
            //setRecyclerView();
            setListeners();
            setFontStyle();
            disableBack(rootView);
            return rootView;
        } else {
            return rootView;
        }
    }

    //Set Adapter Data
    private void setAdapterData(ArrayList<ChatListVO> chatList) {
        //sortChatListData();
        ChatListAdapter chatListAdapter = new ChatListAdapter(mContext, chatList);
        rvChatList.setAdapter(chatListAdapter);
        chatListAdapter.notifyDataSetChanged();
        //imageView_placeholder.setVisibility(View.GONE);
    }

    private void sortChatListData() {
        /*Collections.sort(chatList, new Comparator<ChatListVO>() {
            @Override
            public int compare(FilterVo s1, FilterVo s2) {
                return s1.getName().compareToIgnoreCase(s2.getName());
            }
        });*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    //Set toolbar settings
    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).showHideToolbarRemainder(false);
        if (((BaseActivity) mContext).searching) {
            ((BaseActivity) mContext).showHideToolbarSerachEdittext(true);
            ((BaseActivity) mContext).showHideToolbarSearchCancelButton(true);
            ((BaseActivity) mContext).showHideToolbarSearchButton(false);
            ((BaseActivity) mContext).showHideToolbarTitle(false);
            ((BaseActivity) mContext).showHideToolbarMenuButton(false);
        } else {
            ((BaseActivity) mContext).showHideToolbarSerachEdittext(false);
            ((BaseActivity) mContext).showHideToolbarSearchCancelButton(false);
            ((BaseActivity) mContext).showHideToolbarTitle(true);
            ((BaseActivity) mContext).setToolbarTitle(getString(R.string.l_chat));
            ((BaseActivity) mContext).showHideToolbarSearchButton(true);
            ((BaseActivity) mContext).showHideToolbarMenuButton(true);
        }

        /*((BaseActivity)mContext).setToolbarTitle(mContext.getString(R.string.l_chat));
        ((BaseActivity)mContext).showHideToolbarTitle(true);
        ((BaseActivity)mContext).showHideToolbarMenuButton(true);
        ((BaseActivity)mContext).showHideToolbarSearchButton(true);*/

        ((BaseActivity) mContext).showHideToolbarBackButton(false);
        ((BaseActivity) mContext).showHideToolbarChatEmail(false);
        ((BaseActivity) mContext).showHideToolbarChatImage(false);
        ((BaseActivity) mContext).showHideToolbarChatOfferTitle(false);

        disableBack(rootView);
        EventBus.getDefault().post(new UpdateSearchedData());
    }

    //set the fonts styles
    private void setFontStyle() {
    }

    //set the listeners
    private void setListeners() {
    }


    //initialize the components
    private void init() {
        // progressDialog = CustomDialog.ctor(getActivity());
        tvNoChat = (TextView) rootView.findViewById(R.id.tvNoChat);
        getActivity().findViewById(R.id.toolbar_ivSearchCancel).setOnClickListener(this);
        getActivity().findViewById(R.id.ivToolbarsearchbutton).setOnClickListener(this);
        // dbRefToChatTB = mDatabase.getReference(Constants.CHAT_ROOM_TB).child("messages");
        dbRefToUser = mDatabase.getReference(Constants.USER_MESSAGES_TB);
        dbRefToBusiness = mDatabase.getReference(Constants.BUSINESS_USER_TB);
        chatKeyList = new HashMap<>();
        chatList = new ArrayList<>();
        chatListModelList = new ArrayList<>();

        rvChatList = (RecyclerView) rootView.findViewById(R.id.rvChatList);
        rvChatList.setHasFixedSize(true);
        rvChatList.setLayoutManager(new ScrollingLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false, 1000));

        getAllChatKeyList();

        EventBus.getDefault().register(this);

    }

    //HashMap<String,List<String>> chatListKey = new HashMap<>();

    //get the key of chat messages
    private void getAllChatKeyList() {
        if (NetworkUtil.isOnline(mContext)) {

            //progressDialog.show();

            chatListModelList.clear();
            dbRefToUser.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    chatListModelList.clear();

                    if (dataSnapshot.getChildrenCount() <= 0)
                        tvNoChat.setVisibility(View.VISIBLE);
                    else
                        tvNoChat.setVisibility(View.GONE);


                    if (dataSnapshot.getValue() != null) {
                        Log.e(TAG, dataSnapshot.getKey());
                        for (DataSnapshot offerskey : dataSnapshot.getChildren()) {
                            Log.e(TAG, offerskey.getKey());


                            for (DataSnapshot userkey : offerskey.getChildren()) {

                                boolean found = false;
                                for (ChatListModel chatList : chatListModelList) {
                                    if (userkey.getKey().equalsIgnoreCase(chatList.getReceiverID()) &&
                                            offerskey.getKey().equalsIgnoreCase(chatList.getOfferId())) {
                                        found = true;
                                        break;
                                    }
                                }

                                if (!found) {
                                    Log.e(TAG, userkey.getKey());
                                    ChatListModel chatListModel = new ChatListModel();
                                    chatListModel.setOfferId(offerskey.getKey());
                                    chatListModel.setReceiverID(userkey.getKey());
                                    chatListModelList.add(chatListModel);
                                }

                            }
                        }
                        getAllReceiverDetail(chatListModelList);

                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
           /* dbRefToUser.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).
                    addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            //progressDialog.dismiss();
                            chatListModelList.clear();

                            if (dataSnapshot.getChildrenCount() <= 0)
                                tvNoChat.setVisibility(View.VISIBLE);
                            else
                                tvNoChat.setVisibility(View.GONE);


                            if (dataSnapshot.getValue() != null) {
                                Log.e(TAG, dataSnapshot.getKey());
                                for (DataSnapshot offerskey : dataSnapshot.getChildren()) {
                                    Log.e(TAG, offerskey.getKey());


                                    for (DataSnapshot userkey : offerskey.getChildren()) {

                                        boolean found = false;
                                        for (ChatListModel chatList : chatListModelList) {
                                            if (userkey.getKey().equalsIgnoreCase(chatList.getReceiverID()) &&
                                                    offerskey.getKey().equalsIgnoreCase(chatList.getOfferId())) {
                                                found = true;
                                                break;
                                            }
                                        }

                                        if (!found) {
                                            Log.e(TAG, userkey.getKey());
                                            ChatListModel chatListModel = new ChatListModel();
                                            chatListModel.setOfferId(offerskey.getKey());
                                            chatListModel.setReceiverID(userkey.getKey());
                                            chatListModelList.add(chatListModel);
                                        }

                                    }
                                }
                                getAllReceiverDetail(chatListModelList);

                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            //progressDialog.dismiss();
                        }
                    });*/
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }

    //get the details of receiver
    private void getAllReceiverDetail(ArrayList<ChatListModel> list/*HashMap<String,List<String>> chatList*/) {
        if (NetworkUtil.isOnline(mContext)) {
            // progressDialog.show();
            chatList.clear();
            DatabaseReference dbRefToOffer = mDatabase.getReference(Constants.OFFER);
            for (final ChatListModel chatListModel : list) {
                dbRefToOffer.child(chatListModel.getOfferId()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //  progressDialog.dismiss();
                        if (dataSnapshot.getValue() != null) {
                            ChatListVO chatListVo = new ChatListVO();
                            chatListVo.setOfferID(chatListModel.getOfferId());
                            chatListVo.setReceiverID(chatListModel.getReceiverID());
                            chatListVo.setOfferTitle(dataSnapshot.child(Constants.OFFER_TITLE).getValue(String.class));
                            chatListVo.setOfferImage(dataSnapshot.child(Constants.OFFER_IMAGE_URL).getValue(String.class));

                            LogUtils.LOG_E(TAG, "Offer Image Url :" + chatListVo.getOfferImage());
                            LogUtils.LOG_E(TAG, "Offer Title :" + chatListVo.getOfferTitle());
                            if (!chatListModel.getReceiverID().isEmpty()) {
                                getBusienssUserEmailId(chatListModel.getReceiverID(), chatListVo);
                            }

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        //progressDialog.dismiss();
                        LogUtils.LOG_E(TAG, databaseError.getMessage());
                    }
                });
            }
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }

    private void getBusienssUserEmailId(String businessUserId, final ChatListVO chatListVo) {
        // progressDialog.show();
        dbRefToBusiness.child(businessUserId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // progressDialog.dismiss();
                if (dataSnapshot.hasChild(Constants.EMAIL)) {
                    Log.e(TAG, "Receiver Email : " + dataSnapshot.child(Constants.EMAIL).getValue(String.class));
                    chatListVo.setReceiverEmail(dataSnapshot.child(Constants.EMAIL).getValue(String.class));
                    getConsumerEmail(chatListVo);
                }

                /* chatList.add(chatListVo);
                setAdapterData(chatList);*/
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //progressDialog.dismiss();
                LogUtils.LOG_E(TAG, databaseError.getMessage());
            }
        });
    }

    private void getConsumerEmail(final ChatListVO chatListVo) {
        mDatabase.getReference(Constants.CONSUMER_USER_MASTER)
                .child(SharedPreferenceUtil.getString(Constants.USER_ID, ""))
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        chatListVo.setSenderID(SharedPreferenceUtil.getString(Constants.USER_ID, ""));
                        chatListVo.setSenderEmail(dataSnapshot.child(Constants.EMAIL).getValue(String.class));
                        Log.e(TAG, "Sender Email : " + dataSnapshot.child(Constants.EMAIL).getValue(String.class));

                        boolean found = false;

                        for (int i = 0; i < chatList.size(); i++) {
                            if (chatList.get(i).getReceiverID().equalsIgnoreCase(chatListVo.getReceiverID()) &&
                                    chatList.get(i).getOfferID().equalsIgnoreCase(chatListVo.getOfferID())) {
                                found = true;
                                break;
                            }
                        }

                        if(!found) {
                            chatList.add(chatListVo);
                            setAdapterData(chatList);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


    }


    //Get Search Text Data
    public void onEvent(UpdateSearchedData updateSearchedData) {
        Log.e(TAG, updateSearchedData.getSearchCriteria());
        if (!updateSearchedData.getSearchCriteria().equals("")) {

            ArrayList<ChatListVO> searchChat = new ArrayList<>();
            for (ChatListVO chatListVO : chatList) {
                if ((chatListVO.getReceiverEmail().toLowerCase() + chatListVO.getOfferTitle().toLowerCase())
                        .contains(updateSearchedData.getSearchCriteria().toLowerCase())) {
                    searchChat.add(chatListVO);
                }
            }
            Log.e("Searched data", "data");
            setAdapterData(searchChat);
            if (searchChat.size() == 0) {
                //imageView_placeholder.setVisibility(View.VISIBLE);
            } else {
                //imageView_placeholder.setVisibility(View.GONE);
            }

        } else {
            setAdapterData(chatList);

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_ivSearchCancel:
                ((BaseActivity) mContext).searching = false;
                EventBus.getDefault().post(new UpdateSearchedData()); //Set Default Data When Cancel on search
                ((BaseActivity) mContext).showHideToolbarTitle(true);
                ((BaseActivity) mContext).showHideToolbarBackButton(false);
                ((BaseActivity) mContext).showHideToolbarMenuButton(true);
                ((BaseActivity) mContext).showHideToolbarSearchButton(true);
                ((BaseActivity) mContext).showHideToolbarSerachEdittext(false);
                ((BaseActivity) mContext).showHideToolbarSerachEdittext(false);
                ((BaseActivity) mContext).showHideToolbarSearchCancelButton(false);
                hideKeyboard();
                break;
            case R.id.ivToolbarsearchbutton: // show search
                ((BaseActivity) mContext).searching = true;
                ((BaseActivity) mContext).showHideToolbarSerachEdittext(true);
                ((BaseActivity) mContext).showHideToolbarSearchCancelButton(true);
                ((BaseActivity) mContext).showHideToolbarSearchButton(false);
                ((BaseActivity) mContext).showHideToolbarMenuButton(false);
                ((BaseActivity) mContext).showHideToolbarBackButton(false);
                ((BaseActivity) mContext).showHideToolbarTitle(false);
                ((BaseActivity) mContext).etToolbarSearch.requestFocus();
                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(((BaseActivity) mContext).etToolbarSearch, InputMethodManager.SHOW_IMPLICIT);
                break;
        }
    }
}
