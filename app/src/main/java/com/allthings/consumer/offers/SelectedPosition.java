package com.allthings.consumer.offers;

import com.allthings.consumer.filter.FilterVo;

/**
 * Created by ABC on 6/12/2017.
 */

public interface SelectedPosition {
   void onItemClicked(int groupPosition, int childPosition, boolean isChecked, FilterVo filterVo);
}
