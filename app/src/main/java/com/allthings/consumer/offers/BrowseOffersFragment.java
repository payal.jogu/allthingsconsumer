package com.allthings.consumer.offers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.utils.LogUtils;

public class BrowseOffersFragment extends BaseFragment implements TabLayout.OnTabSelectedListener, UpdateCategoryTitle {
    private View rootView;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private UpdateCategoryTitle updateTitle;
    private TabAdapter1 mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_browseoffers, container, false);
            init();
            setListeners();
            //setFontStyle();
            disableBack(rootView);
            //fragmentManager.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
            return rootView;
        } else {
            return rootView;
        }

    }

    //Set toolbar settings
    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) mContext).showToolbar(true);
        if (((BaseActivity) mContext).searching) {
            ((BaseActivity) mContext).showHideToolbarSerachEdittext(true);
            ((BaseActivity) mContext).showHideToolbarSearchCancelButton(true);
            ((BaseActivity) mContext).showHideToolbarSearchButton(false);
            ((BaseActivity) mContext).showHideToolbarTitle(false);
            ((BaseActivity) mContext).showHideToolbarMenuButton(false);
        } else {
            ((BaseActivity) mContext).showHideToolbarSerachEdittext(false);
            ((BaseActivity) mContext).showHideToolbarSearchCancelButton(false);
            ((BaseActivity) mContext).showHideToolbarTitle(true);
            ((BaseActivity) mContext).setToolbarTitle(getString(R.string.l_browse_offers));
            ((BaseActivity) mContext).showHideToolbarSearchButton(true);
            ((BaseActivity) mContext).showHideToolbarMenuButton(true);
        }
        ((BaseActivity) mContext).showHideToolbarBackButton(false);
        ((BaseActivity) mContext).showHideToolbarRemainder(false);

      /*  ((BaseActivity)mContext).setToolbarTitle(mContext.getString(R.string.l_browse_offers));
        ((BaseActivity)mContext).showHideToolbarMenuButton(true);
        ((BaseActivity)mContext).showHideToolbarSearchButton(true);*/
        ((BaseActivity) mContext).showHideToolbarChatOfferTitle(false);
        ((BaseActivity) mContext).showHideToolbarChatImage(false);
        ((BaseActivity) mContext).showHideToolbarChatEmail(false);
    }

    //Set view pager in tab layout
    private void setListeners() {
        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(this);
    }

    //Initialize components
    private void init() {
        updateTitle = this;
        tabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout);
        viewPager = (ViewPager) rootView.findViewById(R.id.pager);
        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.gold)));
        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.silver)));
        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.bronze)));
        mAdapter = new TabAdapter1(getChildFragmentManager());
        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(3);


    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition()); //Select Tab And Change It Position

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    //Set Category  With Count
    @Override
    public void setCateTitle(String title, int count) {
        if (title.equalsIgnoreCase(mContext.getString(R.string.gold))) {
            tabLayout.getTabAt(0).setText(mContext.getString(R.string.gold) + "(" + count + ")");
        } else if (title.equalsIgnoreCase(mContext.getString(R.string.silver))) {
            tabLayout.getTabAt(1).setText(mContext.getString(R.string.silver) + "(" + count + ")");
        } else if (title.equalsIgnoreCase(mContext.getString(R.string.bronze))) {
            tabLayout.getTabAt(2).setText(mContext.getString(R.string.bronze) + "(" + count + ")");
        }
    }

    //Adapter For Tab
    private class TabAdapter1 extends FragmentPagerAdapter {
        TabAdapter1(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int index) {
            String title = "";
            if (index == 0) {
                title = "Gold";
            } else if (index == 1) {
                title = "Silver";
            } else if (index == 2) {
                title = "Bronze";
            }
            return OffersFragment.newInstance(title, updateTitle);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return super.getPageTitle(position);
        }

        @Override
        public int getCount() {
            return 3;
        }

        public int getItemPosition(Object object){
            return POSITION_NONE;
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(mContext).registerReceiver((mMessageReceiver),
                new IntentFilter("offerRemainder")
        );
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mMessageReceiver);
    }

    // handle all type notification from foreground
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.LOG_E("OFFER REMIANDER", "OFFER REMIANDER ...................");
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        }
    };
}
