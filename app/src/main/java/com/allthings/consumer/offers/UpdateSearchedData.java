package com.allthings.consumer.offers;

/**
 * Created by Vishal Dhaduk on 6/20/2017.
 */
public class UpdateSearchedData {

    public String getSearchCriteria() {
        return searchCriteria;
    }

    public void setSearchCriteria(String searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    private String searchCriteria="";
}
