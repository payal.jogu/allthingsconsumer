package com.allthings.consumer.offers;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.chat.ChatDetailsFragment;
import com.allthings.consumer.feedback.FeedbackFragment;
import com.allthings.consumer.offerRemainder.AlarmReceiver;
import com.allthings.consumer.offerRemainder.OfferRemainderFragment;
import com.allthings.consumer.offerRemainder.RemainderDialogInterface;
import com.allthings.consumer.offerRemainder.SetRemainderInterface;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.GPSTracker;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.greenrobot.event.EventBus;

public class MyOfferDetailsFragment extends BaseFragment implements View.OnClickListener, RemainderDialogInterface {
    View rootView;
    private TextView textViewTitle, textViewTitleLabel, textViewDescription, textViewDescriptionLabel,
            textViewCategory, textViewCategoryLabel, textViewDiscount, textViewDiscountLabel, textViewOfferType,
            textViewOfferTypeLabel, textViewCompanyName, textViewCompanyNameLabel,
            textViewFeedback, tvLabelStartDate, tvStartDate, tvLabelExpiryDate, tvExpiryDate, tvLabelWebsite,
            tvWebsite, tvCompanyAddress, tvLabelCompanyAddress, tvLabelCity, tvCity;
    private ImageView imageViewCall, imageViewMessage, imageViewEmail, imageViewOffer, imageViewTootlbarBack,
            ivMap;
    OffersVO offersVO;
    private RatingBar ratingBar;
    private String TAG = "OfferDetailsFragment", receiverEmail, senderEmail, offerType;
    private View imgContainer;
    private CollapsingToolbarLayout collapsingToolbar;
    private AppBarLayout appBarLayout;
    private CoordinatorLayout coordinatorLayout;
    private double companyLatitude, companyLongitude, currentLatitude, currentLongitude;
    private boolean isRemainderSelected = true;
    private SimpleDateFormat dateFormat;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_my_offer_details, container, false);
            init();
            getArgument();
            setListeners();
            setFontStyle();
            //disableBack(rootView);
            return rootView;
        } else {
            return rootView;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void init() {

        //Convert Timestamp into date format
        dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        EventBus.getDefault().register(this);

        tvCompanyAddress = rootView.findViewById(R.id.tvCompanyAddress);
        tvLabelCompanyAddress = rootView.findViewById(R.id.tvLabelCompanyAddress);

        textViewTitle = rootView.findViewById(R.id.tvTitle);
        textViewTitleLabel = rootView.findViewById(R.id.tvLabelTitle);

        textViewDescription = rootView.findViewById(R.id.tvDescription);
        textViewDescriptionLabel = rootView.findViewById(R.id.tvLabelDescription);

        textViewCategory = rootView.findViewById(R.id.tvCategory);
        textViewCategoryLabel = rootView.findViewById(R.id.tvLabelCategory);

        textViewDiscount = rootView.findViewById(R.id.tvDiscount);
        textViewDiscountLabel = rootView.findViewById(R.id.tvLabelDiscount);

        textViewOfferType = rootView.findViewById(R.id.tvOfferType);
        textViewOfferTypeLabel = rootView.findViewById(R.id.tvLabelOfferType);

        tvLabelStartDate = rootView.findViewById(R.id.tvLabelStartDate);
        tvStartDate = rootView.findViewById(R.id.tvStartDate);

        textViewCompanyName = rootView.findViewById(R.id.tvCompanyName);
        textViewCompanyNameLabel = rootView.findViewById(R.id.tvLabelCompanyName);

        textViewFeedback = rootView.findViewById(R.id.tvFeedback);

        /*tvLabelExpiryDate=(TextView)rootView.findViewById(R.id.tvLabelExpiryDate);
        tvExpiryDate=(TextView)rootView.findViewById(R.id.tvExpiryDate);*/

        tvLabelWebsite = rootView.findViewById(R.id.tvLabelWebsite);
        tvWebsite = rootView.findViewById(R.id.tvWebsite);

        tvLabelCity = rootView.findViewById(R.id.tvLabelCity);
        tvCity = rootView.findViewById(R.id.tvCity);

        ratingBar = rootView.findViewById(R.id.ratingbar_offerdetails);
        imgContainer = rootView.findViewById(R.id.linear_rating);
        //imageViewTootlbarBack=(ImageView)rootView.findViewById(R.id.ivBack);
        imageViewCall = rootView.findViewById(R.id.ivCall);
        imageViewEmail = rootView.findViewById(R.id.ivEmail);
        imageViewMessage = rootView.findViewById(R.id.ivMessage);
        imageViewOffer = rootView.findViewById(R.id.ivOfferImage);

        ((BaseActivity) mContext).findViewById(R.id.ivToolbar_remainder).setOnClickListener(this);

        //((BaseActivity)mContext).findViewById(R.id.ivToolbarbackicon).setOnClickListener(this);
        ivMap = rootView.findViewById(R.id.ivMap);

        //toolbar = (Toolbar)rootView.findViewById(R.id.toolbar_android);
        collapsingToolbar = rootView.findViewById(R.id.collapsing_toolbar);
        appBarLayout = rootView.findViewById(R.id.app_bar_layout);
        coordinatorLayout = rootView.findViewById(R.id.coordinatorLayout);

        getCurrentLatLong();
    }

    private void setRemainderOfferIdToDatabase(String offerId) {

        SharedPreferenceUtil.putValue("remainder", offerId);

      /*  mDatabase.getReference(Constants.CONSUMER_USER_MASTER).child(SharedPreferenceUtil
                .getString(Constants.USER_ID,"")).child("remainder").setValue();*/
    }

    private void getArgument() {
        if (getArguments() != null) {
            Bundle bundle1 = getArguments();

            if (bundle1.containsKey("from")) {
                Log.e("OFFER_ID", bundle1.getString(Constants.OFFER_ID));
                if (bundle1.getString("from").equals("baseActivity")) {
                    mDatabase.getReference(Constants.OFFER).child(bundle1.getString(Constants.OFFER_ID))
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    offersVO = dataSnapshot.getValue(OffersVO.class);
                                    Log.e("dataSnapshot", offersVO.toString());
                                    if (offersVO != null) {
                                        getDataFromOfferList();
                                        getReceiverEmail();
                                        getsenderEmail();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    Log.e("databaseError", databaseError.getMessage());
                                }
                            });
                }
            } else {
                offersVO = (OffersVO) getArguments().getSerializable(Constants.FROM_OFFERLIST);
                offerType = getArguments().getString("OfferType");
                Log.e("Business Id", offersVO.getBusinessID());
                if (offersVO != null) {
                    getDataFromOfferList();
                    getReceiverEmail();
                    getsenderEmail();
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);

        SharedPreferenceUtil.putValue("IsOfferDetailScreen", false);
        SharedPreferenceUtil.putValue("OfferIdScreen", "");
        SharedPreferenceUtil.save();
    }

    public void onEvent(SetRemainderInterface remainderInterface) {
        refreshRemainder();

        if (offersVO != null) {
            if (SharedPreferenceUtil.contains("IsOfferDetailScreen") && SharedPreferenceUtil.getBoolean
                    ("IsOfferDetailScreen", false) && SharedPreferenceUtil.getString("OfferIdScreen", "")
                    .equalsIgnoreCase(offersVO.getOfferID())) {
                showDialogAlertPositiveButton(getString(R.string.offer_remainder), offersVO.getOfferTitle());

            }
        }

    }

    private void getDataFromOfferList() {
        if (offersVO != null) {

            SharedPreferenceUtil.putValue("IsOfferDetailScreen", true);
            SharedPreferenceUtil.putValue("OfferIdScreen", offersVO.getOfferID());
            SharedPreferenceUtil.save();

            refreshRemainder();

            FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(mContext);
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, offersVO.getOfferID());
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, offersVO.getOfferTitle());
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "text");
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, bundle);
            analytics.logEvent("offer_details", bundle);

            Query query = mDatabase.getReference().child(Constants.OFFER_RATING_TABLE).orderByChild(Constants.OFFER_ID).equalTo(offersVO.getOfferID());
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //Log.e(TAG, String.valueOf(dataSnapshot.getChildrenCount()));
                    float NumberOfRatings = (int) dataSnapshot.getChildrenCount();
                    float Ratings = 0;

                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        Log.e(TAG, String.valueOf(child.child(Constants.FEEDBACK_RATINGS).getValue()));
                        if (child.child(Constants.FEEDBACK_RATINGS).getValue() != "") {
                            Ratings = Ratings + Float.valueOf(String.valueOf(child.child(Constants.FEEDBACK_RATINGS).getValue()));
                        }
                    }
                    //Log.e(TAG,"AVERAGE:"+Ratings/NumberOfRatings);
                    ratingBar.setRating(Ratings / NumberOfRatings); //set average ratings of offer

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            if (!offersVO.getOfferTitle().equals("")) {
                textViewTitle.setText(offersVO.getOfferTitle());
            }
            tvCompanyAddress.setText(offersVO.getCompanyAddress());
            textViewDescription.setText(offersVO.getOfferDiscription());

            String categoryName = offersVO.getCategory();
            categoryName = String.valueOf(categoryName.charAt(0)).toUpperCase() + categoryName.subSequence(1, categoryName.length());

            String subCategoryName = offersVO.getSubCategory();
            subCategoryName = String.valueOf(subCategoryName.charAt(0)).toUpperCase() + subCategoryName.subSequence(1, subCategoryName.length());

            textViewCategory.setText(categoryName + " / " + subCategoryName);

            // textViewCategory.setText(offersVO.getCategory() + " / " + offersVO.getSubCategory());
            textViewDiscount.setText(offersVO.getOfferDiscount());

            String offerType = offersVO.getOfferType();
            offerType = String.valueOf(offerType.charAt(0)).toUpperCase() + offerType.subSequence(1, offerType.length());
            textViewOfferType.setText(offerType);

            textViewCompanyName.setText(offersVO.getCompanyName().toUpperCase());
            tvWebsite.setText(offersVO.getWebsite());
            tvCity.setText(offersVO.getCity());

            getLatLongFromAddress(tvCompanyAddress.getText().toString());


            SpannableStringBuilder builder = new SpannableStringBuilder();

            //TODO Convert timestamp of seconds to timestamp in milliseconds // StartDate
            Timestamp offerStartDate = new Timestamp(Long.valueOf(offersVO.getOfferStartDate()) * 1000);
            //Timestamp offerStartDate = new Timestamp(offersVO.getOfferStartDate());
            SpannableString startDateSpan = new SpannableString(dateFormat.format(offerStartDate));
            startDateSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.dark_black)),
                    0, startDateSpan.length(), 0);
            builder.append(startDateSpan);

            //TODO Convert timestamp of seconds to timestamp in milliseconds //ExpiryDate
            Timestamp offerExpiryDate = new Timestamp(Long.valueOf(offersVO.getOfferExpiryDate()) * 1000);
            // Timestamp offerExpiryDate = new Timestamp(offersVO.getOfferExpiryDate());
            SpannableString expiryDateSpan = new SpannableString(dateFormat.format(offerExpiryDate));
            expiryDateSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.colorRed)),
                    0, expiryDateSpan.length(), 0);
            builder.append(" / ");
            builder.append(expiryDateSpan);

            tvStartDate.setText(builder, TextView.BufferType.SPANNABLE);

            if (!offersVO.getOfferImageURL().isEmpty()) {
                Picasso.with(mContext).load(String.valueOf(offersVO.getOfferImageURL())).placeholder
                        (R.drawable.offer_details_placeholder).into(imageViewOffer);
            }
        }

    }

    //Set toolbar settings
    @Override
    public void onResume() {
        super.onResume();

        refreshRemainder();

        hideKeyboard();
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).showHideToolbarTitle(true);
        ((BaseActivity) mContext).setToolbarTitle(getString(R.string.l_browse_offers));
        //((BaseActivity) mContext).setToolbarTitleBold();

        ((BaseActivity) mContext).showHideToolbarBackButton(true);
        ((BaseActivity) mContext).showHideToolbarMenuButton(false);
        ((BaseActivity) mContext).showHideToolbarSearchButton(false);
        ((BaseActivity) mContext).showHideToolbarSerachEdittext(false);
        ((BaseActivity) mContext).showHideToolbarSearchCancelButton(false);
        ((BaseActivity) mContext).showHideToolbarChatEmail(false);
        ((BaseActivity) mContext).showHideToolbarChatImage(false);
        ((BaseActivity) mContext).showHideToolbarChatOfferTitle(false);


        if (offerType != null && offerType.equals("ExpiryOffer")) {
            ((BaseActivity) mContext).showHideToolbarRemainder(false);
        } else
            ((BaseActivity) mContext).showHideToolbarRemainder(true);

        //coordinatorLayout = (CoordinatorLayout) rootView.findViewById(R.id.coordinatorLayout);
        appBarLayout.post(new Runnable() {
            @Override
            public void run() {
                int heightPx = imageViewOffer.getHeight();
                setAppBarOffset(heightPx / 2);
            }
        });

        /*appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    // collapsingToolbar.setCollapsedTitleTextAppearance(ContextCompat.getColor(getActivity(),R.color.white));
                   // collapsingToolbar.setTitle("Browse Offers          ");
                    //imageViewTootlbarBack.setVisibility(View.VISIBLE);
                    isShow = true;
                } else if(isShow) {
                    collapsingToolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    //imageViewTootlbarBack.setVisibility(View.GONE);
                    isShow = false;
                }
            }
        });*/


    }

    private void setAppBarOffset(int offsetPx) {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
        behavior.onNestedPreScroll(coordinatorLayout, appBarLayout, null, 0, offsetPx, new int[]{0, 0});
    }


    //set font styles
    private void setFontStyle() {
        setTypeFaceTextView(textViewTitle, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(textViewTitleLabel, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(textViewDescription, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(textViewDescriptionLabel, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(textViewCategory, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(textViewCategoryLabel, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(textViewDiscount, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(textViewDiscountLabel, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(textViewOfferType, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(textViewOfferTypeLabel, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(textViewFeedback, mContext.getString(R.string.fontOpenSansBoldItalic));
        setTypeFaceTextView(textViewCompanyName, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(textViewCompanyNameLabel, mContext.getString(R.string.fontOpenSansSemiBold));

        setTypeFaceTextView(tvLabelStartDate, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvStartDate, mContext.getString(R.string.fontOpenSansSemiBold));

        setTypeFaceTextView(tvLabelCity, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvCity, mContext.getString(R.string.fontOpenSansSemiBold));

        setTypeFaceTextView(tvCompanyAddress, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvLabelCompanyAddress, mContext.getString(R.string.fontOpenSansSemiBold));

      /*  setTypeFaceTextView(tvLabelExpiryDate,mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvExpiryDate,mContext.getString(R.string.fontOpenSansSemiBold));
*/
        setTypeFaceTextView(tvLabelWebsite, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvWebsite, mContext.getString(R.string.fontOpenSansSemiBold));

      /*  Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                "OpenSans-Semibold.ttf");
        collapsingToolbar.setCollapsedTitleTypeface(tf);*/

    }

    private void setListeners() {
        imageViewCall.setOnClickListener(this);
        imageViewEmail.setOnClickListener(this);
        imageViewMessage.setOnClickListener(this);
        ivMap.setOnClickListener(this);
        imageViewOffer.setOnClickListener(this);
        textViewFeedback.setOnClickListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e(TAG, "onRequestPermissionsResult" + requestCode);
        switch (requestCode) {

            case REQUEST_CALL_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "PERMISSION GRANTED");
                    goToMakeCall();
                } else {
                    Log.e(TAG, "PERMISSION NOT GRANTED");

                }
                return;
            }
            case REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    LogUtils.LOG_E(TAG, "PERMISSION GRANTED");
                    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        //showImageChooseDialog(true);
                    }
                } else {
                    Log.e(TAG, "PERMISSION NOT GRANTED");
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        showAlertForGettingPermission();
                    }
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }

                return;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivCall:
                hideKeyboard();
                if (Build.VERSION.SDK_INT >= 23) {
                    if (!isCallPermissionExist()) {
                        requestCallPermission();
                    } else {
                        goToMakeCall();
                    }
                } else {
                    goToMakeCall();
                }
                break;
            case R.id.ivEmail:
                goToEmail();
                break;
            case R.id.ivMessage:
                // toolbar.setVisibility(View.GONE);
                // ((BaseActivity)mContext).showToolbar(true);
                goToChat(); //Go to Chat
                break;
            case R.id.tvFeedback:
                gotoFeedback();//go to feedback fragment
                break;
            case R.id.ivMap:
                getMapWithMarker();
                break;
            case R.id.ivOfferImage:
                showImageSlider(offersVO.getOfferImageURL());
                break;
            case R.id.ivToolbar_remainder:
                if (isRemainderSelected) {
                    /*isRemainderSelected =false;
                    ((BaseActivity)mContext).ivToolbarRemainder.setSelected(true);
                    Calendar calendar  = Calendar.getInstance();
                    calendar.set(Calendar.MONTH, 9);
                    calendar.set(Calendar.YEAR, 2017);
                    calendar.set(Calendar.DAY_OF_MONTH, 26);

                    calendar.set(Calendar.HOUR,12);
                    calendar.set(Calendar.MINUTE, 42);
                    calendar.set(Calendar.SECOND, 0);
                    calendar.set(Calendar.AM_PM,Calendar.PM);

                    Intent intentReceiver = new Intent(mContext, AlarmReceiver.class);
                    intentReceiver.putExtra(Constants.OFFER_ID,offersVO.getOfferID());
                    intentReceiver.putExtra(Constants.OFFER_TITLE,offersVO.getOfferTitle());
                    intentReceiver.putExtra(Constants.OFFER_EXPIRY_DATE,offersVO.getOfferExpiryDate());

                    PendingIntent pendingIntent = PendingIntent.getBroadcast(
                            mContext,1, intentReceiver,  PendingIntent.FLAG_UPDATE_CURRENT);

                    AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(ALARM_SERVICE);
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP,6000, pendingIntent);
                    Log.e("Alarm","set");*/
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.OFFER_ID, offersVO.getOfferID());
                    bundle.putString(Constants.OFFER_TITLE, offersVO.getOfferTitle());
                    bundle.putString(Constants.OFFER_EXPIRY_DATE, String.valueOf(offersVO.getOfferExpiryDate()));

                    DialogFragment newFragment = new OfferRemainderFragment();
                    newFragment.setArguments(bundle);
                    newFragment.setTargetFragment(this, 0);
                    newFragment.show(fragmentManager, "datePicker");
                } else {
                    if (SharedPreferenceUtil.contains(offersVO.getOfferID())) {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
                        builder.setMessage("Are you sure to cancel remainder.")
                                .setCancelable(false)
                                .setTitle(getString(R.string.app_name))
                                .setPositiveButton(mContext.getString(R.string.ok),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();

                                                AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
                                                Intent myIntent = new Intent(getContext(), AlarmReceiver.class);
                                                PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(),
                                                        SharedPreferenceUtil.getInt(offersVO.getOfferID(), 0),
                                                        myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                                                alarmManager.cancel(pendingIntent);

                                                Log.e("Alarm", "cancel" + SharedPreferenceUtil.getInt(offersVO.getOfferID(), 0));

                                                //showDialogAlertPositiveButton(getString(R.string.app_name),"Alarm is already set");
                                                SharedPreferenceUtil.remove(offersVO.getOfferID());
                                                SharedPreferenceUtil.save();

                                                ((BaseActivity) mContext).ivToolbarRemainder.setSelected(false);
                                                isRemainderSelected = true;

                                            }
                                        })
                                .setNegativeButton(mContext.getString(R.string.cancel),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int i) {
                                                dialog.dismiss();
                                            }
                                        });

                        android.app.AlertDialog alert = builder.create();
                        alert.show();

                    }
                }
                break;
           /* case R.id.ivToolbarbackicon:
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutBaseContainer,new BrowseOffersFragment()).commit();
                break;*/
        }
    }

    @Override
    public void refreshRemainder() {
        if (offersVO != null) {
            if (SharedPreferenceUtil.contains(offersVO.getOfferID())) {
                // if (SharedPreferenceUtil.getBoolean(offersVO.getOfferID(), false)) {
                isRemainderSelected = false;
                ((BaseActivity) mContext).ivToolbarRemainder.setSelected(true);
                // }
            } else {
                isRemainderSelected = true;
                ((BaseActivity) mContext).ivToolbarRemainder.setSelected(false);
            }
        }
    }

    //get Map with polyline way between company address and current user address
    private void getMapWithMarker() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!isAccess_Fine_Location_Exist()) {
                requestAccessFineLocation();
            }
        }
        if (currentLatitude == 0.0 && currentLongitude == 0.0) {
            //showToast("Please enable location service");
            getCurrentLatLong();
        } else if (companyLongitude == 0.0 && companyLatitude == 0.0) {
            getLatLongFromAddress(tvCompanyAddress.getText().toString());
        } else {
            String latitude_source = String.valueOf(currentLatitude),
                    longitude_source = String.valueOf(currentLongitude),
                    latitude_dest = String.valueOf(companyLatitude),
                    longitude_dest = String.valueOf(companyLongitude);

            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr="
                    + latitude_source + ","
                    + longitude_source
                    + "&daddr=" + latitude_dest
                    + "," + longitude_dest));
            startActivity(i);
        }
    }


    //Go to Chat
    private void goToChat() {
        ChatDetailsFragment chatDetailsFragment = new ChatDetailsFragment();

        Bundle bundle = new Bundle();

        bundle.putString("receiverID", SharedPreferenceUtil.getString(Constants.USER_ID, ""));
        bundle.putString("senderID", offersVO.getBusinessID());
        bundle.putString("offerID", offersVO.getOfferID());
        bundle.putString("receiverEmail", senderEmail);
        bundle.putString("senderEmail", receiverEmail);
        bundle.putString("offerTitle", offersVO.getOfferTitle());
        bundle.putString("offerImage", offersVO.getOfferImageURL());

        /*bundle.putString("businessId",offersVO.getBusinessID());
        bundle.putString(Constants.OFFER_ID,offersVO.getOfferID());*/
        chatDetailsFragment.setArguments(bundle);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, chatDetailsFragment).addToBackStack(null).commit();
    }

    private void getReceiverEmail() {
        if (offersVO != null) {
            if (offersVO.getBusinessID() != null) {

                mDatabase.getReference(Constants.BUSINESS_USER_TB)
                        .child(offersVO.getBusinessID()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        receiverEmail = dataSnapshot.child(Constants.EMAIL).getValue(String.class);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        }
    }

    private void getsenderEmail() {

        if (offersVO.getBusinessID() != null) {

            mDatabase.getReference(Constants.CONSUMER_USER_MASTER)
                    .child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    senderEmail = dataSnapshot.child(Constants.EMAIL).getValue(String.class);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }

    }

    //go to feedback fragment
    private void gotoFeedback() {
        FragmentManager fm = getFragmentManager();

        Bundle bundle = new Bundle();
        bundle.putString(Constants.OFFER_ID, offersVO.getOfferID());

        FeedbackFragment feedbackFragment = new FeedbackFragment();
        feedbackFragment.setArguments(bundle);
        feedbackFragment.show(fm, "Sample Fragment");
    }

    private void goToEmail() {
        /*Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:" + offersVO.getEmail()));
        startActivity(intent);*/

        Timestamp offerStartDate = new Timestamp(Long.valueOf(offersVO.getOfferStartDate()) * 1000);

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:" + offersVO.getEmail()));
        intent.putExtra(Intent.EXTRA_SUBJECT, mContext.getString(R.string.offer_email_subject));
        intent.putExtra(Intent.EXTRA_TEXT, mContext.getString(R.string.offer_body) +
                "\n\n Offer Title - " + offersVO.getOfferTitle() + "\n\n Offer date - " + dateFormat.format(offerStartDate));
        startActivity(Intent.createChooser(intent, "Choose email app"));
    }

    private void goToMakeCall() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + offersVO.getPhoneNumber()));
        startActivity(intent);
    }


    private void getLatLongFromAddress(String companyAddress) {

        if (companyAddress != null &&
                !companyAddress.equalsIgnoreCase("")) {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

            try {
                List addressList = geocoder.getFromLocationName(companyAddress, 1);
                if (addressList != null && addressList.size() > 0) {
                    Address address = (Address) addressList.get(0);
                    LogUtils.LOG_E(TAG, address.getLatitude() + "");
                    LogUtils.LOG_E(TAG, address.getLongitude() + "");
                    companyLatitude = address.getLatitude();
                    companyLongitude = address.getLongitude();

                    //LogUtils.LOG_E(TAG,"CityName :"+address.getLocale());


                }
            } catch (IOException e) {
                LogUtils.LOG_E(TAG, "Unable to connect to Geocoder" + e);
            }
        }

    }

    private void getCurrentLatLong() {

        //if (Build.VERSION.SDK_INT < 23)
        //{
        // check if GPS enabled
        GPSTracker gps = new GPSTracker(mContext);
       /*
        if(isGPSEnabled()){
            gps.canGetLocation = true;
        }*/
        if (gps.canGetLocation()) {

            currentLatitude = gps.getLatitude();
            currentLongitude = gps.getLongitude();

            //showToast("Your Location is - \nLat: " + currentLatitude + "\nLong: " + currentLongitude);
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();

        }
        //}

    }

    public void showImageSlider(String imageUrl) {
        String imageList[] = {imageUrl};

        if (!imageUrl.isEmpty()) {
            new ImageViewer.Builder(mContext, imageList)
                    .setStartPosition(0)
                    .show();
        } else {
            showDialogAlertPositiveButton(getString(R.string.app_name), "Offer Images Not Found..");
        }
    }


}
