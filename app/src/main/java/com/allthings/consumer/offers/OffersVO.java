package com.allthings.consumer.offers;

import java.io.Serializable;

/**
 * Created by ABC on 6/9/2017.
 */

public class OffersVO implements Serializable {

    private String category = "", offerDiscount = "", email = "", offerDiscription = "", offerImageURL = "",
            offerStatus = "", offerTitle = "", offerType = "", phoneNumber = "", subCategory = "", businessID = "",city = "",
            offerID = "", companyName = "", website = "", companyAddress = "", gender = "", offerExpiryDate, offerStartDate, status;
    private long createdAt = 0, updatedAt = 0;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOfferDiscription() {
        return offerDiscription;
    }

    public void setOfferDiscription(String offerDiscription) {
        this.offerDiscription = offerDiscription;
    }

    public String getOfferExpiryDate() {
        return offerExpiryDate;
    }

    public void setOfferExpiryDate(Object offerExpiryDate) {
        this.offerExpiryDate = offerExpiryDate + "";
    }

    public String getOfferStartDate() {
        return offerStartDate;
    }

    public void setOfferStartDate(Object offerStartDate) {
        this.offerStartDate = offerStartDate + "";
    }

    public String getOfferStatus() {
        return offerStatus;
    }

    public void setOfferStatus(String offerStatus) {
        this.offerStatus = offerStatus;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status + "";
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }


    public String getOfferImageURL() {
        return offerImageURL;
    }

    public void setOfferImageURL(String offerImageURL) {
        this.offerImageURL = offerImageURL;
    }

    public String getOfferDiscount() {
        return offerDiscount;
    }

    public void setOfferDiscount(String offerDiscount) {
        this.offerDiscount = offerDiscount;
    }

    public String getOfferID() {
        return offerID;
    }

    public void setOfferID(String offerID) {
        this.offerID = offerID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public String toString() {
        return category + subCategory + offerDiscount + offerDiscription + offerExpiryDate + offerStartDate + offerTitle + companyName + offerType;
    }

    public String getBusinessID() {
        return businessID;
    }

    public void setBusinessID(String businessID) {
        this.businessID = businessID;
    }
}
