package com.allthings.consumer.offers;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.allthings.consumer.R;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.DateUtil;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.ViewHolder> {
    private ArrayList<OffersVO> offer;
    private Context context;
    private FragmentManager fragmentManager;
    private int lastPosition = -1;
    private String offerTypes;

    public OffersAdapter(Context context, ArrayList<OffersVO> offer, String offerTypes) {
        this.offer = offer;
        this.context = context;
        this.offerTypes = offerTypes;
    }

    @Override
    public OffersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_browseoffers_list, viewGroup, false);
        return new OffersAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder,int position) {
//        LogUtils.LOG_E("Adapter", "CreatedAt " + DateUtil.getDateFromString(offer.get(position).getCreatedAt()));
//        LogUtils.LOG_E("Adapter", "UpdatedAt " + DateUtil.getDateFromString(offer.get(position).getUpdatedAt()));
//        LogUtils.LOG_E("Adapter", "OfferTitle " + offer.get(position).getOfferTitle());
        holder.tvOfferDiscount.setText(offer.get(position).getOfferDiscount());
        holder.tvOfferCompanyName.setText(offer.get(position).getOfferTitle());

        if (SharedPreferenceUtil.contains(offer.get(position).getOfferID())) {
            // if(SharedPreferenceUtil.getBoolean(offer.get(position).getOfferID(),false)) {
            holder.ivOfferRemainder.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.reminder_btn_on));
            // }
        } else {
            holder.ivOfferRemainder.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.reminder_btn));
        }

        // Drawable drawable = ContextCompat.getDrawable(context,holder.ivOfferImageBackground);
        // int height = drawable.getIntrinsicHeight();
        if (!offer.get(position).getOfferImageURL().isEmpty()) {
            Picasso.with(context).load(offer.get(position).getOfferImageURL()).fit().into(holder.ivOfferImageBackground);
        }
       /* Picasso.with(mContext).load(String.valueOf(offersVO.getOfferImageURL())).placeholder
                (R.drawable.offer_details_placeholder).into(imageViewOffer);*/

      /*  Picasso.with(context).load(offer.get(position).getOfferImageURL()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Drawable drawable = new BitmapDrawable(context.getResources(), bitmap);


                holder.ivOfferImageBackground.setImageDrawable(drawable);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });*/

        holder.view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                MyOfferDetailsFragment myOfferDetailsFragment = new MyOfferDetailsFragment();

                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                fragmentManager = activity.getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                bundle.putSerializable(Constants.FROM_OFFERLIST, offer.get(holder.getAdapterPosition()));
                //LogUtils.LOG_E("Name",offer.get(holder.getAdapterPosition()).getOfferTitle());
                Log.e("OFFER ID", offer.get(holder.getAdapterPosition()).getOfferID());
                bundle.putString("OfferType", offerTypes);
                myOfferDetailsFragment.setArguments(bundle);

                InputMethodManager inputManager = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(new View(context).getWindowToken(), 0);
                //fragmentTransaction.replace(R.id.frameLayoutBaseContainer,offerDetailsFragment).addToBackStack(null).commitAllowingStateLoss();
                fragmentTransaction.replace(R.id.frameLayoutBaseContainer, myOfferDetailsFragment).addToBackStack(null).commitAllowingStateLoss();


            }
        });
    }

    @Override
    public int getItemCount() {
        return offer.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvOfferDiscount, tvOfferDiscountText, tvOfferCompanyName;
        private ImageView ivOfferImageBackground, ivOfferRemainder;
        View view1;

        public ViewHolder(View view) {
            super(view);
            view1 = view;
            tvOfferDiscount = (TextView) view.findViewById(R.id.tvDiscountPercentage);
            tvOfferDiscountText = (TextView) view.findViewById(R.id.tvDiscountText);
            tvOfferCompanyName = (TextView) view.findViewById(R.id.tvOfferCompanyName);
            ivOfferImageBackground = (ImageView) view.findViewById(R.id.ivOfferImageBackground);
            ivOfferRemainder = (ImageView) view.findViewById(R.id.ivOfferRemainder);

            Typeface tf = Typeface.createFromAsset(view.getContext().getAssets(),
                    "OpenSans-Semibold.ttf");
            tvOfferCompanyName.setTypeface(tf);
            tvOfferDiscount.setTypeface(tf);
            tvOfferDiscountText.setTypeface(tf);

            if (offerTypes.equals("ExpiryOffer")) {
                ivOfferRemainder.setVisibility(View.GONE);
            } else
                ivOfferRemainder.setVisibility(View.VISIBLE);

        }

    }
}