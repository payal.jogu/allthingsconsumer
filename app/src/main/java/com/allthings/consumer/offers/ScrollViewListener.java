package com.allthings.consumer.offers;

/**
 * Created by Vishal Dhaduk on 7/7/2017.
 */

public interface ScrollViewListener {
    void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy);
}
