package com.allthings.consumer;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Payal jogu on 23/8/17.
 */

public class AllThingsConsumerApplication extends Application {

    //private Context mContext;
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        //mContext = this;
        //SharedPreferenceUtil.init(this);
        FirebaseApp.initializeApp(this);
        //Fresco.initialize(this);
        MultiDex.install(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
