package com.allthings.consumer.register;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.R;
import com.allthings.consumer.filter.FilterVo;
import com.allthings.consumer.filter.SubCategoryDialog;
import com.allthings.consumer.offers.SelectedPosition;
import com.allthings.consumer.utils.LogUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ABC on 6/13/2017.
 */

public class CategoriesAdapter extends BaseExpandableListAdapter {
    // Define activity context
    private static final String TAG = "CategoriesAdapter";
    private Context mContext;
    private SelectedPosition selectedPosition;
    public SparseBooleanArray selectedItems0;
    public SparseBooleanArray selectedItems1;
    public SparseBooleanArray selectedItems2;

    /*
     * Here we have a Hashmap containing a String key
     * (can be Integer or other type but I was testing
     * with contacts so I used contact name as the key)
     */
    // private HashMap<String, List<String>> mListDataChild;
    private HashMap<String, List<FilterVo>> mListDataChild;
    // ArrayList that is what each key in the above
    // hashmap points to
    private List<String> mListDataGroup;

    // Hashmap for keeping track of our checkbox check states
    public HashMap<Integer, boolean[]> mChildCheckStates, mChildSelectStates;

    // Our getChildView & getGroupView use the viewholder patter
    // Here are the viewholders defined, the inner classes are
    // at the bottom
    private ChildViewHolder childViewHolder;
    private GroupViewHolder groupViewHolder;

    /*
     *  For the purpose of this document, I'm only using a single
     *	textview in the group (parent) and child, but you're limited only
     *	by your XML view for each group item :)
     */
    private String groupText;
    private String childText;

    /*  Here's the constructor we'll use to pass in our calling
     *  activity's context, group items, and child items
     */
    public CategoriesAdapter(Context context,
                             List<String> listDataGroup, HashMap<String, List<FilterVo>> listDataChild,
                             SelectedPosition selectedPosition) {

        mContext = context;
        mListDataGroup = listDataGroup;
        mListDataChild = listDataChild;
        this.selectedPosition = selectedPosition;

        // Initialize our hashmap containing our check states here
        mChildCheckStates = new HashMap<Integer, boolean[]>();
        mChildSelectStates = new HashMap<Integer, boolean[]>();

    }
   /* public CategoriesAdapter(Context context,
                             List<String> listDataGroup, HashMap<String, List<String>> listDataChild,
                             SelectedPosition selectedPosition) {

        mContext = context;
        mListDataGroup = listDataGroup;
        mListDataChild = listDataChild;
        this.selectedPosition=selectedPosition;
        selectedItems0=new SparseBooleanArray();
        selectedItems1=new SparseBooleanArray();
        selectedItems2=new SparseBooleanArray();

        // Initialize our hashmap containing our check states here
        mChildCheckStates = new HashMap<Integer, boolean[]>();
        mChildSelectStates=new HashMap<Integer,boolean[]>();

    }
*/

    @Override
    public int getGroupCount() {
        return mListDataGroup.size();
    }

    /*
     * This defaults to "public object getGroup" if you auto import the methods
     * I've always make a point to change it from "object" to whatever item
     * I passed through the constructor
     */
    @Override
    public String getGroup(int groupPosition) {
        return mListDataGroup.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        //  I passed a text string into an activity holding a getter/setter
        //  which I passed in through "ExpListGroupItems".
        //  Here is where I call the getter to get that text
        groupText = getGroup(groupPosition);

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.category_list_row, null);

            // Initialize the GroupViewHolder defined at the bottom of this document
            groupViewHolder = new GroupViewHolder();

            groupViewHolder.mGroupText = convertView.findViewById(R.id.tvCategoryName);

            convertView.setTag(groupViewHolder);
        } else {

            groupViewHolder = (GroupViewHolder) convertView.getTag();
        }

        // groupViewHolder.mGroupText.setText(groupText);
        groupViewHolder.mGroupText.setTypeface(null, Typeface.BOLD);
        groupViewHolder.mGroupText.setText(groupText);

        if (isExpanded) {
            groupViewHolder.mGroupText.setSelected(true);
            groupViewHolder.mGroupText.setBackground(ContextCompat.getDrawable(mContext, R.drawable.selected_edit_text));
            groupViewHolder.mGroupText.setTextColor(ContextCompat.getColor(mContext, R.color.colorRed));
        } else {
            groupViewHolder.mGroupText.setSelected(false);
            groupViewHolder.mGroupText.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
            groupViewHolder.mGroupText.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        }

        return convertView;
    }

    public void toggleSelection(SparseBooleanArray selectedItems, int pos) {
        Log.e(TAG, "Previous SelectedItems Pos :" + pos + " value : " + selectedItems.get(pos, false));
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        Log.e(TAG, "After SelectedItems Pos :" + pos + " value : " + selectedItems.get(pos, false));
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mListDataChild.get(mListDataGroup.get(groupPosition)).size();
    }

    /*
     * This defaults to "public object getChild" if you auto import the methods
     * I've always make a point to change it from "object" to whatever item
     * I passed through the constructor
     */
    @Override
    public String getChild(int groupPosition, int childPosition) {
        return mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).getName();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final int mGroupPosition = groupPosition;
        final int mChildPosition = childPosition;

        //  I passed a text string into an activity holding a getter/setter
        //  which I passed in through "ExpListChildItems".
        //  Here is where I call the getter to get that text
        childText = getChild(mGroupPosition, mChildPosition);

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.sub_category_list_row, null);

            childViewHolder = new ChildViewHolder();

            childViewHolder.mChildText = convertView.findViewById(R.id.tvSubCategoryName);
            childViewHolder.mCheckBox = convertView.findViewById(R.id.chkSelect);
            childViewHolder.imgInfo = convertView.findViewById(R.id.imgInfo);
            //childViewHolder.mCheckedTextView=(CheckedTextView)convertView.findViewById(R.id.chkTextView);
            childViewHolder.layoutChildParent = convertView.findViewById(R.id.layoutChildParent);

            convertView.setTag(R.layout.sub_category_list_row, childViewHolder);

        } else {

            childViewHolder = (ChildViewHolder) convertView
                    .getTag(R.layout.sub_category_list_row);
        }

        childViewHolder.mChildText.setText(childText);
        //childViewHolder.mCheckedTextView.setText(childText);
        //childViewHolder.mCheckBox.setText(childText);


        /*
         * You have to set the onCheckChangedListener to null
         * before restoring check states because each call to
         * "setChecked" is accompanied by a call to the
         * onCheckChangedListener
         */
        childViewHolder.mCheckBox.setOnCheckedChangeListener(null);

        boolean isSelected = mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).isSelectedItem();

        if (isSelected) {
            childViewHolder.mChildText.setTextColor(ContextCompat.getColor(mContext, R.color.colorRed));
        } else {
            childViewHolder.mChildText.setTextColor(ContextCompat.getColor(mContext, R.color.colorGrey));
        }

        String name = mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).getName();
        childViewHolder.mCheckBox.setChecked(isSelected);

        String categoryName = name;
        categoryName = String.valueOf(categoryName.charAt(0)).toUpperCase() + categoryName.subSequence(1, categoryName.length());
        childViewHolder.mChildText.setText(categoryName);


        childViewHolder.mChildText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterVo filterVo = mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition);
                //mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).setSelectedItem(isChecked);
                if (!filterVo.isSelectedItem()) {
                    selectedPosition.onItemClicked(mGroupPosition, mChildPosition, true, filterVo);
                } else {
                    selectedPosition.onItemClicked(mGroupPosition, mChildPosition, false, filterVo);
                }

            }
        });

        childViewHolder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                FilterVo filterVo = mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition);
                //mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).setSelectedItem(isChecked);

                selectedPosition.onItemClicked(mGroupPosition, mChildPosition, isChecked, filterVo);

            }
        });

        childViewHolder.imgInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(mContext,"Category",Toast.LENGTH_SHORT).show();

                SubCategoryDialog dialogFragment = new SubCategoryDialog();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition));
                dialogFragment.setArguments(bundle);
                dialogFragment.show(((BaseActivity) mContext).getFragmentManager(), "dialog");
            }
        });

        //childViewHolder.mCheckBox.setOnClickListener(null);
        //childViewHolder.mCheckedTextView.setOnClickListener(null);

        //setSelectedCategory();

        //  childViewHolder.mChildText.setOnClickListener(null);


      /*  if (mChildCheckStates.containsKey(mGroupPosition)) {

            boolean getChecked[] = mChildCheckStates.get(mGroupPosition);

            // set the check state of this position's checkbox based on the
            // boolean value of getChecked[position]
            childViewHolder.mCheckBox.setChecked(getChecked[mChildPosition]);
           // childViewHolder.mCheckedTextView.setChecked(getChecked[mChildPosition]);
            childViewHolder.mChildText.setSelected(getChecked[mChildPosition]);



        } else {

			*//*
         * if the hashmap mChildCheckStates<Integer, Boolean[]> does not
         * contain the value of the parent view (group) of this child (aka, the key),
         * (aka, the key), then initialize getChecked[] as a new boolean array
         *  and set it's size to the total number of children associated with
         *  the parent group
         *//*
            boolean getChecked[] = new boolean[getChildrenCount(mGroupPosition)];

            // add getChecked[] to the mChildCheckStates hashmap using mGroupPosition as the key
            mChildCheckStates.put(mGroupPosition, getChecked);

            // set the check state of this position's checkbox based on the
            // boolean value of getChecked[position]
            childViewHolder.mCheckBox.setChecked(false);
            childViewHolder.mChildText.setSelected(false);
        }
        if(childViewHolder.mChildText.isSelected()){
            childViewHolder.mChildText.setTextColor(ContextCompat.getColor(mContext,R.color.colorRed));
        }else{
            childViewHolder.mChildText.setTextColor(ContextCompat.getColor(mContext,R.color.gray));
        }
        childViewHolder.mChildText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.isSelected()) {

                    boolean getChecked[] = mChildCheckStates.get(mGroupPosition);
                    getChecked[mChildPosition] = view.isSelected();
                    mChildCheckStates.put(mGroupPosition, getChecked);
                    // childViewHolder.mCheckBox.setTextColor(R.color.colorRed);


                    // String s=getChild(mGroupPosition,mChildPosition).toString();
                    if (mGroupPosition==0)
                    {
                        toggleSelection(selectedItems0,mChildPosition);

                    }
                    else if (mGroupPosition==1 )
                    {
                        toggleSelection(selectedItems1,mChildPosition);
                    }
                    else if (mGroupPosition ==2)
                    {
                        toggleSelection(selectedItems2,mChildPosition);
                    }
                    selectedPosition.onItemClicked(mGroupPosition,mChildPosition,true,null);


                } else {

                    boolean getChecked[] = mChildCheckStates.get(mGroupPosition);
                    getChecked[mChildPosition] = view.isSelected();
                    mChildCheckStates.put(mGroupPosition, getChecked);
                    //childViewHolder.mCheckBox.setTextColor(R.color.dark_black);

                    if (mGroupPosition==0)
                    {
                        toggleSelection(selectedItems0,mChildPosition);
                    }
                    else if (mGroupPosition==1 )
                    {
                        toggleSelection(selectedItems1,mChildPosition);
                    }
                    else if (mGroupPosition ==2)
                    {
                        toggleSelection(selectedItems2,mChildPosition);
                    }
                    selectedPosition.onItemClicked(mGroupPosition,mChildPosition,false,null);


                }


            }
        });
        childViewHolder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    boolean getChecked[] = mChildCheckStates.get(mGroupPosition);
                    getChecked[mChildPosition] = isChecked;
                    mChildCheckStates.put(mGroupPosition, getChecked);
                       // childViewHolder.mCheckBox.setTextColor(R.color.colorRed);


                        // String s=getChild(mGroupPosition,mChildPosition).toString();
                    if (mGroupPosition==0)
                    {
                        toggleSelection(selectedItems0,mChildPosition);

                    }
                    else if (mGroupPosition==1 )
                    {
                        toggleSelection(selectedItems1,mChildPosition);
                    }
                    else if (mGroupPosition ==2)
                    {
                        toggleSelection(selectedItems2,mChildPosition);
                    }
                    selectedPosition.onItemClicked(mGroupPosition,mChildPosition,true,null);


                } else {

                    boolean getChecked[] = mChildCheckStates.get(mGroupPosition);
                    getChecked[mChildPosition] = isChecked;
                    mChildCheckStates.put(mGroupPosition, getChecked);
                        //childViewHolder.mCheckBox.setTextColor(R.color.dark_black);

                    if (mGroupPosition==0)
                    {
                        toggleSelection(selectedItems0,mChildPosition);
                    }
                    else if (mGroupPosition==1 )
                    {
                        toggleSelection(selectedItems1,mChildPosition);
                    }
                    else if (mGroupPosition ==2)
                    {
                        toggleSelection(selectedItems2,mChildPosition);
                    }
                    selectedPosition.onItemClicked(mGroupPosition,mChildPosition,false,null);


                }

            }
        });*/
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    public final class GroupViewHolder {

        TextView mGroupText;
    }

    public final class ChildViewHolder {

        TextView mChildText;
        CheckBox mCheckBox;
        RelativeLayout layoutChildParent;
        ImageView imgInfo;
    }
}


