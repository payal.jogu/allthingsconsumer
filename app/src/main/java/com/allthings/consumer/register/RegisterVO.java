package com.allthings.consumer.register;

import com.google.firebase.database.ServerValue;

import java.io.Serializable;

/**
 * Created by ABC on 6/12/2017.
 */

public class RegisterVO implements Serializable{

    private String Country="";
    private String county = "";
    private String Location="";
    private String Email="";
    private String PhoneNo="";
    private String profileImageURL="";
    private String Latitude="";
    private String Longitude="";
    private String CreateBy="";
    private String UpdateBy="";
    private String status="";
    private String consumerID="";
    private String city="";

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getProfileImageURL() {
        return profileImageURL;
    }

    public void setProfileImageURL(String profileImageURL) {
        this.profileImageURL = profileImageURL;
    }

    private String gender="";

    private Object CreateDate = ServerValue.TIMESTAMP,UpdateDate= ServerValue.TIMESTAMP;

    public RegisterVO() {
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String createBy) {
        CreateBy = createBy;
    }

    public String getUpdateBy() {
        return UpdateBy;
    }

    public void setUpdateBy(String updateBy) {
        UpdateBy = updateBy;
    }

    public Object getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(Object createDate) {
        CreateDate = createDate;
    }

    public Object getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(Object updateDate) {
        UpdateDate = updateDate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getConsumerID() {
        return consumerID;
    }

    public void setConsumerID(String consumerID) {
        this.consumerID = consumerID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

}
