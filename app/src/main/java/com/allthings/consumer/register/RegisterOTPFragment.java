package com.allthings.consumer.register;

/**
 * Created by ABC on 6/8/2017.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

/**
 * Created by ABC on 6/8/2017.
 */

public class RegisterOTPFragment extends BaseFragment implements View.OnClickListener {

    private final static String TAG = "RegisterOTPFragment";
    private View rootView;
    private TextView tvPleaseVerify, tvForMobileVerificaion, tvSendOtp, tvTimer, tvSixDigit;
    private Button btnVerify;
    private EditText etSendOtp;
    RegisterVO registerVO;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    String mVerificationId = "", token1 = "";
    DatabaseReference tbRegister;
    Uri downloadUrl;
    int count;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_register_otp, container, false);
            init();
            sendOTP();
            disableBack(rootView);
            setListeners();
            setFontStyle();

            return rootView;
        } else {
            return rootView;
        }

    }

    private void setFontStyle() {

        setTypeFaceButton(btnVerify, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceEdittext(etSendOtp, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvForMobileVerificaion, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvPleaseVerify, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvSendOtp, mContext.getString(R.string.fontOpenSansBold));
        setTypeFaceTextView(tvTimer, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvSixDigit, mContext.getString(R.string.fontOpenSansSemiBold));

    }

    private void setListeners() {
        btnVerify.setOnClickListener(this);
        tvSendOtp.setOnClickListener(this);

       /* etSendOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.e(TAG,"onText Changedc"+charSequence+"i"+i+"i1"+i1+"i2"+i2);
                count = 0;
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.e(TAG,"onText Changed c"+charSequence+"i"+i+"i1"+i1+"count"+count);

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.e(TAG,"e"+editable);
                if(editable.length() == 3){
                    etSendOtp.setText(etSendOtp.getText().toString()+" ");
                    etSendOtp.setSelection(etSendOtp.getText().length());
                }
            }
        });*/

    }

    @Override
    public void onResume() {
        super.onResume();
        setToolbarSetting();
    }

    private void setToolbarSetting() {
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).setToolbarTitle(mContext.getString(R.string.register));
        ((BaseActivity) mContext).showHideToolbarBackButton(false);
    }

    private void init() {

        btnVerify = (Button) rootView.findViewById(R.id.btn_register3_verify);
        tvForMobileVerificaion = (TextView) rootView.findViewById(R.id.tvForMobileVerification);
        tvPleaseVerify = (TextView) rootView.findViewById(R.id.tvPleaseVerifyMobile);
        tvSendOtp = (TextView) rootView.findViewById(R.id.tvSendOTPRegister);
        tvTimer = (TextView) rootView.findViewById(R.id.tvTimer);
        tvSixDigit = (TextView) rootView.findViewById(R.id.tvEnterSixDigit);
        etSendOtp = (EditText) rootView.findViewById(R.id.etRegisterOTP);
        tbRegister = mDatabase.getReference().child(Constants.CONSUMER_USER_MASTER);
        registerVO = (RegisterVO) getArguments().getSerializable(
                "REGISTER1");
        tvPleaseVerify.setText(mContext.getString(R.string.l_verify) + " " + registerVO.getPhoneNo());


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register3_verify:

                if (isValidDetails()) {
                    getVerifyOtp();
                }

                // gotoRegister4();
                break;
            case R.id.tvSendOTPRegister:
                if (tvTimer.getText().equals(mContext.getString(R.string.timer))) {
                    sendOTP(); //Send OTP
                }
                break;


        }

    }

    //Validations
    private boolean isValidDetails() {
        if (etSendOtp.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_otp));
            etSendOtp.requestFocus();
            return false;
        }
        return true;
    }

    private void sendOTP() {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            //Log.d(TAG, "Phone number:" + registerVO.getPhoneNo());
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    registerVO.getPhoneNo(),// Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    getActivity(),               // Activity (for callback binding)
                    mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                        @Override
                        public void onVerificationCompleted(final PhoneAuthCredential credential) {
                            progressDialog.dismiss();
                            // This callback will be invoked in two situations:
                            // 1 - Instant verification. In some cases the phone number can be instantly
                            //     verified without needing to send or enter a verification code.
                            // 2 - Auto-retrieval. On some devices Google Play services can automatically
                            //     detect the incoming verification SMS and perform verificaiton without
                            //     user action.
                            Log.d(TAG, "onVerificationCompleted:" + credential);

                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setMessage("Instant Mobile No. Verification Completed")
                                    .setCancelable(false)
                                    .setTitle(getString(R.string.app_name))
                                    .setPositiveButton(mContext.getString(R.string.ok),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                    signInWithPhoneAuthCredential(credential);
                                                }
                                            });

                            AlertDialog alert = builder.create();
                            alert.show();


                        }

                        @Override
                        public void onVerificationFailed(FirebaseException e) {
                            // This callback is invoked in an invalid request for verification is made,
                            // for instance if the the phone number format is not valid.
                            progressDialog.dismiss();
                            Log.e(TAG, "onVerificationFailed" + e.getMessage().toString());

                            // showToast(e.getMessage());

                          /*  if (e instanceof FirebaseAuthInvalidCredentialsException) {
                                // Invalid request
                                showToast(e.getMessage());
                            } else if (e instanceof FirebaseTooManyRequestsException) {
                                // The SMS quota for the project has been exceeded
                                showToast(e.getMessage());
                            }*/

                            // Show a message and update the UI
                            // ...
                        }

                        @Override
                        public void onCodeSent(String verificationId,
                                               PhoneAuthProvider.ForceResendingToken token) {
                            progressDialog.dismiss();
                            // The SMS verification code has been sent to the provided phone number, we
                            // now need to ask the user to enter the code and then construct a credential
                            // by combining the code with a verification ID.
                            Log.d(TAG, "onCodeSent:" + verificationId);
                            // showToast("Code sent");

                            // Save verification ID and resending token so we can use them later
                            mVerificationId = verificationId;
                            token1 = String.valueOf(token);

                            enableDisableOtpField(true);

                            // ...
                            new CountDownTimer(120000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    //tvTimer.setText("00:"+ millisUntilFinished / 1000);
                                    tvTimer.setText("" + String.format("%02d:%02d",
                                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                                    //here you can have your logic to set text to edittext
                                }

                                public void onFinish() {
                                    etSendOtp.setText("");
                                    enableDisableOtpField(false);
                                    tvTimer.setText(mContext.getString(R.string.timer));
                                }

                            }.start();
                        }

                        @Override
                        public void onCodeAutoRetrievalTimeOut(String s) {
                            super.onCodeAutoRetrievalTimeOut(s);
                            showToast(s);
                        }
                    }
            );
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    private void enableDisableOtpField(boolean status){
        if(status){
            etSendOtp.setEnabled(true);
            btnVerify.setEnabled(true);
        }else {
            etSendOtp.setEnabled(false);
            btnVerify.setEnabled(false);
        }
    }

    private void getVerifyOtp() {
        if (NetworkUtil.isOnline(mContext)) {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, etSendOtp.getText().toString().trim());
            signInWithPhoneAuthCredential(credential);
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressDialog.dismiss();
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.e(TAG, "signInWithCredential:success");

                                FirebaseUser user = task.getResult().getUser();

                                SharedPreferenceUtil.putValue(Constants.USER_ID, user.getUid());
                                SharedPreferenceUtil.save();



                                registerVO.setConsumerID(SharedPreferenceUtil.getString(Constants.USER_ID, ""));
                                registerVO.setCreateBy(SharedPreferenceUtil.getString(Constants.USER_ID, ""));
                                registerVO.setUpdateBy(SharedPreferenceUtil.getString(Constants.USER_ID, ""));
                                registerVO.setStatus(Constants.STATUS_VALUE);

                                //Save image with data
                                if (!registerVO.getProfileImageURL().equals("")) {
                                    sendRequestForRegistartionWithImage();
                                    gotoRegister4();
                                } else {
                                    //Save data without image to firebase
                                    sendRequestForRegistartionWithoutIamge();
                                    gotoRegister4();
                                }
                            } else {
                                // Sign in failed, display a message and update the UI
                                Log.w(TAG, "signInWithCredential:failure", task.getException());
                                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                    // The verification code entered was invalid
                                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_valid_otp));
                                }
                            }
                        }
                    });
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }

    //Save data with image to firebase
    private void sendRequestForRegistartionWithImage() {

        if (NetworkUtil.isOnline(mContext)) {

            final StorageReference storageReference = mStorage.getReference(Constants.CONSUMER_USER_MASTER)
                    .child(SharedPreferenceUtil.getString(Constants.USER_ID, ""));
            InputStream stream = null;
            try {
                stream = new FileInputStream(new File(registerVO.getProfileImageURL()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            UploadTask uploadTask = storageReference.putStream(stream);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            downloadUrl = uri;
                            LogUtils.LOG_E("Download Url:", String.valueOf(uri));
                            registerVO.setProfileImageURL(String.valueOf(uri));
                            //LogUtils.LOG_E("Download Url Org:", registerVO.getProfileImageUrl());
                            sendRequestForRegistartionWithoutIamge();
                        }
                    });
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.

                }
            });

        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    private void sendRequestForRegistartionWithoutIamge() {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            registerVO.setStatus(Constants.STATUS_VALUE);
            tbRegister.child(SharedPreferenceUtil.getString(Constants.USER_ID, ""))
                    .setValue(registerVO).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    tbRegister.child(SharedPreferenceUtil.getString(Constants.USER_ID, ""))
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    progressDialog.dismiss();
                                    SharedPreferenceUtil.putValue(Constants.EMAIL, dataSnapshot.child(Constants.EMAIL).getValue(String.class));
                                    if (dataSnapshot.hasChild(Constants.PHONENUMBER)) {
                                        SharedPreferenceUtil.putValue(Constants.PHONENUMBER, dataSnapshot.child(Constants.PHONENUMBER).getValue(String.class).substring(3));
                                        SharedPreferenceUtil.putValue(Constants.COUNTRY_CODE, dataSnapshot.child(Constants.PHONENUMBER).getValue(String.class).substring(0, 3));
                                    }
                                    SharedPreferenceUtil.putValue(Constants.CITY, dataSnapshot.child(Constants.CITY).getValue(String.class));
                                    SharedPreferenceUtil.putValue(Constants.COUNTRY, dataSnapshot.child(Constants.COUNTRY).getValue(String.class));
                                    SharedPreferenceUtil.putValue(Constants.COUNTY, dataSnapshot.child(Constants.COUNTY).getValue(String.class));
                                    SharedPreferenceUtil.putValue(Constants.LOCATION, dataSnapshot.child(Constants.LOCATION).getValue(String.class));
                                    SharedPreferenceUtil.putValue(Constants.PROFILEPHOTOURL, dataSnapshot.child(Constants.PROFILEPHOTOURL).getValue(String.class));
                                    SharedPreferenceUtil.putValue(Constants.GENDER, dataSnapshot.child(Constants.GENDER).getValue(String.class));
                                    SharedPreferenceUtil.save();

                                    ((BaseActivity)mContext).getFirebaseTokenForFCM();
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    progressDialog.dismiss();

                                }
                            });

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.problem_to_database));
                }
            });
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }


    //Go to the next screen
    private void gotoRegister4() {

        hideKeyboard();

        Bundle bundle = new Bundle();
        RegisterVerificationFragment registerFragmet4 = new RegisterVerificationFragment();
        bundle.putSerializable("REGISTER1", registerVO);
        registerFragmet4.setArguments(bundle);

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, registerFragmet4).addToBackStack(null).commit();
    }
}