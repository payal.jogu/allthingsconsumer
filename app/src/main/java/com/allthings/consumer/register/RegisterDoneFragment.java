package com.allthings.consumer.register;

/**
 * Created by ABC on 6/8/2017.
 */

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.offers.BrowseOffersFragment;



public class RegisterDoneFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;
    private TextView tvCongratulations,tvRegisterComplete;
    private Button btnGetStarted;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_register_done, container, false);
            init();
            setListeners();
            setFontStyle();
            disableBack(rootView);
            return rootView;
        } else {
            return rootView;
        }

    }

    private void setFontStyle() {
        setTypeFaceButton(btnGetStarted,mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvCongratulations,mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvRegisterComplete,mContext.getString(R.string.fontOpenSansSemiBold));


    }

    private void setListeners() {
        btnGetStarted.setOnClickListener(this);
    }

    //Set toolbar settings
    @Override
    public void onResume() {
        super.onResume();

        setToolbarSettings();
    }
    //Set toolbar settings
    private void setToolbarSettings() {
        ((BaseActivity)mContext).showToolbar(true);
        ((BaseActivity)mContext).setToolbarTitle(mContext.getString(R.string.register));
        ((BaseActivity)mContext).showHideToolbarBackButton(false);
    }

    private void init() {

        btnGetStarted=(Button)rootView.findViewById(R.id.btn_getstarted);
        tvCongratulations=(TextView)rootView.findViewById(R.id.tvCongratulations);
        tvRegisterComplete=(TextView)rootView.findViewById(R.id.tvRegisterSuccess);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_getstarted:
                gotoBrowseOffers();
                break;
        }

    }

    //Go to the next screen
    private void gotoBrowseOffers() {
        hideKeyboard();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
        ((BaseActivity)mContext).setSelectedItem(0);
        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new BrowseOffersFragment()).commit();
        //clearBackStack();
    }
}