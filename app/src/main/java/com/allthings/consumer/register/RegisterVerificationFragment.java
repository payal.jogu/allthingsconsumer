package com.allthings.consumer.register;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


/**
 * Created by ABC on 6/8/2017.
 */

public class RegisterVerificationFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;
    private TextView tvMobileVerificationDone,tvEmailVerification;
    private Button btnContinue;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_register_verification, container, false);
            init();
            setListeners();
            setFontStyle();

            return rootView;
        } else {
            return rootView;
        }

    }

    private void setFontStyle() {

        setTypeFaceTextView(tvEmailVerification,mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvMobileVerificationDone,mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceButton(btnContinue,mContext.getString(R.string.fontOpenSansSemiBold));

    }

    @Override
    public void onResume() {
        super.onResume();
        setToolbarSetting();
    }

    private void setToolbarSetting() {
        disableBack(rootView);
        ((BaseActivity)mContext).showToolbar(true);
        ((BaseActivity)mContext).setToolbarTitle(mContext.getString(R.string.register));
        ((BaseActivity)mContext).showHideToolbarBackButton(false);
    }

    private void setListeners() {
        btnContinue.setOnClickListener(this);
    }

    private void init() {
        btnContinue=(Button)rootView.findViewById(R.id.btn_register4_continue);
        tvEmailVerification=(TextView)rootView.findViewById(R.id.tvEmailVerification);
        tvMobileVerificationDone=(TextView)rootView.findViewById(R.id.tvMobileVerificationDone);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register4_continue:
                gotoRegister5();
                break;
        }

    }

    //Go to the next screen
    private void gotoRegister5() {

        if(NetworkUtil.isOnline(mContext)) {
            progressDialog.show();

            mDatabase.getReference().child(Constants.CONSUMER_USER_MASTER).child(SharedPreferenceUtil.getString(Constants.USER_ID,""))
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    progressDialog.dismiss();
                    if(dataSnapshot.getValue() != null)
                    {
                        Bundle bundle = new Bundle();
                        RegisterCategoryFragment register6 = new RegisterCategoryFragment();
                        hideKeyboard();
                        RegisterVO registerVO = new RegisterVO();
                        registerVO.setConsumerID(SharedPreferenceUtil.getString(Constants.USER_ID,""));

                        registerVO.setCountry(dataSnapshot.child(Constants.COUNTRY).getValue(String.class));
                        registerVO.setCounty(dataSnapshot.child(Constants.COUNTY).getValue(String.class));

                        registerVO.setEmail(dataSnapshot.child(Constants.EMAIL).getValue(String.class));

                        registerVO.setLatitude(dataSnapshot.child(Constants.LATITUDE).getValue(String.class));
                        registerVO.setLongitude(dataSnapshot.child(Constants.LONGITUDE).getValue(String.class));

                        registerVO.setLocation(dataSnapshot.child(Constants.LOCATION).getValue(String.class));

                        registerVO.setPhoneNo(dataSnapshot.child(Constants.PHONENUMBER).getValue(String.class));

                        registerVO.setProfileImageURL(dataSnapshot.child(Constants.PROFILEPHOTOURL).getValue(String.class));

                        registerVO.setCreateBy(dataSnapshot.child(Constants.CREATED_BY).getValue(String.class));
                        registerVO.setCreateDate(dataSnapshot.child(Constants.CREATED_DATE).getValue());

                        registerVO.setUpdateBy(dataSnapshot.child(Constants.UPDATED_BY).getValue(String.class));
                        registerVO.setUpdateDate(dataSnapshot.child(Constants.UPDATED_DATE).getValue());

                        registerVO.setCity(dataSnapshot.child(Constants.CITY).getValue(String.class));

                        registerVO.setGender(dataSnapshot.child(Constants.GENDER).getValue(String.class));

                        registerVO.setStatus(Constants.STATUS_VALUE);

                        SharedPreferenceUtil.putValue(Constants.EMAIL,dataSnapshot.child(Constants.EMAIL).getValue(String.class));
                        SharedPreferenceUtil.putValue(Constants.PHONENUMBER,dataSnapshot.child(Constants.PHONENUMBER).getValue(String.class).substring(3));
                        SharedPreferenceUtil.putValue(Constants.COUNTRY_CODE,dataSnapshot.child(Constants.PHONENUMBER).getValue(String.class).substring(0, 3));
                        SharedPreferenceUtil.putValue(Constants.COUNTRY,dataSnapshot.child(Constants.COUNTRY).getValue(String.class));
                        SharedPreferenceUtil.putValue(Constants.COUNTY,dataSnapshot.child(Constants.COUNTY).getValue(String.class));
                        SharedPreferenceUtil.putValue(Constants.LOCATION,dataSnapshot.child(Constants.LOCATION).getValue(String.class));
                        SharedPreferenceUtil.putValue(Constants.PROFILEPHOTOURL,dataSnapshot.child(Constants.PROFILEPHOTOURL).getValue(String.class));
                        SharedPreferenceUtil.putValue(Constants.GENDER,dataSnapshot.child(Constants.GENDER).getValue(String.class));
                        SharedPreferenceUtil.save();

                        bundle.putSerializable("REGISTER1", registerVO);

                        register6.setArguments(bundle);

                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, register6).commit();

                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    progressDialog.dismiss();

                }
            });

        }
        else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings),mContext.getString(R.string.please_check_network));
        }


    }
}