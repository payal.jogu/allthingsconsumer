package com.allthings.consumer.register;

/**
 * Created by ABC on 6/8/2017.
 */

import android.content.Intent;
import android.graphics.Paint;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.ListPopupWindow;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.adapters.PlaceAutoCompleteAdapter;
import com.allthings.consumer.model.CountryDetailsVo;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.GeocoderApi;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.OnResultInterface;
import com.allthings.consumer.utils.SwipeGesture;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class RegisterBasicDetailsFragment extends BaseFragment implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        View.OnFocusChangeListener, OnResultInterface {

    private static final String TAG = "RegisterBasicFragment";
    private View rootView;
    // private AutoCompleteTextView autoCompleteTextViewSelectLocation;
    private EditText etEmail, etPassword, etConfirmPassword, etPhonenumber, etCountry, etSelectRegion,
            etLocationRegister, etCity;
    private TextView tvSelectCountry, tvSelectLocation, tvEmail, tvPassword, tvConfirmPassword,
            tvPhonenumber, tvCountryCode, tvRegisterSwipeLable, tvSelectRegion, tvSelectGender, tvCity,
            text_terms_condition,text_agree_statement;
    private Button btnSwipe;
    private RelativeLayout relativeRegisterSwipeNext;
    private PlaceAutoCompleteAdapter mAdapter;
    private ArrayList<String> countryList, countyName;
    private GoogleApiClient mGoogleApiClient;
    private double Longitude, Latitude;
    private Geocoder geocoder;
    private DatabaseReference mRefrenceToCountry;
    ArrayAdapter<String> dataAdapter;
    private ArrayList<CountryDetailsVo> countryDetailsList;
    private String selectedCountryName, cityName = "", gender = "";
    private CountryDetailsVo countryObject;
    private double latitude, longitude;
    private RadioButton rbMale, rbFemale;
    private boolean isSelectedText = false;
    private int PLACE_PICKER_REQUEST = 1;
    private CheckBox checkbox_terms_condition;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_register_basicdetails, container, false);
            init();
            getCountryList();
            setListeners();
            setFontStyle();
            return rootView;
        } else {
            return rootView;
        }

    }

    private void init() {
        etCountry = rootView.findViewById(R.id.etSelectCountry);
        etSelectRegion = rootView.findViewById(R.id.etSelectRegion);
        //autoCompleteTextViewSelectLocation=(AutoCompleteTextView) rootView.findViewById(R.id.etLocationRegister);
        etLocationRegister = rootView.findViewById(R.id.etLocationRegister);
        etEmail = rootView.findViewById(R.id.etEmail);
        etPassword = rootView.findViewById(R.id.etPassword);
        etConfirmPassword = rootView.findViewById(R.id.etCPassword);
        etPhonenumber = rootView.findViewById(R.id.etPhonenumber);
        etCity = rootView.findViewById(R.id.etCity);

        if (Build.VERSION.SDK_INT >= 21) {
            etCountry.setShowSoftInputOnFocus(false);
            etSelectRegion.setShowSoftInputOnFocus(false);
        }

        tvSelectCountry = rootView.findViewById(R.id.tvSelectCountry);
        tvSelectRegion = rootView.findViewById(R.id.tvSelectRegion);
        tvSelectLocation = rootView.findViewById(R.id.tvLocation);
        tvEmail = rootView.findViewById(R.id.tvEmail);
        tvPassword = rootView.findViewById(R.id.tvPassword);
        tvConfirmPassword = rootView.findViewById(R.id.tvCPassword);
        tvPhonenumber = rootView.findViewById(R.id.tvPhonenumber);
        tvCountryCode = rootView.findViewById(R.id.tvCountryCode);
        tvRegisterSwipeLable = rootView.findViewById(R.id.tvRegisterSwipeLable);
        tvSelectGender = rootView.findViewById(R.id.tvSelectGender);
        tvCity = rootView.findViewById(R.id.tvCity);

        rbMale = rootView.findViewById(R.id.rbMale);
        rbFemale = rootView.findViewById(R.id.rbFemale);

        gender = rbMale.getText().toString().trim();

        btnSwipe = rootView.findViewById(R.id.btn_register1_swipe);

        relativeRegisterSwipeNext = rootView.findViewById(R.id.relativeRegisterSwipeNext);

        mRefrenceToCountry = mDatabase.getReference(Constants.COUNTRY_MASTER);

        countryList = new ArrayList<>();
        countryDetailsList = new ArrayList<>();
        countyName = new ArrayList<>();

        checkbox_terms_condition = rootView.findViewById(R.id.checkbox_terms_condition);
        text_terms_condition = rootView.findViewById(R.id.text_terms_condition);
        text_terms_condition.setPaintFlags(text_terms_condition.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        text_agree_statement = rootView.findViewById(R.id.text_agree_statement);

        dataAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, countryList);

        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        // autoCompleteTextViewSelectLocation.setThreshold(2);
    }

    //Get Country List From Firebase
    private void getCountryList() {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            mRefrenceToCountry.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    progressDialog.dismiss();
                    if (dataSnapshot.getValue() != null) {
                        countryList.clear();
                        countryDetailsList.clear();

                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            CountryDetailsVo country = child.getValue(CountryDetailsVo.class);
                            if (country != null && country.getStatus() != null &&
                                    country.getStatus().equals(Constants.STATUS_VALUE)) {
                                countryDetailsList.add(country);
                                countryList.add(child.getKey());
                            }
                            //LogUtils.LOG_E("STATUS",child.child("status").getValue().toString());
                        }
                        dataAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    progressDialog.dismiss();
                    LogUtils.LOG_E(TAG, databaseError.getMessage());

                }
            });
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    //get County list from database
    private void getCountyList() {
        if (NetworkUtil.isOnline(mContext)) {
            if (etCountry.getText().toString().trim().length() > 0) {
                mRefrenceToCountry.child(etCountry.getText().toString()).child(Constants.COUNTY_MASTER).
                        addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.getValue() != null) {
                                    countyName.clear();
                                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                                        if (child.hasChild(Constants.STATUS)) {
                                            if (child.child(Constants.STATUS).getValue().equals(Constants.STATUS_VALUE)) {
                                                countyName.add(child.getKey());
                                                LogUtils.LOG_I(TAG, child.getKey());
                                            }

                                        }
                                    }
                                    setCounty();
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                LogUtils.LOG_E(TAG, databaseError.getMessage());
                            }
                        });

            }
        } else {
            showDialogAlertPositiveButton(getString(R.string.app_name), getString(R.string.please_check_network));
        }

    }

    private void setCounty() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1,
                countyName);

        if(getContext() != null) {
            final ListPopupWindow lpw = new ListPopupWindow(getContext());
            lpw.setAdapter(dataAdapter);
            lpw.setAnchorView(etSelectRegion);
            lpw.setModal(true);
            lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    String item = countyName.get(position);
                    etSelectRegion.setText(item);

//                autoCompleteTextViewSelectLocation.setText("");
//                autoCompleteTextViewSelectLocation.setHint(getString(R.string.hint_location));
                    lpw.dismiss();
                }
            });
            lpw.show();
        }
    }

    //Open Country Select Dialog
    private void showCountryDialog() {
        if (NetworkUtil.isOnline(mContext)) {
            final ListPopupWindow lpw = new ListPopupWindow(mContext);
            lpw.setAdapter(dataAdapter);
            lpw.setAnchorView(etCountry);
            lpw.setModal(true);
            lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    String item = countryList.get(position);
                    etCountry.setText(item);

                    selectedCountryName = item;
                    countryObject = countryDetailsList.get(position);
                    tvCountryCode.setText(countryObject.getCountryCode());

                    etSelectRegion.setText("");
                    etSelectRegion.setHint(getString(R.string.hint_select_region));

                    //  LogUtils.LOG_E("Selected Country Length",countryObject.getMaxPhoneLength());
                    //getCountyList();
                    //autoCompleteTextViewSelectLocation.setEnabled(true);

                    setAutoCompleteCountryAddress();
                    lpw.dismiss();
                }
            });
            lpw.show();

        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    //Set The Address Of Country
    private void setAutoCompleteCountryAddress() {

        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            geocoder = new Geocoder(getActivity(), Locale.getDefault());

            if(countryObject.getCountryName() != null &&
                    !countryObject.getCountryName().equalsIgnoreCase("")) {
                try {
                    List addressList = geocoder.getFromLocationName(countryObject.getCountryName(), 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = (Address) addressList.get(0);
                        setBounds(address.getLatitude(), address.getLongitude());
                        progressDialog.dismiss();
                    }
                } catch (IOException e) {
                    // LogUtils.LOG_E(TAG, "Unable to connect Geocoder"+ e);
                    progressDialog.dismiss();
                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.unable_to_connect_geocoder));
                }
            }
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }

    private void setListeners() {
        btnSwipe.setOnClickListener(this);
        etCountry.setOnClickListener(this);
        etSelectRegion.setOnClickListener(this);
        etLocationRegister.setOnClickListener(this);

        etPhonenumber.setOnFocusChangeListener(this);
        etEmail.setOnFocusChangeListener(this);
        etCountry.setOnFocusChangeListener(this);
        etSelectRegion.setOnFocusChangeListener(this);
        etCity.setOnFocusChangeListener(this);

        text_terms_condition.setOnClickListener(this);
        //autoCompleteTextViewSelectLocation.setOnFocusChangeListener(this);

        /*autoCompleteTextViewSelectLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                isSelectedText = true;
            }
        });

        autoCompleteTextViewSelectLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isSelectedText = false;
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });*/
        //Left Swipe To Go Next Screen
        /*btnSwipe.setOnTouchListener(new SwipeGesture(mContext){
            public void onSwipeLeft() {

                goToNextScreen();
            }
        });*/

        relativeRegisterSwipeNext.setOnTouchListener(new SwipeGesture(mContext) {
            @Override
            public void onSwipeLeft() {
                goToNextScreen();
            }

            @Override
            public void onButtonClick() {
                goToNextScreen();

            }
        });
      /*  btnSwipe.setOnTouchListener(new SwipeGesture(mContext) {
            @Override
            public void onSwipeLeft() {
                goToNextScreen();
            }
            @Override
            public void onButtonClick() {
                goToNextScreen();

            }
        });*/

        rbMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gender = rbMale.getText().toString().trim();
                rbMale.setChecked(true);
                rbFemale.setChecked(false);
                rbFemale.setEnabled(true);
            }
        });

        rbFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gender = rbFemale.getText().toString().trim();
                rbFemale.setChecked(true);
                rbMale.setChecked(false);
                rbMale.setEnabled(true);
            }
        });
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId()) {
            case R.id.etSelectCountry:
                if (!b)
                    etCountry.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
                else {
                    etCountry.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
                    if (countryList.size() == 0) {
                        getCountryList();
                    }
                    showCountryDialog();
                }
                break;
            case R.id.etSelectRegion:
                if (!b)
                    etSelectRegion.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
                else {
                    etSelectRegion.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
                    if (etCountry.getText().toString().trim().length() == 0) {
                        showDialogAlertPositiveButton(getString(R.string.app_name), getString(R.string.please_select_country));
                    } else {
                        getCountyList();
                    }
                }
                break;
            case R.id.etLocationRegister:
//                if (!b)
//                    autoCompleteTextViewSelectLocation.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
//                else {
//                    if(etCountry.getText().toString().trim().length() <= 0 ||
//                            etSelectRegion.getText().toString().trim().length() <= 0){
//                        showDialogAlertPositiveButton(getString(R.string.app_name),getString(R.string.please_select_country));
//                        autoCompleteTextViewSelectLocation.setEnabled(false);
//                    }else {
//                        autoCompleteTextViewSelectLocation.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
//                        //autoCompleteTextViewSelectLocation.setEnabled(true);
//                    }
//                }
                break;
            case R.id.etEmail:
                if (!b)
                    etEmail.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
                else
                    etEmail.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
                break;
            case R.id.etPhonenumber:
                if (!b)
                    etPhonenumber.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
                else
                    etPhonenumber.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
                break;
            case R.id.etCity:
                if (!b)
                    etCity.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
                else
                    etCity.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_bold_border));
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setToolbarSetting();
    }

    //Set Toolbar Settings
    private void setToolbarSetting() {
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).setToolbarTitle(mContext.getString(R.string.register));
        ((BaseActivity) mContext).showHideToolbarBackButton(true);
        ((BaseActivity) mContext).showHideToolbarMenuButton(false);
        ((BaseActivity) mContext).showHideToolbarSearchButton(false);
    }


    private void setFontStyle() {

        setTypeFaceEdittext(etConfirmPassword, mContext.getString(R.string.fontOpenSansRegular));
        setTypeFaceEdittext(etEmail, mContext.getString(R.string.fontOpenSansRegular));
        setTypeFaceEdittext(etPassword, mContext.getString(R.string.fontOpenSansRegular));
        setTypeFaceEdittext(etPhonenumber, mContext.getString(R.string.fontOpenSansRegular));
        setTypeFaceEdittext(etSelectRegion, mContext.getString(R.string.fontOpenSansRegular));
        setTypeFaceEdittext(etCountry, mContext.getString(R.string.fontOpenSansRegular));
        //setTypeFaceEdittext(autoCompleteTextViewSelectLocation,mContext.getString(R.string.fontOpenSansRegular));
        setTypeFaceEdittext(etLocationRegister, mContext.getString(R.string.fontOpenSansRegular));
        setTypeFaceEdittext(etCity, mContext.getString(R.string.fontOpenSansRegular));

        setTypeFaceTextView(text_agree_statement, mContext.getString(R.string.fontOpenSansRegular));
        setTypeFaceTextView(text_terms_condition, mContext.getString(R.string.fontOpenSansSemiBold));

        setTypeFaceTextView(tvConfirmPassword, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvSelectRegion, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvEmail, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvPhonenumber, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvPassword, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvSelectCountry, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvSelectLocation, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvRegisterSwipeLable, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvSelectGender, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvCity, mContext.getString(R.string.fontOpenSansSemiBold));

        setTypeFaceButton(btnSwipe, mContext.getString(R.string.fontOpenSansSemiBold));

    }


    private void goToNextScreen() {
        if (isValidDetails()) {
            if (NetworkUtil.isOnline(mContext)) {
                progressDialog.show();
                Query query = mDatabase.getReference().child(Constants.CONSUMER_USER_MASTER).
                        orderByChild(Constants.PHONENUMBER).
                        equalTo(tvCountryCode.getText().toString() + etPhonenumber.getText().toString().trim());
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        progressDialog.dismiss();
                        if (dataSnapshot.getValue() != null) {
                            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.phone_number_already_exist));
                            etPhonenumber.requestFocus();
                        } else {
                            gotoRegister2();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progressDialog.dismiss();
                        Log.e(TAG, databaseError.getMessage());

                    }
                });


               /* mAuth.fetchProvidersForEmail(etEmail.getText().toString().trim()).addOnCompleteListener
                        (new OnCompleteListener<ProviderQueryResult>() {
                            @Override
                            public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                                if (task.getResult().getProviders().size() > 0) {

                                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.email_already_exist));
                                    etEmail.requestFocus();

                                } else {

                                    gotoRegister2();
                                }
                            }
                        });*/


            } else {
                showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
            }

        }
        //gotoRegister2();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    //Set The Lat Long
    private void setBounds(double Lat, double Long) {

        AutocompleteFilter filter = new AutocompleteFilter.Builder()
                .setCountry(countryObject.getRegionCode())
                .build();
        double latRadian, degLatKm = 110.574235, degLongKm, deltaLat, deltaLong, minLat, minLong, maxLat, maxLong;
        int mDistanceInMeters = 1000;
        latRadian = Math.toRadians(Lat);

        System.out.print("Here");

        degLongKm = 110.572833 * Math.cos(latRadian);
        deltaLat = mDistanceInMeters / 1000.0 / degLatKm;
        deltaLong = mDistanceInMeters / 1000.0 / degLongKm;

        minLat = Lat - deltaLat;
        minLong = Long - deltaLong;
        maxLat = Lat + deltaLat;
        maxLong = Long + deltaLong;

        mAdapter = new PlaceAutoCompleteAdapter(mContext, android.R.layout.simple_list_item_1,
                mGoogleApiClient, new LatLngBounds(new LatLng(minLat, minLong), new LatLng(maxLat, maxLong)), filter);
        // autoCompleteTextViewSelectLocation.setAdapter(mAdapter);
    }

    //Get Lat Long from selected Address
    private void getLatLongFromAddress(String companyAddress) {

        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
            List addressList = geocoder.getFromLocationName(companyAddress, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = (Address) addressList.get(0);
                //LogUtils.LOG_E(TAG,address.getLatitude()+"");
                // LogUtils.LOG_E(TAG,address.getLongitude()+"");
                latitude = address.getLatitude();
                longitude = address.getLongitude();
                cityName = address.getLocality();
            }
        } catch (IOException e) {
            // LogUtils.LOG_E(TAG, "Unable to connect to Geocoder"+e);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

    }

    @Override
    public void onStop() {
        if (mGoogleApiClient.isConnected() && mGoogleApiClient != null) {

            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.etSelectRegion:
                if (etCountry.getText().toString().trim().length() == 0) {
                    showDialogAlertPositiveButton(getString(R.string.app_name), getString(R.string.please_select_country));
                } else {
                    getCountyList();
                }
                break;
            case R.id.etSelectCountry:
                if (countryList.size() == 0) {
                    getCountryList();
                }
                showCountryDialog();
                break;
            case R.id.etLocationRegister:
                openPlacePicker();
                break;
            case R.id.text_terms_condition:
                openTermsCondition();
                break;
        }

    }

    private void openPlacePicker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, mContext);

                if (place.getAddress() != null) {
                    etLocationRegister.setText(String.valueOf(place.getAddress()));
                    LogUtils.LOG_E("Address", String.valueOf(place.getAddress()));
                }

                if (place.getLatLng() != null) {
                    latitude = place.getLatLng().latitude;
                    longitude = place.getLatLng().longitude;
                    LogUtils.LOG_E("LAT", String.valueOf(latitude));
                    LogUtils.LOG_E("LONG", String.valueOf(longitude));

                    GeocoderApi geocoderApi = new GeocoderApi(this);
                    geocoderApi.execute(latitude + "," + longitude);

                   /* Geocoder geocoder = new Geocoder(mContext,Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
                        //String address = addresses.get(0).getAddressLine(0);
                        cityName = addresses.get(0).getLocality();
                        if (cityName != null)
                            LogUtils.LOG_E("CITY NAME", cityName);
                        //String country = addresses.get(0).getAddressLine(2);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                }

            }
        }
    }

    //Go to Next Screen
    private void gotoRegister2() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        hideKeyboard();
        // getLatLongFromAddress(autoCompleteTextViewSelectLocation.getText().toString());

        if (etCity.getText().toString().length() > 0) {
            cityName = etCity.getText().toString().trim();
            addFirebaseAnalytics(FirebaseAnalytics.Event.SELECT_CONTENT, "RegisterBasicDetails");

            RegisterVO registerVO = new RegisterVO();
            registerVO.setCountry(etCountry.getText().toString());
            registerVO.setCounty(etSelectRegion.getText().toString());
            // registerVO.setLocation(autoCompleteTextViewSelectLocation.getText().toString());
            registerVO.setLocation(etLocationRegister.getText().toString().trim());
            registerVO.setEmail(etEmail.getText().toString().trim());
            registerVO.setPhoneNo(tvCountryCode.getText().toString() + etPhonenumber.getText().toString());
            registerVO.setLatitude(String.valueOf(latitude));
            registerVO.setLongitude(String.valueOf(longitude));
            registerVO.setCity(cityName);
            registerVO.setGender(gender);


            //Added Male Female Into FirebaseAnalytics List
            addFirebaseAnalytics(gender, gender);


            Bundle bundle = new Bundle();
            bundle.putSerializable("REGISTER1", registerVO);

            RegisterUploadPicFragment registerFragmet2 = new RegisterUploadPicFragment();
            registerFragmet2.setArguments(bundle);

            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right,
                    R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, registerFragmet2).addToBackStack(null).commit();
        } else
            showDialogAlertPositiveButton(getString(R.string.app_name), "Please select location with city");

    }

    //Validations
    private boolean isValidDetails() {
        if (etCountry.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_select_country));
            etCountry.requestFocus();
            return false;
        }
        if (etSelectRegion.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_select_county));
            etSelectRegion.requestFocus();
            return false;
        }
        if (etCity.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_city));
            etCity.requestFocus();
            return false;
        }
//        if(autoCompleteTextViewSelectLocation.getText().toString().trim().length() < 1)
//        {
//            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_select_location));
//            autoCompleteTextViewSelectLocation.requestFocus();
//            return false;
//        }
        if (etLocationRegister.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_select_location));
            etLocationRegister.requestFocus();
            return false;
        }
        if (etEmail.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_email));
            etEmail.requestFocus();
            return false;
        }

        if (!isValidEmail(etEmail.getText().toString().trim())) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_valid_email));
            etEmail.requestFocus();
            return false;

        }
        if (etPhonenumber.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_phonenumber));
            etPhonenumber.requestFocus();
            return false;
        }
        if (etPhonenumber.getText().toString().trim().length() < 9 || etPhonenumber.getText().toString().trim().length() > 20) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_valid_phone));
            etPhonenumber.requestFocus();
            return false;
        }

        if(!checkbox_terms_condition.isChecked()){
            //showToast("Please accept terms and condition.");
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.alert_terms_condition));
            return false;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LogUtils.LOG_E(TAG, "GoogleClientApi is connected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        // LogUtils.LOG_E(TAG,"GoogleClientApi is hot connected");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //LogUtils.LOG_E(TAG,"GoogleClientApi is disconnected"+connectionResult.toString());

    }

    @Override
    public void getResult(String response,String countryName) {
        cityName = response;

        if (cityName.equalsIgnoreCase("")) {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

            try {
                List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
                //String address = addresses.get(0).getAddressLine(0);
                if (addresses.get(0).getLocality() != null)
                    cityName = addresses.get(0).getLocality();
                else if (addresses.get(0).getSubLocality() != null)
                    cityName = addresses.get(0).getSubLocality();


                if (cityName != null)
                    etCity.setText(cityName);


            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            etCity.setText(cityName);
        }
    }

    private void openTermsCondition(){
        String termsConditionUrl = Constants.TERMS_CONDITION_URL;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(termsConditionUrl));
        startActivity(browserIntent);
    }
}