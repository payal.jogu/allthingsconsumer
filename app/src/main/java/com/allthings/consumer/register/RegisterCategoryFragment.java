package com.allthings.consumer.register;

/**
 * Created by ABC on 6/8/2017.
 */

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.filter.FilterVo;
import com.allthings.consumer.offers.SelectedPosition;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class RegisterCategoryFragment extends BaseFragment implements View.OnClickListener {

    private final static String TAG = "CategoryFragment";
    private View rootView;
    private TextView tvSelectOfferCategories;
    private Button btnRegister;
    private ExpandableListView elvCategories;
    //private List<String> listDataHeader;
    //private HashMap<String, List<String>> listDataChild;
    private List<String> listDataHeader;
    private HashMap<String, List<FilterVo>> listDataChild;
    CategoriesAdapter mAdapter;
    private DatabaseReference tbCategory;
    //ArrayList<String> categoryList0,categoryList1,categoryList2;
    ArrayList<FilterVo> categoryList0, categoryList1, categoryList2;
    DatabaseReference tbRegister;
    private RegisterVO registerVO;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_register_category, container, false);
            init();
            prepareListData();
            getCategorylist();
            setListeners();
            setFontStyle();
            setDataAdapter();

            return rootView;
        } else {
            return rootView;
        }
    }


    //set data adpter according to check uncheck the category
    private void setDataAdapter()

    {
        elvCategories.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                mAdapter = (CategoriesAdapter) elvCategories.getExpandableListAdapter();
                if (mAdapter == null) {
                    return;
                }
                for (int i = 0; i < mAdapter.getGroupCount(); i++) {
                    if (i != groupPosition) {
                        elvCategories.collapseGroup(i);
                    }
                }
            }
        });

        /*mAdapter=new CategoriesAdapter(mContext, listDataHeader, listDataChild, new SelectedPosition() {
            @Override
            public void onItemClicked(int groupPosition, int childPosition, boolean isChecked, FilterVo filterVo) {
                if (groupPosition == 0)
                {
                    if (isChecked)
                    {
                        categoryList1.remove(categoryList0.get(childPosition));
                        categoryList2.remove(categoryList0.get(childPosition));
                    }
                    else
                    {
                        categoryList1.add(categoryList0.get(childPosition));
                        categoryList2.add(categoryList0.get(childPosition));
                    }
                }
                else if (groupPosition == 1)
                {
                    if (isChecked)
                    {
                        categoryList0.remove(categoryList1.get(childPosition));
                        categoryList2.remove(categoryList1.get(childPosition));
                    }
                    else
                    {
                        categoryList0.add(categoryList1.get(childPosition));
                        categoryList2.add(categoryList1.get(childPosition));
                    }
                }
                else if (groupPosition == 2)
                {
                    if (isChecked)
                    {
                        categoryList0.remove(categoryList2.get(childPosition));
                        categoryList1.remove(categoryList2.get(childPosition));
                    }
                    else
                    {
                        categoryList0.add(categoryList2.get(childPosition));
                        categoryList1.add(categoryList2.get(childPosition));
                    }
                }
                sortCategory();
            }
        });
        elvCategories.setAdapter(mAdapter);*/
        mAdapter = new CategoriesAdapter(mContext, listDataHeader, listDataChild, new SelectedPosition() {
            @Override
            public void onItemClicked(int groupPosition, int childPosition, boolean isChecked, FilterVo filterVo) {
                FilterVo filterVo1 = new FilterVo();
                if (groupPosition == 0) {
                    if (isChecked) {
                        removeData(categoryList1, filterVo);
                        removeData(categoryList2, filterVo);
                    } else {
                        categoryList1.add(filterVo);
                        categoryList2.add(filterVo);
                    }
                } else if (groupPosition == 1) {
                    if (isChecked) {
                        removeData(categoryList0, filterVo);
                        removeData(categoryList2, filterVo);
                    } else {
                        categoryList0.add(filterVo);
                        categoryList2.add(filterVo);
                    }
                } else if (groupPosition == 2) {
                    if (isChecked) {
                        removeData(categoryList0, filterVo);
                        removeData(categoryList1, filterVo);
                    } else {
                        categoryList0.add(filterVo);
                        categoryList1.add(filterVo);
                    }
                }
                filterVo.setSelectedItem(isChecked);
                mAdapter.notifyDataSetChanged();
                sortCategory();
            }
        });
        elvCategories.setAdapter(mAdapter);
    }

    private void removeData(ArrayList<FilterVo> filterVos, FilterVo filterVo) {
        for (FilterVo filterVo1 : filterVos) {
            if (filterVo1.getName().equalsIgnoreCase(filterVo.getName())) {
                filterVos.remove(filterVo1);
                break;
            }

        }
    }


    private void setFontStyle() {

        setTypeFaceTextView(tvSelectOfferCategories, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceButton(btnRegister, mContext.getString(R.string.fontOpenSansSemiBold));

    }

    @Override
    public void onResume() {
        super.onResume();
        setToolbarSetting();
        if (!NetworkUtil.isOnline(mContext)) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }


    }

    //Set the toolbar
    private void setToolbarSetting() {
        disableBack(rootView);
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).setToolbarTitle(mContext.getString(R.string.register));
        ((BaseActivity) mContext).showHideToolbarBackButton(false);
    }

    private void setListeners() {
        btnRegister.setOnClickListener(this);
    }

    private void init() {

        btnRegister = (Button) rootView.findViewById(R.id.btnRegister);
        tvSelectOfferCategories = (TextView) rootView.findViewById(R.id.tvSelectOfferCategories);
        elvCategories = (ExpandableListView) rootView.findViewById(R.id.elvCategories);

        tbCategory = mDatabase.getReference(Constants.CATEGORY_MASTER);
        categoryList0 = new ArrayList<>();
        categoryList1 = new ArrayList<>();
        categoryList2 = new ArrayList<>();
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        // Adding Header Of List
        listDataHeader.add(mContext.getString(R.string.l_gold));
        listDataHeader.add(mContext.getString(R.string.l_Silver));
        listDataHeader.add(mContext.getString(R.string.l_bronze));
        tbRegister = mDatabase.getReference().child(Constants.CONSUMER_USER_MASTER);


        registerVO = (RegisterVO) getArguments().getSerializable(
                "REGISTER1");
        Log.e(TAG, registerVO.getEmail());


    }


    //Sort Category in Alphabatic Order
    private void sortCategory() {
        Collections.sort(categoryList0, new Comparator<FilterVo>() {
            @Override
            public int compare(FilterVo s1, FilterVo s2) {
                return s1.getName().compareToIgnoreCase(s2.getName());
            }
        });
        Collections.sort(categoryList1, new Comparator<FilterVo>() {
            @Override
            public int compare(FilterVo s1, FilterVo s2) {
                return s1.getName().compareToIgnoreCase(s2.getName());
            }
        });
        Collections.sort(categoryList2, new Comparator<FilterVo>() {
            @Override
            public int compare(FilterVo s1, FilterVo s2) {
                return s1.getName().compareToIgnoreCase(s2.getName());
            }
        });

        mAdapter.notifyDataSetChanged();
    }

    //Get Selected Gold items
    private HashMap getGoldItems() {
        HashMap<String, String> map = new HashMap<>();

          /* for (int i = 0; i < mAdapter.selectedItems0.size(); i++) {
               String category = categoryList0.get(mAdapter.selectedItems0.keyAt(i));

                   map.put(category, category);
           }*/
        for (int i = 0; i < categoryList0.size(); i++) {
            if (categoryList0.get(i).isSelectedItem()) {
                String category = categoryList0.get(i).getName();
                map.put(category, category);
                addFirebaseAnalytics(category, category);
            }
        }
        return map;
    }

    //Get Selected Silver items
    private HashMap getSilverItems() {
        HashMap<String, String> map = new HashMap<>();
       /* for (int i=0;i<mAdapter.selectedItems1.size();i++)
        {
            String category=categoryList1.get(mAdapter.selectedItems1.keyAt(i));

                map.put(category,category);

        }*/
        for (int i = 0; i < categoryList1.size(); i++) {
            if (categoryList1.get(i).isSelectedItem()) {
                String category = categoryList1.get(i).getName();
                map.put(category, category);
                addFirebaseAnalytics(category, category);

            }

        }
        return map;
    }

    //Get Selected Bronze items
    private HashMap getBronzeItems() {
        HashMap<String, String> map = new HashMap<>();
        /*for (int i=0;i<mAdapter.selectedItems2.size();i++)
        {
            String category=categoryList2.get(mAdapter.selectedItems2.keyAt(i));

                map.put(category,category);

        }*/
        for (int i = 0; i < categoryList2.size(); i++) {
            if (categoryList2.get(i).isSelectedItem()) {
                String category = categoryList2.get(i).getName();

                map.put(category, category);
                addFirebaseAnalytics(category, category);

            }

        }
        return map;
    }


    //Get category list from the firebase
    private void getCategorylist() {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            tbCategory.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (final DataSnapshot child : dataSnapshot.getChildren()) {
                        System.out.println(child.getKey());

                        if (child.getKey().length() != 0) {
                            if (child.hasChild(Constants.STATUS) && child.child(Constants.STATUS).getValue().equals(Constants.STATUS_VALUE)) {
                                child.getRef().child("subCategory").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                        ArrayList<String> subCateList = new ArrayList<>();
                                        for(DataSnapshot subCate : dataSnapshot.getChildren()){
                                            LogUtils.LOG_E(TAG,"SubCategory : "+ subCate.getKey()+"\n");
                                            subCateList.add(subCate.getKey());
                                        }

                                        FilterVo filterVo;
                                        filterVo = new FilterVo();
                                        filterVo.setName(child.getKey());
                                        filterVo.setSubCateList(subCateList);
                                        categoryList0.add(filterVo);

                                        filterVo = new FilterVo();
                                        filterVo.setName(child.getKey());
                                        filterVo.setSubCateList(subCateList);
                                        categoryList1.add(filterVo);

                                        filterVo = new FilterVo();
                                        filterVo.setName(child.getKey());
                                        filterVo.setSubCateList(subCateList);
                                        categoryList2.add(filterVo);


                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                               /* FilterVo filterVo;
                                filterVo = new FilterVo();
                                filterVo.setName(child.getKey());
                                categoryList0.add(filterVo);

                                filterVo = new FilterVo();
                                filterVo.setName(child.getKey());
                                categoryList1.add(filterVo);

                                filterVo = new FilterVo();
                                filterVo.setName(child.getKey());
                                categoryList2.add(filterVo);*/


                            }

                        }
                    }
                    mAdapter.notifyDataSetChanged();
                    progressDialog.dismiss();


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    progressDialog.dismiss();

                }
            });
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }

    //Put the header data and child data in Expandable list view
    private void prepareListData() {

        if (listDataChild.size() == 0) {
            listDataChild.put(listDataHeader.get(0), categoryList0); // Header, Child data
            listDataChild.put(listDataHeader.get(1), categoryList1);
            listDataChild.put(listDataHeader.get(2), categoryList2);
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:
                if (getGoldItems().isEmpty() && getSilverItems().isEmpty() && getBronzeItems().isEmpty()) {
                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_select_categories));
                } else {
                    sendRequestForRegistration();
                }
                break;
        }

    }

    //Go to the next screen
    private void gotoRegister7() {
        hideKeyboard();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new RegisterDoneFragment()).commit();
    }

    //request for firebase registration
    private void sendRequestForRegistration() {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            saveDataToFirabase();
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }

    // Save data without firebase
    private void saveDataToFirabase() {
        if (NetworkUtil.isOnline(mContext)) {
            registerVO.setStatus(Constants.STATUS_VALUE);
            registerVO.setUpdateDate(ServerValue.TIMESTAMP);
            //registerVO.setUpdatedBy(SharedPreferenceUtil.getString(Constants.USER_ID, ""));

            tbRegister.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).setValue(registerVO)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            progressDialog.dismiss();
                            tbRegister.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).
                                    child(Constants.CATEGORY).child(Constants.GOLD).setValue(getGoldItems());

                            tbRegister.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).
                                    child(Constants.CATEGORY).child(Constants.SILVER).setValue(getSilverItems());

                            tbRegister.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).
                                    child(Constants.CATEGORY).child(Constants.BRONZE).setValue(getBronzeItems());


                            if (!SharedPreferenceUtil.getString("firebaseRegToken", "").equalsIgnoreCase("")) {
                                HashMap<String, String> deviceModel = new HashMap<>();
                                deviceModel.put("Device Model", Build.MODEL);
                                tbRegister.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).
                                        child(Constants.FCM_TOKEN)
                                        .child(SharedPreferenceUtil.getString("firebaseRegToken", ""))
                                        .setValue(deviceModel);
                            }


                            gotoRegister7();

                            SharedPreferenceUtil.putValue(Constants.IS_LOGIN, "login");
                            SharedPreferenceUtil.save();

                            ((BaseActivity)mContext).checkUserStatus();

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Log.e(TAG, e.getMessage());
                }
            });
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

}