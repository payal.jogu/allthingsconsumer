package com.allthings.consumer.register;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.utils.Constants;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import eu.janmuller.android.simplecropimage.CropImage;

/**
 * Created by ABC on 6/8/2017.
 */

public class RegisterUploadPicFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;
    private TextView tvUploadPic,tvOptional;
    private Button btnRegisterVerify;
    ImageView ivUploadPic;
    String uploadPic="";
    protected String TAG = "Register Upload Pic";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_register_uploadpic, container, false);
            init();
            setListeners();
            setFontStyle();
            return rootView;
        } else {
            return rootView;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        setToobarSetting();

    }

    //Set Toolbar settings
    private void setToobarSetting() {
        ((BaseActivity)mContext).showToolbar(true);
        ((BaseActivity)mContext).setToolbarTitle(mContext.getString(R.string.register));
        ((BaseActivity)mContext).showHideToolbarBackButton(true);
    }


    private void setFontStyle() {

        setTypeFaceTextView(tvOptional,mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvUploadPic,mContext.getString(R.string.fontOpenSansSemiBold));

        setTypeFaceButton(btnRegisterVerify,mContext.getString(R.string.fontOpenSansSemiBold));

    }

    private void setListeners() {
        btnRegisterVerify.setOnClickListener(this);
        ivUploadPic.setOnClickListener(this);
    }

    private void init() {

        tvUploadPic=(TextView)rootView.findViewById(R.id.tvUploadPic);
        tvOptional=(TextView)rootView.findViewById(R.id.tvOptional);
        btnRegisterVerify=(Button) rootView.findViewById(R.id.btn_register2_verify);
        ivUploadPic=(ImageView)rootView.findViewById(R.id.ivProfilePic);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e(TAG, "onRequestPermissionsResult" + requestCode);
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    Log.e(TAG, "PERMISSION GRANTED");
                    if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED){
                        showImageChooseDialog(true);
                    }

                } else {
                    Log.e(TAG, "PERMISSION NOT GRANTED");

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }
            case REQUEST_WRITE_STORAGE_PERMISSION:{
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                   Log.e(TAG, "PERMISSION GRANTED");
                    showImageChooseDialog(true);
                }else {
                    Log.e(TAG, "PERMISSION NOT GRANTED");

                }
                return;
            }
        }

    }
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register2_verify:
                gotoRegister3();
                break;
            case R.id.ivProfilePic:
                hideKeyboard();

                if (Build.VERSION.SDK_INT >= 23)
                {
                    if (!isCameraPermissionExist()) {
                        requestCameraPermission();
                    } else if (!isWriteStoragePermissionExist()) {
                        requestWriteStoragePermission();
                    } else {
                        showImageChooseDialog(true);
                    }
                }
                else
                {
                    showImageChooseDialog(true);
                }
                break;



        }

    }

    //Go to next screen
    private void gotoRegister3() {
        logout();
        hideKeyboard();
        Bundle bundle=new Bundle();
        RegisterOTPFragment registerFragmet3=new RegisterOTPFragment();

        RegisterVO registerVO = (RegisterVO) getArguments().getSerializable(
                "REGISTER1");
        registerVO.setStatus(Constants.STATUS_VALUE);
        registerVO.setProfileImageURL(uploadPic);

       // LogUtils.LOG_E("EMail",registerVO.getEmail());
        bundle.putSerializable("REGISTER1",registerVO);

        registerFragmet3.setArguments(bundle);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, registerFragmet3).addToBackStack(null).commit();
    }
    //Choose image from gallary
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != Activity.RESULT_OK) {

            return;
        }
        Bitmap bitmap;

        switch (requestCode) {

            case REQUEST_CODE_GALLERY:

                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();

                    startCropImage();

                } catch (Exception e) {

                   // LogUtils.LOG_E("Galary", "Error while creating temp file");
                }
                break;
            case REQUEST_CODE_TAKE_PICTURE:
                startCropImage();
                break;
            case FOR_CROPED_GALLERY:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {

                    return;
                }

                bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                uploadPic=mFileTemp.getPath();
                ivUploadPic.setImageBitmap(roundedTransform(bitmap,R.drawable.dp_placeholder));
              //  LogUtils.LOG_E("Base64 Image",strProfilePicBase64);
                break;
        }
    }
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
    }
}