package com.allthings.consumer.register;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
/**
 * Created by ABC on 6/8/2017.
 */

public class RegisterSelectRadiusFragment extends BaseFragment implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private View rootView;
    private TextView tvSelectAreaRadius,tvSelectedRadius,tvZeroMile,tvFiveMile,tvTenMile;
    private Button btnContinue;
    private SeekBar seekBarRadius;
    String radius="5";
    public DatabaseReference mRefrenceToConsumerUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_register_radius, container, false);
            init();
            setListeners();
            setFontStyle();
            return rootView;
        } else {
            return rootView;
        }

    }
    private void setFontStyle() {
        setTypeFaceButton(btnContinue,mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvSelectAreaRadius,mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvSelectedRadius,mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvZeroMile,mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvFiveMile,mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvTenMile,mContext.getString(R.string.fontOpenSansSemiBold));
    }

    @Override
    public void onResume() {
        super.onResume();
        setToolbarSetting();
    }

    private void setToolbarSetting() {
        ((BaseActivity)mContext).showToolbar(true);
        ((BaseActivity)mContext).setToolbarTitle(mContext.getString(R.string.register));
        ((BaseActivity)mContext).showHideToolbarBackButton(true);
    }

    private void setListeners() {
        btnContinue.setOnClickListener(this);
        seekBarRadius.setOnSeekBarChangeListener(this);
    }
    private void init() {
        btnContinue=(Button)rootView.findViewById(R.id.btn_register5_continue);
        tvSelectAreaRadius=(TextView)rootView.findViewById(R.id.tvSelectAreaRadius);
        tvSelectedRadius=(TextView)rootView.findViewById(R.id.tvSelectedRadius);
        tvZeroMile=(TextView)rootView.findViewById(R.id.tvZeroMile);
        tvFiveMile=(TextView)rootView.findViewById(R.id.tvFiveMile);
        tvTenMile=(TextView)rootView.findViewById(R.id.tvTenMile);
        seekBarRadius=(SeekBar)rootView.findViewById(R.id.seekbar_radius);
        mRefrenceToConsumerUser=mDatabase.getReference(Constants.CONSUMER_USER_MASTER);
        seekBarRadius.setMax(10);

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register5_continue:
                gotoRegister6();
                break;
        }
    }
    // Go to the next screen
    private void gotoRegister6() {
        if(NetworkUtil.isOnline(mContext)) {
            progressDialog.show();

            Query query=mDatabase.getReference().child(Constants.CONSUMER_USER_MASTER).child(SharedPreferenceUtil.getString(Constants.USER_ID,""));
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists())
                    {
                        Bundle bundle = new Bundle();
                        RegisterCategoryFragment register6 = new RegisterCategoryFragment();
                        hideKeyboard();
                        RegisterVO registerVO = new RegisterVO();
                        registerVO.setCountry(dataSnapshot.child(Constants.COUNTRY).getValue(String.class));
                        registerVO.setEmail(dataSnapshot.child(Constants.EMAIL).getValue(String.class));
                        registerVO.setLatitude(dataSnapshot.child(Constants.LATITUDE).getValue(String.class));
                        registerVO.setLongitude(dataSnapshot.child(Constants.LONGITUDE).getValue(String.class));
                        registerVO.setLocation(dataSnapshot.child(Constants.LOCATION).getValue(String.class));
                        registerVO.setPhoneNo(dataSnapshot.child(Constants.PHONENUMBER).getValue(String.class));
                        registerVO.setProfileImageURL(dataSnapshot.child(Constants.PROFILEPHOTOURL).getValue(String.class));
                        //registerVO.setAreaRadius(String.valueOf(tvSelectedRadius.getText()));
                        registerVO.setCreateDate(dataSnapshot.child(Constants.CREATED_DATE).getValue(String.class));
                        registerVO.setCreateBy(dataSnapshot.child(Constants.CREATED_BY).getValue(String.class));

                        SharedPreferenceUtil.putValue(Constants.EMAIL,dataSnapshot.child(Constants.EMAIL).getValue(String.class));
                        SharedPreferenceUtil.putValue(Constants.PHONENUMBER,dataSnapshot.child(Constants.PHONENUMBER).getValue(String.class).substring(3));
                        SharedPreferenceUtil.putValue(Constants.COUNTRY_CODE,dataSnapshot.child(Constants.PHONENUMBER).getValue(String.class).substring(0, 3));
                        SharedPreferenceUtil.putValue(Constants.COUNTRY,dataSnapshot.child(Constants.COUNTRY).getValue(String.class));
                        SharedPreferenceUtil.putValue(Constants.LOCATION,dataSnapshot.child(Constants.LOCATION).getValue(String.class));
                        SharedPreferenceUtil.putValue(Constants.PROFILEPHOTOURL,dataSnapshot.child(Constants.PROFILEPHOTOURL).getValue(String.class));
                        SharedPreferenceUtil.save();
                        bundle.putSerializable("REGISTER1", registerVO);
                        progressDialog.dismiss();
                        register6.setArguments(bundle);
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, register6).addToBackStack(null).commit();

                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    progressDialog.dismiss();

                }
            });

        }
        else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings),mContext.getString(R.string.please_check_network));
        }
    }
    //Select the radius
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
      /*  int val = (progress * (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
        tvSelectedRadius.setText("" + progress + " Miles");
        tvSelectedRadius.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);
        radius= String.valueOf(progress);*/
        /* if(progress == 10){
                    int val = (8* (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
                    tvSeekBarTooltip.setText("" + progress + " Miles");
                    tvSeekBarTooltip.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);
                }else{*/
        if(progress <= 3){
            int val = (progress * (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
            tvSelectedRadius.setText("" + progress + " Miles");
            tvSelectedRadius.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);
        }else if(progress == 4 || progress == 5){
            float pro = progress-0.50f;
            float val = (pro* (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
            tvSelectedRadius.setText("" + progress + " Miles");
            tvSelectedRadius.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);
        }
        else if(progress == 10){
            //float pro = 9-8.70f;
            float val = (8.80f* (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
            tvSelectedRadius.setText("" + progress + " Miles");
            tvSelectedRadius.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);
        } else{
            float pro = progress-0.68f;
            float val = (pro* (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
            tvSelectedRadius.setText("" + progress + " Miles");
            tvSelectedRadius.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);
        }

        //}

    }
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        tvSelectedRadius.setVisibility(View.VISIBLE);

    }
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        tvSelectedRadius.setVisibility(View.VISIBLE);

    }

}