package com.allthings.consumer.pushNotification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.R;
import com.allthings.consumer.offers.BrowseOffersFragment;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bypt1 on 15/5/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;
    private NotificationManager notificationManager;

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
        SharedPreferenceUtil.putValue("firebaseRegToken", token);
        SharedPreferenceUtil.save();

        if (SharedPreferenceUtil.getString(Constants.IS_LOGIN, "").equals("login") &&
                !SharedPreferenceUtil.getString(Constants.USER_ID, "").equalsIgnoreCase("")) {
            HashMap<String, String> deviceModel = new HashMap<>();
            deviceModel.put("Device Model", Build.MODEL);
            DatabaseReference dbRefToUser = FirebaseDatabase.getInstance().getReference(Constants.CONSUMER_USER_MASTER);
            dbRefToUser.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).
                    child(Constants.FCM_TOKEN)
                    .child(token)
                    .setValue(deviceModel);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        initChannels(this);
    }

    public void initChannels(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }
        NotificationChannel channel = new NotificationChannel("default",
                "AllThings",
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("customer app");
        notificationManager.createNotificationChannel(channel);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            if(getApplicationContext() != null){
                if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                    //sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getData());
                    JSONObject dataJson = new JSONObject(remoteMessage.getData());
                    try {
                        String senderId = dataJson.getString(Constants.RECEIVERID);
                        String receiverId = dataJson.getString(Constants.SENDERID);
                        String offerId = dataJson.getString(Constants.OFFERID);

                        Log.e(TAG, "senderId: " + senderId);
                        Log.e(TAG, "receiverId: " + receiverId);
                        Log.e(TAG, "offerId: " + offerId);

                        Log.e(TAG, "IsChatFragment: " + SharedPreferenceUtil.getBoolean("IsChatFragment",true));
                        Log.e(TAG, "ChatSenderID: " + SharedPreferenceUtil.getString("ChatSenderID",""));
                        Log.e(TAG, "ChatReceiverID: " + SharedPreferenceUtil.getString("ChatReceiverID",""));
                        Log.e(TAG, "ChatOfferID: " + SharedPreferenceUtil.getString("ChatOfferID",""));

                        if(!SharedPreferenceUtil.getBoolean("IsChatFragment",true) &&
                                !SharedPreferenceUtil.getString("ChatSenderID","").equals(senderId) &&
                                !SharedPreferenceUtil.getString("ChatReceiverID","").equals(receiverId) &&
                                !SharedPreferenceUtil.getString("ChatOfferID","").equals(offerId) ){
                            sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getData());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    //BaseFragment.handleNotification(remoteMessage.getNotification().getBody(), remoteMessage.getData().toString());
                }
            }
            //handleNotification(remoteMessage.getNotification().getBody());
        }

    }

  /*  private void handleNotification(String message) {
       // sendNotification(message);
      *//*  if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
          *//**//*  Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();*//**//*

            sendNotification(message);
        }else{
            sendNotification(message);
        }*//*
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {
            JSONObject data = json.getJSONObject("data");

            String title = data.getString("title");
            String message = data.getString("message");
            boolean isBackground = data.getBoolean("is_background");
            String imageUrl = data.getString("image");
            String timestamp = data.getString("timestamp");
            JSONObject payload = data.getJSONObject("payload");

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "isBackground: " + isBackground);
            Log.e(TAG, "payload: " + payload.toString());
            Log.e(TAG, "imageUrl: " + imageUrl);
            Log.e(TAG, "timestamp: " + timestamp);


            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();
            } else {
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), BaseActivity.class);
                resultIntent.putExtra("message", message);

                // check for image attachment
                if (TextUtils.isEmpty(imageUrl)) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }*/

  private static int value = 0;
    private void sendNotification(String notificationPayload,Map<String,String> dataPayload) {
            String userID = "", message = "";
            Log.e("notificationPayload : ", notificationPayload);
            Log.e("dataPayload : ", dataPayload.toString());

            try {
                JSONObject dataJson = new JSONObject(dataPayload);
                JSONObject notificationJson = new JSONObject(dataPayload);
                String email = dataJson.getString(Constants.RECEIVER_EMAIL);
                int Id = (int)dataJson.get("from");
                Log.e("from : ", ""+Id);

                Bundle notificationData = new Bundle();

                for(Map.Entry<String,String> value : dataPayload.entrySet()){
                    notificationData.putString(value.getKey(),value.getValue());
                }
                //Log.e(TAG, "data: " + data.toString());

                //JSONObject data = dataJson.getJSONObject("data");

                int requestID = (int) System.currentTimeMillis();

                Intent intent = new Intent(this, BaseActivity.class);
                intent.putExtra("PUSHNOTIFICATION", "PUSHNOTIFICATION");
                intent.putExtra("data", notificationData);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                        PendingIntent.FLAG_ONE_SHOT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                Notification.InboxStyle inboxStyle = new Notification.InboxStyle();

                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,"default")
                        .setSmallIcon(R.drawable.notification_icon)
                        .setColor(ContextCompat.getColor(this,R.color.colorRed))
                        .setContentTitle(email)
                        .setContentText(notificationPayload)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setContentIntent(pendingIntent);

                inboxStyle.setBigContentTitle("Enter Content Text");
                        inboxStyle.addLine("hi events "+value);

                        // builder.setStyle(inboxStyle);

                /*NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);*/

                notificationManager.notify( 0/* ID of notification */, notificationBuilder.build());

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

}
