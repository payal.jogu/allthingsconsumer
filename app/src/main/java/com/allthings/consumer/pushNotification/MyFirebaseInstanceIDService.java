package com.allthings.consumer.pushNotification;

import android.util.Log;

import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.i(TAG, "sendRegistrationToServer: " +refreshedToken);
        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        //sendRegistrationToServer(refreshedToken);

    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server-
        Log.e(TAG, "sendRegistrationToServer: " + token);
    }

    private void storeRegIdInPref(String token) {
        //SharedPreferenceUtil.putValue("firebaseRegToken",token);
        //SharedPreferenceUtil.save();
        SharedPreferenceUtil.putValue("firebaseRegToken", token);
        SharedPreferenceUtil.save();
    }
}