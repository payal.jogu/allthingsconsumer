package com.allthings.consumer.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by ABC on 6/9/2017.
 */


public class TabAdapter extends FragmentPagerAdapter {
    public TabAdapter(FragmentManager fm) {
        super(fm);
    }
    @Override
    public Fragment getItem(int index) {
        String title="";
        if (index == 0)
        {
            title="Gold";
        }
        else if (index == 1)
        {
            title="Silver";
        }
        else if (index == 2)
        {
            title="Bronze";
        }
        return null;
        //return OffersFragment.newInstance(title);
        /*switch (index) {
            case 0:
                return new OffersFragment();

            case 1:

                return new SilverOffersFragment();

            case 2:
                return new BronzeOffersFragment();

            }
                    return null;*/
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }

    @Override
    public int getCount()
    {
        return 3;
    }

}
