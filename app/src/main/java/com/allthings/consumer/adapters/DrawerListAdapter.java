package com.allthings.consumer.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.allthings.consumer.R;
import com.allthings.consumer.callback.OnRecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Vishal Dhaduk 8/6/2017
 */
public class DrawerListAdapter extends RecyclerView.Adapter<DrawerListAdapter.ViewHolder> {
    private Context mContext;
    private List<String> listMenu;
    private List<Integer> imageMenu;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;
    private ArrayList<View> viewArrayList;

    public DrawerListAdapter(Context mContext, List<String> listMenu, ArrayList<Integer> imageMenu, OnRecyclerItemClickListener
            onRecyclerItemClickListener) {
        this.mContext = mContext;
        this.listMenu = listMenu;
        this.imageMenu = imageMenu;
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
        viewArrayList = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_drawer_list_row, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.txtViewMenuName.setText(listMenu.get(position));
       /* holder.imgViewIcon.setImageResource(imageMenu.get(position));*/
        holder.imgViewIcon.setImageDrawable(ContextCompat.getDrawable(mContext, imageMenu.get(position)));
        holder.view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRecyclerItemClickListener.onClick(position);
                selectDeselcetMenuItem(position);

            }
        });
        if (listMenu.size() - 1 == position) {
            // holder.viewBottom.setVisibility(View.VISIBLE);
            ViewGroup.LayoutParams params = holder.viewBottom.getLayoutParams();
            params.height = 2;
            holder.viewBottom.setLayoutParams(params);
        } else {
            // holder.viewBottom.setVisibility(View.GONE);
            ViewGroup.LayoutParams params = holder.viewBottom.getLayoutParams();
            params.height = 0;
            holder.viewBottom.setLayoutParams(params);
        }

    }

    public void selectDeselcetMenuItem(int position) {
        for (int i = 0; i < viewArrayList.size(); i++) {
            if (i == position) {
                viewArrayList.get(i).setSelected(true);
            } else {
                viewArrayList.get(i).setSelected(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listMenu.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtViewMenuName;
        ImageView imgViewIcon;
        View view1, viewTop, viewBottom;

        public ViewHolder(View itemView) {
            super(itemView);
            view1 = itemView;
            txtViewMenuName = (TextView) itemView.findViewById(R.id.txtViewMenuName);
            imgViewIcon = (ImageView) itemView.findViewById(R.id.imgViewIcon);
            viewTop = itemView.findViewById(R.id.viewTop);
            viewBottom = itemView.findViewById(R.id.viewBottom);
            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(),
                    mContext.getString(R.string.fontOpenSansSemiBold));
            txtViewMenuName.setTypeface(typeface);
            viewArrayList.add(view1);
        }
    }

}
