package com.allthings.consumer.support;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.model.AdminVo;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by parth on 15/05/18.
 */

public class SupportFragment extends BaseFragment implements View.OnClickListener {
    private final String TAG = "SupportFragment";
    private View rootView;
    private TextView txtViewChat1, txtViewChat2, txtViewCall1, txtViewCall2, txtViewEmai11, txtViewEmai12;
    private RelativeLayout layoutCall, layoutEmail,layoutChat;
    private DatabaseReference dbRefToAdminUser;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_support, container, false);
            init();
            setFontStyle();
            setListeners();
            return rootView;
        }
        return rootView;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    protected void setFontStyle() {
        setTypeFaceTextView(txtViewChat1, getString(R.string.fontOpenSansRegular));
        setTypeFaceTextView(txtViewChat2, getString(R.string.fontOpenSansRegular));
        setTypeFaceTextView(txtViewCall1, getString(R.string.fontOpenSansRegular));
        setTypeFaceTextView(txtViewCall2, getString(R.string.fontOpenSansRegular));
        setTypeFaceTextView(txtViewEmai11, getString(R.string.fontOpenSansRegular));
        setTypeFaceTextView(txtViewEmai12, getString(R.string.fontOpenSansRegular));

    }


    private void setListeners() {
        layoutCall.setOnClickListener(this);
        layoutEmail.setOnClickListener(this);
        layoutChat.setOnClickListener(this);
    }

    private void init() {
        txtViewChat1 = rootView.findViewById(R.id.txtViewChat1);
        txtViewChat2 = rootView.findViewById(R.id.txtViewChat2);
        txtViewCall1 = rootView.findViewById(R.id.txtViewCall1);
        txtViewCall2 = rootView.findViewById(R.id.txtViewCall2);
        txtViewEmai11 = rootView.findViewById(R.id.txtViewEmai11);
        txtViewEmai12 = rootView.findViewById(R.id.txtViewEmai12);

        layoutCall = rootView.findViewById(R.id.layoutCall);
        layoutEmail = rootView.findViewById(R.id.layoutEmail);
        layoutChat = rootView.findViewById(R.id.layoutChat);

        dbRefToAdminUser = mDatabase.getReference("adminUser").child("jPL203GEhOR0uEEEklnX3SsCl4B2");
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).setToolbarTitle(mContext.getResources().getString(R.string.support));
        ((BaseActivity) mContext).showHideToolbarMenuButton(true);
        ((BaseActivity) mContext).showHideToolbarSearchButton(false);
        ((BaseActivity) mContext).showHideToolbarBackButton(false);
        ((BaseActivity) mContext).showHideToolbarSerachEdittext(false);
        ((BaseActivity) mContext).showHideToolbarSearchCancelButton(false);
        ((BaseActivity) mContext).showHideToolbarTitle(true);
        ((BaseActivity) mContext).showHideToolbarChatEmail(false);
        ((BaseActivity) mContext).showHideToolbarChatImage(false);
        ((BaseActivity) mContext).showHideToolbarChatOfferTitle(false);
        ((BaseActivity) mContext).showHideToolbarRemainder(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layoutCall:
                goToCall();
                break;
            case R.id.layoutEmail:
                goToEmail();
                break;
            case R.id.layoutChat:
                getAdminDetails();
                break;

        }
    }

    private void goToEmail() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject));
        intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{getString(R.string.support_email_address)});
        intent.putExtra(Intent.EXTRA_TEXT, "");
        Intent mailer = Intent.createChooser(intent, null);
        startActivity(mailer);
    }

    private void goToCall() {
        if (isCallPermissionExist()) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + getString(R.string.support_number)));
            startActivity(intent);
        } else {
            requestCallPermission();
        }

    }

    private void getAdminDetails(){
        if(NetworkUtil.isOnline(mContext)){
            dbRefToAdminUser.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    AdminVo adminVo = dataSnapshot.getValue(AdminVo.class);

                    if(adminVo != null && adminVo.getStatus() != null && adminVo.getStatus().equalsIgnoreCase("1")) {
                        Bundle sendData = new Bundle();
                        sendData.putSerializable("AdminDetails", adminVo);

                        Fragment supportChatFragment = new SupportChatFragment();
                        supportChatFragment.setArguments(sendData);

                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right,
                                R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, supportChatFragment);
                        fragmentTransaction.addToBackStack(TAG);
                        fragmentTransaction.commit();

                       /* Intent intent = new Intent(mContext,SupportChatActivity.class);
                        intent.putExtras(sendData);

                        startActivity(intent);*/
                    }else {
                        showToast("Comming soon");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    LogUtils.LOG_E(TAG,databaseError.getMessage());
                    showToast("Admin details not found.");
                }
            });
        }else
            showToast(mContext.getString(R.string.please_check_network));
    }


}


