package com.allthings.consumer.support;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.R;
import com.allthings.consumer.chat.ChatDetailsAdapter;
import com.allthings.consumer.chat.ChatDetailsVO;
import com.allthings.consumer.model.AdminVo;
import com.allthings.consumer.offers.ScrollingLinearLayoutManager;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.InternalStorageContentProvider;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.RoundedImageTransform;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import eu.janmuller.android.simplecropimage.CropImage;

public class SupportChatActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "ChatFragment";
    private RecyclerView rvSupportChat;
    private TextView tvToolChatSupport,tvToolChatAdmin;
    private ImageView ivSupportChatSent, ivSupportChatCamera,img_back,img_chat_image;
    private EditText etSupportChatText;
    private DatabaseReference dbRefToUserMsg, dbRefToChatMsg;
    private String offerId, receiverId, senderID, receiverEmail, senderEmail, offerTitle, offerImage, status;
    private List<ChatDetailsVO> chatList;
    private ChatDetailsAdapter chatAdapter;
    private DatabaseReference dbRefToTypingIndicator;
    private boolean typingStarted = false;
    private LinearLayout layoutTyping;
    private Timer typingTimer;
    private AdminVo adminVo;
    protected FirebaseDatabase mDatabase;
    protected FirebaseStorage mStorage;
    protected ProgressDialog progressDialog;


    protected File mFileTemp;

    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final int REQUEST_CODE_GALLERY = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int FOR_CROPED_GALLERY = 4;
    public static final int REQUEST_CAMERA_PERMISSION = 1;
    public static final int REQUEST_CALL_PERMISSION = 2;
    protected static final int REQUEST_WRITE_STORAGE_PERMISSION = 3;
    protected static final int REQUEST_SMS_PERMISSION = 4;
    protected static final int REQUEST_ACCESS_FINE_LOCATION = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_support_chat);
        init();
        hideKeyboard();
        setFontStyle();
        setListeners();
    }

    private void init() {

        mDatabase = FirebaseDatabase.getInstance();
        mStorage = FirebaseStorage.getInstance();

        tvToolChatAdmin = findViewById(R.id.tvToolChatAdmin);
        tvToolChatSupport = findViewById(R.id.tvToolChatSupport);

        img_chat_image = findViewById(R.id.img_chat_image);
        img_back = findViewById(R.id.img_back);

        rvSupportChat = findViewById(R.id.rvSupportChat);
        ivSupportChatSent = findViewById(R.id.ivSupportChatSent);
        ivSupportChatCamera = findViewById(R.id.ivSupportChatCamera);
        etSupportChatText = findViewById(R.id.etSupportChatText);
        ivSupportChatSent.setVisibility(View.INVISIBLE);

        layoutTyping = findViewById(R.id.layoutTyping);

        dbRefToUserMsg = mDatabase.getReference(Constants.USER_MESSAGES_TB);
        dbRefToChatMsg = mDatabase.getReference(Constants.CHAT_ROOM_TB).child("messages");
        dbRefToTypingIndicator = mDatabase.getReference(Constants.CHAT_ROOM_TB).child(Constants.TB_TYPING_INDICATOR);

        chatList = new ArrayList<>();

        //ScrollingLinearLayoutManager scrollingLinearLayoutManager = new ScrollingLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false, 1000);
        //rvChat.setLayoutManager(scrollingLinearLayoutManager);

        rvSupportChat.setLayoutManager(new ScrollingLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false, 1000));

        chatAdapter = new ChatDetailsAdapter(this, chatList, SharedPreferenceUtil.getString(Constants.USER_ID, ""));

        rvSupportChat.setAdapter(chatAdapter);

        getArgument();
        getChatData();

        // getActivity().findViewById(R.id.imgViewChatBlock).setVisibility(View.GONE);
        //getActivity().findViewById(R.id.imgViewChatBlock).setOnClickListener(this);

    }

    private void setFontStyle() {

    }

    private void setListeners() {
        img_back.setOnClickListener(this);
        ivSupportChatSent.setOnClickListener(this);
        ivSupportChatCamera.setOnClickListener(this);

        etSupportChatText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (etSupportChatText.getText().toString().trim().length() > 0)
                    ivSupportChatSent.setVisibility(View.VISIBLE);
                else
                    ivSupportChatSent.setVisibility(View.INVISIBLE);

                if (NetworkUtil.isOnline(SupportChatActivity.this)) {
                    // ivChatSent.setEnabled(true);
                    Log.e(TAG, "typing started");
                    typingStarted = true;
                    Map<String, Object> indicator = new HashMap<>();
                    indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
                    if (typingTimer != null) {
                        typingTimer.cancel();
                    }
                    dbRefToTypingIndicator.updateChildren(indicator);

                    //send typing started status


                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (NetworkUtil.isOnline(SupportChatActivity.this)) {
                    //if (!myBlockStatus.equals("Blocked")) {
                  /*  if (!TextUtils.isEmpty(s.toString()) && s.toString().trim().length() == 1) {
                        // ivChatSent.setEnabled(true);

                        Log.e(TAG, "typing started");
                        typingStarted = true;
                        Map<String, Object> indicator = new HashMap<>();
                        indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
                        dbRefToTypingIndicator.updateChildren(indicator);

                        //send typing started status

                    } else if (s.toString().trim().length() == 0 && typingStarted) {*/

                    Log.i(TAG, "typing stopped event");
                    typingTimer = new Timer();
                    typingTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            typingStarted = false;
                            Map<String, Object> indicator = new HashMap<>();
                            indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
                            dbRefToTypingIndicator.updateChildren(indicator);
                        }
                    }, 600);


                    //send typing stopped status

                    // }
                    // }
                }
            }
        });
    }

    private void getArgument() {
        Bundle data =getIntent().getExtras();
        if (data != null) {
            adminVo = (AdminVo) data.getSerializable("AdminDetails");

            senderID = SharedPreferenceUtil.getString(Constants.USER_ID, "");
            senderEmail = SharedPreferenceUtil.getString(Constants.EMAIL, "");

            receiverId = adminVo.getUserId();
            receiverEmail = adminVo.getEmail();
            offerImage = adminVo.getProfileImageURL();
            status = adminVo.getStatus();

            tvToolChatAdmin.setText("Support");
            tvToolChatSupport.setText("Admin");

            if (offerImage != null) {
                setToolbarChatOfferImage(offerImage);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        hideKeyboard();
       /* ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).showHideToolbarBackButton(true);
        ((BaseActivity) mContext).showHideToolbarMenuButton(false);
        ((BaseActivity) mContext).showHideToolbarSearchButton(false);
        ((BaseActivity) mContext).showHideToolbarSerachEdittext(false);
        ((BaseActivity) mContext).showHideToolbarSearchCancelButton(false);
        ((BaseActivity) mContext).showHideToolbarTitle(false);
        ((BaseActivity) mContext).showHideToolbarChatEmail(true);
        ((BaseActivity) mContext).showHideToolbarChatImage(true);
        ((BaseActivity) mContext).showHideToolbarChatOfferTitle(true);
        ((BaseActivity) mContext).showHideToolbarRemainder(false);

        ((BaseActivity) mContext).setToolbarChatOfferTitle("Support");
        ((BaseActivity) mContext).setToolbarChatEmailTitle("Admin");

        if (offerImage != null) {
            ((BaseActivity) mContext).setToolbarChatOfferImage(offerImage);
        }*/

        if (etSupportChatText.getText().toString().trim().length() > 0) {
            typingStarted = true;
            Map<String, Object> indicator = new HashMap<>();
            indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
            dbRefToTypingIndicator.updateChildren(indicator);
        } else {
            typingStarted = false;
            Map<String, Object> indicator = new HashMap<>();
            indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
            dbRefToTypingIndicator.updateChildren(indicator);
        }
    }

    public void setToolbarChatOfferImage(String imageUrl) {

        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.company_placeholder);
        if (!imageUrl.isEmpty()) {
            Picasso.with(this).load(imageUrl)
                    .resize(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight())
                    .transform(new RoundedImageTransform(this, 0, R.drawable.company_placeholder))
                    .placeholder(R.drawable.company_placeholder)
                    .into(img_chat_image);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        typingStarted = false;
        Map<String, Object> indicator = new HashMap<>();
        indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
        dbRefToTypingIndicator.updateChildren(indicator);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.ivSupportChatSent:
                if (NetworkUtil.isOnline(this)) {
                    sendMessage();
                } else {
                    showDialogAlertPositiveButton(getString(R.string.app_name), getString(R.string.please_check_network));
                }
                break;
            case R.id.ivSupportChatCamera:
                if (Build.VERSION.SDK_INT >= 23) {
                    if (!isCameraPermissionExist()) {
                        requestCameraPermission();
                    } else if (!isWriteStoragePermissionExist()) {
                        requestWriteStoragePermission();
                    } else {
                        showImageChooseDialog(true);
                    }
                } else {
                    showImageChooseDialog(true);
                }
                break;

        }
    }



    //camera permission
    protected void requestCameraPermission() {
        Log.i("CAMERA PERMISSION", "CAMERA permission has NOT been granted. Requesting permission.");
        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.CAMERA,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CAMERA_PERMISSION);


        } else {
            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.CAMERA,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CAMERA_PERMISSION);

        }
    }

    protected void requestWriteStoragePermission() {
        Log.i("WRITE STORAGE ", "Write storge permission has NOT been granted. Requesting permission.");
        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            /*ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE_PERMISSION);*/

            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE_PERMISSION);

        } else {
            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE_PERMISSION);
            // Camera permission has not been granted yet. Request it directly.
           /* ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE_PERMISSION);*/
        }
    }


    //send the message to firebase server
    public void sendMessage() {
        if (etSupportChatText.getText().toString().trim().length() > 0) {
            if (isValid()) {
                ChatDetailsVO chat = new ChatDetailsVO(etSupportChatText.getText().toString(), senderID,
                        receiverId,
                        receiverEmail
                );

                chat.setIsReceiverBlock("");
                chat.setIsSenderBlock("");

                String textKey = dbRefToChatMsg.push().getKey();
                dbRefToChatMsg.child(textKey).setValue(chat);
                Map<String, Object> msgKey = new HashMap<>();
                msgKey.put(textKey, "1");
                dbRefToUserMsg.child(senderID).child("admin").child(receiverId).updateChildren(msgKey);
                dbRefToUserMsg.child(receiverId).child("consumerUser").child(senderID).updateChildren(msgKey);

                // sendNotification(etSupportChatText.getText().toString());

            } else {
                showToast("Message not Sent");
            }
            etSupportChatText.setText("");
        } else {
            showDialogAlertPositiveButton(getString(R.string.app_name), "Please Enter Message..");
        }
    }

    //check validations
    public Boolean isValid() {
        if (TextUtils.isEmpty(receiverEmail)) {
            showDialogAlertPositiveButton(getString(R.string.app_name), "SenderName Not Found");
            return false;
        }
        if (TextUtils.isEmpty(SharedPreferenceUtil.getString(Constants.USER_ID, ""))) {
            showDialogAlertPositiveButton(getString(R.string.app_name), "senderId Not Found");
            return false;
        }
        if (TextUtils.isEmpty(receiverId)) {
            showDialogAlertPositiveButton(getString(R.string.app_name), "receiverId Not Found");
            return false;
        }
        return true;
    }

    private void getChatData() {
        chatList.clear();
        dbRefToUserMsg.child(senderID).child("admin").
                child(receiverId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    dbRefToChatMsg.child(dataSnapshot.getKey()).addListenerForSingleValueEvent
                            (new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    ChatDetailsVO chatVO = dataSnapshot.getValue(ChatDetailsVO.class);
                                   /*   if (!chatVO.getIsReceiverBlock().equals("")) {
                                  if (!chatVO.getToId().equalsIgnoreCase(senderID) &&
                                            !chatVO.getIsSenderBlock().equals("")) {*/
                                    chatList.add(dataSnapshot.getValue(ChatDetailsVO.class));
                                    chatAdapter.notifyDataSetChanged();
                                    rvSupportChat.getLayoutManager().scrollToPosition(chatAdapter.getItemCount() - 1);
                                     /*}
                                   if(rvChat.getChildAt(0) != null) {
                                        rvChat.getLayoutManager().smoothScrollToPosition(rvChat, null, chatAdapter.getItemCount() - 1);
                                    }*/
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    LogUtils.LOG_E(TAG, databaseError.getMessage());
                                }
                            });
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {
                LogUtils.LOG_E(TAG, dataSnapshot.getValue(String.class));
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                LogUtils.LOG_E(TAG, dataSnapshot.getValue(String.class));
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) {
                LogUtils.LOG_E(TAG, dataSnapshot.getValue(String.class));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                LogUtils.LOG_E(TAG, databaseError.getMessage());
            }
        });

    }

    //upload image to firebase
    private void uploadImage(String imagePath) {

        StorageReference refToChatImg = mStorage.getReference(Constants.CHAT_STORAGE)
                .child(SharedPreferenceUtil.getString(Constants.USER_ID, ""));
        final StorageReference refToImage = refToChatImg.child(String.valueOf(System.currentTimeMillis() / 1000))
                .child("image.jpg");

        InputStream stream = null;
        try {
            stream = new FileInputStream(new File(imagePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        UploadTask uploadTask = refToImage.putStream(stream);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
               // progressDialog.dismiss();
                Log.e(TAG, exception.getMessage());

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
               // progressDialog.dismiss();
                refToImage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Log.e(TAG, String.valueOf(uri));

                        ChatDetailsVO chat = new ChatDetailsVO();
                        chat.setFromId(senderID);
                        chat.setToId(receiverId);
                        chat.setIsReceiverBlock("");
                        chat.setIsSenderBlock("");
                        chat.setImageUrl(uri.toString());
                        chat.setImageHeight(600);
                        chat.setImageWidth(600);
                        chat.setTimestamp(ServerValue.TIMESTAMP);

                        String textKey = dbRefToChatMsg.push().getKey();
                        dbRefToChatMsg.child(textKey).setValue(chat);
                        Map<String, Object> msgKey = new HashMap<>();
                        msgKey.put(textKey, "1");
                        dbRefToUserMsg.child(senderID).child("admin").child(receiverId).updateChildren(msgKey);
                        dbRefToUserMsg.child(receiverId).child("consumerUser").child(senderID).updateChildren(msgKey);
                    }
                });

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {

            return;
        }
        Bitmap bitmap;

        switch (requestCode) {

            case REQUEST_CODE_GALLERY:
                try {
                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();
                    startCropImage();

                } catch (Exception e) {
                    Log.e("Galary", "Error while creating temp file", e);
                }
                break;

            case REQUEST_CODE_TAKE_PICTURE:
                startCropImage();
                break;

            case FOR_CROPED_GALLERY:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {

                    return;
                }

                // bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                // offerImageUrl = mFileTemp.getAbsolutePath();
                Log.d(TAG, path);
                uploadImage(path);
                // ivOfferImage.setImageBitmap(bitmap);
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        LogUtils.LOG_E(TAG, "onRequestPermissionsResult" + requestCode);
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    LogUtils.LOG_E(TAG, "PERMISSION GRANTED");
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        showImageChooseDialog(true);
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "PERMISSION NOT GRANTED");
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(this
                            ,Manifest.permission.CAMERA)) {
                        showAlertForGettingPermission();
                    }
                }

                return;
            }
            case REQUEST_WRITE_STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LogUtils.LOG_E(TAG, "PERMISSION GRANTED");
                    showImageChooseDialog(true);
                } else {
                    Log.e(TAG, "PERMISSION NOT GRANTED");
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        showAlertForGettingPermission();
                    }
                }
                return;
            }
        }

    }

    //alert for getting permission
    protected void showAlertForGettingPermission() {
        final android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Permissions Required")
                .setMessage("You have forcefully denied some of the required permissions " +
                        "for this action. Please open settings, go to permissions and allow them.")
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    //image choose dialog open
    protected void showImageChooseDialog(final boolean isCropEnable) {
        // TODO Auto-generated method stub

        final Dialog dlgAlert = new Dialog(this);
        dlgAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgAlert.setContentView(R.layout.custom_dialog_layout);
        dlgAlert.setCanceledOnTouchOutside(false);
        TextView txtViewChoosePic, txtViewCamera, txtViewGallery, txtViewCancel;

        txtViewChoosePic = (TextView) dlgAlert.findViewById(R.id.txtViewChoosePic);
        txtViewCamera = (TextView) dlgAlert.findViewById(R.id.txtViewCamera);
        txtViewGallery = (TextView) dlgAlert.findViewById(R.id.txtViewGallery);
        txtViewCancel = (TextView) dlgAlert.findViewById(R.id.txtViewCancel);

        txtViewCamera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View dialog) {
                // TODO Auto-generated method stub
                dlgAlert.dismiss();
                if (isCropEnable) {
                    takePicture();
                } else {
                    cameraIntent();
                }

            }
        });
        txtViewGallery.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View dialog) {
                // TODO Auto-generated method stub
                dlgAlert.dismiss();
                openGallery();

            }
        });
        txtViewCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View dialog) {
                // TODO Auto-generated method stub
                dlgAlert.dismiss();

            }
        });

        WindowManager.LayoutParams wmlp = dlgAlert.getWindow().getAttributes();
        wmlp.width = getDeviceWidth() - 40;
        wmlp.gravity = Gravity.BOTTOM;
        dlgAlert.getWindow().getAttributes().windowAnimations = R.style.dialog_animation_bottom_to_up;
        dlgAlert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dlgAlert.show();
    }

    protected int getDeviceWidth() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    //take the photo
    protected void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    mImageCaptureUri = Uri.fromFile(mFileTemp);
                } else {
                    //File file = new File(getPhotoFileUri().getPath());
                    mImageCaptureUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", mFileTemp);
                    //intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                }

            } else {
                /*
                 * The solution is taken from here: http://stackoverflow.com/questions/10042695/how-to-get-camera-result-as-a-uri-in-data-folder
                 */
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }
            Log.e("mImageCaptureUri", "" + mImageCaptureUri);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {

            Log.d("TAG", "cannot take picture", e);
        }
    }

    //open camera
    protected void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }

    //open gallery
    protected void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    //crop the image
    protected void startCropImage() {
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 2);
        intent.putExtra(CropImage.ASPECT_Y, 2);

        startActivityForResult(intent, FOR_CROPED_GALLERY);
    }


    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    //hide the keyboard
    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    //show alert dialog
    protected void showDialogAlertPositiveButton(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setTitle(title)
                .setPositiveButton(getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

        AlertDialog alert = builder.create();
        alert.show();

    }

    protected boolean isCameraPermissionExist() {
        // Check if the Camera permission is already available.
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Camera permission has not been granted.
            return false;
        } else {
            return true;
            // Camera permissions is already available, show the camera preview.
        }
    }


    protected boolean isWriteStoragePermissionExist() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    //toast message
    protected void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

}
