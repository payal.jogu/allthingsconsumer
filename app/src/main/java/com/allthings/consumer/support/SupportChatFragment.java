package com.allthings.consumer.support;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.allthings.consumer.chat.ChatDetailsAdapter;
import com.allthings.consumer.chat.ChatDetailsVO;
import com.allthings.consumer.model.AdminVo;
import com.allthings.consumer.offers.ScrollingLinearLayoutManager;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import eu.janmuller.android.simplecropimage.CropImage;

public class SupportChatFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = "ChatFragment";
    private View rootView;
    private RecyclerView rvSupportChat;
    private ImageView ivSupportChatSent, ivSupportChatCamera;
    private EditText etSupportChatText;
    private DatabaseReference dbRefToUserMsg, dbRefToChatMsg;
    private String offerId, receiverId, senderID, receiverEmail, senderEmail, offerTitle, offerImage, status;
    private List<ChatDetailsVO> chatList;
    private ChatDetailsAdapter chatAdapter;
    private DatabaseReference dbRefToTypingIndicator;
    private boolean typingStarted = false;
    private LinearLayout layoutTyping;
    private Timer typingTimer;
    private AdminVo adminVo;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_support_chat, container, false);
            init();
            hideKeyboard();
            setFontStyle();
            setListeners();
            //userIsTyping();
            return rootView;
        }
        return rootView;
    }

    private void init() {
        rvSupportChat = rootView.findViewById(R.id.rvSupportChat);
        ivSupportChatSent = rootView.findViewById(R.id.ivSupportChatSent);
        ivSupportChatCamera = rootView.findViewById(R.id.ivSupportChatCamera);
        etSupportChatText = rootView.findViewById(R.id.etSupportChatText);
        ivSupportChatSent.setVisibility(View.INVISIBLE);

        layoutTyping = rootView.findViewById(R.id.layoutTyping);

        dbRefToUserMsg = mDatabase.getReference(Constants.USER_MESSAGES_TB);
        dbRefToChatMsg = mDatabase.getReference(Constants.CHAT_ROOM_TB).child("messages");
        dbRefToTypingIndicator = mDatabase.getReference(Constants.CHAT_ROOM_TB).child(Constants.TB_TYPING_INDICATOR);

        chatList = new ArrayList<>();

        //ScrollingLinearLayoutManager scrollingLinearLayoutManager = new ScrollingLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false, 1000);
        //rvChat.setLayoutManager(scrollingLinearLayoutManager);

        rvSupportChat.setLayoutManager(new ScrollingLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false, 1000));

        chatAdapter = new ChatDetailsAdapter(getActivity(), chatList, SharedPreferenceUtil.getString(Constants.USER_ID, ""));

        rvSupportChat.setAdapter(chatAdapter);

        getArgument();
        getChatData();

        // getActivity().findViewById(R.id.imgViewChatBlock).setVisibility(View.GONE);
        //getActivity().findViewById(R.id.imgViewChatBlock).setOnClickListener(this);

    }

    private void setFontStyle() {

    }

    private void setListeners() {
        ivSupportChatSent.setOnClickListener(this);
        ivSupportChatCamera.setOnClickListener(this);

        etSupportChatText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (etSupportChatText.getText().toString().trim().length() > 0)
                    ivSupportChatSent.setVisibility(View.VISIBLE);
                else
                    ivSupportChatSent.setVisibility(View.INVISIBLE);

                if (NetworkUtil.isOnline(mContext)) {
                    // ivChatSent.setEnabled(true);
                    Log.e(TAG, "typing started");
                    typingStarted = true;
                    Map<String, Object> indicator = new HashMap<>();
                    indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
                    if (typingTimer != null) {
                        typingTimer.cancel();
                    }
                    dbRefToTypingIndicator.updateChildren(indicator);

                    //send typing started status


                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (NetworkUtil.isOnline(mContext)) {
                    //if (!myBlockStatus.equals("Blocked")) {
                  /*  if (!TextUtils.isEmpty(s.toString()) && s.toString().trim().length() == 1) {
                        // ivChatSent.setEnabled(true);

                        Log.e(TAG, "typing started");
                        typingStarted = true;
                        Map<String, Object> indicator = new HashMap<>();
                        indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
                        dbRefToTypingIndicator.updateChildren(indicator);

                        //send typing started status

                    } else if (s.toString().trim().length() == 0 && typingStarted) {*/

                    Log.i(TAG, "typing stopped event");
                    typingTimer = new Timer();
                    typingTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            typingStarted = false;
                            Map<String, Object> indicator = new HashMap<>();
                            indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
                            dbRefToTypingIndicator.updateChildren(indicator);
                        }
                    }, 600);


                    //send typing stopped status

                    // }
                    // }
                }
            }
        });
    }

    private void getArgument() {
        Bundle data = getArguments();
        if (data != null) {
            adminVo = (AdminVo) data.getSerializable("AdminDetails");

            senderID = SharedPreferenceUtil.getString(Constants.USER_ID, "");
            senderEmail = SharedPreferenceUtil.getString(Constants.EMAIL, "");

            receiverId = adminVo.getUserId();
            receiverEmail = adminVo.getEmail();
            offerImage = adminVo.getProfileImageURL();
            status = adminVo.getStatus();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        hideKeyboard();
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).showHideToolbarBackButton(true);
        ((BaseActivity) mContext).showHideToolbarMenuButton(false);
        ((BaseActivity) mContext).showHideToolbarSearchButton(false);
        ((BaseActivity) mContext).showHideToolbarSerachEdittext(false);
        ((BaseActivity) mContext).showHideToolbarSearchCancelButton(false);
        ((BaseActivity) mContext).showHideToolbarTitle(false);
        ((BaseActivity) mContext).showHideToolbarChatEmail(true);
        ((BaseActivity) mContext).showHideToolbarChatImage(true);
        ((BaseActivity) mContext).showHideToolbarChatOfferTitle(true);
        ((BaseActivity) mContext).showHideToolbarRemainder(false);

        ((BaseActivity) mContext).setToolbarChatOfferTitle("Support");
        ((BaseActivity) mContext).setToolbarChatEmailTitle("Admin");
       /* if (receiverEmail != null && !receiverEmail.isEmpty()) {
            ((BaseActivity) mContext).setToolbarChatEmailTitle(receiverEmail);
        }*/

        if (offerImage != null) {
            ((BaseActivity) mContext).setToolbarChatOfferImage(offerImage);
        }

        if (etSupportChatText.getText().toString().trim().length() > 0) {
            typingStarted = true;
            Map<String, Object> indicator = new HashMap<>();
            indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
            dbRefToTypingIndicator.updateChildren(indicator);
        } else {
            typingStarted = false;
            Map<String, Object> indicator = new HashMap<>();
            indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
            dbRefToTypingIndicator.updateChildren(indicator);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        typingStarted = false;
        Map<String, Object> indicator = new HashMap<>();
        indicator.put(SharedPreferenceUtil.getString(Constants.USER_ID, ""), typingStarted);
        dbRefToTypingIndicator.updateChildren(indicator);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivSupportChatSent:
                if (NetworkUtil.isOnline(mContext)) {
                    sendMessage();
                } else {
                    showDialogAlertPositiveButton(getString(R.string.app_name), getString(R.string.please_check_network));
                }
                break;
            case R.id.ivSupportChatCamera:
                if (Build.VERSION.SDK_INT >= 23) {
                    if (!isCameraPermissionExist()) {
                        requestCameraPermission();
                    } else if (!isWriteStoragePermissionExist()) {
                        requestWriteStoragePermission();
                    } else {
                        showImageChooseDialog(true);
                    }
                } else {
                    showImageChooseDialog(true);
                }
                break;

        }
    }

    //send the message to firebase server
    public void sendMessage() {
        if (etSupportChatText.getText().toString().trim().length() > 0) {
            if (isValid()) {
                ChatDetailsVO chat = new ChatDetailsVO(etSupportChatText.getText().toString(), senderID,
                        receiverId,
                        receiverEmail
                );

                chat.setIsReceiverBlock("");
                chat.setIsSenderBlock("");

                String textKey = dbRefToChatMsg.push().getKey();
                dbRefToChatMsg.child(textKey).setValue(chat);
                Map<String, Object> msgKey = new HashMap<>();
                msgKey.put(textKey, "1");
                dbRefToUserMsg.child(senderID).child("admin").child(receiverId).updateChildren(msgKey);
                dbRefToUserMsg.child(receiverId).child("consumerUser").child(senderID).updateChildren(msgKey);

                // sendNotification(etSupportChatText.getText().toString());

            } else {
                showToast("Message not Sent");
            }
            etSupportChatText.setText("");
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.app_name), "Please Enter Message..");
        }
    }

    //check validations
    public Boolean isValid() {
        if (TextUtils.isEmpty(receiverEmail)) {
            showDialogAlertPositiveButton(mContext.getString(R.string.app_name), "SenderName Not Found");
            return false;
        }
        if (TextUtils.isEmpty(SharedPreferenceUtil.getString(Constants.USER_ID, ""))) {
            showDialogAlertPositiveButton(mContext.getString(R.string.app_name), "senderId Not Found");
            return false;
        }
        if (TextUtils.isEmpty(receiverId)) {
            showDialogAlertPositiveButton(mContext.getString(R.string.app_name), "receiverId Not Found");
            return false;
        }
        return true;
    }

    private void getChatData() {
        chatList.clear();
        dbRefToUserMsg.child(senderID).child("admin").
                child(receiverId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    dbRefToChatMsg.child(dataSnapshot.getKey()).addListenerForSingleValueEvent
                            (new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    ChatDetailsVO chatVO = dataSnapshot.getValue(ChatDetailsVO.class);
                                   /*   if (!chatVO.getIsReceiverBlock().equals("")) {
                                  if (!chatVO.getToId().equalsIgnoreCase(senderID) &&
                                            !chatVO.getIsSenderBlock().equals("")) {*/
                                    chatList.add(dataSnapshot.getValue(ChatDetailsVO.class));
                                    chatAdapter.notifyDataSetChanged();
                                    rvSupportChat.getLayoutManager().scrollToPosition(chatAdapter.getItemCount() - 1);
                                     /*}
                                   if(rvChat.getChildAt(0) != null) {
                                        rvChat.getLayoutManager().smoothScrollToPosition(rvChat, null, chatAdapter.getItemCount() - 1);
                                    }*/
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    LogUtils.LOG_E(TAG, databaseError.getMessage());
                                }
                            });
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {
                LogUtils.LOG_E(TAG, dataSnapshot.getValue(String.class));
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                LogUtils.LOG_E(TAG, dataSnapshot.getValue(String.class));
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) {
                LogUtils.LOG_E(TAG, dataSnapshot.getValue(String.class));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                LogUtils.LOG_E(TAG, databaseError.getMessage());
            }
        });

    }

    //upload image to firebase
    private void uploadImage(String imagePath) {

        StorageReference refToChatImg = mStorage.getReference(Constants.CHAT_STORAGE)
                .child(SharedPreferenceUtil.getString(Constants.USER_ID, ""));
        final StorageReference refToImage = refToChatImg.child(String.valueOf(System.currentTimeMillis() / 1000))
                .child("image.jpg");

        InputStream stream = null;
        try {
            stream = new FileInputStream(new File(imagePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        UploadTask uploadTask = refToImage.putStream(stream);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                progressDialog.dismiss();
                Log.e(TAG, exception.getMessage());

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                progressDialog.dismiss();
                refToImage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Log.e(TAG, String.valueOf(uri));

                        ChatDetailsVO chat = new ChatDetailsVO();
                        chat.setFromId(senderID);
                        chat.setToId(receiverId);
                        chat.setIsReceiverBlock("");
                        chat.setIsSenderBlock("");
                        chat.setImageUrl(uri.toString());
                        chat.setImageHeight(600);
                        chat.setImageWidth(600);
                        chat.setTimestamp(ServerValue.TIMESTAMP);

                        String textKey = dbRefToChatMsg.push().getKey();
                        dbRefToChatMsg.child(textKey).setValue(chat);
                        Map<String, Object> msgKey = new HashMap<>();
                        msgKey.put(textKey, "1");
                        dbRefToUserMsg.child(senderID).child("admin").child(receiverId).updateChildren(msgKey);
                        dbRefToUserMsg.child(receiverId).child("consumerUser").child(senderID).updateChildren(msgKey);
                    }
                });

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {

            return;
        }
        Bitmap bitmap;

        switch (requestCode) {

            case REQUEST_CODE_GALLERY:
                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();
                    startCropImage();

                } catch (Exception e) {
                    Log.e("Galary", "Error while creating temp file", e);
                }
                break;

            case REQUEST_CODE_TAKE_PICTURE:
                startCropImage();
                break;

            case FOR_CROPED_GALLERY:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {

                    return;
                }

                // bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                // offerImageUrl = mFileTemp.getAbsolutePath();
                Log.d(TAG, path);
                uploadImage(path);
                // ivOfferImage.setImageBitmap(bitmap);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        LogUtils.LOG_E(TAG, "onRequestPermissionsResult" + requestCode);
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    LogUtils.LOG_E(TAG, "PERMISSION GRANTED");
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        showImageChooseDialog(true);
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "PERMISSION NOT GRANTED");
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                        showAlertForGettingPermission();
                    }
                }

                return;
            }
            case REQUEST_WRITE_STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LogUtils.LOG_E(TAG, "PERMISSION GRANTED");
                    showImageChooseDialog(true);
                } else {
                    Log.e(TAG, "PERMISSION NOT GRANTED");
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        showAlertForGettingPermission();
                    }
                }
                return;
            }
        }

    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

}
