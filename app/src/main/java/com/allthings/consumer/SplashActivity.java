package com.allthings.consumer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends Activity {

    private static final int SPLASH_DISPLAY_TIME = 3000; // splash screen delay time
    private TextView tvLabel;
    private Intent intent = new Intent();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        Fresco.initialize(this);
        setContentView(R.layout.layout_splash);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        init();
        setFontStyle();

    }

    //set font style
    private void setFontStyle() {
        setTypeFaceTextView(tvLabel, getString(R.string.fontOpenSansBoldItalic));
    }

    //initialize component
    private void init() {
        tvLabel = (TextView) findViewById(R.id.txtViewLabel);
        SharedPreferenceUtil.init(SplashActivity.this);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (SharedPreferenceUtil.getString(Constants.IS_LOGIN, "").equalsIgnoreCase("login")) {
                    intent.setClass(SplashActivity.this, BaseActivity.class);
                } else {
                    intent.setClass(SplashActivity.this, WelcomeActivity.class);
                }
                startActivity(intent);
                finish();
            }
        }, SPLASH_DISPLAY_TIME);

    }

    //set type face
    private void setTypeFaceTextView(TextView txtView, String typeFace) {
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(),
                typeFace);
        txtView.setTypeface(tf);
    }
}


