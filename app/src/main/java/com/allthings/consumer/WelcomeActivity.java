package com.allthings.consumer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.allthings.consumer.utils.SwipeGesture;

public class WelcomeActivity extends Activity {

    private TextView tvWelcome,tvBringingyou,tvAlwaysDeliver,tvLogo,tvWelcomeSwipeLable;
    private Button btnWelcomeSwipeNext;
    private RelativeLayout relativeWelcomeSwipeNext,layout_welcome;
    private GestureDetector gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_welcome);
        init();
       // swipe();
        setFontStyle();
        setListener();
    }

    //swipe
    private void swipe() {
       // gestureDetector = new GestureDetector(getApplicationContext(),new WelcomeActivity.SwipeGestureDetector());
    }

    //initialize components
    private void init() {
        //LogUtils.LOG_E("Test",SharedPreferenceUtil.getString("ab",""));
        SharedPreferenceUtil.init(WelcomeActivity.this);
        tvWelcome=(TextView)findViewById(R.id.tvWelcometoallthings);
        tvWelcomeSwipeLable=(TextView)findViewById(R.id.tvWelcomeSwipeLable);
        tvBringingyou=(TextView)findViewById(R.id.tvDiscription);
        tvAlwaysDeliver=(TextView)findViewById(R.id.tvAlways);
        tvLogo=(TextView)findViewById(R.id.tvLogo);

        layout_welcome = (RelativeLayout) findViewById(R.id.layout_welcome);
        relativeWelcomeSwipeNext = (RelativeLayout) findViewById(R.id.relativeWelcomeSwipeNext);
       // btnWelcomeSwipeNext=(Button)findViewById(R.id.btnWelcomeSwipeNext);
    }

    //set Fonts styles
    private void setFontStyle() {
        setTypeFaceTextView(tvWelcome,getString(R.string.fontOpenSansSemiBold));

        setTypeFaceTextView(tvAlwaysDeliver,getString(R.string.fontOpenSansBold));
        setTypeFaceTextView(tvBringingyou,getString(R.string.fontOpenSansRegular));
        setTypeFaceTextView(tvLogo,getString(R.string.fontOpenSansBold));

        setTypeFaceTextView(tvWelcomeSwipeLable,getString(R.string.fontOpenSansBold));

        //setTypeFaceButton(btnWelcomeSwipeNext,getString(R.string.fontOpenSansSemiBold));
    }
    private void setTypeFaceTextView(TextView txtView, String typeFace) {
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(),
                typeFace);
        txtView.setTypeface(tf);
    }
    protected void setTypeFaceButton(Button button, String typeFace) {
        Typeface tf = Typeface.createFromAsset(
                getApplicationContext().getAssets(),
                typeFace);
        button.setTypeface(tf);
    }
    //set the listeners
    private void setListener(){
        relativeWelcomeSwipeNext.setOnTouchListener(new SwipeGesture(this){
            @Override
            public void onSwipeLeft() {
                gotoRegister1();
            }
            @Override
            public void onButtonClick() {
                gotoRegister1();

            }
        });

        layout_welcome.setOnTouchListener(new SwipeGesture(this){
            @Override
            public void onSwipeLeft() {
                gotoRegister1();
            }
        });
       /* btnWelcomeSwipeNext.setOnTouchListener(new SwipeGesture(this) {
            @Override
            public void onSwipeLeft() {
                gotoRegister1();
            }
            @Override
            public void onButtonClick() {
                gotoRegister1();

            }
        });*/
       //btnWelcomeSwipeNext.setOnClickListener(this);
    }

    //go to register 1 screen
    private void gotoRegister1() {
        Intent intent=new Intent();
        intent.setClass(WelcomeActivity.this, BaseActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        finish();
    }

}

