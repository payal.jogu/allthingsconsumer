package com.allthings.consumer.filter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.allthings.consumer.R;

import java.util.ArrayList;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.ViewHolder> {

    private ArrayList<String> subCateList;
    private Context context;

    SubCategoryAdapter(ArrayList<String> subCateList, Context context) {
        this.subCateList = subCateList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_subcat_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String subCategoryName = subCateList.get(position);
        subCategoryName = String.valueOf(subCategoryName.charAt(0)).toUpperCase() + subCategoryName.subSequence(1, subCategoryName.length());
        holder.txtSubCate.setText(subCategoryName);
    }

    @Override
    public int getItemCount() {
        return subCateList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtSubCate;

        ViewHolder(View itemView) {
            super(itemView);
            txtSubCate = itemView.findViewById(R.id.txt_subcat);
        }
    }
}
