package com.allthings.consumer.filter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.offers.BrowseOffersFragment;
import com.allthings.consumer.offers.SelectedPosition;
import com.allthings.consumer.profile.ProfileFragment;
import com.allthings.consumer.register.RegisterDoneFragment;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.allthings.consumer.R;


/**
 * Created by Payal jogu on 27/7/17.
 */

public class FilterFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = "FilterCategories";
    private View rootView;
    private TextView tvSelectOfferCategories;
    private Button btnRegister;
    private ExpandableListView elvCategories;
    private List<String> listDataHeader;
    private HashMap<String, List<FilterVo>> listDataChild;
    FilterAdapter mAdapter;
    private DatabaseReference tbCategory;
    ArrayList<FilterVo> categoryList0, categoryList1, categoryList2;
    ArrayList<String> strCategoryList0, strCategoryList1, strCategoryList2;

    DatabaseReference tbRegister;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_register_category, container, false);
            init();
            prepareListData();
            setListeners();
            setFontStyle();
            setDataAdapter();
            getCategorylist();
            disableBack(rootView);
            return rootView;
        } else {
            return rootView;
        }
    }


    //set data adpter according to check uncheck the category
    private void setDataAdapter() {
        mAdapter = new FilterAdapter(mContext, listDataHeader, listDataChild, new SelectedPosition() {
            @Override
            public void onItemClicked(int groupPosition, int childPosition, boolean isChecked, FilterVo filterVo) {
                FilterVo filterVo1 = new FilterVo();
                if (groupPosition == 0) {
                    if (isChecked) {
                        removeData(categoryList1, filterVo);
                        removeData(categoryList2, filterVo);
                    } else {
                        categoryList1.add(filterVo);
                        categoryList2.add(filterVo);
                    }
                } else if (groupPosition == 1) {
                    if (isChecked) {
                        removeData(categoryList0, filterVo);
                        removeData(categoryList2, filterVo);
                    } else {
                        categoryList0.add(filterVo);
                        categoryList2.add(filterVo);
                    }
                } else if (groupPosition == 2) {
                    if (isChecked) {
                        removeData(categoryList0, filterVo);
                        removeData(categoryList1, filterVo);
                    } else {
                        categoryList0.add(filterVo);
                        categoryList1.add(filterVo);
                    }
                }
                filterVo.setSelectedItem(isChecked);
                mAdapter.notifyDataSetChanged();
                sortCategory();
            }
        });
        elvCategories.setAdapter(mAdapter);
    }


    private void setFontStyle() {

        setTypeFaceTextView(tvSelectOfferCategories, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceButton(btnRegister, mContext.getString(R.string.fontOpenSansSemiBold));

    }


    @Override
    public void onResume() {
        super.onResume();
        setToolbarSetting();
        if (!NetworkUtil.isOnline(mContext)) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }


    }

    private void removeData(ArrayList<FilterVo> filterVos, FilterVo filterVo) {
        for (FilterVo filterVo1 : filterVos) {
            if (filterVo1.getName().equalsIgnoreCase(filterVo.getName())) {
                filterVos.remove(filterVo1);
                break;
            }

        }
    }

    //Set the toolbar
    private void setToolbarSetting() {
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).setToolbarTitle("Category");
        ((BaseActivity) mContext).showHideToolbarBackButton(false);
        ((BaseActivity) mContext).showHideToolbarMenuButton(true);
        ((BaseActivity) mContext).showHideToolbarSearchButton(false);

    }

    private void setListeners() {
        btnRegister.setOnClickListener(this);

        elvCategories.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                mAdapter = (FilterAdapter) elvCategories.getExpandableListAdapter();
                if (mAdapter == null) {
                    return;
                }

                //mAdapter.setSelectedCategory(groupPosition);

                for (int i = 0; i < mAdapter.getGroupCount(); i++) {
                    if (i != groupPosition) {
                        elvCategories.collapseGroup(i);
                    }
                }
            }
        });
    }

    private void init() {

        btnRegister = rootView.findViewById(R.id.btnRegister);
        btnRegister.setText("Update");
        tvSelectOfferCategories = rootView.findViewById(R.id.tvSelectOfferCategories);
        elvCategories = rootView.findViewById(R.id.elvCategories);

        tbCategory = mDatabase.getReference(Constants.CATEGORY_MASTER);
        categoryList0 = new ArrayList<>();
        categoryList1 = new ArrayList<>();
        categoryList2 = new ArrayList<>();
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();
        strCategoryList0 = new ArrayList<>();
        strCategoryList1 = new ArrayList<>();
        strCategoryList2 = new ArrayList<>();
        // Adding Header Of List
        listDataHeader.add(mContext.getString(R.string.l_gold));
        listDataHeader.add(mContext.getString(R.string.l_Silver));
        listDataHeader.add(mContext.getString(R.string.l_bronze));
        tbRegister = mDatabase.getReference().child(Constants.CONSUMER_USER_MASTER);


    }

    //Sort Category in Alphabatic Order
    private void sortCategory() {
        Collections.sort(categoryList0, new Comparator<FilterVo>() {
            @Override
            public int compare(FilterVo s1, FilterVo s2) {
                return s1.getName().compareToIgnoreCase(s2.getName());
            }
        });
        Collections.sort(categoryList1, new Comparator<FilterVo>() {
            @Override
            public int compare(FilterVo s1, FilterVo s2) {
                return s1.getName().compareToIgnoreCase(s2.getName());
            }
        });
        Collections.sort(categoryList2, new Comparator<FilterVo>() {
            @Override
            public int compare(FilterVo s1, FilterVo s2) {
                return s1.getName().compareToIgnoreCase(s2.getName());
            }
        });

        mAdapter.notifyDataSetChanged();
    }


    //Get Selected Gold items
    private HashMap getGoldItems() {
        HashMap<String, String> map = new HashMap<>();

        for (int i = 0; i < categoryList0.size(); i++) {
            if (categoryList0.get(i).isSelectedItem()) {
                String category = categoryList0.get(i).getName();
                map.put(category, category);
            }
        }

        return map;
    }

    //Get Selected Silver items
    private HashMap getSilverItems() {
        HashMap<String, String> map = new HashMap<>();
        for (int i = 0; i < categoryList1.size(); i++) {
            if (categoryList1.get(i).isSelectedItem()) {
                String category = categoryList1.get(i).getName();
                map.put(category, category);
            }

        }
        return map;
    }

    //Get Selected Bronze items
    private HashMap getBronzeItems() {
        HashMap<String, String> map = new HashMap<>();
        for (int i = 0; i < categoryList2.size(); i++) {
            if (categoryList2.get(i).isSelectedItem()) {
                String category = categoryList2.get(i).getName();

                map.put(category, category);
            }

        }
        return map;
    }


    //get user selected category list from database
    private void getUserSelectedCategory() {
        DatabaseReference dbRefToConsumer = mDatabase.getReference(Constants.CONSUMER_USER_MASTER).
                child(SharedPreferenceUtil.getString(Constants.USER_ID, ""))
                .child(Constants.CATEGORY);

        dbRefToConsumer.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot typeChild : dataSnapshot.getChildren()) {

                    LogUtils.LOG_E(TAG, "Type: " + typeChild.getKey());
                    GenericTypeIndicator<Map<String, String>> typeMap = new GenericTypeIndicator<Map<String, String>>() {
                    };
                    Map<String, String> categoryList = typeChild.getValue(typeMap);

                    if (typeChild.getKey().equalsIgnoreCase("gold")) {
                        for (Map.Entry<String, String> entry : categoryList.entrySet()) {
                            System.out.println(entry.getKey() + "/" + entry.getValue());

                            for (FilterVo filterVo : categoryList0) {
                                if (filterVo.getName().equalsIgnoreCase(entry.getValue())) {
                                    strCategoryList0.add(filterVo.getName());
                                    filterVo.setSelectedItem(true);
                                    break;
                                }
                            }
                        }
                    } else if (typeChild.getKey().equalsIgnoreCase("silver")) {
                        for (Map.Entry<String, String> entry : categoryList.entrySet()) {
                            System.out.println(entry.getKey() + "/" + entry.getValue());

                            for (FilterVo filterVo : categoryList1) {
                                if (filterVo.getName().equalsIgnoreCase(entry.getValue())) {
                                    strCategoryList1.add(filterVo.getName());
                                    filterVo.setSelectedItem(true);
                                    break;
                                }
                            }
                        }
                    } else if (typeChild.getKey().equalsIgnoreCase("bronze")) {
                        for (Map.Entry<String, String> entry : categoryList.entrySet()) {
                            System.out.println(entry.getKey() + "/" + entry.getValue());

                            for (FilterVo filterVo : categoryList2) {
                                if (filterVo.getName().equalsIgnoreCase(entry.getValue())) {
                                    strCategoryList2.add(filterVo.getName());
                                    filterVo.setSelectedItem(true);
                                    break;
                                }
                            }
                        }
                    }

                }
                removeDuplicateData();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void removeDuplicateData() {
        ArrayList<String> goldTemp = new ArrayList<>();
        ArrayList<String> silTemp = new ArrayList<>();
        ArrayList<String> broTemp = new ArrayList<>();

        goldTemp.addAll(strCategoryList1);
        goldTemp.addAll(strCategoryList2);

        silTemp.addAll(strCategoryList0);
        silTemp.addAll(strCategoryList2);

        broTemp.addAll(strCategoryList0);
        broTemp.addAll(strCategoryList1);

        for (int i = 0; i < goldTemp.size(); i++) {
            for (int j = 0; j < categoryList0.size(); j++) {
                if (categoryList0.get(j).getName().equalsIgnoreCase(goldTemp.get(i))) {
                    categoryList0.remove(categoryList0.get(j));
                    break;
                }
            }
        }
        for (int i = 0; i < silTemp.size(); i++) {
            for (int j = 0; j < categoryList1.size(); j++) {
                if (categoryList1.get(j).getName().equalsIgnoreCase(silTemp.get(i))) {
                    categoryList1.remove(categoryList1.get(j));
                    break;
                }
            }
        }

        for (int i = 0; i < broTemp.size(); i++) {
            for (int j = 0; j < categoryList2.size(); j++) {
                if (categoryList2.get(j).getName().equalsIgnoreCase(broTemp.get(i))) {
                    categoryList2.remove(categoryList2.get(j));
                    break;
                }
            }
        }

        sortCategory();
        //mAdapter.notifyDataSetChanged();

    }

    //Get category list from the firebase
    private void getCategorylist() {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();

            //Get Category from firebase
            tbCategory.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    categoryList0.clear();
                    categoryList1.clear();
                    categoryList2.clear();
                    for (final DataSnapshot child : dataSnapshot.getChildren()) {
                        System.out.println(child.getKey());

                        if (child.getKey().length() != 0) {

                            //Get subcategory for category
                            if (child.hasChild(Constants.STATUS) && child.child(Constants.STATUS).getValue().equals(Constants.STATUS_VALUE)) {

                                LogUtils.LOG_E(TAG, "Category Name : " + child.getKey() +" \n");

                                child.getRef().child("subCategory").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                        ArrayList<String> subCateList = new ArrayList<>();
                                        for(DataSnapshot subCate : dataSnapshot.getChildren()){
                                            LogUtils.LOG_E(TAG,"SubCategory : "+ subCate.getKey()+"\n");
                                            subCateList.add(subCate.getKey());
                                        }

                                        FilterVo filterVo;
                                        filterVo = new FilterVo();
                                        filterVo.setName(child.getKey());
                                        filterVo.setSubCateList(subCateList);
                                        categoryList0.add(filterVo);

                                        filterVo = new FilterVo();
                                        filterVo.setName(child.getKey());
                                        filterVo.setSubCateList(subCateList);
                                        categoryList1.add(filterVo);

                                        filterVo = new FilterVo();
                                        filterVo.setName(child.getKey());
                                        filterVo.setSubCateList(subCateList);
                                        categoryList2.add(filterVo);


                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        }
                    }
                    progressDialog.dismiss();
                    getUserSelectedCategory();

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    progressDialog.dismiss();

                }
            });
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }

    //Put the header data and child data in Expandable list view
    private void prepareListData() {
        if (listDataChild.size() == 0) {
            listDataChild.put(listDataHeader.get(0), categoryList0); // Header, Child data
            listDataChild.put(listDataHeader.get(1), categoryList1);
            listDataChild.put(listDataHeader.get(2), categoryList2);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:
                /*getGoldItems();
                getSilverItems();
                getBronzeItems();*/
                if (getGoldItems().isEmpty() && getSilverItems().isEmpty() && getBronzeItems().isEmpty()) {
                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_select_categories));
                } else {
                    sendRequestForRegistration();
                }
                break;
        }

    }

    //Go to the next screen
    private void gotoRegister7() {
        hideKeyboard();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new RegisterDoneFragment()).addToBackStack(null).commit();
    }

    //request for firebase registration
    private void sendRequestForRegistration() {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            saveDataToFirabase();
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    // Save data without firebase
    private void saveDataToFirabase() {
        if (NetworkUtil.isOnline(mContext)) {
           /* registerVO.setStatus(Constants.STATUS_VALUE);
            registerVO.setUpdatedDate(String.valueOf(ServerValue.TIMESTAMP));
            registerVO.setUpdatedBy(SharedPreferenceUtil.getString(Constants.USER_ID, ""));
*/
            //if (getGoldItems().size() != 0) {
            tbRegister.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).
                    child(Constants.CATEGORY).child(Constants.GOLD).setValue(getGoldItems()).
                    addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            tbRegister.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).
                                    child(Constants.CATEGORY).child(Constants.SILVER).setValue(getSilverItems())
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            tbRegister.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).
                                                    child(Constants.CATEGORY).child(Constants.BRONZE).setValue(getBronzeItems())
                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
                                                            progressDialog.dismiss();
                                                            showDialogAlertPositiveButton(getString(R.string.app_name), "Category list updated");
                                                        }
                                                    }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    progressDialog.dismiss();
                                                }
                                            });
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    progressDialog.dismiss();
                                }
                            });
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                }
            });
            // }

            //if (getSilverItems().size() != 0) {

            //}

            //if (getBronzeItems().size() != 0) {


            // }


        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    //show alert dialog
    protected void showDialogAlertPositiveButton(String title, String message) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(message)
                    .setCancelable(false)
                    .setTitle(title)
                    .setPositiveButton(mContext.getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    clearBackStack();
                                    ((BaseActivity) mContext).setSelectedItem(0);
                                    ((BaseActivity) mContext).lastFragmentPos = 0;
                                    fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new BrowseOffersFragment()).commit();
                                }
                            });

            AlertDialog alert = builder.create();
            alert.show();
        }

    }


}

