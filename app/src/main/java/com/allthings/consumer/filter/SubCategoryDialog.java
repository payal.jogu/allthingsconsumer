package com.allthings.consumer.filter;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.allthings.consumer.R;

import java.util.ArrayList;

public class SubCategoryDialog extends DialogFragment {

    private SubCategoryAdapter subCategoryAdapter;
    private RecyclerView rvSubCateList;
    private FilterVo filterVo;
    private ArrayList<String> subCateList;
    private Context mContext;
    private ImageView imgClose;
    private Button btn_cancel;
    private TextView txt_header;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_subcategory_layout, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog() != null && getDialog().getWindow() != null) {
            int height = getResources().getDimensionPixelSize(R.dimen.popup_height);
            int width = getResources().getDimensionPixelSize(R.dimen.popup_width);
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, height);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getDialog() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            getDialog().setCancelable(false);
            getDialog().setCanceledOnTouchOutside(false);


        }

        rvSubCateList = view.findViewById(R.id.recyclerView_subcat);
        imgClose = view.findViewById(R.id.imgClose);
        txt_header = view.findViewById(R.id.txt_header);
        btn_cancel = view.findViewById(R.id.btn_cancel);

        mContext = getActivity();

        setTypeFaceTextView(txt_header, getActivity().getString(R.string.fontOpenSansSemiBold));
        setTypeFace(btn_cancel, mContext.getString(R.string.fontOpenSansSemiBold));

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
            }
        });

        subCateList = new ArrayList<>();
        readBundle(getArguments());
        initAdapter();

    }

    //set typeface
    protected void setTypeFace(Button view, String typeFace) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        view.setTypeface(tf);
    }

    //set typeface
    protected void setTypeFaceTextView(TextView view, String typeFace) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        view.setTypeface(tf);
    }



    private void readBundle(Bundle data) {
        if (data != null && data.containsKey("data")) {
            filterVo = (FilterVo) data.getSerializable("data");
            txt_header.setText(filterVo.getName());
            subCateList.addAll(filterVo.getSubCateList());
        }
    }

    private void initAdapter() {
        subCategoryAdapter = new SubCategoryAdapter(subCateList, mContext);
        rvSubCateList.setLayoutManager(new LinearLayoutManager(mContext));
        rvSubCateList.setAdapter(subCategoryAdapter);
    }
}
