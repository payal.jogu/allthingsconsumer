package com.allthings.consumer.filter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Payal jogu on 27/7/17.
 */

public class FilterVo implements Serializable {

    private String name="";

    private ArrayList<String> subCateList = new ArrayList<>();

    public ArrayList<String> getSubCateList() {
        return subCateList;
    }

    public void setSubCateList(ArrayList<String> subCateList) {
        this.subCateList = subCateList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public boolean isSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(boolean selectedItem) {
        this.selectedItem = selectedItem;
    }

    private boolean selectedItem;
}
