package com.allthings.consumer.filter;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.R;
import com.allthings.consumer.offers.SelectedPosition;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Payal jogu on 27/7/17.
 */

public class FilterAdapter extends BaseExpandableListAdapter {
    // Define activity context
    private static final String TAG = "CategoriesAdapter";
    private Context mContext;
    private SelectedPosition selectedPosition;

    /*
     * Here we have a Hashmap containing a String key
     * (can be Integer or other type but I was testing
     * with contacts so I used contact name as the key)
     */
    private HashMap<String, List<FilterVo>> mListDataChild;

    // ArrayList that is what each key in the above
    // hashmap points to
    private List<String> mListDataGroup;

    // Hashmap for keeping track of our checkbox check states
    public HashMap<Integer, boolean[]> mChildCheckStates, mChildSelectStates;

    // Our getChildView & getGroupView use the viewholder patter
    // Here are the viewholders defined, the inner classes are
    // at the bottom
    private FilterAdapter.ChildViewHolder childViewHolder;
    private FilterAdapter.GroupViewHolder groupViewHolder;

    /*
     *  For the purpose of this document, I'm only using a single
     *	textview in the group (parent) and child, but you're limited only
     *	by your XML view for each group item :)
     */
    private String groupText;
    private String childText;

    /*  Here's the constructor we'll use to pass in our calling
     *  activity's context, group items, and child items
     */
    public FilterAdapter(Context context,
                         List<String> listDataGroup, HashMap<String, List<FilterVo>> listDataChild,
                         SelectedPosition selectedPosition) {

        mContext = context;
        mListDataGroup = listDataGroup;
        mListDataChild = listDataChild;
        this.selectedPosition = selectedPosition;

        // Initialize our hashmap containing our check states here
        mChildCheckStates = new HashMap<Integer, boolean[]>();
        mChildSelectStates = new HashMap<Integer, boolean[]>();

    }

    @Override
    public int getGroupCount() {
        return mListDataGroup.size();
    }

    /*
     * This defaults to "public object getGroup" if you auto import the methods
     * I've always make a point to change it from "object" to whatever item
     * I passed through the constructor
     */
    @Override
    public String getGroup(int groupPosition) {
        return mListDataGroup.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        //  I passed a text string into an activity holding a getter/setter
        //  which I passed in through "ExpListGroupItems".
        //  Here is where I call the getter to get that text
        groupText = getGroup(groupPosition);

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.category_list_row, null);

            // Initialize the GroupViewHolder defined at the bottom of this document
            groupViewHolder = new FilterAdapter.GroupViewHolder();
            groupViewHolder.mGroupText = convertView.findViewById(R.id.tvCategoryName);

            convertView.setTag(groupViewHolder);
        } else {

            groupViewHolder = (FilterAdapter.GroupViewHolder) convertView.getTag();
        }

        // groupViewHolder.mGroupText.setText(groupText);
        groupViewHolder.mGroupText.setTypeface(null, Typeface.BOLD);

        //String header = mListDataGroup.get(groupPosition);
        //header = String.valueOf(header.charAt(0)).toUpperCase() + header.subSequence(1, header.length());
        groupViewHolder.mGroupText.setText(groupText);

        if (isExpanded) {
            groupViewHolder.mGroupText.setSelected(true);
            groupViewHolder.mGroupText.setBackground(ContextCompat.getDrawable(mContext, R.drawable.selected_edit_text));
            groupViewHolder.mGroupText.setTextColor(ContextCompat.getColor(mContext, R.color.colorRed));
        } else {
            groupViewHolder.mGroupText.setSelected(false);
            groupViewHolder.mGroupText.setBackground(ContextCompat.getDrawable(mContext, R.drawable.edittext_rect));
            groupViewHolder.mGroupText.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        return mListDataChild.get(mListDataGroup.get(groupPosition)).size();

    }

    /*
     * This defaults to "public object getChild" if you auto import the methods
     * I've always make a point to change it from "object" to whatever item
     * I passed through the constructor
     */
    @Override
    public String getChild(int groupPosition, int childPosition) {
        return mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).getName();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final int mGroupPosition = groupPosition;
        final int mChildPosition = childPosition;

        //  I passed a text string into an activity holding a getter/setter
        //  which I passed in through "ExpListChildItems".
        //  Here is where I call the getter to get that text
        childText = getChild(mGroupPosition, mChildPosition);

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.sub_category_list_row, null);

            childViewHolder = new FilterAdapter.ChildViewHolder();


            childViewHolder.mCheckBox = convertView
                    .findViewById(R.id.chkSelect);

            childViewHolder.tvSubCategoryName = convertView.findViewById(R.id.tvSubCategoryName);

            childViewHolder.imgInfo = convertView.findViewById(R.id.imgInfo);

            childViewHolder.layoutChildParent = convertView
                    .findViewById(R.id.layoutChildParent);

            convertView.setTag(R.layout.sub_category_list_row, childViewHolder);

        } else {

            childViewHolder = (FilterAdapter.ChildViewHolder) convertView
                    .getTag(R.layout.sub_category_list_row);
        }

        /*
         * You have to set the onCheckChangedListener to null
         * before restoring check states because each call to
         * "setChecked" is accompanied by a call to the
         * onCheckChangedListener
         */
        childViewHolder.mCheckBox.setOnCheckedChangeListener(null);
        //childViewHolder.mCheckBox.setOnClickListener(null);

        //setSelectedCategory();

        //  childViewHolder.mChildText.setOnClickListener(null);
        boolean isSelected = mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).isSelectedItem();

        if (isSelected) {
            childViewHolder.tvSubCategoryName.setTextColor(ContextCompat.getColor(mContext, R.color.colorRed));
        } else {
            childViewHolder.tvSubCategoryName.setTextColor(ContextCompat.getColor(mContext, R.color.colorGrey));
        }

        String name = mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).getName();
        childViewHolder.mCheckBox.setChecked(isSelected);

        String categoryName = name;
        categoryName = String.valueOf(categoryName.charAt(0)).toUpperCase() + categoryName.subSequence(1, categoryName.length());
        childViewHolder.tvSubCategoryName.setText(categoryName);
        /*if (groupPosition == 0)
        {
            boolean isSelected=mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).isSelectedItem();
            String name=mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).getName();
            childViewHolder.mCheckBox.setChecked(isSelected);
            childViewHolder.mCheckBox.setText(name);
        }
        else if (groupPosition == 1)
        {
            boolean isSelected=mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).isSelectedItem();
            String name=mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).getName();
            childViewHolder.mCheckBox.setChecked(isSelected);
            childViewHolder.mCheckBox.setText(name);
        }
        else if (groupPosition == 2)
        {
            boolean isSelected=mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).isSelectedItem();
            String name=mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).getName();
            childViewHolder.mCheckBox.setChecked(isSelected);
            childViewHolder.mCheckBox.setText(name);
        }*/

        childViewHolder.tvSubCategoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterVo filterVo = mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition);
                //mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).setSelectedItem(isChecked);
                if (!filterVo.isSelectedItem()) {
                    selectedPosition.onItemClicked(mGroupPosition, mChildPosition, true, filterVo);
                } else {
                    selectedPosition.onItemClicked(mGroupPosition, mChildPosition, false, filterVo);
                }

            }
        });

        childViewHolder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                FilterVo filterVo = mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition);
                //mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).setSelectedItem(isChecked);

                selectedPosition.onItemClicked(mGroupPosition, mChildPosition, isChecked, filterVo);

            }
        });

        childViewHolder.imgInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(mContext,"Category",Toast.LENGTH_SHORT).show();

                if(mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition).getSubCateList().size() > 0) {
                    SubCategoryDialog dialogFragment = new SubCategoryDialog();

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition));

                    dialogFragment.setArguments(bundle);
                    dialogFragment.show(((BaseActivity) mContext).getFragmentManager(), "dialog");
                }else
                    Toast.makeText(mContext,mContext.getString(R.string.subcategory_not_found),Toast.LENGTH_SHORT).show();
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    public final class GroupViewHolder {

        TextView mGroupText;
    }

    public final class ChildViewHolder {

        CheckBox mCheckBox;
        RelativeLayout layoutChildParent;
        TextView tvSubCategoryName;
        ImageView imgInfo;
    }
}



