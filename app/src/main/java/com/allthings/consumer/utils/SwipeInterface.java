package com.allthings.consumer.utils;

import android.view.View;

/**
 * Created by ABC on 6/7/2017.
 */

public interface SwipeInterface {
    public void onLeftToRight(View v);

    public void onRightToLeft(View v);
}
