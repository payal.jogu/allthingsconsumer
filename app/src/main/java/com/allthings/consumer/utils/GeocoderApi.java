package com.allthings.consumer.utils;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class GeocoderApi extends AsyncTask<String, Void, String[]> {
    //ProgressDialog dialog = new ProgressDialog(MainActivity.this);
    OnResultInterface onResultInterface;

    public GeocoderApi(OnResultInterface onResultInterface) {
        this.onResultInterface = onResultInterface;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
            /*dialog.setMessage("Please wait...");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();*/
    }

    @Override
    protected String[] doInBackground(String... params) {
        String address = params[0];
        String response;
        try {
            response = getLatLongByURL("https://maps.google.com/maps/api/geocode/json?latlng="
                    + address + "&sensor=true&key=AIzaSyDPpUQ7t4RO5SqW6bf6UwwuIwiJj6RdLHU");//latlng=40.714224,-73.961452
            Log.d("response", "" + response);
            return new String[]{response};
        } catch (Exception e) {
            return new String[]{"error"};
        }
    }

    @Override
    protected void onPostExecute(String... result) {
        try {
            JSONObject jsonObject = new JSONObject(result[0]);

           /* double lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lng");

            double lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lat");*/

            String postalTown = "";
            String countryName = "";
            if(jsonObject.has("results")) {
                JSONArray addressComArray = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components");
                for (int i = 0; i < addressComArray.length(); i++) {
                    if ((addressComArray.getJSONObject(i).getJSONArray("types").getString(0).equalsIgnoreCase("postal_town"))) {
                        postalTown = addressComArray.getJSONObject(i).getString("long_name");
                        break;
                    } /*else if (addressComArray.getJSONObject(i).getJSONArray("types").getString(0).equalsIgnoreCase("locality")) {
                    postalTown = addressComArray.getJSONObject(i).getString("long_name");
                    break;
                }*/
                }

                if (postalTown.equalsIgnoreCase("")) {
                    for (int i = 0; i < addressComArray.length(); i++) {
                        if (addressComArray.getJSONObject(i).getJSONArray("types").getString(0).equalsIgnoreCase("locality")) {
                            postalTown = addressComArray.getJSONObject(i).getString("long_name");
                            break;
                        }
                    }
                }

                for (int i = 0; i < addressComArray.length(); i++) {
                    if ((addressComArray.getJSONObject(i).getJSONArray("types").getString(0).equalsIgnoreCase("country"))) {
                        countryName = addressComArray.getJSONObject(i).getString("long_name");
                        break;
                    } /*else if (addressComArray.getJSONObject(i).getJSONArray("types").getString(0).equalsIgnoreCase("locality")) {
                    postalTown = addressComArray.getJSONObject(i).getString("long_name");
                    break;
                }*/
                }
            }

           /* if(((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONArray("address_components").getJSONObject(3)
                    .getJSONArray("types").getString(0).equalsIgnoreCase("postal_town")) {

                 postalTown = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(3).getString("long_name");
            }else {
                postalTown = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(2).getString("long_name");
            }*/

            Log.d("postalTown", "" + postalTown);
            Log.d("countryName", "" + countryName);

            if (postalTown != null) {
                onResultInterface.getResult(postalTown,countryName);
            } else {
                onResultInterface.getResult("",countryName);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            onResultInterface.getResult("","");
        }
            /*if (dialog.isShowing()) {
                dialog.dismiss();
            }*/
    }

    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            /*StringBuilder jsonResults = new StringBuilder();
            url = new URL(requestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
            response = jsonResults.toString();*/
            url = new URL(requestURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            //connection.setRequestMethod("GET");
            connection.connect();

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {

                BufferedReader reader = null;

                InputStream inputStream = connection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {

                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    return null;
                }

                Log.d("", buffer.toString());
                response = buffer.toString();
            } else {
                Log.i("", "Unsuccessful HTTP Response Code: " + responseCode + connection.getResponseMessage());
            }
           /* HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}





