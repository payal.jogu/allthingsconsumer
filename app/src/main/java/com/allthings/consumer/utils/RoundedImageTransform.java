package com.allthings.consumer.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.squareup.picasso.Transformation;

public class RoundedImageTransform implements Transformation {
    private final int margin;// dp
    private Context context;
    private Bitmap newBitmap;
    private int drawableId=0;
    // radius is corner radii in dp
    // margin is the board in dp
    public RoundedImageTransform(Context context, final int margin, int drawableId) {
        this.margin = margin;
        this.context = context;
        this.drawableId =drawableId;
    }

    @Override
    public Bitmap transform(final Bitmap source) {
    	//Drawable drawable = context.getResources().getDrawable(drawableId);
        Drawable drawable = ContextCompat.getDrawable(context,drawableId);
    	
    	float scale = Math.min(((float)drawable.getIntrinsicHeight() / source.getWidth()), ((float) drawable.getIntrinsicWidth() / source.getHeight()));

    	Matrix matrix = new Matrix();
    	matrix.postScale(scale, scale);

    	Bitmap newBitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

    	final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(new BitmapShader(newBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        Bitmap output = Bitmap.createBitmap(drawable.getIntrinsicHeight(), drawable.getIntrinsicWidth(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        canvas.drawCircle((drawable.getIntrinsicHeight()- margin)/2, (drawable.getIntrinsicWidth() - margin)/2, getDrawableSize(), paint);

        if (source != output) 
        {
            source.recycle();
        }
        return output;
    }

    @Override
    public String key() {
        return getClass().getName();
    }
    public int getDrawableSize()
    {
        Drawable drawable = ContextCompat.getDrawable(context,drawableId);
    	//Drawable drawable = context.getResources().getDrawable(drawableId);
    	return  Math.max(drawable.getIntrinsicHeight()/2,drawable.getIntrinsicWidth()/2);
    }

}
