package com.allthings.consumer.utils;

/**
 * Created by bypt4 on 1/6/17.
 */

public class Constants {

    public static String IS_LOGIN ="is_login";
    public static boolean DEV_MODE = true;
    public static boolean  IS_DEV_MODE = false;

    public static final String DEV_SERVER_KEY =
            "AAAAvag7GpE:APA91bHJj7OizYEqpIiOXRQhEXSkAY2boeUFuYFKpq2YFbHWl15WJXIgm2Wd4mGXCDeK0S_ypfbhtdfbtvW4h0zlSFk2AKyKl30sfZSwJG5LApGeYMBcGwvuJgLpk3Jz2KE5ekE50-6a";
            //"AAAAxkUOQ9Q:APA91bHIG2-Lg3w1jhNxBy1_3fA_2ws2KUEogPUCAdV2Ep5JSybQLmyfLprFx4PI5FfIX4mYPJYrPIvalR7fU1Z-GCOMwqsnpB0-J5UKZtMT2vGMQkIKX_LoIjGI0RqahMgOfIa3WmK1ozPIGVRaJsGi9Mq3Sy85IQ";
            //"AAAASKsHJ9I:APA91bFV79bzzUNfaK2pbnaYoGxeYGrJv6k1BimxTMSX0DACd2Hs013epv1zsgbi0bVhuAlsxZHJMAUiGgu9f3-qgr8wzHb7VArj31uh5J_O_jueJjCtVoQzC-aGJkL7_IyxUEtJf8ph";

    public static final String CLIENT_SERVER_KEY =
            "AAAAvag7GpE:APA91bHJj7OizYEqpIiOXRQhEXSkAY2boeUFuYFKpq2YFbHWl15WJXIgm2Wd4mGXCDeK0S_ypfbhtdfbtvW4h0zlSFk2AKyKl30sfZSwJG5LApGeYMBcGwvuJgLpk3Jz2KE5ekE50-6a";
            //"AAAAxkUOQ9Q:APA91bHIG2-Lg3w1jhNxBy1_3fA_2ws2KUEogPUCAdV2Ep5JSybQLmyfLprFx4PI5FfIX4mYPJYrPIvalR7fU1Z-GCOMwqsnpB0-J5UKZtMT2vGMQkIKX_LoIjGI0RqahMgOfIa3WmK1ozPIGVRaJsGi9Mq3Sy85IQ";
           // "AAAAc7j_oFE:APA91bG3GTWPsq3v2JK9bgKYKHajxhvL0616Sj3qI7iiPIx9NvuZMKGAP8UIytYh9tc2Mdy3_qdVG0rjFhqQFOYuDXxSisVign9HtaaK9H7rlHI18vCUccKMdTjN5_NERIlM3M5_CLLq" ;

    public static final String REGISTER_FRAGMENT = "RegisterFragment";
    public static final String OFFER_FRAGMENT = "OfferFragment";
    //public static final String CURRENT_SCREEN = "CurrentScreen";
   /* Database Constants*/
    public static final String COUNTRY_MASTER = "countryMaster";
    public static final String COUNTY_MASTER ="countyMaster";
    public static final String OFFER_TYPE_MASTER = "offerTypeMaster";
    public static final String SUBSCRIPTION_TYPE_MASTER = "subscriptionPlanMaster";
    public static final String CONSUMER_USER_MASTER = "consumerUser";
    public static final String OFFER_NAME = "offerName";
    public static final String OFFER_STATUS = "offerStatus";
    public static final String CATEGORY_MASTER = "categoryMaster";
    public static final String REGISTER_AREA_RADIUS = "areaRadius";
    public static final String USER_ID = "userId";
    public static final String GOLD = "gold";
    public static final String SILVER = "silver";
    public static final String BRONZE = "bronze";
    public static final String CATEGORY = "selectedCategory";
    public static final String PROFILE_PIC_PATH = "profilePath";
    public static final String STATUS = "status";
    public static final String STATUS_VALUE = "1";
    public static final String EMAIL = "email";
    public static final String COUNTY = "county";
    public static final String COUNTRY = "country";
    public static final String LOCATION = "location";
    public static final String PHONENUMBER = "phoneNo";
    public static final String  PROFILEPHOTOURL= "profileImageURL";
    public static final String  LATITUDE= "latitude";
    public static final String  LONGITUDE= "longitude";
    public static final String OFFER = "offer";
    public static final String FROM_OFFERLIST = "from_offerlist";
    public static final String OFFER_ID = "offerID";
    public static final String CREATED_DATE = "createDate";
    public static final String CREATED_BY = "createBy";
    public static final String UPDATED_DATE = "updateDate";
    public static final String UPDATED_BY = "updateBy";
    public static final String CITY = "city";
    public static final String GENDER = "gender";
    public static final String COMPANY_ADDRESS = "companyAddress";


    public static final String OFFER_RATING_TABLE="offerRating";
    public static final String COUNTRY_NAME="countryName";
    public static final String COUNTRY_REGION_CODE="regionCode";
    public static final String CURRENT_FRAGMENT="current_fragment";
    public static final String FEEDBACK_RATINGS="feedbackRatings";
    public static final String COUNTRY_CODE="countryCode";
    public static final String CHAT_ROOM_TB="chatRooms";
    public static final String USER_MESSAGES_TB="userMessages";
    public static final String BUSINESS_USER_TB="businessUser";
    public static final String TB_TYPING_INDICATOR="typingIndicator";
    public static final String CHAT_STORAGE="user_messages_images";
    public static final String MESSAGES="messages";
    public static final String TO_NAME="toName";
    public static final String TO_ID="toId";
    public static final String FROM_ID="fromId";
    public static final String TEXT="text";
    public static final String COMPANY_LOGOURL="companyLogoURL";

    public static final String OFFER_TITLE="offerTitle";
    public static final String OFFER_EXPIRY_DATE="offerExpiryDate";
    public static final String OFFER_IMAGE_URL="offerImageURL";

    public static final String FCM_TOKEN="FCMToken";

    public static final String LOCAL="Local";
    public static final String NATIONAL ="National";
    public static final String PLAN_TYPE = "planType";

    public static final String SENDERID="senderID";
    public static final String RECEIVERID="receiverID";
    public static final String SENDER_EMAIL="senderEmail";
    public static final String RECEIVER_EMAIL="receiverEmail";
    public static final String OFFERID="offerID";
    public static final String OFFER_IMAGE="offerImage";
    public static final String NOTIFICATION_TYPE="notificationType";

    public static final String GENDER_TYPE_ALL = "All";


    /*Advanced search*/
    public static final String SEARCH_CATEGORY="SearchCategory";
    public static final String SEARCH_OFFER_TYPE="SearchOfferType";
    public static final String SEARCH_LOCATION="SearchLocation";
    public static final String SEARCH_GENDER="SearchGender";

    public static final String SEARCHCATEGORY = "category";
    public static final String USER_BLOCK = "userBlock";

    public static final String TERMS_CONDITION_URL = "https://firebasestorage.googleapis.com/v0/b/allthings-fcd26.appspot.com/o/Terms_condition%2Fallthings_terms_condition.html?alt=media&token=d4053857-c725-4313-975f-3debc4fc4be5";








}
