package com.allthings.consumer.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.allthings.consumer.R;

/**
 * Created by Vishal Dhaduk on 6/29/2017.
 */

public class AlertDialogUtil {
    public static void showDialogAlertPositiveButton(String title, String message, Context mContext) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(message)
                .setCancelable(false)
                .setTitle(title)
                .setPositiveButton(mContext.getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

        AlertDialog alert = builder.create();
        alert.show();

    }
}
