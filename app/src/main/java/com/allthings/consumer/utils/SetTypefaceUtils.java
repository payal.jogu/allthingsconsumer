package com.allthings.consumer.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Created by Vishal Dhaduk on 6/29/2017.
 */

public class SetTypefaceUtils {



    //set typeface
    public static void setTypeFaceTextView(TextView txtView, String typeFace, Context mContext) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        txtView.setTypeface(tf);
    }

    public static void setTypeFaceEdittext(EditText editText, String typeFace,Context mContext) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        editText.setTypeface(tf);
    }
    public static void setTypeFaceAutoCompleteTextView(AutoCompleteTextView autoCompleteTextView, String typeFace,Context mContext) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        autoCompleteTextView.setTypeface(tf);
    }

    public static void setTypeFaceButton(Button button, String typeFace,Context mContext) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        button.setTypeface(tf);
    }

    public static void setTypeFaceRadioButton(RadioButton button, String typeFace,Context mContext) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        button.setTypeface(tf);
    }

    public static void setTypeFaceCheckbox(CheckBox checkBox, String typeFace,Context mContext) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        checkBox.setTypeface(tf);
    }

}
