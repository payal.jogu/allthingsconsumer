package com.allthings.consumer.login;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListPopupWindow;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.register.RegisterBasicDetailsFragment;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.NetworkUtil;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * Created by ABC on 6/15/2017.
 */

public class LoginFragment extends BaseFragment implements View.OnClickListener {

    private final static String TAG = "LoginFragment";
    private View rootView;
    private TextView tvAppsLogo, tvPhone, tvOr, tvLoginCountryCode;
    private EditText etPhoneNumber;
    private Button btnNext, btnRegister;
    private DatabaseReference mRefrenceToCountry;
    private ArrayList<String> countryCodeList;
    ArrayAdapter<String> dataAdapter;
    private DatabaseReference mRefrenceToConsumerUser;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_login, container, false);
            init();
            firebaseSignIn();
            getCountryCode();
            setListeners();
            setFontStyle();
            disableBack(rootView);
            return rootView;
        } else {
            firebaseSignIn();
            return rootView;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // firebaseSignIn();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    //Set toolbar settings
    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).showHideToolbarBackButton(false);
        ((BaseActivity) mContext).showHideToolbarMenuButton(false);
        ((BaseActivity) mContext).showHideToolbarSearchButton(false);
        ((BaseActivity) mContext).showHideToolbarChatOfferTitle(false);
        ((BaseActivity) mContext).showHideToolbarChatEmail(false);
        ((BaseActivity) mContext).setToolbarTitle(mContext.getString(R.string.l_login));

        ((BaseActivity) mContext).showHideToolbarSerachEdittext(false);
        ((BaseActivity) mContext).showHideToolbarSearchCancelButton(false);
        ((BaseActivity) mContext).showHideToolbarTitle(true);
        ((BaseActivity) mContext).showHideToolbarChatEmail(false);
        ((BaseActivity) mContext).showHideToolbarChatImage(false);
        ((BaseActivity) mContext).showHideToolbarChatOfferTitle(false);
        ((BaseActivity) mContext).showHideToolbarRemainder(false);


    }

    //Set The Fonts Styles
    private void setFontStyle() {
        setTypeFaceTextView(tvAppsLogo, mContext.getString(R.string.fontOpenSansBold));
        setTypeFaceTextView(tvOr, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvPhone, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceEdittext(etPhoneNumber, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvLoginCountryCode, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceButton(btnNext, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceButton(btnRegister, mContext.getString(R.string.fontOpenSansSemiBold));

    }

    //Set The Listeners
    private void setListeners() {
        btnNext.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        tvLoginCountryCode.setOnClickListener(this);

    }

    //Get Country Code List From Firebase
    private void getCountryCode() {
        if (NetworkUtil.isOnline(mContext)) {
            //progressDialog.show();
            countryCodeList.clear();
            mRefrenceToCountry.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //progressDialog.dismiss();
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        if (child.hasChild(Constants.STATUS) && child.child(Constants.STATUS).getValue().equals(Constants.STATUS_VALUE)) {
                            if (!countryCodeList.contains(child.child("countryCode").getValue(String.class)))
                                countryCodeList.add(child.child("countryCode").getValue(String.class));
                        }
                    }
                    dataAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    //progressDialog.dismiss();
                    Log.e(TAG, databaseError.getMessage());
                }
            });
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    //Show Country Code Choose Dialog
    private void showCountryCodeDialog() {
        if (NetworkUtil.isOnline(mContext)) {

            final ListPopupWindow lpw = new ListPopupWindow(mContext);
            lpw.setAdapter(dataAdapter);
            lpw.setAnchorView(tvLoginCountryCode);
            lpw.setModal(true);
            lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    String item = countryCodeList.get(position);
                    tvLoginCountryCode.setText(item);

                    lpw.dismiss();
                }
            });
            lpw.show();

        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }

    }

    //Initialize The Components
    private void init() {

        tvAppsLogo = rootView.findViewById(R.id.tvAppsLogo);
        tvPhone = rootView.findViewById(R.id.tvPhone);
        tvOr = rootView.findViewById(R.id.tvOr);
        tvLoginCountryCode = rootView.findViewById(R.id.tvLoginCountryCode);

        etPhoneNumber = rootView.findViewById(R.id.etLoginPhone);

        btnNext = rootView.findViewById(R.id.btn_login_next);
        btnRegister = rootView.findViewById(R.id.btn_login_register);

        mRefrenceToCountry = mDatabase.getReference(Constants.COUNTRY_MASTER);
        mRefrenceToConsumerUser = mDatabase.getReference(Constants.CONSUMER_USER_MASTER);

        countryCodeList = new ArrayList<>();
        dataAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, countryCodeList);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login_next:
                navigateToOtherScreen(1);//Go to Passcode
                break;
            case R.id.btn_login_register:
                navigateToOtherScreen(2); // Go to register
                break;
            case R.id.tvLoginCountryCode://Get Country Code List
                if (countryCodeList.size() == 0) {
                    getCountryCode();
                }
                showCountryCodeDialog();
                break;


        }
    }

    //Go to other screens
    private void navigateToOtherScreen(int i) {
        hideKeyboard();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
                R.anim.enter_from_left, R.anim.exit_to_right);
        if (i == 1) {
            if (isValidDetails()) {
                if (NetworkUtil.isOnline(mContext)) {
                    progressDialog.show();

                    Log.e(TAG, "CountryCode" + tvLoginCountryCode.getText().toString());
                    Log.e(TAG, "Phone No :" + etPhoneNumber.getText().toString().trim());

                    //Check phone number register or not in firebase
                    Query phoneNumber = mDatabase.getReference(Constants.CONSUMER_USER_MASTER)
                            .orderByChild(Constants.PHONENUMBER).equalTo(tvLoginCountryCode.getText().toString() +
                                    etPhoneNumber.getText().toString().trim());
                    phoneNumber.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.e(TAG, "UserCount" + dataSnapshot.getChildrenCount());
                            progressDialog.dismiss();
                            if (dataSnapshot.getValue() != null) {

                                DataSnapshot child = null ;

                                for(DataSnapshot childSnapshot : dataSnapshot.getChildren()){
                                    child = childSnapshot;
                                }


                                if(child != null) {
                                    if (child.hasChild("status") && child.child("status").getValue().equals("0")) {
                                        showToast(mContext.getString(R.string.deactivate_user));
                                    } else {
                                        Log.e("EXIST", "EXIST");
                                        Bundle bundle = new Bundle();
                                        PasscodeFragment passcodeFragment = new PasscodeFragment();
                                        bundle.putString(Constants.PHONENUMBER,
                                                tvLoginCountryCode.getText().toString().trim() + etPhoneNumber.getText().toString().trim());
                                        passcodeFragment.setArguments(bundle);
                                        fragmentTransaction.replace(R.id.frameLayoutBaseContainer,
                                                passcodeFragment).addToBackStack("LoginFragment").commit();
                                    }
                                }

                            } else {
                                // Log.e("NOT EXIST","NOT EXIST");
                                progressDialog.dismiss();
                                showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings),
                                        mContext.getString(R.string.please_register_your_phone));
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progressDialog.dismiss();
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });


             /*       Query phonenumber = mDatabase.getReference().child(Constants.CONSUMER_USER_MASTER).
                            orderByChild(Constants.PHONENUMBER).
                            equalTo(tvLoginCountryCode.getText().toString() + etPhoneNumber.getText().toString().trim());

                    phonenumber.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {


                        }
                    });*/
                } else {
                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
                }

            }
        } else if (i == 2) {
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new RegisterBasicDetailsFragment())
                    .addToBackStack("LoginFragment")
                    .commit();
        }
    }

    //Validations
    private boolean isValidDetails() {
        if (tvLoginCountryCode.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_select_countrycode));
            tvLoginCountryCode.requestFocus();
            return false;
        }
        if (etPhoneNumber.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_phonenumber));
            etPhoneNumber.requestFocus();
            return false;
        }

        if (etPhoneNumber.getText().toString().trim().length() < 6 || etPhoneNumber.getText().toString().trim().length() > 15) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_valid_phone));
            etPhoneNumber.requestFocus();
            return false;
        }
        return true;
    }


}