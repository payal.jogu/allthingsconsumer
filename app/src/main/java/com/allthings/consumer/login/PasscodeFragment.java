package com.allthings.consumer.login;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.offers.BrowseOffersFragment;
import com.allthings.consumer.register.RegisterVerificationFragment;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ABC on 6/15/2017.
 */

public class PasscodeFragment extends BaseFragment implements View.OnClickListener {

    private final static String TAG = "PasscodeFragment";
    private View rootView;
    private TextView tvEnterPasscode, tvSendOtp, tvTimer, tvSixDigit;
    private EditText etOtp;
    private Button btnLogin;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    String mVerificationId = "", token1 = "";
    DatabaseReference tbRegister;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_passcode, container, false);
            logout();
            init();
            sendOTP();
            setListeners();
            setFontStyle();
            return rootView;
        } else {
            logout();
            return rootView;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   logout();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    //Set toolbar settings
    @Override
    public void onResume() {
        // LocalBroadcastManager.getInstance(mContext).registerReceiver(receiver, new IntentFilter("otp"));

        super.onResume();
        ((BaseActivity) mContext).showToolbar(true);
        ((BaseActivity) mContext).showHideToolbarBackButton(true);
        ((BaseActivity) mContext).showHideToolbarMenuButton(false);
        ((BaseActivity) mContext).showHideToolbarSearchButton(false);
        ((BaseActivity) mContext).setToolbarTitle(mContext.getString(R.string.passcode));

    }

    @Override
    public void onPause() {
        super.onPause();
        // LocalBroadcastManager.getInstance(mContext).unregisterReceiver(receiver);
    }

    //set font styles
    private void setFontStyle() {
        setTypeFaceTextView(tvEnterPasscode, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvSendOtp, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvTimer, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(tvSixDigit, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceButton(btnLogin, mContext.getString(R.string.fontOpenSansSemiBold));
        setTypeFaceEdittext(etOtp, mContext.getString(R.string.fontOpenSansSemiBold));

    }

    //set listeners
    private void setListeners() {
        btnLogin.setOnClickListener(this);
        tvSendOtp.setOnClickListener(this);

       /* etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Log.e(TAG,"onText Changedc"+charSequence+"i"+i+"i1"+i1+"i2"+i2);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
               // Log.e(TAG,"onText Changed c"+charSequence+"i"+i+"i1"+i1);

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.e(TAG,"e"+editable);

                if(editable.length() == 3){
                    etOtp.setText(etOtp.getText().toString()+" ");
                    etOtp.setSelection(etOtp.getText().length());
                }
            }
        });*/

    }

    //initialize the components
    private void init() {
        tvEnterPasscode = (TextView) rootView.findViewById(R.id.tvPleaseEnterPasscode);
        tvSendOtp = (TextView) rootView.findViewById(R.id.tvOTP);
        tvTimer = (TextView) rootView.findViewById(R.id.tvTimer);
        tvSixDigit = (TextView) rootView.findViewById(R.id.tvEnterSixDigit);
        btnLogin = (Button) rootView.findViewById(R.id.btn_passcode_login);
        etOtp = (EditText) rootView.findViewById(R.id.etOTP);
        tvEnterPasscode.setText(mContext.getString(R.string.l_verify) + " " + getArguments().getString(Constants.PHONENUMBER));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_passcode_login:
                if (isValidDetails()) {
                    getVerifyOtp(); // go to verify OTP
                    //btnLogin.setEnabled(false);
                }
                break;
            case R.id.tvOTP:
                if (tvTimer.getText().equals(mContext.getString(R.string.timer))) {
                    sendOTP(); //Send OTP
                }

                break;
        }
    }

    //Validations
    private boolean isValidDetails() {
        if (etOtp.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_otp));
            etOtp.requestFocus();
            return false;
        }
        return true;
    }

    //Send OTP
    private void sendOTP() {
        if (NetworkUtil.isOnline(mContext)) {
           // Log.e(TAG, "PhoneNo:" + getArguments().getString(Constants.PHONENUMBER));
            mPhoneProvider.verifyPhoneNumber(
                    getArguments().getString(Constants.PHONENUMBER),// Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    getActivity(),               // Activity (for callback binding)
                    mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                        @Override
                        public void onVerificationCompleted(final PhoneAuthCredential credential) {
                            // This callback will be invoked in two situations:
                            // 1 - Instant verification. In some cases the phone number can be instantly
                            //     verified without needing to send or enter a verification code.
                            // 2 - Auto-retrieval. On some devices Google Play services can automatically
                            //     detect the incoming verification SMS and perform verificaiton without
                            //     user action.
                            Log.e(TAG, "onVerificationCompleted:" + credential);

                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setMessage("Instant Mobile No. Verification Completed")
                                    .setCancelable(false)
                                    .setTitle(getString(R.string.app_name))
                                    .setPositiveButton(mContext.getString(R.string.ok),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                    signInWithPhoneAuthCredential(credential);
                                                }
                                            });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }

                        @Override
                        public void onVerificationFailed(FirebaseException e) {
                            // This callback is invoked in an invalid request for verification is made,
                            // for instance if the the phone number format is not valid.
                            Log.e(TAG, "onVerificationFailed", e);

                            // showToast(e.getMessage());

                            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                                // Invalid request
                                Log.e(TAG, "onVerificationFailed", e);
                                // ...
                            } else if (e instanceof FirebaseTooManyRequestsException) {
                                // The SMS quota for the project has been exceeded
                                // ...
                                Log.e(TAG, "onVerificationFailed", e);
                            }

                            // Show a message and update the UI
                            // ...
                        }

                        @Override
                        public void onCodeSent(String verificationId,
                                               PhoneAuthProvider.ForceResendingToken token) {
                            // The SMS verification code has been sent to the provided phone number, we
                            // now need to ask the user to enter the code and then construct a credential
                            // by combining the code with a verification ID.
                            Log.e(TAG, "onCodeSent:" + verificationId);

                            // Save verification ID and resending token so we can use them later
                            mVerificationId = verificationId;
                            token1 = String.valueOf(token);
                            enableDisableOtpField(true);//enable field
                            Log.e(TAG, "Token:" + token1);

                           /* receiver = new BroadcastReceiver() {
                                @Override
                                public void onReceive(Context context, Intent intent) {
                                    if (intent.getAction().equalsIgnoreCase("otp")) {
                                        final String message = intent.getStringExtra("message");
                                        String code = parseCode(message);//Parse verification code
                                        etOtp.setText(code);
                                        //Do whatever you want with the code here
                                    }
                                }
                            };*/

                            new CountDownTimer(120000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    //tvTimer.setText("00:"+ millisUntilFinished / 1000);
                                    //tvTimer.setText((millisUntilFinished / 60000)+":"+(millisUntilFinished % 60000 / 1000));
                                    tvTimer.setText("" + String.format("%02d:%02d",
                                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                                    //here you can have your logic to set text to edittext
                                }

                                public void onFinish() {
                                    etOtp.setText("");
                                    tvTimer.setText(mContext.getString(R.string.timer));
                                    enableDisableOtpField(false);//disable field
                                }

                            }.start();
                        }
                    }
            );
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
       /* smsVerifyCatcher = new SmsVerifyCatcher(getActivity(), new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = parseCode(message);//Parse verification code
                etOtp.setText(code);//set code in edit text
                //then you can send verification code to server
            }
        });
*/
    }

   /* private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }*/

   private void enableDisableOtpField(boolean status){
       if(status){
           etOtp.setEnabled(true);
           btnLogin.setEnabled(true);
       }else {
           etOtp.setEnabled(false);
           btnLogin.setEnabled(false);
       }
   }

    //Verify the OTP
    private void getVerifyOtp() {
        if (NetworkUtil.isOnline(mContext)) {
            if (mVerificationId != null) {
                try {
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, etOtp.getText().toString().trim());
                    signInWithPhoneAuthCredential(credential);
                }catch (Exception e){
                    LogUtils.LOG_E(TAG,e.getMessage());
                }
            }
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }


    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.e(TAG, "signInWithCredential:success");


                                FirebaseUser user = task.getResult().getUser();
                                if (user.getUid() != null) {
                                    SharedPreferenceUtil.putValue(Constants.USER_ID, user.getUid());
                                    SharedPreferenceUtil.putValue(Constants.IS_LOGIN, "login");
                                    SharedPreferenceUtil.save();

                                    ((BaseActivity)mContext).getFirebaseTokenForFCM();

                                    /*if (!SharedPreferenceUtil.getString("firebaseRegToken", "").equalsIgnoreCase("")) {
                                        HashMap<String, String> deviceModel = new HashMap<>();
                                        deviceModel.put("Device Model", Build.MODEL);
                                        mDatabase.getReference(Constants.CONSUMER_USER_MASTER).child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).
                                                child(Constants.FCM_TOKEN)
                                                .child(SharedPreferenceUtil.getString("firebaseRegToken", ""))
                                                .setValue(deviceModel);
                                    }*/

                                    mDatabase.getReference(Constants.CONSUMER_USER_MASTER).child(SharedPreferenceUtil.getString(Constants.USER_ID, ""))
                                            .addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            progressDialog.dismiss();
                                            SharedPreferenceUtil.putValue(Constants.EMAIL, dataSnapshot.child(Constants.EMAIL).getValue(String.class));
                                            if (dataSnapshot.hasChild(Constants.PHONENUMBER)) {
                                                SharedPreferenceUtil.putValue(Constants.PHONENUMBER, dataSnapshot.child(Constants.PHONENUMBER).getValue(String.class).substring(3));
                                            }
                                            SharedPreferenceUtil.putValue(Constants.COUNTRY_CODE, dataSnapshot.child(Constants.PHONENUMBER).getValue(String.class).substring(0, 3));
                                            SharedPreferenceUtil.putValue(Constants.CITY, dataSnapshot.child(Constants.CITY).getValue(String.class));
                                            SharedPreferenceUtil.putValue(Constants.COUNTRY, dataSnapshot.child(Constants.COUNTRY).getValue(String.class));
                                            SharedPreferenceUtil.putValue(Constants.COUNTY, dataSnapshot.child(Constants.COUNTY).getValue(String.class));
                                            SharedPreferenceUtil.putValue(Constants.LOCATION, dataSnapshot.child(Constants.LOCATION).getValue(String.class));
                                            SharedPreferenceUtil.putValue(Constants.PROFILEPHOTOURL, dataSnapshot.child(Constants.PROFILEPHOTOURL).getValue(String.class));
                                            SharedPreferenceUtil.putValue(Constants.GENDER, dataSnapshot.child(Constants.GENDER).getValue(String.class));
                                            SharedPreferenceUtil.save();
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            progressDialog.dismiss();

                                        }
                                    });

                                    progressDialog.dismiss();
                                    //Check registration process is complete or not
                                    Query status = mDatabase.getReference().child(Constants.CONSUMER_USER_MASTER).
                                            child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).child(Constants.STATUS);

                                    status.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.getValue().equals("0")) {
                                                goToRegisterProcess(); //go to registration  process
                                            } else {
                                                goToBrowseOffers(); // go to home
                                                ((BaseActivity)mContext).checkUserStatus();

                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            progressDialog.dismiss();

                                        }
                                    });

                                }
                                // ...
                            } else {
                                // Sign in failed, display a message and update the UI
                                Log.w(TAG, "signInWithCredential:failure", task.getException());
                                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                    // The verification code entered was invalid
                                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_enter_valid_otp));
                                    progressDialog.dismiss();
                                }
                            }
                        }
                    });
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }

    //go to registration  process
    private void goToRegisterProcess() {
        hideKeyboard();
        //fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new RegisterVerificationFragment()).commit();
        clearBackStack();
    }


    //go to the home screen
    private void goToBrowseOffers() {
        hideKeyboard();

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ((BaseActivity) mContext).setSelectedItem(0);
        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new BrowseOffersFragment()).commit();
        //fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

    }
}