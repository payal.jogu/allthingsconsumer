package com.allthings.consumer.history;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.offers.OffersAdapter;
import com.allthings.consumer.offers.OffersVO;
import com.allthings.consumer.offers.UpdateCategoryTitle;
import com.allthings.consumer.offers.UpdateSearchedData;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.DateUtil;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;

public class ExpiryOffersFragment extends BaseFragment implements View.OnClickListener, SearchView.OnQueryTextListener {
    private static final String TAG = "OffersFragment";
    private View rootView;
    private ArrayList<OffersVO> mOfferList;
    private RecyclerView rvExpiryOffers;
    private Context mContext;
    private static UpdateCategoryTitle updateTitle;
    private DatabaseReference mOfferTable, mConsumerUser;
    public ImageView imageView_placeholder;
    HashMap<String, String> categoryListFromFirebase;
    GenericTypeIndicator<HashMap<String, String>> t;
    private String city, country, gender;

    public static ExpiryOffersFragment newInstance(String title1, UpdateCategoryTitle updateTitle1) {
        Bundle args = new Bundle();
        args.putString("Title", title1);
        ExpiryOffersFragment fragment = new ExpiryOffersFragment();
        fragment.setArguments(args);
        updateTitle = updateTitle1;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_expiry_offers, container, false);
            disableBack(rootView);
            init();
            setRecyclerView();
            setFontStyle();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setRecyclerViewData();
                }
            }, 500);
            //setListeners();
            return rootView;
        } else {
            disableBack(rootView);
            return rootView;
        }
    }

    //Initialize Components
    private void init() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "1");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "OfferFragment");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "text");

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST, bundle);
        mOfferTable = mDatabase.getReference(Constants.OFFER);
        mConsumerUser = mDatabase.getReference(Constants.CONSUMER_USER_MASTER);
        imageView_placeholder = (ImageView) rootView.findViewById(R.id.iv_placeholder);
        t = new GenericTypeIndicator<HashMap<String, String>>() {
        };

        categoryListFromFirebase = new HashMap<String, String>();
        getActivity().findViewById(R.id.toolbar_ivSearchCancel).setOnClickListener(this);
        EventBus.getDefault().register(this);

        //getActivity().findViewById(R.id.ivToolbarsearchbutton).setOnClickListener(this);

    }

    /**
     * Get Offer Category Data From Business and set it according to Category
     */
    private void getOfferCategory() {
        mOfferList.clear();
        if (NetworkUtil.isOnline(mContext)) {
            Query getOfferListQuery = mOfferTable.orderByChild(Constants.COUNTRY).equalTo(country);
            getOfferListQuery.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String s) {
                    if (categoryListFromFirebase != null) {
                        if (dataSnapshot.hasChild(Constants.GENDER)) {
                            if (!gender.isEmpty()) {
                                if (gender.equalsIgnoreCase(dataSnapshot.child(Constants.GENDER).getValue(String.class))) {
                                    getOfferUsingPlanType(dataSnapshot);
                                } else if (dataSnapshot.child(Constants.GENDER).getValue(String.class).equalsIgnoreCase("All")) {
                                    getOfferUsingPlanType(dataSnapshot);
                                }
                            }
                        }

                    }
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {
                    //showToast("Child Change");
                    LogUtils.LOG_E("onChildChanged", dataSnapshot.toString());
                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                    //showToast("Child Remove");
                    LogUtils.LOG_E("onChildRemoved", dataSnapshot.toString());
                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) {
                    //showToast("Child Move");
                    LogUtils.LOG_E("onChildMoved", dataSnapshot.toString());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    //showToast("Child Cancle");

                }
            });
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }

    private void getOfferUsingPlanType(DataSnapshot dataSnapshot) {
        if (dataSnapshot.hasChild(Constants.PLAN_TYPE)) {
            if (dataSnapshot.child(Constants.PLAN_TYPE).getValue(String.class)
                    .equalsIgnoreCase(Constants.LOCAL)) {
                                    /*if (dataSnapshot.hasChild(Constants.COUNTRY)) {
                                        if (dataSnapshot.child(Constants.COUNTRY).getValue(String.class)
                                                .equalsIgnoreCase(country)) {*/
                if (dataSnapshot.hasChild(Constants.CITY)) {
                    if (dataSnapshot.child(Constants.CITY).getValue(String.class)
                            .equalsIgnoreCase(city)) {
                        setOfferList(dataSnapshot);
                    }
                }
                                       /* }
                                    }*/

            } else if (dataSnapshot.child(Constants.PLAN_TYPE).getValue(String.class)
                    .equalsIgnoreCase(Constants.NATIONAL)) {
                                   /* if (dataSnapshot.hasChild(Constants.COUNTRY)) {
                                        if (dataSnapshot.child(Constants.COUNTRY).getValue(String.class)
                                                .equalsIgnoreCase(country)) {*/
                setOfferList(dataSnapshot);
                                        /*}
                                    }*/
            }
        }
    }

    private void setOfferList(DataSnapshot dataSnapshot) {
        try {
            Object offer_time = dataSnapshot.child("offerExpiryDate").getValue();
            Long offer_expiry_time = Long.valueOf(offer_time + "");

            Timestamp expiryDateTimeStamp = new Timestamp(offer_expiry_time * 1000);
            Date today = new Date();

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            String offerEndDate = dateFormat.format(expiryDateTimeStamp);
            String todayDateString = dateFormat.format(today);

            Date endDate = dateFormat.parse(offerEndDate);
            Date todayDate = dateFormat.parse(todayDateString);

            long diff = todayDate.getTime() - endDate.getTime();
            long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

            if (days > 0 && days <= 40) {

                if (getArguments().getString("Title").equalsIgnoreCase(mContext.getString(R.string.gold))) {
                    if (categoryListFromFirebase.containsValue(dataSnapshot.child("category").getValue(String.class))) {
                        OffersVO offersVO = dataSnapshot.getValue(OffersVO.class);
                        offersVO.setOfferID(dataSnapshot.getKey());
                        mOfferList.add(offersVO);
                    }
                }

                if (getArguments().getString("Title").equalsIgnoreCase(mContext.getString(R.string.silver))) {
                    if (categoryListFromFirebase.containsValue(dataSnapshot.child("category").getValue(String.class))) {
                        OffersVO offersVO = dataSnapshot.getValue(OffersVO.class);
                        offersVO.setOfferID(dataSnapshot.getKey());
                        mOfferList.add(offersVO);
                    }
                }

                if (getArguments().getString("Title").equalsIgnoreCase(mContext.getString(R.string.bronze))) {
                    if (categoryListFromFirebase.containsValue(dataSnapshot.child("category").getValue(String.class))) {
                        OffersVO offersVO = dataSnapshot.getValue(OffersVO.class);
                        offersVO.setOfferID(dataSnapshot.getKey());
                        mOfferList.add(offersVO);
                    }
                }
                // setAdapterData(mOfferList);
                sortList(mOfferList);

                if (updateTitle != null)
                    updateTitle.setCateTitle(getArguments().getString("Title"), mOfferList.size());

                if (mOfferList.size() == 0) {
                    imageView_placeholder.setVisibility(View.VISIBLE);
                } else {
                    imageView_placeholder.setVisibility(View.GONE);
                }

            }


        } catch (DatabaseException e) {
            LogUtils.LOG_E(TAG, e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    //Set RecyclerView
    private void setRecyclerView() {
        rvExpiryOffers = (RecyclerView) rootView.findViewById(R.id.rvExpiryOffers);
        rvExpiryOffers.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 3);
        rvExpiryOffers.setLayoutManager(layoutManager);
        mOfferList = new ArrayList<>();
    }

    //Set Adapter Data
    private void setAdapterData(ArrayList<OffersVO> mOfferList) {
        OffersAdapter offersAdapter = new OffersAdapter(mContext, mOfferList, "ExpiryOffer");
        rvExpiryOffers.setAdapter(offersAdapter);
        offersAdapter.notifyDataSetChanged();
        imageView_placeholder.setVisibility(View.GONE);
    }

    //Set toolbar settings
    @Override
    public void onResume() {
        super.onResume();
        //disableBack(rootView);
        EventBus.getDefault().post(new UpdateSearchedData());
        ((BaseActivity) mContext).setToolbarTitle(mContext.getString(R.string.l_offers_history));

    }

    public void SearchOffer(String text) {

    }

    //Get Search Text Data
    public void onEvent(UpdateSearchedData updateSearchedData) {

        //Log.e(TAG,updateSearchedData.getSearchCriteria());

        if (!updateSearchedData.getSearchCriteria().trim().equals("")) {
            ArrayList<OffersVO> serchOfferList = new ArrayList<>();
            for (OffersVO offersVO : mOfferList) {
                if (offersVO.toString().toLowerCase().contains(updateSearchedData.getSearchCriteria().toLowerCase())) {
                    serchOfferList.add(offersVO);
                }
            }
            //Log.e("Searched data", "data");
            //setAdapterData(serchOfferList);
            sortList(mOfferList);
            updateTitle.setCateTitle(getArguments().getString("Title"), serchOfferList.size());
            if (serchOfferList.size() == 0) {
                imageView_placeholder.setVisibility(View.VISIBLE);
            } else {
                imageView_placeholder.setVisibility(View.GONE);
            }

        } else {
            //setAdapterData(mOfferList);
            sortList(mOfferList);
            updateTitle.setCateTitle(getArguments().getString("Title"), mOfferList.size());
            if (mOfferList.size() == 0) {
                imageView_placeholder.setVisibility(View.VISIBLE);
            } else {
                imageView_placeholder.setVisibility(View.GONE);
            }


        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //setRecyclerViewData();

    }

    //Set RecyclerView Data
    private void setRecyclerViewData() {
        categoryListFromFirebase.clear();
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            if (!SharedPreferenceUtil.getString(Constants.USER_ID, "").equals("")) {
                mConsumerUser.child(SharedPreferenceUtil.getString(Constants.USER_ID, ""))
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                progressDialog.dismiss();
                                if (dataSnapshot.getValue() != null) {
                                    if (dataSnapshot.child(Constants.STATUS).getValue().equals(Constants.STATUS_VALUE)) {

                                        if ((getArguments().getString("Title").equalsIgnoreCase(mContext.getString(R.string.gold)))) {
                                            categoryListFromFirebase = dataSnapshot.child(Constants.CATEGORY).child(Constants.GOLD).getValue(t);
                                        } else if ((getArguments().getString("Title").equalsIgnoreCase(mContext.getString(R.string.silver)))) {
                                            categoryListFromFirebase = dataSnapshot.child(Constants.CATEGORY).child(Constants.SILVER).getValue(t);
                                        } else if ((getArguments().getString("Title").equalsIgnoreCase(mContext.getString(R.string.bronze)))) {
                                            categoryListFromFirebase = dataSnapshot.child(Constants.CATEGORY).child(Constants.BRONZE).getValue(t);
                                        }

                                        if (dataSnapshot.hasChild(Constants.CITY)) {
                                            city = dataSnapshot.child(Constants.CITY).getValue(String.class);
                                        }
                                        if (dataSnapshot.hasChild(Constants.COUNTRY)) {
                                            country = dataSnapshot.child(Constants.COUNTRY).getValue(String.class);
                                        }
                                        if (dataSnapshot.hasChild(Constants.GENDER)) {
                                            gender = dataSnapshot.child(Constants.GENDER).getValue(String.class);
                                        }

                                        //LogUtils.LOG_E("CATEGORY:", String.valueOf(categoryListFromFirebase));
                                        getOfferCategory();
                                    }
                                }

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                progressDialog.dismiss();
                                LogUtils.LOG_E(TAG, databaseError.getMessage());
                            }
                        });
            }


        } else {
            // showDialogAlertPositiveButton(mContext.getString(R.string.alert_allthings), mContext.getString(R.string.please_check_network));
        }
    }


    private void setFontStyle() {

    }

    //Set Listeners
    private void setListeners() {


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_ivSearchCancel:
                ((BaseActivity) mContext).searching = false;
                EventBus.getDefault().post(new UpdateSearchedData()); //Set Default Data When Cancel on search
                ((BaseActivity) mContext).showHideToolbarTitle(true);
                ((BaseActivity) mContext).showHideToolbarBackButton(false);
                ((BaseActivity) mContext).showHideToolbarMenuButton(true);
                ((BaseActivity) mContext).showHideToolbarSearchButton(true);
                ((BaseActivity) mContext).showHideToolbarSerachEdittext(false);
                /* ((BaseActivity)mContext).showHideToolbarSearchView(false);*/
                ((BaseActivity) mContext).showHideToolbarSearchCancelButton(false);
                hideKeyboard();
                break;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        SearchOffer(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        SearchOffer(query);
        return false;
    }

    private void sortList(ArrayList<OffersVO> mOfferList) {
        Collections.sort(mOfferList, new Comparator<OffersVO>() {
            public int compare(OffersVO o1, OffersVO o2) {
                Date date1, date2;
                if (o1.getCreatedAt() > 0) {
                    date1 = DateUtil.getDateTime(o1.getCreatedAt());
                } else
                    date1 = DateUtil.getDateTime(o1.getUpdatedAt());

                if (o2.getCreatedAt() > 0) {
                    date2 = DateUtil.getDateTime(o2.getCreatedAt());
                } else
                    date2 = DateUtil.getDateTime(o2.getUpdatedAt());

                if (date1 == null || date2 == null)
                    return 0;
                return date2.compareTo(date1);
            }

        });

        //sortListByDate(mOfferList);
        //Collections.reverse(mOfferList);
        setAdapterData(mOfferList);
    }

}
