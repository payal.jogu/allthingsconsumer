package com.allthings.consumer.history;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.allthings.consumer.BaseActivity;
import com.allthings.consumer.BaseFragment;
import com.allthings.consumer.R;
import com.allthings.consumer.offers.BrowseOffersFragment;
import com.allthings.consumer.offers.OffersFragment;
import com.allthings.consumer.offers.UpdateCategoryTitle;


public class BrowseExpiryOfferFragment extends BaseFragment implements TabLayout.OnTabSelectedListener ,UpdateCategoryTitle {

    private View rootView;
    private BrowseExpiryOfferFragment.TabAdapter1 mAdapter;
    private ViewPager expiry_offer_view_pager;
    private TabLayout expiry_offer_tablayout;
    private UpdateCategoryTitle updateTitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_browse_expiry_offer, container, false);
            init();
            setListeners();
            setFontStyle();
            disableBack(rootView);
            //fragmentManager.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
            return rootView;
        } else {
            return rootView;
        }

    }

    //Set toolbar settings
    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity)mContext).showToolbar(true);
     /*   if(((BaseActivity) mContext).searching)
        {
            ((BaseActivity)mContext).showHideToolbarSerachEdittext(true);
            ((BaseActivity)mContext).showHideToolbarSearchCancelButton(true);
            ((BaseActivity)mContext).showHideToolbarSearchButton(false);
            ((BaseActivity)mContext).showHideToolbarTitle(false);
            ((BaseActivity)mContext).showHideToolbarMenuButton(false);
        }else {
            ((BaseActivity)mContext).showHideToolbarSerachEdittext(false);
            ((BaseActivity)mContext).showHideToolbarSearchCancelButton(false);
            ((BaseActivity)mContext).showHideToolbarTitle(true);
            ((BaseActivity)mContext).setToolbarTitle(getString(R.string.l_offers_history));
            ((BaseActivity)mContext).showHideToolbarSearchButton(true);
            ((BaseActivity)mContext).showHideToolbarMenuButton(true);
        }*/
        ((BaseActivity)mContext).showHideToolbarSerachEdittext(false);
        ((BaseActivity)mContext).showHideToolbarSearchCancelButton(false);
        ((BaseActivity)mContext).showHideToolbarTitle(true);
        ((BaseActivity)mContext).setToolbarTitle(getString(R.string.l_offers_history));
        ((BaseActivity)mContext).showHideToolbarSearchButton(false);
        ((BaseActivity)mContext).showHideToolbarMenuButton(true);
        ((BaseActivity)mContext).showHideToolbarBackButton(false);
        ((BaseActivity)mContext).showHideToolbarRemainder(false);

      /*  ((BaseActivity)mContext).setToolbarTitle(mContext.getString(R.string.l_browse_offers));
        ((BaseActivity)mContext).showHideToolbarMenuButton(true);
        ((BaseActivity)mContext).showHideToolbarSearchButton(true);*/
        ((BaseActivity)mContext).showHideToolbarChatOfferTitle(false);
        ((BaseActivity)mContext).showHideToolbarChatImage(false);
        ((BaseActivity)mContext).showHideToolbarChatEmail(false);
    }
    private void setFontStyle() {

    }
    //Set view pager in tab layout
    private void setListeners() {
        expiry_offer_view_pager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(expiry_offer_tablayout));
        expiry_offer_tablayout.addOnTabSelectedListener(this);
    }
    //Initialize components
    private void init() {
        updateTitle=this;
        expiry_offer_tablayout=(TabLayout)rootView.findViewById(R.id.expiry_offer_tablayout);
        expiry_offer_view_pager=(ViewPager)rootView.findViewById(R.id.expiry_offer_view_pager);

        expiry_offer_tablayout.addTab(expiry_offer_tablayout.newTab().setText(mContext.getString(R.string.gold)));
        expiry_offer_tablayout.addTab(expiry_offer_tablayout.newTab().setText(mContext.getString(R.string.silver)));
        expiry_offer_tablayout.addTab(expiry_offer_tablayout.newTab().setText(mContext.getString(R.string.bronze)));

        expiry_offer_view_pager.setAdapter(new TabAdapter1(getChildFragmentManager()));
        expiry_offer_view_pager.setOffscreenPageLimit(3);

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        expiry_offer_view_pager.setCurrentItem(tab.getPosition()); //Select Tab And Change It Position

    }
    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    //Set Category  With Count
    @Override
    public void setCateTitle(String title, int count) {
        if (title.equalsIgnoreCase(mContext.getString(R.string.gold)))
        {
            expiry_offer_tablayout.getTabAt(0).setText(mContext.getString(R.string.gold)+"("+count+")");
        }
        else if (title.equalsIgnoreCase(mContext.getString(R.string.silver)))
        {
            expiry_offer_tablayout.getTabAt(1).setText(mContext.getString(R.string.silver)+"("+count+")");
        }
        else if (title.equalsIgnoreCase(mContext.getString(R.string.bronze)))
        {
            expiry_offer_tablayout.getTabAt(2).setText(mContext.getString(R.string.bronze)+"("+count+")");
        }
    }
    //Adapter For Tab
    private class TabAdapter1 extends FragmentPagerAdapter {
        TabAdapter1(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int index) {
            String title="";
            if (index == 0)
            {
                title="Gold";
            }
            else if (index == 1)
            {
                title="Silver";
            }
            else if (index == 2)
            {
                title="Bronze";
            }
            return ExpiryOffersFragment.newInstance(title,updateTitle);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return super.getPageTitle(position);
        }

        @Override
        public int getCount()
        {
            return 3;
        }

    }

}
