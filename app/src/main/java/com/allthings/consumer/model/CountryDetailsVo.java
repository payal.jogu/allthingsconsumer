package com.allthings.consumer.model;

import java.io.Serializable;

/**
 * Created by Payal jogu on 12/6/17.
 */

public class CountryDetailsVo implements Serializable{
    public String countryCode="",countryName="",currencyCode="",currencyName="",maxPhoneLength,regionCode="",status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CountryDetailsVo() {
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public CountryDetailsVo(String countryCode, String countryName, String currencyCode,
                            String currencyName, String maxPhoneLength, String regionCode) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.currencyCode = currencyCode;
        this.currencyName = currencyName;
        this.maxPhoneLength = maxPhoneLength;
        this.regionCode = regionCode;

    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getMaxPhoneLength() {
        return maxPhoneLength;
    }

    public void setMaxPhoneLength(String maxPhoneLength) {
        this.maxPhoneLength = maxPhoneLength;
    }
}
