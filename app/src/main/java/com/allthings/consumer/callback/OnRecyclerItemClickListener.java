package com.allthings.consumer.callback;

/**
 * Created by Vishal Dhaduk 8/6/2017
 */
public interface OnRecyclerItemClickListener {
    public void onClick(int position);
}
