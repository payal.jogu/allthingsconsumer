package com.allthings.consumer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.allthings.consumer.about.AboutFragment;
import com.allthings.consumer.adapters.DrawerListAdapter;
import com.allthings.consumer.callback.OnRecyclerItemClickListener;
import com.allthings.consumer.chat.ChatDetailsFragment;
import com.allthings.consumer.chat.ChatListFragment;
import com.allthings.consumer.filter.FilterFragment;
import com.allthings.consumer.history.BrowseExpiryOfferFragment;
import com.allthings.consumer.login.LoginFragment;
import com.allthings.consumer.offers.BrowseOffersFragment;
import com.allthings.consumer.offers.MyOfferDetailsFragment;
import com.allthings.consumer.offers.UpdateSearchedData;
import com.allthings.consumer.profile.ProfileFragment;
import com.allthings.consumer.support.SupportFragment;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.LogUtils;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.RoundedImageTransform;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import de.greenrobot.event.EventBus;

/**
 * Created by Bypt on 6/7/2017.
 */

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    private String TAG = "BaseActivity";
    private TextView txtViewToolbarTitle, txtMenuTitle, txtMenuVersion, tvToolChatEmail, tvToolChatOffer;
    private LinearLayout toolbar;
    private ArrayList<String> listMenu = new ArrayList<>();
    private ArrayList<Integer> imageMenu = new ArrayList<>();
    private boolean browseofferFlag = false;
    public FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private boolean backNotificationFlag = false;
    public ImageView ivToolbarbackicon, ivToolbarmenuicon, ivToolbarsearchbutton, ivSearchCancel,
            ivToolChatImage, ivToolbarRemainder, imgViewChatBlock;
    public DrawerListAdapter adapter;
    public RecyclerView rvListDrawer;
    public LinearLayoutManager linearLayoutManager;
    public EditText etToolbarSearch;
    public ActionBarDrawerToggle mToggle;
    public DrawerLayout drawerLayout;
    public int lastFragmentPos = 0;
    public boolean searching = false, isFromRemainder = false;
    public DatabaseReference dbRefToUser;
    private AlertDialog.Builder deactiveUserAlertBuilder;

    //public static Screen currentDisplayScreen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.layout_base);
        //firebaseSignIn();
        init();
        checkUserStatus();
        getSearchTextListener();
        setListeners();
        setFontStyles();
        handleScreenNavigation();
        setDrawerToggle();

        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);



    }

    public void  getFirebaseTokenForFCM(){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        SharedPreferenceUtil.putValue("firebaseRegToken", token);
                        SharedPreferenceUtil.save();

                        Log.d(TAG,"getInstanceId  "+ token);

                        if (!SharedPreferenceUtil.getString("firebaseRegToken", "").equalsIgnoreCase("") &&
                                !SharedPreferenceUtil.getString(Constants.USER_ID, "").equalsIgnoreCase("")) {
                            HashMap<String, String> deviceModel = new HashMap<>();
                            deviceModel.put("Device Model", Build.MODEL);

                            dbRefToUser.child(SharedPreferenceUtil.getString(Constants.USER_ID, "")).
                                    child(Constants.FCM_TOKEN)
                                    .child(SharedPreferenceUtil.getString("firebaseRegToken", ""))
                                    .setValue(deviceModel);
                        }
                        //Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });

    }


    //Push the fragment when drawer slide
    private void setDrawerToggle() {
        mToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.common_open_on_phone, R.string.all_things) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                View container = findViewById(R.id.mainView);
                container.setTranslationX(slideOffset * drawerView.getWidth());
            }

        };
        drawerLayout.addDrawerListener(mToggle);

    }


    //Initial Screen
    public void handleScreenNavigation() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        if (getIntent().hasExtra("Type") && getIntent().getExtras().getString("Type").equalsIgnoreCase("OfferRemainder")) {

            if (SharedPreferenceUtil.contains(getIntent().getExtras().getString(Constants.OFFER_ID))) {
                SharedPreferenceUtil.remove(getIntent().getExtras().getString(Constants.OFFER_ID));
                SharedPreferenceUtil.save();

            }

            isFromRemainder = true;
            MyOfferDetailsFragment fragment = new MyOfferDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putString("from", "baseActivity");
            bundle.putString(Constants.OFFER_ID, getIntent().getExtras().getString(Constants.OFFER_ID));
            fragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, fragment).commit();

        } else if (getIntent().hasExtra("PUSHNOTIFICATION")) {
            LogUtils.LOG_E("Base Activity :", "Forground Notification");
            if (getIntent().hasExtra("data")) {

                Log.e("BaseActivity", getIntent().getExtras().getBundle("data").toString());

                if (getIntent().getExtras().getBundle("data").containsKey("notificationType")) {

                    if (getIntent().getExtras().getBundle("data").getString("notificationType").equals("newChat")) {
                        Log.e("BaseActivity", getIntent().getExtras().getBundle("data").getString("receiverID"));

                        ChatDetailsFragment chatDetailsFragment = new ChatDetailsFragment();
                        chatDetailsFragment.setArguments(getIntent().getExtras().getBundle("data"));
                        backNotificationFlag = true;
                        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, chatDetailsFragment).commit();

                        //set chat selected when come from nofication click event
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                setSelectedItem(1);
                                lastFragmentPos = 1;
                            }
                        }, 1000);
                        /*NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.cancel(getIntent().getExtras().getBundle("data").getInt("from"));*/
                    }
                }

              /*  for (String key : getIntent().getExtras().getString("data")) {
                    String value = getIntent().getExtras().getString(key);
                }*/
            }
            //getIntent().getExtras().getString("Notification").equals("PUSHNOTIFICATION")

        } else if (getIntent().getExtras() != null) {
            LogUtils.LOG_E("Base Activity :", "Background Notification");
            Bundle bundle = new Bundle();
            for (String key : getIntent().getExtras().keySet()) {
                String value = getIntent().getExtras().getString(key);
                Log.e("BaseActivity", "Key: " + key + " Value: " + value);
                bundle.putString(key, value);
            }

            if (bundle.containsKey("notificationType")) {
                if (bundle.getString("notificationType").equalsIgnoreCase("newChat")) {
                    ChatDetailsFragment chatDetailsFragment = new ChatDetailsFragment();
                    chatDetailsFragment.setArguments(bundle);
                    backNotificationFlag = true;
                    fragmentTransaction.replace(R.id.frameLayoutBaseContainer, chatDetailsFragment)
                            .commit();//312107018194 0:1505991004702575%8018604b8018604b

                    //set chat selected when come from nofication click event
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setSelectedItem(1);
                            lastFragmentPos = 1;
                        }
                    }, 1000);
                } else {
                    startApp();
                }
            } else {
                startApp();
            }

            Log.e("BaseActivity", "from" + bundle.get("from"));
            //Integer id = Integer.valueOf(String.valueOf(bundle.get("from")));

           /* NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(id);*/


        } else if (SharedPreferenceUtil.getString(Constants.IS_LOGIN, "").equals("login")) {
            browseofferFlag = true;
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new BrowseOffersFragment()).commit();
        } else {
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new LoginFragment()).commit();
        }
    }

    private void startApp() {
        if (SharedPreferenceUtil.getString(Constants.IS_LOGIN, "").equals("login")) {
            browseofferFlag = true;
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new BrowseOffersFragment()).commit();
        } else {
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new LoginFragment()).commit();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    //Set The Fonts
    private void setFontStyles() {
        setTypeFaceTextView(txtViewToolbarTitle, getString(R.string.fontOpenSansSemiBold));
        // setTypeFaceTextView(txtMenuTitle,getString(R.string.fontOpenSansSemiBold));
        setTypeFaceTextView(txtMenuVersion, getString(R.string.fontOpenSansSemiBold));
    }

    //Set Listeners
    private void setListeners() {
        ivToolbarbackicon.setOnClickListener(this);
        ivToolbarmenuicon.setOnClickListener(this);
        ivToolbarsearchbutton.setOnClickListener(this);
        // ivSearchCancel.setOnClickListener(this);

    }

    //Initiate Components
    private void init() {
        SharedPreferenceUtil.init(BaseActivity.this);

        imageMenu.add(R.drawable.menu_myoffer_selector);
        imageMenu.add(R.drawable.menu_chat_selector);
        imageMenu.add(R.drawable.category_filter_selector);
        imageMenu.add(R.drawable.selector_history_btn);
        imageMenu.add(R.drawable.menu_myprofile_selector);
        imageMenu.add(R.drawable.menu_chat_selector);
        imageMenu.add(R.drawable.menu_myoffer_selector);
        imageMenu.add(R.drawable.selector_history_btn);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        toolbar = findViewById(R.id.layoutToolbar);
        tvToolChatEmail = findViewById(R.id.tvToolChatEmail);
        tvToolChatOffer = findViewById(R.id.tvToolChatOffer);

        txtViewToolbarTitle = findViewById(R.id.tvToolbarTitle);
        ivToolbarbackicon = findViewById(R.id.ivToolbarbackicon);
        ivToolbarmenuicon = findViewById(R.id.ivToolbarmenubutton);
        ivToolbarsearchbutton = findViewById(R.id.ivToolbarsearchbutton);
        ivSearchCancel = findViewById(R.id.toolbar_ivSearchCancel);
        ivToolChatImage = findViewById(R.id.ivToolChatImage);
        ivToolbarRemainder = findViewById(R.id.ivToolbar_remainder);
        imgViewChatBlock = findViewById(R.id.imgViewChatBlock);

        rvListDrawer = findViewById(R.id.rvListDrawer);

        drawerLayout = findViewById(R.id.drawerLayout);

        //txtMenuTitle=(TextView)findViewById(R.id.txtMenuTitle);
        txtMenuVersion = findViewById(R.id.txtMenuVersion);
        txtMenuVersion.setText("v" + BuildConfig.VERSION_NAME);

        etToolbarSearch = findViewById(R.id.toolbar_etSearch);

        //drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        linearLayoutManager = new LinearLayoutManager(this);

        rvListDrawer.setLayoutManager(linearLayoutManager);

        listMenu = getMenuList();

        adapter = new DrawerListAdapter(this, listMenu, imageMenu, onRecyclerItemClickListener);

        rvListDrawer.setAdapter(adapter);

        dbRefToUser = FirebaseDatabase.getInstance().getReference(Constants.CONSUMER_USER_MASTER);
        getFirebaseTokenForFCM();
    }

    //Get The Drawer List
    private ArrayList<String> getMenuList() {
        ArrayList<String> list = new ArrayList<>();

        list.add(getString(R.string.browse_offer));
        list.add(getString(R.string.chat));
        list.add(getString(R.string.filter_categories));
        list.add(getString(R.string.offer_history));
        list.add(getString(R.string.profile));
        list.add(getString(R.string.support));
        list.add(getString(R.string.about));
        list.add(getString(R.string.logout));
        return list;
    }


    //Handle Drawer Open Close
    public void handleDrawerLeftOpenClose() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
           /* mHandler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    drawerLayout.closeDrawer(Gravity.START);
                }
            }, 150);*/
            drawerLayout.closeDrawer(Gravity.START);

        } else {
            drawerLayout.openDrawer(Gravity.START);


        }
        if (browseofferFlag) {
            setSelectedItem(0);
            browseofferFlag = false;
        }
    }

    //set Type Face
    protected void setTypeFaceTextView(TextView txtView, String typeFace) {
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(),
                typeFace);
        txtView.setTypeface(tf);
    }


    protected void setTypeFaceButton(Button button, String typeFace) {
        Typeface tf = Typeface.createFromAsset(
                getApplicationContext().getAssets(),
                typeFace);
        button.setTypeface(tf);
    }

    //Show Hide Toolbar And Components
    public void showToolbar(boolean isShow) {
        toolbar.setVisibility(isShow ? View.VISIBLE : View.GONE);

    }

    public void showHideToolbarBackButton(boolean isShow) {
        ivToolbarbackicon.setVisibility(isShow ? View.VISIBLE : View.GONE);

    }

    public void showHideToolbarSearchButton(boolean isShow) {
        ivToolbarsearchbutton.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    public void showHideToolbarSearchCancelButton(boolean isShow) {
        ivSearchCancel.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    public void showHideToolbarTitle(boolean isShow) {
        txtViewToolbarTitle.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    public void showHideToolbarSerachEdittext(boolean isShow) {
        etToolbarSearch.setVisibility(isShow ? View.VISIBLE : View.GONE);
        etToolbarSearch.requestFocus();
    }

    public void showHideToolbarRemainder(boolean isShow) {
        ivToolbarRemainder.setVisibility(isShow ? View.VISIBLE : View.GONE);

    }

    public void showHideToolbarMenuButton(boolean isShow) {
        if (isShow) {
            ivToolbarmenuicon.setVisibility(View.VISIBLE);
            unlocakDrawer();
        } else {
            ivToolbarmenuicon.setVisibility(View.INVISIBLE);
            lockDrawer();
        }
        //ivToolbarmenuicon.setVisibility(isShow?View.VISIBLE:View.GONE);
    }

    public void setToolbarTitle(String title) {
        setToolbarTitleBold();
        txtViewToolbarTitle.setText(title);
    }

    public void setToolbarTitleBold() {
        txtViewToolbarTitle.setTypeface(Typeface.DEFAULT_BOLD);
    }


    public void showHideToolbarChatImage(boolean isShow) {
        ivToolChatImage.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    public void showHideToolbarChatOfferTitle(boolean isShow) {
        tvToolChatOffer.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    public void showHideToolbarChatEmail(boolean isShow) {
        tvToolChatEmail.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    public void setToolbarChatOfferTitle(String title) {
        tvToolChatOffer.setText(title);
    }

    public void setToolbarChatEmailTitle(String title) {
        tvToolChatEmail.setText(title);
    }

    public void setToolbarChatOfferImage(String imageUrl) {

        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.company_placeholder);
        if (!imageUrl.isEmpty()) {
            Picasso.with(this).load(imageUrl)
                    .resize(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight())
                    .transform(new RoundedImageTransform(this, 0, R.drawable.company_placeholder))
                    .placeholder(R.drawable.company_placeholder)
                    .into(ivToolChatImage);
        }
    }

    public void lockDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void unlocakDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawer(Gravity.START);
        } else {
            if (backNotificationFlag) {
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new ChatListFragment()).commit();
                backNotificationFlag = false;

            } else if (fragmentManager.getBackStackEntryCount() > 0) {
                fragmentManager.popBackStack();
            } else {
                finish();
            }
        }
        //overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }


    public void setSelectedItem(int position) {
        if (adapter != null)
            adapter.selectDeselcetMenuItem(position);
    }

    //Click On Drawer Go to Differents Screens
    private OnRecyclerItemClickListener onRecyclerItemClickListener = new OnRecyclerItemClickListener() {
        @Override
        public void onClick(int position) {
            String menu = listMenu.get(position);
            imageMenu.get(position);

            fragmentTransaction = fragmentManager.beginTransaction();

            if (menu.equals(getString(R.string.browse_offer))) {
                handleDrawerLeftOpenClose();
                if (position != lastFragmentPos) {
                    navigateToFragment(1);
                    lastFragmentPos = position;
                }
            } else if (menu.equals(getString(R.string.chat))) {
                handleDrawerLeftOpenClose();
                if (position != lastFragmentPos) {
                    navigateToFragment(2);
                    lastFragmentPos = position;
                }
            } else if (menu.equals(getString(R.string.profile))) {
                handleDrawerLeftOpenClose();
                if (position != lastFragmentPos) {
                    navigateToFragment(3);
                    lastFragmentPos = position;
                }
            } else if (menu.equals(getString(R.string.logout))) {
                handleDrawerLeftOpenClose();
                navigateToFragment(4);
            } else if (menu.equals(getString(R.string.filter_categories))) {
                handleDrawerLeftOpenClose();
                if (position != lastFragmentPos) {
                    navigateToFragment(5);
                    lastFragmentPos = position;
                }
            } else if (menu.equals(getString(R.string.offer_history))) {
                handleDrawerLeftOpenClose();
                if (position != lastFragmentPos) {
                    navigateToFragment(6);
                    lastFragmentPos = position;
                }
            } else if (menu.equals(getString(R.string.support))) {
                handleDrawerLeftOpenClose();
                if (position != lastFragmentPos) {
                    navigateToFragment(7);
                    lastFragmentPos = position;
                }
            } else if (menu.equals(getString(R.string.about))) {
                handleDrawerLeftOpenClose();
                if (position != lastFragmentPos) {
                    navigateToFragment(8);
                    lastFragmentPos = position;
                }
            }

        }
    };

    //Drawer Fragments
    private void navigateToFragment(int type) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        if (type == 1) {
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new BrowseOffersFragment()).commit();
        }
        if (type == 2) {
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new ChatListFragment()).commit();
        }
        if (type == 3) {
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new ProfileFragment()).commit();

            // fragmentTransaction.replace(R.id.frameLayoutBaseContainer,new ProfileFragment()).addToBackStack(null).commit()
        }
        if (type == 4) {
            if (NetworkUtil.isOnline(this)) {
                if (!SharedPreferenceUtil.getString("firebaseRegToken", "").equalsIgnoreCase("")) {
                   /* HashMap<String,String> deviceModel = new HashMap<>();
                    deviceModel.put("Device Model", Build.MODEL);*/
                    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                    firebaseDatabase.getReference(Constants.CONSUMER_USER_MASTER)
                            .child(SharedPreferenceUtil.getString(Constants.USER_ID, ""))
                            .child(Constants.FCM_TOKEN)
                            .child(SharedPreferenceUtil.getString("firebaseRegToken", ""))
                            .removeValue()
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    LogUtils.LOG_E("BaseActivity", "Token removed");
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            LogUtils.LOG_E("BaseActivity", "Token not removed");
                        }
                    });

                }
                String regToken = SharedPreferenceUtil.getString("firebaseRegToken", "");

                SharedPreferenceUtil.clear();
                //SharedPreferenceUtil.save();

                SharedPreferenceUtil.putValue("firebaseRegToken", regToken);
                SharedPreferenceUtil.save();

                FirebaseAuth.getInstance().signOut();

                fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new LoginFragment()).commit();
                // fragmentTransaction.commit();

            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.please_check_network))
                        .setCancelable(false)
                        .setTitle(getString(R.string.app_name))
                        .setPositiveButton(this.getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });

                AlertDialog alert = builder.create();
                alert.show();
            }
          /*  SharedPreferenceUtil.clear();

            FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
            mFirebaseAuth.signOut();*/
        }
        if (type == 5) {
            // fragmentTransaction.replace(R.id.frameLayoutBaseContainer,new FilterCategories()).commit();
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new FilterFragment()).commit();
        }
        if (type == 6) {
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new BrowseExpiryOfferFragment()).commit();
        }
        if (type == 7) {
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new SupportFragment()).commit();
        }
        if (type == 8) {
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new AboutFragment()).commit();
        }


    }

    //Get Search Text
    protected void getSearchTextListener() {
        if (etToolbarSearch != null) {
            etToolbarSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    UpdateSearchedData updateSearchedData = new UpdateSearchedData();
                    updateSearchedData.setSearchCriteria(etToolbarSearch.getText().toString());
                    EventBus.getDefault().post(updateSearchedData);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            /*etToolbarSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                            hideKeyboard();
                            UpdateSearchedData updateSearchedData = new UpdateSearchedData();
                            updateSearchedData.setSearchCriteria(etToolbarSearch.getText().toString());
                            EventBus.getDefault().post(updateSearchedData);
                            //searchCategory.updateSearchedData(etToolbarSearch.getText().toString());

                        return true;


                    }
                    return false;
                }
            });*/
        }

    }

    //hide keyboard
    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    public void popBackStack() {
        if (backNotificationFlag) {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new ChatListFragment()).commit();
            backNotificationFlag = false;

        } else if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivToolbarbackicon:
                if (isFromRemainder) {
                    isFromRemainder = false;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new BrowseOffersFragment()).commit();
                    setSelectedItem(0);
                    lastFragmentPos = 0;
                } else {
                    hideKeyboard();
                    popBackStack();
                }
                break;
            case R.id.ivToolbarmenubutton:
                handleDrawerLeftOpenClose();// drawer
                break;
           /* case R.id.ivToolbarsearchbutton: // show search
                searching = true;
                showHideToolbarSerachEdittext(true);
                showHideToolbarSearchCancelButton(true);
                showHideToolbarSearchButton(false);
                showHideToolbarMenuButton(false);
                showHideToolbarBackButton(false);
                showHideToolbarTitle(false);
                etToolbarSearch.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(etToolbarSearch, InputMethodManager.SHOW_IMPLICIT);
                break;*/
          /*  case R.id.toolbar_ivSearchCancel: // cancel search
               /* showHideToolbarTitle(true);
                showHideToolbarBackButton(false);
                showHideToolbarMenuButton(true);
                showHideToolbarSearchButton(true);
                showHideToolbarSerachEdittext(false);
                showHideToolbarSearchCancelButton(false);
                hideKeyboard();

                break;*/
        }
    }

    public void checkUserStatus() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("consumerUser").
                child(SharedPreferenceUtil.getString(Constants.USER_ID,""));

        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
               // LogUtils.LOG_E("getChildrenCount ", "" + dataSnapshot.getChildrenCount());
                //LogUtils.LOG_E("getChildrenCount ", "" + dataSnapshot.getValue());
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
               /* LogUtils.LOG_E("getChildrenCount ", "" + dataSnapshot.getChildrenCount());
                LogUtils.LOG_E("getKey ", "" + dataSnapshot.getKey());
                LogUtils.LOG_E("getValue ", "" + dataSnapshot.getValue());*/

                if (dataSnapshot.getKey() != null &&
                        dataSnapshot.getKey().equals(Constants.STATUS) && dataSnapshot.getValue(String.class).equals("0")) {
                    //logoutUser();
                    forceFullyLogout();

                }


            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void forceFullyLogout() {
        if(deactiveUserAlertBuilder == null) {
            if (!SharedPreferenceUtil.getString("firebaseRegToken", "").equalsIgnoreCase("")) {
                       /*HashMap<String,String> deviceModel = new HashMap<>();
                       deviceModel.put("Device Model", Build.MODEL);*/
                FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                firebaseDatabase.getReference(Constants.CONSUMER_USER_MASTER)
                        .child(SharedPreferenceUtil.getString(Constants.USER_ID, ""))
                        .child(Constants.FCM_TOKEN)
                        .child(SharedPreferenceUtil.getString("firebaseRegToken", ""))
                        .removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                LogUtils.LOG_E("BaseActivity", "Token removed");
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        LogUtils.LOG_E("BaseActivity", "Token not removed");
                    }
                });
            }

            String regToken = SharedPreferenceUtil.getString("firebaseRegToken", "");
            SharedPreferenceUtil.clear();
            SharedPreferenceUtil.putValue("firebaseRegToken", regToken);
            SharedPreferenceUtil.save();
            FirebaseAuth.getInstance().signOut();

            deactiveUserAlertBuilder = new AlertDialog.Builder(BaseActivity.this);
            deactiveUserAlertBuilder.setMessage(getString(R.string.deactivate_user))
                    .setCancelable(false)
                    .setTitle(getString(R.string.app_name))
                    .setPositiveButton(getApplicationContext().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    clearBackStack();
                                    deactiveUserAlertBuilder = null;
                                    fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_right,
                                            R.anim.exit_to_left);
                                    fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new LoginFragment());
                                    fragmentTransaction.commitAllowingStateLoss();
                                }
                            });

            AlertDialog alert = deactiveUserAlertBuilder.create();

            if (!isFinishing())
                alert.show();

        }


    }

    protected void clearBackStack() {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(0, 0);
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

    }


}
