package com.allthings.consumer.feedback;

/**
 * Created by Vishal Dhaduk on 6/26/2017.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.allthings.consumer.R;
import com.allthings.consumer.utils.RoundedImageTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class FeedbackListAdapter extends RecyclerView.Adapter<FeedbackListAdapter.ViewHolder> {
    private ArrayList<FeedbackVO> feedback;
    private Context context;
    private FragmentManager fragmentManager;
    private int lastPosition = -1;

    public FeedbackListAdapter(Context context, ArrayList<FeedbackVO> feedback) {
        this.feedback = feedback;
        this.context = context;
    }

    @Override
    public FeedbackListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feedback_row, viewGroup, false);
        return new FeedbackListAdapter.ViewHolder(view);

    }


    //bind the view holder
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String string = feedback.get(position).getFeedbackEmail();
        if (string != null) {
            String[] parts = string.split("@");
            if (parts.length > 0) {
                String part1 = parts[0];
                holder.tvFeedbackEmail.setText(part1);
            }
        } else {
            holder.tvFeedbackEmail.setText("Not Available");
        }


        holder.tvFeedbackDetails.setText(feedback.get(position).getFeedbackDescription());
        if (feedback.get(position).getFeedbackRatings() != "") {
            holder.ratingBarFeedback.setRating(Float.parseFloat(feedback.get(position).getFeedbackRatings()));
        }
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.feedback_dp_placeholder);
        int height = drawable.getIntrinsicHeight();
        if (!feedback.get(position).getFeebackProfileURL().isEmpty())
            Picasso.with(context).load(feedback.get(position).getFeebackProfileURL()).placeholder(R.drawable.feedback_dp_placeholder).
                    resize(height, height).transform(new RoundedImageTransform(context, 0, R.drawable.feedback_dp_placeholder)).
                    into(holder.ivFeedbackLogo);


    }

    @Override
    public int getItemCount() {
        return feedback.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvFeedbackEmail, tvFeedbackDetails;
        private ImageView ivFeedbackLogo;
        private RatingBar ratingBarFeedback;

        public ViewHolder(View view) {
            super(view);

            tvFeedbackDetails = view.findViewById(R.id.tvFeedbackRowDetails);
            tvFeedbackEmail = view.findViewById(R.id.tvFeedbackRowEmail);
            ivFeedbackLogo = view.findViewById(R.id.ivFeedbackRowProfile);
            ratingBarFeedback = view.findViewById(R.id.ratingbarFeedbackRow);


            Typeface tf = Typeface.createFromAsset(view.getContext().getAssets(),
                    "OpenSans-Semibold.ttf");

            tvFeedbackEmail.setTypeface(tf);
            tvFeedbackDetails.setTypeface(tf);

        }
    }
}