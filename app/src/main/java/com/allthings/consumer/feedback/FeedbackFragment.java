package com.allthings.consumer.feedback;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.allthings.consumer.R;
import com.allthings.consumer.utils.AlertDialogUtil;
import com.allthings.consumer.utils.Constants;
import com.allthings.consumer.utils.CustomDialog;
import com.allthings.consumer.utils.NetworkUtil;
import com.allthings.consumer.utils.SetTypefaceUtils;
import com.allthings.consumer.utils.SharedPreferenceUtil;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.ArrayList;

/**
 * Created by Vishal Dhaduk on 6/16/2017.
 */

public class FeedbackFragment extends DialogFragment implements
        View.OnClickListener, RatingBar.OnRatingBarChangeListener, SimpleRatingBar.OnRatingBarChangeListener {

    private View rootView;
    private TextView tvFeedbackLabel, tvGiveRatingsLabel;
    public EditText etFeedbackText;
    private Button btnSubmit, btnCancel;
    public SimpleRatingBar ratingBarFeedback;
    public FeedbackListAdapter feedbackListAdapter;
    public ArrayList<FeedbackVO> feedbackList;
    public RecyclerView recyclerView;
    String rate = "0.0", offerID = "", email = "", profilePicURL = "";
    DatabaseReference feedbackTable;
    protected FirebaseDatabase mDatabase;
    ProgressDialog progressDialog;
    private Context mContext;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_feedback, container, false);
            init();
            setRecyclerView();// set recyclerview
            setListeners();// set Listener
            setFontStyle(); // set Fonts styles
            return rootView;
        } else {
            return rootView;
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setFeedbackListData(); //set feedback data
    }

    //set Recycler view
    private void setRecyclerView() {
        recyclerView = rootView.findViewById(R.id.rvFeedback);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        feedbackList = new ArrayList<>();
        feedbackListAdapter = new FeedbackListAdapter(getContext(), feedbackList);
        recyclerView.setAdapter(feedbackListAdapter);
    }

    //set feedback data into list
    private void setFeedbackListData() {

        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();

            Query query = mDatabase.getReference().child(Constants.OFFER_RATING_TABLE).orderByChild(Constants.OFFER_ID).equalTo(offerID);

            query.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String s) {
                    Log.e("KEY", dataSnapshot.getKey());

                    FeedbackVO feedbackVO = dataSnapshot.getValue(FeedbackVO.class);
                    feedbackList.add(feedbackVO);
                    feedbackListAdapter.notifyDataSetChanged();
                    if (feedbackList.size() != 0) {
                        tvFeedbackLabel.setText("Feedback" + " (" + feedbackList.size() + ")");
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
        } else {
            AlertDialogUtil.showDialogAlertPositiveButton(getContext().getString(R.string.alert_allthings), getContext().getString(R.string.please_check_network), getContext());
        }

    }

    //Set toolbar settings
    @Override
    public void onResume() {
        super.onResume();
        Dialog dialog = getDialog();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    //Set Font Styles
    private void setFontStyle() {
        SetTypefaceUtils.setTypeFaceTextView(tvFeedbackLabel, getContext().getString(R.string.fontOpenSansSemiBold), getContext());
        SetTypefaceUtils.setTypeFaceTextView(tvGiveRatingsLabel, getContext().getString(R.string.fontOpenSansSemiBold), getContext());
        SetTypefaceUtils.setTypeFaceButton(btnCancel, getContext().getString(R.string.fontOpenSansSemiBold), getContext());
        SetTypefaceUtils.setTypeFaceButton(btnSubmit, getContext().getString(R.string.fontOpenSansSemiBold), getContext());
        SetTypefaceUtils.setTypeFaceEdittext(etFeedbackText, getContext().getString(R.string.fontOpenSansSemiBoldItalic), getContext());
    }

    //Set Listeners
    private void setListeners() {
        btnSubmit.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        ratingBarFeedback.setOnRatingBarChangeListener(this);

    }

    //Initialize the components
    private void init() {
        tvFeedbackLabel = rootView.findViewById(R.id.tvFeedbackLabel);
        tvGiveRatingsLabel = rootView.findViewById(R.id.tvGiveFeedbackAndRatings);
        ratingBarFeedback = rootView.findViewById(R.id.ratingbarRatingList);
        etFeedbackText = rootView.findViewById(R.id.etFeedbackList);
        btnCancel = rootView.findViewById(R.id.btnCancel);
        btnSubmit = rootView.findViewById(R.id.btnSubmit);

        etFeedbackText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etFeedbackText.setRawInputType(InputType.TYPE_CLASS_TEXT);
        etFeedbackText.setHorizontallyScrolling(false);
        etFeedbackText.setLines(3);

        mDatabase = FirebaseDatabase.getInstance();
        progressDialog = CustomDialog.ctor(getContext());
        feedbackTable = mDatabase.getReference().child(Constants.OFFER_RATING_TABLE);
        if (getArguments() != null) {
            offerID = getArguments().getString(Constants.OFFER_ID);
        }
        //Retrieve USER PROFILE DATA FROM FIREBASE
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            mDatabase.getReference().child(Constants.CONSUMER_USER_MASTER).child(SharedPreferenceUtil.getString(Constants.USER_ID, ""))
                    .addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    Log.e("Email", String.valueOf(dataSnapshot.child(Constants.EMAIL).getValue()));
                    Log.e("Feedback", SharedPreferenceUtil.getString(Constants.USER_ID, ""));
                    email = String.valueOf(dataSnapshot.child(Constants.EMAIL).getValue());
                    profilePicURL = String.valueOf(dataSnapshot.child(Constants.PROFILEPHOTOURL).getValue());
                    progressDialog.dismiss();

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    progressDialog.dismiss();
                }
            });
        } else {
            AlertDialogUtil.showDialogAlertPositiveButton(getContext().getString(R.string.alert_allthings), getContext().getString(R.string.please_check_network), getContext());
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnSubmit:
                if (Float.parseFloat(rate) > 0) {
                    sendRequestForSaveFeedback();
                } else {
                    AlertDialogUtil.showDialogAlertPositiveButton(getContext().getString(R.string.alert_allthings),
                            getContext().getString(R.string.feedback_alert), getContext());
                }
                break;
            case R.id.btnCancel:
                dismiss();
                break;
        }

    }

    boolean status = false;

    //save feedback to firebase database
    private void sendRequestForSaveFeedback() {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            Log.e("Feedback", SharedPreferenceUtil.getString(Constants.USER_ID, ""));

            Query query = mDatabase.getReference().child(Constants.OFFER_RATING_TABLE).
                    orderByChild(Constants.CREATED_BY).equalTo(SharedPreferenceUtil.getString(Constants.USER_ID, ""));

            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    progressDialog.dismiss();
                    if (dataSnapshot.getValue() != null) {
                        //boolean status = false;
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            if (child.child(Constants.OFFER_ID).getValue().equals(offerID)) {
                                status = true;
                                break;
                                //Log.e("FEEDBACK", "FEEDBACK EXIST");

                            } else {
                                status = false;
                                // Log.e("FEEDBACK", "FEEDBACK NOT EXIST");
                                // break;
                            }
                        }
                        setFeedBackStatus(status);
                    } else {
                        setFeedBackStatus(status);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    progressDialog.dismiss();
                }
            });

        } else {
            AlertDialogUtil.showDialogAlertPositiveButton(getContext().getString(R.string.alert_allthings), getContext().getString(R.string.please_check_network), getContext());
        }
    }

    private void setFeedBackStatus(boolean status) {
        if (!status) {
            AddFeedbackToFirebase();
        } else {
            AlertDialogUtil.showDialogAlertPositiveButton(getContext().getString(R.string.alert_allthings), getContext().getString(R.string.feedback_already_exist), getContext());
        }
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
    }

    @Override
    public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
        rate = String.valueOf(rating);
    }

    public void AddFeedbackToFirebase() {
        progressDialog.show();

        FeedbackVO feedbackVO = new FeedbackVO();
        feedbackVO.setFeedbackRatings(rate);
        feedbackVO.setFeedbackDescription(etFeedbackText.getText().toString().trim());
        feedbackVO.setFeedbackEmail("");
        feedbackVO.setCreateBy(SharedPreferenceUtil.getString(Constants.USER_ID, ""));
        //feedbackVO.setUpdateBy(SharedPreferenceUtil.getString(Constants.USER_ID, ""));
        feedbackVO.setCreateDate(ServerValue.TIMESTAMP);
        //feedbackVO.setUpdatedDate(String.valueOf(ServerValue.TIMESTAMP));
        feedbackVO.setStatus("1");
        feedbackVO.setOfferID(offerID);
        feedbackVO.setFeedbackEmail(email);
        feedbackVO.setFeebackProfileURL(profilePicURL);
        String key = feedbackTable.push().getKey();
        if (key != null) {
            feedbackTable.child(key).setValue(feedbackVO).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                    progressDialog.dismiss();
                    etFeedbackText.setText("");
                    ratingBarFeedback.setRating(0);
                    //dismiss();

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                }
            });
        }
    }
}

