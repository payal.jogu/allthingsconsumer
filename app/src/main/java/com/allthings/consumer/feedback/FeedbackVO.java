package com.allthings.consumer.feedback;

/**
 * Created by Vishal Dhaduk on 6/26/2017.
 */

public class FeedbackVO {
    private String FeebackProfileURL = "", FeedbackEmail = "", FeedbackDescription = "",
            FeedbackRatings = "0.0",CreateBy="",UpdateBy="",
            Status="",OfferID="";
    private Object CreateDate,UpdateDate;

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String createBy) {
        CreateBy = createBy;
    }

    public String getUpdateBy() {
        return UpdateBy;
    }

    public void setUpdateBy(String updateBy) {
        UpdateBy = updateBy;
    }

    public Object getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(Object createDate) {
        CreateDate = createDate;
    }

    public Object getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(Object updateDate) {
        UpdateDate = updateDate;
    }

    public String getFeedbackEmail() {
        return FeedbackEmail;
    }

    public void setFeedbackEmail(String feedbackEmail) {
        FeedbackEmail = feedbackEmail;
    }

    public String getFeedbackDescription() {
        return FeedbackDescription;
    }

    public void setFeedbackDescription(String feedbackDescription) {
        FeedbackDescription = feedbackDescription;
    }

    public String getFeedbackRatings() {
        return FeedbackRatings;
    }

    public void setFeedbackRatings(String feedbackRatings) {
        FeedbackRatings = feedbackRatings;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getFeebackProfileURL() {
        return FeebackProfileURL;
    }

    public void setFeebackProfileURL(String feebackProfileURL) {
        FeebackProfileURL = feebackProfileURL;
    }

    public String getOfferID() {
        return OfferID;
    }

    public void setOfferID(String offerID) {
        OfferID = offerID;
    }
}
